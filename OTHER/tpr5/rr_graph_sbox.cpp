
#include "util.h"
#include "ipr_types.h"
#include "fpga3d.h"


using namespace std;

// ======================================================================= //
/* Switch box: 2D                                                          *
 *                    TOP (CHANY)                                          *
 *                    | | | | | |                                          *
 *                   +-----------+                                         *
 *                 --|           |--                                       *
 *                 --|           |--                                       *
 *           LEFT  --|           |-- RIGHT                                 *
 *          (CHANX)--|           |--(CHANX)                                *
 *                 --|           |--                                       *
 *                 --|           |--                                       *
 *                   +-----------+                                         *
 *                    | | | | | |                                          *
 *                   BOTTOM (CHANY)                                        */
// ======================================================================= //
// ======================================================================= //
/* Switch box: 3D                 BELOW (CHANZ)                            *
 *                    TOP (CHANY)  /                                       *
 *                    | | | | | | /                                        *
 *                   +-----------+                                         *
 *                 --|          /|--                                       *
 *                 --|         / |--                                       *
 *           LEFT  --|           |-- RIGHT                                 *
 *          (CHANX)--|           |--(CHANX)                                *
 *                 --| /         |--                                       *
 *                 --|/          |--                                       *
 *                   +-----------+                                         *
 *                  /  | | | | | |                                         *
 *                 /  BOTTOM (CHANY)                                       *                                   
 *              ABOVE (CHANZ)                                              */
// ======================================================================= //
// cristia:
// Fs = 3 for 2D switch box: pin 0 on an edge connects to other three
// pin 0 on the other three edges for "subset" type switch-box.
// Fs = 5 for 3D.
//


// ======================================================================= //

void s_fpga3d::alloc_and_load_switch_block_conn (
      int nodes_per_chan,
      int vias_per_zchan,
      enum e_switch_block_type switch_block_type) 
{
// Allocates and loads the "switch_block_conn" data structure.  
// This structure lists which tracks connect to which at each 
// switch-block.                 
  enum e_side from_side,to_side;
  int from=0,to=0;
  int from_track=0,to_track=0;


  // [0..5][0..5][0..nodes_per_chan-1].  Structure below is indexed as:       
  // [from_side][to_side][from_track].  That yields an integer vector (ivec)  
  // of the tracks to which from_track connects in the proper to_location.    
  // For simple switch boxes this is overkill, but it will allow complicated  
  // switch boxes with Fs > 3, etc. without trouble.
  switch_block_conn = (struct s_ivec ***) alloc_matrix3 (0, 5, 0, 5, 0, 
                      nodes_per_chan - 1, sizeof (struct s_ivec));
  // (a fost 0,3,0,3... si l-am inlocuit cu 0,5,0,5... to account for
  // above and below)

  for ( from=0;from<=5;from++ ) {
    // cristia:  I added connections to above(abv) and below(blw)
    // layers to implement the 3D switch-box.  Layer 0 will not
    // have connection abv and last layer will not have connection blw.
    // I also have to go and make sure Fs = 5.
    // OBS: Use of ABOVE and BELOW should be discouraged!
    if(from==0) from_side = TOP;
    if(from==1) from_side = BOTTOM;
    if(from==2) from_side = LEFT;
    if(from==3) from_side = RIGHT;
    if(from==4) from_side = ABOVE;
    if(from==5) from_side = BELOW;

    for ( to=0;to<=5;to++ ) {
      if(to==0) to_side = TOP;
      if(to==1) to_side = BOTTOM;
      if(to==2) to_side = LEFT;
      if(to==3) to_side = RIGHT;
      if(to==4) to_side = ABOVE;
      if(to==5) to_side = BELOW;
 
      // cristinel.ababei
      // Assumption:  vias_per_zchan is smaller than nodes_per_chan!
      
      assert ( vias_per_zchan < nodes_per_chan );

      if ( (from <= 3) && (to <= 3) ) { // X,Y --> X,Y
	// -----------------------------------------------------
	for (from_track=0;from_track<nodes_per_chan;from_track++) {
	  if (from != to) {
	    switch_block_conn[from][to][from_track].nelem = 1;
	    switch_block_conn[from][to][from_track].list = 
	      (int *) my_malloc (sizeof (int));

	    to_track = get_simple_switch_block_track (from_side, to_side, 
			  from_track, switch_block_type, nodes_per_chan);
	    switch_block_conn[from][to][from_track].list[0] = to_track;
	  }
	  else {   // from_side == to_side -> no connection.
	    switch_block_conn[from][to][from_track].nelem = 0;
	    switch_block_conn[from][to][from_track].list = NULL;
	  }
	}//for
	// -----------------------------------------------------
      }
      else {
	// ((from <= 3)&&(to > 3)) ||  // X,Y --> Z
	// ((from > 3)&&(to <= 3)) ||  // Z --> X,Y
	// ((from > 3)&&(to > 3))      // Z --> Z
	// -----------------------------------------------------
	for (from_track=0;from_track<vias_per_zchan;from_track++) {
	  if (from != to) {
	    switch_block_conn[from][to][from_track].nelem = 1;
	    switch_block_conn[from][to][from_track].list = 
	      (int *) my_malloc (sizeof (int));

	    to_track = get_simple_switch_block_track (from_side, to_side, 
			  from_track, switch_block_type, nodes_per_chan);
	    switch_block_conn[from][to][from_track].list[0] = to_track;
	  }
	  else {   // from_side == to_side -> no connection.
	    switch_block_conn[from][to][from_track].nelem = 0;
	    switch_block_conn[from][to][from_track].list = NULL;
	  }
	}//for
	for (from_track=vias_per_zchan;from_track<nodes_per_chan;from_track++) {
	  // from_side == to_side -> no connection.
	  switch_block_conn[from][to][from_track].nelem = 0;
	  switch_block_conn[from][to][from_track].list = NULL;
	}//for
	// -----------------------------------------------------
      }// if else
     
    }//for ( to=0;...
  }//for
  //printf("Done alloc_and_load_switch_block_conn.\n");
}//eof
// ======================================================================= //

void s_fpga3d::free_switch_block_conn (int nodes_per_chan) 
{
// Frees the switch_block_conn data structure.
  
  free_ivec_matrix3 (switch_block_conn, 0, 5, 0, 5, 0, nodes_per_chan - 1);
}//eof
// ======================================================================= //

#define SBOX_ERROR -1

// ======================================================================= //
int s_fpga3d::get_simple_switch_block_track (
	     enum e_side from_side, enum e_side to_side, 
	     int from_track, 
	     enum e_switch_block_type switch_block_type, 
	     int nodes_per_chan ) 
{
// This routine returns the track number to which the from_track should 
// connect.  It supports three simple, Fs = 3, switch blocks.          

 int to_track;
 to_track = SBOX_ERROR;   // Can check to see if it's not set later. 



 if (switch_block_type == SUBSET) {   // NB:  Global routing uses SUBSET too 
    to_track = from_track;
 }
 // cristia:  Currently I support only SUBSET type switch-block for
 // 3D integration and routing.  The code below needs to be modified
 // easily to work WILTON or UNIVERSAL as well.  This is future work.




 // See S. Wilton Phd thesis, U of T, 1996 p. 103 for details on following.
 else if (switch_block_type == WILTON) {

    if (from_side == LEFT) {

       if (to_side == RIGHT) {        /* CHANX to CHANX */
          to_track = from_track;
       } 
       else if (to_side == TOP) {     /* from CHANX to CHANY */
          to_track = (nodes_per_chan - from_track) % nodes_per_chan;
       }
       else if (to_side == BOTTOM) {
          to_track = (nodes_per_chan + from_track -1) % nodes_per_chan;
       }
    }

    else if (from_side == RIGHT) {
       if (to_side == LEFT) {         /* CHANX to CHANX */
          to_track = from_track;
       }
       else if (to_side == TOP) {     /* from CHANX to CHANY */
          to_track = (nodes_per_chan + from_track - 1) % nodes_per_chan;
       }  
       else if (to_side == BOTTOM) { 
          to_track = (2 * nodes_per_chan - 2 - from_track) % nodes_per_chan;
       }  
    }

    else if (from_side == BOTTOM) {
       if (to_side == TOP) {         /* CHANY to CHANY */   
          to_track = from_track;
       }   
       else if (to_side == LEFT) {     /* from CHANY to CHANX */
          to_track = (from_track + 1) % nodes_per_chan;
       } 
       else if (to_side == RIGHT) { 
          to_track = (2 * nodes_per_chan - 2 - from_track) % nodes_per_chan;
       } 
    }

    else if (from_side == TOP) { 
       if (to_side == BOTTOM) {         /* CHANY to CHANY */
          to_track = from_track;
       }   
       else if (to_side == LEFT) {     /* from CHANY to CHANX */
          to_track = (nodes_per_chan - from_track) % nodes_per_chan;
       }
       else if (to_side == RIGHT) {
          to_track = (from_track + 1) % nodes_per_chan;
       }
    }
        
 }    /* End switch_block_type == WILTON case. */
 
 else if (switch_block_type == UNIVERSAL) {

    if (from_side == LEFT) {
 
       if (to_side == RIGHT) {        /* CHANX to CHANX */
          to_track = from_track;
       } 
       else if (to_side == TOP) {     /* from CHANX to CHANY */
          to_track = nodes_per_chan - 1 - from_track;
       } 
       else if (to_side == BOTTOM) {
          to_track = from_track;
       } 
    }

    else if (from_side == RIGHT) {
       if (to_side == LEFT) {         /* CHANX to CHANX */
          to_track = from_track;
       } 
       else if (to_side == TOP) {     /* from CHANX to CHANY */
          to_track = from_track;
       } 
       else if (to_side == BOTTOM) {
          to_track = nodes_per_chan - 1 - from_track;
       } 
    }
 
    else if (from_side == BOTTOM) {
       if (to_side == TOP) {         /* CHANY to CHANY */
          to_track = from_track;
       }  
       else if (to_side == LEFT) {     /* from CHANY to CHANX */
          to_track = from_track;
       } 
       else if (to_side == RIGHT) {
          to_track = nodes_per_chan - 1 - from_track;
       } 
    }
 
    else if (from_side == TOP) { 
       if (to_side == BOTTOM) {         /* CHANY to CHANY */
          to_track = from_track;
       }   
       else if (to_side == LEFT) {     /* from CHANY to CHANX */
          to_track = nodes_per_chan - 1 - from_track;
       } 
       else if (to_side == RIGHT) {
          to_track = from_track;
       }
    }     
 }    /* End switch_block_type == UNIVERSAL case. */
 

 if (to_track == SBOX_ERROR) {
    printf("Error in get_simple_switch_block_track.  Unexpected connection.\n"
           "from_side: %d  to_side: %d  switch_block_type: %d.\n", from_side,
           to_side, switch_block_type);
    exit (1);
 }
 
 return (to_track);
}//eof

// ======================================================================= //

s_ivec s_fpga3d::get_switch_box_tracks ( 
      int from_i, int from_j, int from_track, t_rr_type from_type,//eg. CHANX, 
      int to_i, int to_j, t_rr_type to_type,      //eg. CHANY
      enum e_switch_block_type switch_block_type, // [  ?  ] NOT used  
      int nodes_per_chan )                        // [  ?  ] NOT used  
{
// Returns a vector of the tracks to which from_track at (from_i from_j)
// should connect at (to_i,to_j).  
// OBS:  Used only for connections CHANX <--> CHANY.                    

 enum e_side from_side, to_side;

 // from_side,to_side: { LEFT, RIGHT, BOTTOM, TOP, BELOW or ABOVE}
 // from_type,to_type: { CHANX, CHANY, etc. }
 // Getting in some sense "the way" the switch box is entered and 
 // exited.
 from_side = get_sbox_side (from_i, from_j, from_type, to_i, to_j);
 to_side = get_sbox_side (to_i, to_j, to_type, from_i, from_j);
 
 // cristinel.ababei
 // De exemplu from LEFT to RIGHT or LEFT-->TOP as we look
 // as we look at a switch-box in a 2D layer.  This list of connections
 // is a single number for the simplest switch-box.  However
 // this way it is very flexible and could accomodate more
 // complex switch-boxes.
 return (switch_block_conn[from_side][to_side][from_track]);//it's an array
}//eof


// ======================================================================= //
// ======================================================================= //

  
s_ivec s_fpga3d::get_switch_box_tracks_z_to__x_y (
        int from_i,int from_j,// Loc. of CHANZ.
 	int from_k,           // Identifies zchan
	int from_track,       // Track inside z-chan
	t_rr_type from_type,  // CHANZ
	int to_i,int to_j,    // Which x or y chan; eg. (ABOVE, LEFT x-chan)
 	int to_k,             // "k" of chan x or y
	t_rr_type to_type,    // CHANX or CHANY
	enum e_switch_block_type switch_block_type, // [  ?  ] NOT used  
	int nodes_per_chan,                         // [  ?  ] NOT used  
	int vias_per_zchan )                        // [  ?  ] NOT used  
{
// cristinel.ababei
// Returns a vector of the tracks to which from_track of z-chan 
// identified by from_k (after the way rr_node indices were counted)
// should connect at chan x/y (given by to_type) 
// at loc (i,j) in layer to_k.                         
// OBS:  Used only for connections CHANZ --> CHANX,CHANY.

  enum e_side from_side,to_side;

  // from_side,to_side: { LEFT, RIGHT, BOTTOM, TOP, BELOW or ABOVE}
  // from_type,to_type: { CHANX, CHANY, etc. }
  // Getting in some sense "the way" the switch box is entered and 
  // exited.
  from_side = 
    get_sbox_side_z_to__x_y (from_i,from_j,from_k, from_type,
			     to_i,  to_j,  to_k,   to_type);
  to_side = 
    get_sbox_side_z_to__x_y (to_i,  to_j,  to_k,   to_type,
			     from_i,from_j,from_k, from_type);

  // Possibilities: from BELOW/ABOVE --> LEFT or BELOW/ABOVE --> RIGHT or 
  // BELOW/ABOVE --> BOTTOM or BELOW/ABOVE --> TOP;  All these "sides" are
  // as we look at the 3D switch-box:
  //
  //         ABOVE (layer)
  //            TOP  
  //           ----
  //          /   /|
  // LEFT    / B / |  RIGHT
  //         ----  |
  //        |   | /
  //        |   |/
  //         ----
  //         BELOW (layer)
  //
  // This list of connections is a single number for the simplest 
  // switch-box.  However, this way it is very flexible and could 
  // accomodate more complex switch-boxes.  So, I do it in the same general 
  // way.  Maybe someday somebody will appreciate and offer me an R&D job
  // or at least send a gift. :o)
 return (switch_block_conn[from_side][to_side][from_track]);//It's an array.
}//eof


// ======================================================================= //
// ======================================================================= //

  
s_ivec s_fpga3d::get_switch_box_tracks_x_to_z (
	int from_i,int from_j,// Loc. on CHANX during mersului.
 	int from_k,           // Layer in which xchan lies
	int from_track,       // Track inside xchan
	t_rr_type from_type,  // CHANX
	int to_i,int to_j,    // Which z-chan location
 	int to_k,             // Identifies zchan
	t_rr_type to_type)    // CHANZ
{
// cristinel.ababei
// Returns a vector of the tracks to which from_track of xchan 
// should connect at chan z identified by to_k
// OBS:  Used only for connections CHANX --> CHANZ.

  enum e_side from_side,to_side;

  // from_side,to_side: { LEFT, RIGHT, BOTTOM, TOP, BELOW or ABOVE}
  // from_type,to_type: { CHANX, CHANY, etc. }
  // Getting in some sense "the way" the switch box is entered and 
  // exited.
  from_side = 
    get_sbox_side_x_to_z (from_i,from_j,from_k, from_type,
			  to_i,  to_j,  to_k,   to_type);
  to_side = 
    get_sbox_side_x_to_z (to_i,  to_j,  to_k,   to_type,
			  from_i,from_j,from_k, from_type);

  // Possibilities: from LEFT --> BELOW/ABOVE or RIGHT --> BELOW/ABOVE

  return (switch_block_conn[from_side][to_side][from_track]);//It's an array.
}//eof
// ----------------------------------------------------------------------- //
s_ivec s_fpga3d::get_switch_box_tracks_y_to_z (
	int from_i,int from_j,// Loc. on CHANY during mersului.
 	int from_k,           // Layer in which ychan lies
	int from_track,       // Track inside ychan
	t_rr_type from_type,  // CHANY
	int to_i,int to_j,    // Which z-chan location
 	int to_k,             // Identifies zchan
	t_rr_type to_type)    // CHANZ
{
// cristinel.ababei
// Returns a vector of the tracks to which from_track of y-chan 
// should connect at chan z identified by to_k
// OBS:  Used only for connections CHANY --> CHANZ.

  enum e_side from_side,to_side;

  // from_side,to_side: { LEFT, RIGHT, BOTTOM, TOP, BELOW or ABOVE}
  // from_type,to_type: { CHANX, CHANY, etc. }
  // Getting in some sense "the way" the switch box is entered and 
  // exited.
  from_side = 
    get_sbox_side_y_to_z (from_i,from_j,from_k, from_type,
			  to_i,  to_j,  to_k,   to_type);
  to_side = 
    get_sbox_side_y_to_z (to_i,  to_j,  to_k,   to_type,
			  from_i,from_j,from_k, from_type);

  // Possibilities: from BOTTOM  --> BELOW/ABOVE or TOP --> BELOW/ABOVE; 

  return (switch_block_conn[from_side][to_side][from_track]);//It's an array.
}//eof


// ======================================================================= //
// ======================================================================= //

enum e_side s_fpga3d::get_sbox_side_x_to_z (
	     int get_i,  int get_j,  int get_k,  t_rr_type get_type,
	     int comp_i, int comp_j, int comp_k, t_rr_type comp_type ) 
{
// Returns the side of the switch-box that the get_node is on, 
// as compared to the comp (comparison) node.
  enum e_side side;
 
  // First, see if the "get" is zchan or not.
  if ( get_type == CHANZ ) { // Looking from CHANZ perspective. 
    if (get_k == comp_k) // Based on how I counted rr_node indices. 
      side = BELOW; 
    else                 // Only  get_k = comp_k - 1  is possible.
      side = ABOVE; 
  }
  else { // Looking from CHANX perspective. 
    if ( get_i == comp_i )
      side = LEFT; 
    else 
      side = RIGHT; 
  }//if ( from_type == CHANZ ) else...


  if ( (get_type != CHANX) && (get_type != CHANZ) )
    {
      printf ("Error in get_sbox_side.  Unexpected get_type: %d.\n",get_type);
      exit (1);
    }
  return (side);
}//eof

// ----------------------------------------------------------------------- //
enum e_side s_fpga3d::get_sbox_side_y_to_z (
	     int get_i,  int get_j,  int get_k,  t_rr_type get_type,
	     int comp_i, int comp_j, int comp_k, t_rr_type comp_type ) 
{
// Returns the side of the switch-box that the get_node is on, 
// as compared to the comp (comparison) node.
  enum e_side side;
 
  // First, see if the "get" is zchan or not.
  if ( get_type == CHANZ ) { // Looking from CHANZ perspective. 
    if (get_k == comp_k) // Based on how I counted rr_node indices. 
      side = BELOW; 
    else                 // Only  get_k = comp_k - 1  is possible.
      side = ABOVE; 
  }
  else { // Looking from CHANY perspective. 
    if ( get_j == comp_j )
      side = BOTTOM; 
    else 
      side = TOP; 
  }//if ( from_type == CHANZ ) else...


  if ( (get_type != CHANY) && (get_type != CHANZ) )
    {
      printf ("Error in get_sbox_side.  Unexpected get_type: %d.\n",get_type);
      exit (1);
    }
  return (side);
}//eof

// ----------------------------------------------------------------------- //
enum e_side s_fpga3d::get_sbox_side_z_to__x_y (
	     int get_i,  int get_j,  int get_k,  t_rr_type get_type,
	     int comp_i, int comp_j, int comp_k, t_rr_type comp_type ) 
{
// Returns the side of the switch-box that the get_node is on, 
// as compared to the comp (comparison) node.
  enum e_side side;

  // First, see if the "get" is zchan or not.
  if ( get_type == CHANZ ) // Looking from CHANZ perspectice. 
    if (get_k == comp_k) {// Based on how I counted rr_node indices. 
      side = BELOW; 
    } 
    else {                // Only  get_k = comp_k - 1  is possible.
      side = ABOVE; 
    } 


  else { // Looking from CHAN X / Y perspectice. 
    if ( get_type == CHANX ) {
      if ( get_i == comp_i )
	side = LEFT; 
      else 
	side = RIGHT; 
    }
    else {  // CHANY
      if ( get_j == comp_j )
	side = BOTTOM; 
      else 
	side = TOP; 
    } 
  }//if ( get_type == CHANZ ) else...



  if ( (get_type != CHANX) && (get_type != CHANY) && (get_type != CHANZ) )
    {
      printf ("Error in get_sbox_side.  Unexpected get_type: %d.\n",get_type);
      exit (1);
    }
  
  return (side);
}//eof


// ----------------------------------------------------------------------- //


enum e_side s_fpga3d::get_sbox_side (
             int get_i, int get_j, 
	     t_rr_type get_type, 
	     int comp_i, int comp_j ) 
{
// Returns the side of the switch box that the get_node is on, as compared
// to the comp (comparison) node.                                         

  enum e_side side;

  if (get_type == CHANX) { 
    if (get_i > comp_i) {
      side = RIGHT; 
    } 
    else { 
      side = LEFT; 
    } 
  }
 
  else if (get_type == CHANY) {
    if (get_j > comp_j) {
      side = TOP;
    }
    else {
      side = BOTTOM;
    }
  }

  else if (get_type == CHANZ) {
    if (get_j > comp_j) {
      side = ABOVE;
    }
    else {
      side = BELOW;
    }
  } 

  else {
    printf ("Error in get_sbox_side.  Unexpected get_type: %d.\n", get_type);
    exit (1);
  }
  
  return (side);
}//eof
// ======================================================================= //
