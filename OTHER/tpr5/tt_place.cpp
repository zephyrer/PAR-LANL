// ======================================================================= //
// Cristinel Ababei 10/21/2003
// ======================================================================= //

#include <time.h>
#include "math.h"
#include <fstream>
#include <iostream>

#include "wl_annealer.h"
#include "part_rec.h"


using namespace std;

extern tt_hgraph * tt_hg;
extern KmostPaths * kmp;

// cristinel.ababei "clma.net" 92x92
extern double delta_clb_to_clb_hard[92][92];
extern double delta_clb_to_clb_2_hard[92][92];
extern double delta_inpad_to_clb_hard[93][93];
extern double delta_clb_to_outpad_hard[93][93];
extern double delta_inpad_to_outpad_hard[94][94];

// ======================================================================= //


// ======================================================================= //
void s_fpga3d::timing_kmost_driven_part_based_place(void)
{
  // This routine implements the timing driven - kmost critical paths 
  // approach - partitioning based placement engine.
  // First, top-layer is partitioned with no constraint (all nodes
  // in top-layer are free to be placed everywhere).  Then, layer beneath
  // is placed with constraints imposed by the placement result in the
  // layer(s) above.  Constraints are such that nets with terminals in
  // more than one layer should have their 3D bounding-box minimized.
  // In some sense, it's kind of 3D terminal allignment.
  //
  int i=0;
  clock_t start_time_place, finish_time_place;
  double duration_place=0;
 
  start_time_place = clock();

  // -----------------------------------------------------------------------
  // --- 0 ---
  // -----------------------------------------------------------------------
  // These edge weights are now only slack-based.
  Import_edge_weights_from_tim_graph_to_fpga3d();

  // -----------------------------------------------------------------------
  // --- 1 --- 
  // -----------------------------------------------------------------------
  // Place naively io pads. I moved it inside run_rec(), where
  // asignment is done layer by layer right before its
  // partitioning.
  // assign_locations_to_io_pads_on_all_layers_version01();


  // -----------------------------------------------------------------------
  // --- 2 --- 
  // -----------------------------------------------------------------------
  // Create a Part_Rec object for every layer.  Inside these 
  // objects the rec quad-part will take place.

  Part_Rec * prl[num_layers]; // part rec layer
  //wl_annealer * wlal[num_layers]; // wl annealer layer
  for (i=0;i<num_layers;i++) 
    {
      prl[i] = new Part_Rec(this, i);
      //wlal[i] = new wl_annealer(this, i);
      // --- Right before we do partitioning we assign naively
      // but deterministically locations to all io pads in all layers.
      assign_locations_to_io_pads_on_layer( i );
    }

  for (i=0;i<num_layers;i++) 
    {
      prl[i]->run_rec();
      //wlal[i]->run_wl_anneal_move_one_cell(
      //	  time(NULL),0.99,(num_blocks),0.05);
    }//for
  /*---
  for (i=0;i<num_layers;i++) 
    {
      prl[i]->run_rec();
      //wlal[i]->run_wl_anneal_move_one_cell(
      //	  time(NULL),0.99,(num_blocks),0.05);
    }//for
    ---*/


  // -----------------------------------------------------------------------
  // --- 3 --- 
  // -----------------------------------------------------------------------
  // Mirror/copy all blocks and their locations into clbs,
  // which is the architecture itself.
  int j=0,k=0,subblock=0;
  // Next 3 for loops should be redundant.  This was done in fill_arch()
  // inside read_arch.cpp.  However, here even the corners are reset...
  for (k=0;k<num_layers;k++)
    for (i=0;i<=nx+1;i++)
      for (j=0;j<=ny+1;j++)
	clb[i][j][k].occ = 0;//reset
  
  for (i=0;i<num_blocks;i++) {
    if (block[i].type == CLB) {
      clb[block[i].x][block[i].y][block[i].z].occ = 1;
      clb[block[i].x][block[i].y][block[i].z].u.block = i;
      //printf(" %d",i);
    }
    if( (block[i].type == INPAD)||(block[i].type == OUTPAD) ) {
      // An IO clb may be occupied by more INPAD OUTPAD blocks
      // assigned during io pad assignment on margins/perifery.
      j = clb[block[i].x][block[i].y][block[i].z].occ;
      clb[block[i].x][block[i].y][block[i].z].u.io_blocks[j] = i;
      clb[block[i].x][block[i].y][block[i].z].occ++;
      //printf(" %d,%d %d %d ",block[i].x,block[i].y,i,j);
      //printf(" %d %d ",j,i);
    }
  }//for

  /*---
  // Some sanity check:
  int ipad=0;
  printf("\nsanity check:\n");
  for (i=0;i<num_blocks;i++) {
    if( (block[i].type == INPAD)||(block[i].type == OUTPAD) ) {
      printf(" %d(",i);
      for (ipad=0;ipad<clb[block[i].x][block[i].y][block[i].z].occ;ipad++)
	printf(" %d",clb[block[i].x][block[i].y][block[i].z].u.io_blocks[ipad]);
      printf(") ");
    }
  }
  ---*/

  // -----------------------------------------------------------------------
  // --- 4 ---
  // -----------------------------------------------------------------------
  // Dump out some results stuff. 
  finish_time_place = clock();
  duration_place = (double)(finish_time_place - 
			    start_time_place) / CLOCKS_PER_SEC;
  tt_hg->Compute_Static_DelayForPOs();
  //printf_grid_1st_most_critical_path(0);
  Update_Edge_Weights();
  //tt_hg->Dump_delays_of_all_source_sink_pairs( "dbg_place_delay.echo" );
  cout<<"\n ------------------------------------------------------";
  cout<<"\n Right after placement:";
  cout<<"\n ------------------------------------------------------";
  double place_3D_WL=0,place_Static_Delay=0,place_vert_fraction=0;
  place_3D_WL = get_3D_WL();
  place_Static_Delay = tt_hg->Get_Static_Delay();
  final_data.wl_place = place_3D_WL;
  final_data.delay_place = place_Static_Delay;
  final_data.run_time_place = duration_place;
  cout<<"\n 3D WL="<<place_3D_WL
      <<"   Place-run-time="<<duration_place;
  cout<<"\n Delay estimated after placement="<<place_Static_Delay<<endl;
  place_vert_fraction = get_some_stats();
  final_data.fraction_z = place_vert_fraction;
  //tt_hg->Print_tt_hgraph( "dbg_tt_graph4.echo" );

  // -----------------------------------------------------------------------


}//eof
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::Update_Edge_Weights(void)
{
  // Have weights/criticalities updated in timing graph
  // and fpga3d object.

  // Assign delays to cut wires based on their locations in this
  // layer.  Delay is taken from a LUT based on the optimistic
  // routing obtained with vpr.
  Assign_delay_to_cut_wires();
  // Do the STA for slack update.  This also updates edge-weights
  // based on slack only.  "tt_hg" is global so we can access
  // it directly.
  tt_hg->ComputeArrivalTimesFromPIsToPOs(); // --- it's STA itself
  tt_hg->ComputeRequiredTimesFromPOsToPIs();// --- also slack and Crit OVER-write
  // Re-enumerate paths which will account for the new delays
  // of cut wires.
  tt_hg->PathEnumerationDuringRecursion();
  // Bias net weights on the k-most critical paths so that these
  // nets are discouraged to be cut by hmetis.  Only nets between
  // different clbs have their weights biased; those between inpins and 
  // opins of same clb are unchanged because they are constant.  This
  // I'm talking in the timing graph.
  tt_hg->Bias_edge_weights_on_kmost_paths();
  Import_edge_weights_from_tim_graph_to_fpga3d();
  // At this moment edge weights - in both timing graph and
  // fpga3d.net[] - should be updated as well based on what's inside the
  // timing graph.
  //printf("\nDone Update_Edge_Weights.");
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::Assign_delay_to_cut_wires(void)
{
  // This function sets delay for all nets which have terminals
  // with different coordinates.  Delay is taken from a LUT generated
  // with vpr (by Pongstorn) optimistically.  The 2D lut - matrix - is 
  // indexed by x2-x1,y2-y1.
  // Nets with terminals in at least two different layers have 
  // their delays taken from same lut (but for bounding box of the
  // projection of all terminals on a plane) + delay for vias between
  // layers.

  int i=0;
  int xdist=0,ydist=0,zdist=0;
  int id_nets=0,id_verts=0;
  // --- First, compute bounding boxes of all nets.
  Compute_bounding_boxes_of_nets_fpga3d();


  // ---------------------------------------------------------------
  // 1
  // First way of assigning delay to nets:  same delay for every
  // source-sink pair of a given net.
  // --- Assign delays from lut.  If the net is 3D add extra delay
  // to account for vias delay between layers.
  // ---------------------------------------------------------------


  // ---------------------------------------------------------------
  // 2 
  // Second way of assigning delay to nets:  different delay for every
  // source-sink pair of a given net.  This is closer to reality
  // as routing resources are buffered in fpgas and delay to sinks of 
  // same net may vary.
  // --- Assign delays from lut.  If the net is 3D add extra delay
  // to account for vias delay between layers.
  int j=0, source_block_id=0, sink_block_id=0;
  int id_verts_of_sink=0;
  double delay_of_sink=0;
  enum e_block_types source_type, sink_type;



  for (i=0;i<num_nets;i++)
   {
    if (is_global[i] == false) 
     { 

      // Look at every source-sink pair and compute its delay.
      source_block_id = net[i].blocks[0]; // Source.
      id_nets = net[i].netID_tim_hg;//id of coresp net in timing hg.
      id_verts = (*tt_hg->nets[id_nets].verts.begin())->ID;

      for (j=1;j<net[i].num_pins;j++)    // Go thru all sinks.
	{     
	  delay_of_sink=0; // Reset.
	  sink_block_id = net[i].blocks[j];
	  // Next 2 if's go along the same line as it went for
	  // populating this info in create_timing_tt_hgraph_02()
	  if ( block[sink_block_id].type == OUTPAD )
	    id_verts_of_sink =
	      block[sink_block_id].id_tt_hgraph_02[0];//po
	  if ( block[sink_block_id].type == CLB )
	    id_verts_of_sink =
	      block[sink_block_id].id_tt_hgraph_02[net[i].blk_pin[j]];
	  //cout<<"   "<<source_block_id<<","<<sink_block_id;
	  //cout<<" "<<id_verts<<","<<id_verts_of_sink;



	  xdist = (int) abs( block[sink_block_id].x - 
			     block[source_block_id].x );
	  ydist = (int) abs( block[sink_block_id].y  - 
			     block[source_block_id].y );
	  zdist = (int) abs( block[sink_block_id].z  - 
			     block[source_block_id].z );
	  source_type = block[source_block_id].type;
	  sink_type = block[sink_block_id].type;



	  //OLD:delay_of_sink = DELAY_LUT[xdist][ydist]; // Horiz. contribution.


	  if (source_type == CLB) {
	    if (sink_type == CLB) {
	      // Sanity check:
	      if ( xdist > nx  || ydist > ny ) {
		printf("Error delay estimation at place-level.\n");
		printf("clb to clb =nx ??? xdist=%d ydist=%d.\n",xdist,ydist);
		printf(" source block %d [%d,%d,%d] sink block block %d [%d,%d,%d]",
		       source_block_id,
		       block[source_block_id].x,block[source_block_id].y,
		       block[source_block_id].z,
		       sink_block_id,
		       block[sink_block_id].x,block[sink_block_id].y,
		       block[sink_block_id].z);
		exit(1);
	      }
	      if ( block[source_block_id].y > block[sink_block_id].y )
		delay_of_sink = delta_clb_to_clb_2_hard[xdist][ydist];
	      // Above is correction to VPR.
	      else {
		delay_of_sink = delta_clb_to_clb_hard[xdist][ydist];
		//cout<<" "<<delay_of_sink;
	      }
	    }
	    else if (sink_type == OUTPAD) {
	      // Sanity check:
	      if ( xdist > nx+1  || ydist > ny+1 ) {
		printf("Error delay estimation at place-level.\n");
		printf("clb to outpad =nx ??? xdist=%d ydist=%d.\n",xdist,ydist);
		printf(" source block %d [%d,%d,%d] sink block block %d [%d,%d,%d]",
		       source_block_id,
		       block[source_block_id].x,block[source_block_id].y,
		       block[source_block_id].z,
		       sink_block_id,
		       block[sink_block_id].x,block[sink_block_id].y,
		       block[sink_block_id].z);
		exit(1);
	      }
	      delay_of_sink = delta_clb_to_outpad_hard[xdist][ydist];
	    }
	    else {
	      printf("Error delay estimation at place-level, bad sink_type\n");
	      exit(1);
	    }
	  }
	  else if (source_type == INPAD) {
	    if (sink_type == CLB) {
	      // Sanity check:
	      if ( xdist > nx+1  || ydist > ny+1 ) {
		printf("Error delay estimation at place-level.\n");
		printf("inpad to clb =nx ??? xdist=%d ydist=%d.\n",xdist,ydist);
		printf(" source block %d [%d,%d,%d] sink block block %d [%d,%d,%d]",
		       source_block_id,
		       block[source_block_id].x,block[source_block_id].y,
		       block[source_block_id].z,
		       sink_block_id,
		       block[sink_block_id].x,block[sink_block_id].y,
		       block[sink_block_id].z);
		exit(1);
	      }
	      delay_of_sink = delta_inpad_to_clb_hard[xdist][ydist];
	    }
	    else if (sink_type == OUTPAD)
	      delay_of_sink = delta_inpad_to_outpad_hard[xdist][ydist];
	    else {
	      printf("Error delay estimation at place-level, bad sink_type\n");
	      exit(1);
	    }
	  }
	  else {
	      printf("Error delay estimation at place-level, bad source_type\n");
	      exit(1);
	  }//if imbirligat.


	  // VIA_DELAY is already computed inside place_and_route()
	  // with a specialized function.
	  // Devided by two in order to correct overestimation due to
	  // the way delay look-up tables were constructed.
	  if (zdist > 0) 
	    if (vertical_delay_same == 1) {
	      // In acest caz toate vertical connections 
	      // au acelasi delay = to length-one wire segment.
	      delay_of_sink = delay_of_sink + VIA_DELAY;
	    }
	    else {
	      delay_of_sink = 
		delay_of_sink + (double)zdist * VIA_DELAY;
	    }

	  // Put delay_of_sink in the corresponding fanout 
	  // edge in timing graph.
	  tt_hg->verts[id_verts].fOUTverts[id_verts_of_sink].miu = delay_of_sink;
	}//for j
     }//if (is_global[i] == false)
   }//for i
  // ---------------------------------------------------------------
 

}//eof


// ======================================================================= //
// ======================================================================= //



void tt_hgraph::Bias_edge_weights_on_kmost_paths(void)
{
  // Look at all kmost paths and bias with a constant amount
  // edge-weights, which are already computed as slack-based
  // edge-weights.

  int i=0,j=0;
  int ver_id=0;

  // --- First reset all flags "weight_was_biased" for all nets.
  for(vector<Net>::iterator ii1=nets.begin();ii1!=nets.end();ii1++)
    ii1->weight_was_biased = 0;

  // --- Go visit all nodes along all paths.
  for(j=0;j<kmp->NoOfEnumPaths();j++) {
    // Look at all edges on every path by looking at 
    // all nods along the path except the last one.  The last
    // one is not looked at because it cannot drive any net since
    // it' probably a PO or it drives verts in other partitions
    // (when doing this locally during recursion).
    list<int>::iterator last_valid_iter8 = kmp->Paths()[j].end(); 
    last_valid_iter8--; 
    for(list<int>::iterator it_list8=kmp->Paths()[j].begin();
	it_list8 != kmp->Paths()[j].end();it_list8++) {
      if ( (it_list8 != last_valid_iter8) && //Last valid node disregarded
	   ((*it_list8) != -1) )//sourceS is not a valid vertex
	{
	  // Get vertex id in timing graph.
	  ver_id = (*it_list8);
	  // See if this node in timing graph drives a variable
	  // delay wire or it is internal to some clb.  Nets with
	  // source as ipin of some clb are internal to that clb.
	  // Nets with source an opin of some clb drives a net
	  // with variable delay between clbs based on locations
	  // in placement.
	  // This current vertex on this critical path
	  // drives a net which is inter-clbs.  Therefore we
	  // bias its weight.  But this is done only once
	  // because same vertex may appear on more paths
	  // and we do not want to bias the weight of same 
	  // driven net more times.
	  if ( (nets[verts[ver_id].netID].delay_type == VAR_DELAY) &&
	       (nets[verts[ver_id].netID].weight_was_biased == 0) )
	    {
	      // Bias net because it was not yet.
	      nets[verts[ver_id].netID].weight = 
		    nets[verts[ver_id].netID].weight +
		    BIAS_AMOUNT_FOR_WEIGHTS_ON_KMOST;
	      //cout<<" "<<verts[ver_id].netID<<" "
	      //    <<nets[verts[ver_id].netID].weight<<" ";
	      // Set this net as having its weight biased this time.
	      nets[verts[ver_id].netID].weight_was_biased = 1;
	    }

	}//if
    }//for
  }//for
  //printf("\nDone Bias_edge_weights_on_kmost_paths.");
}//end
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::Import_edge_weights_from_tim_graph_to_fpga3d(void)
{
  // Nets in fpga3d object have couterparts in the timing graph.
  // Bring their weight from the tim hg, where it is computed based on
  // slack and kmost critical paths.
  int i=0;
  for (i=0;i<num_nets;i++) 
    {
      if (is_global[i] == false) { 
	net[i].weight = tt_hg->nets[net[i].netID_tim_hg].weight;
	//cout<<" "<<i<<" "<<net[i].weight<<" ";
      }
    }
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::Compute_bounding_boxes_of_nets_fpga3d(void)
{
  // Goes tru all nets and updates bounding boxe(r)s. :)
  int i=0,j=0;
  int loc_minx=INT_MAX,loc_miny=INT_MAX,loc_minz=INT_MAX;
  int loc_maxx=-1,loc_maxy=-1,loc_maxz=-1;
  int count_nets=0;
  for (i=0;i<num_nets;i++) 
   {
    if (is_global[i] == false) 
     { 

      loc_minx=INT_MAX,loc_miny=INT_MAX,loc_minz=INT_MAX;//reset
      loc_maxx=-1,loc_maxy=-1,loc_maxz=-1;//reset
      for (j=0;j<net[i].num_pins;j++) {//go thru all terminals
	if(block[net[i].blocks[j]].x < loc_minx) loc_minx = 
						   block[net[i].blocks[j]].x;
	if(block[net[i].blocks[j]].y < loc_miny) loc_miny = 
						   block[net[i].blocks[j]].y;
	if(block[net[i].blocks[j]].z < loc_minz) loc_minz = 
						   block[net[i].blocks[j]].z;
	if(block[net[i].blocks[j]].x > loc_maxx) loc_maxx = 
						   block[net[i].blocks[j]].x;
	if(block[net[i].blocks[j]].y > loc_maxy) loc_maxy = 
						   block[net[i].blocks[j]].y;
	if(block[net[i].blocks[j]].z > loc_maxz) loc_maxz = 
						   block[net[i].blocks[j]].z;
      }
      net[i].bb.xmin = loc_minx;
      net[i].bb.ymin = loc_miny;
      net[i].bb.zmin = loc_minz;
      net[i].bb.xmax = loc_maxx;
      net[i].bb.ymax = loc_maxy;
      net[i].bb.zmax = loc_maxz;
      if(loc_minz != loc_maxz) count_nets++;
      //printf(" %d %d ",loc_miny,loc_maxy);
     }//if (is_global[i] == false) 
   }//for
  //printf("\n Num of nets spanning at least two layers= %d",count_nets); 
}//eof
// ======================================================================= //
// ======================================================================= //
double s_fpga3d::get_3D_WL(void)
{
  // Goes and updates all 3d bounding boxes.
  // And sum.
  int wl3d=0;
  Compute_bounding_boxes_of_nets_fpga3d();
  for (int i=0;i<num_nets;i++) 
   {
    if (is_global[i] == false)
     { 
      wl3d = wl3d + 
	( net[i].bb.xmax - net[i].bb.xmin ) + 
	( net[i].bb.ymax - net[i].bb.ymin ) + 
	( net[i].bb.zmax - net[i].bb.zmin )*vertical_uwl;
     }//if (is_global[i] == false)
   }//for
  return wl3d;
}//eof
// ======================================================================= //
// ======================================================================= //
double s_fpga3d::get_some_stats(void)
{
  // Reports usefull stats right after placement.
  double total_out_degree=0,avg_out_degree=0;
  double total_net_terminals=0,avg_net_terminals=0;
  double total_net_wl=0,avg_net_wl=0;
  double total_vertical_wl=0,fraction_vertical_wl=0;
  int i=0,j=0,k=0,pin=0;
  int zdist=0,max_pin=0;
  int num_nets_touching_block=0;
  int contor=0;


  for (i=0;i<num_nets;i++) 
   {
    if (is_global[i] == false) 
     { 
       zdist = net[i].bb.zmax - net[i].bb.zmin;
       if ( zdist > 0 ) {
	 total_vertical_wl = total_vertical_wl + 
	   (double) zdist * vertical_uwl; // unit-wire-length
	 contor++;
       }
       total_net_terminals =  total_net_terminals + net[i].num_pins;
     }//if (is_global[i] == false)
   }//for
 
 
  for (i=0;i<num_blocks;i++) 
    {
      num_nets_touching_block=0;//reset
      if (block[i].type == INPAD || block[i].type == OUTPAD)
	max_pin = 1;
      else
	max_pin = pins_per_clb;
      for (pin=0;pin<max_pin;pin++)//PINS
	{
	  if ( block[i].nets[pin] != OPEN ) 
	    {
	      num_nets_touching_block++;
	    }//if
	}//for
      total_out_degree = total_out_degree + num_nets_touching_block;
    }//for


  int * num_nets_with_terms_in = (int *) my_malloc ((num_layers+1) *sizeof(int));
  int * num_nets_spanning = (int *) my_malloc ((num_layers+1) *sizeof(int));
  int * net_monitor = (int *) my_malloc ((num_layers+1) *sizeof(int));
  int goes_to=0,max_z=0,min_z=num_layers+1;
  for (i=0;i<=num_layers;i++) {//Reset.
    num_nets_with_terms_in[i]=0;
    num_nets_spanning[i]=0;
    net_monitor[i]=0;
  }
  for (i=0;i<num_nets;i++) {
      if (is_global[i] == false) 
      { 
	for (j=0;j<net[i].num_pins;j++) {
	  net_monitor[ block[net[i].blocks[j]].z ]++;
	  if(  block[net[i].blocks[j]].z < min_z )
	    min_z = block[net[i].blocks[j]].z;
	  if(  block[net[i].blocks[j]].z > max_z )
	    max_z = block[net[i].blocks[j]].z;
	}
	goes_to=0;//reset
	for (k=0;k<=num_layers;k++) {
	  if (net_monitor[k] > 0) goes_to++;
	  net_monitor[k]=0;//Reset for monitoring next "i" net.
	}//for
	num_nets_with_terms_in[goes_to]++;
	num_nets_spanning[ max_z - min_z ]++;
	max_z=0,min_z=num_layers+1;//Reset.
      }//if
  }//for


  total_net_wl = get_3D_WL();
  avg_net_wl = total_net_wl / num_nets;
  cout<<"\n Average net wire-length = "<<avg_net_wl;
  avg_out_degree = total_out_degree / num_blocks;
  cout<<"\n Average block out-degree = "<<avg_out_degree;
  avg_net_terminals = total_net_terminals / num_nets;
  cout<<"\n Average net num terminals = "<<avg_net_terminals;
  double vert_fraction = ((double)contor / (double)num_nets);
  cout<<"\n Fraction of nets spanning more layers = "<<vert_fraction;
  fraction_vertical_wl = total_vertical_wl / total_net_wl;
  cout<<"\n Fraction of vertical WL = "<<fraction_vertical_wl<<endl;


  cout<<"\n 1 --- Histogram: #nets with terminals in n different layers.\n";
  for (k=1;k<=num_layers;k++) {
    cout<<" "<<num_nets_with_terms_in[k];
  }
  cout<<"\n 2 --- Histogram: #nets spanning delta_n layers.\n";
  for (k=0;k<num_layers;k++) {
    cout<<" "<<num_nets_spanning[k];
  }
  free(num_nets_with_terms_in);
  free(num_nets_spanning);
  free(net_monitor);
  return vert_fraction;

}//eof
// ======================================================================= //

// ======================================================================= //
void s_fpga3d::assign_locations_to_io_pads_on_all_layers_version01(void)
{
  // Naively assigns io pads to locations around the chip 
  // equidistantly, on every layer.
  int i=0,k=0,temp_i=0;
  int x=0,y=0;
  int step=0,count=0,pipo_layer=0;
  int total_num_lio = 2*(nx + ny); // Total number of io pads on a layer.
  queue<int> myqueue; 

  for (k=0;k<num_layers;k++) {
    if(num_layers == 1)
      pipo_layer = num_p_inputs + num_p_outputs;
    else
      pipo_layer = pi_balance[k] + po_balance[k];

    if( pipo_layer > 0) {
      if(total_num_lio <= pipo_layer)
	step = 1;
      else {
	step = int ( floor (double(total_num_lio)/double(pipo_layer)) );
	// Do a correction of it:
	if( step > (nx/3) ) step = step / 2;
	if( step >= nx ) step = nx - 1;
      }
    }
    else {
      printf("\nThere are no io-pads to assign on layer= %d.", k);
      // Set all clb, though to the center of this layer and then
      // return only.
      for (i=0;i<num_blocks;i++)
	if( block[i].layer == k )
	  block[ i ].setXY(grid[k], ((nx+1)/2),((ny+1)/2), true);
      continue; // Go for next k.
    }

    count = 0;//reset
    // -- First put all io pads on current layer in a queue.
    for (i=0;i<num_blocks;i++)
      if( block[i].layer == k ) {
	if( (block[i].type == INPAD)||(block[i].type == OUTPAD) )
	  myqueue.push( i );
	else {
	  // Assign center as location for all clb to start
	  // with all in the center.
	  block[ i ].setXY(grid[k], ((nx+1)/2),((ny+1)/2), true);
	}
      }//if

    // -- Now, take all from queue and place them "in-circle"
    // on the layer's margins.  If circled more than once
    // io pads will be congested. 
    while ( !myqueue.empty() ) {
      x=0; y=0;//reset
      x = x + step;
      while( (count < pipo_layer) && (x <= nx) && (!myqueue.empty()) ) {
	temp_i = myqueue.front(); // get one io from queue
	myqueue.pop(); // delete it also from queue.
	count++;

	//cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
	block[ temp_i ].setXY(grid[k], x, y, true);
	x = x + step;
      }//while
      x=nx+1; y=0;//reset
      y = y + step;
      while( (count < pipo_layer) && (y <= ny) && (!myqueue.empty()) ) {
	temp_i = myqueue.front(); // get one io from queue
	myqueue.pop(); // delete it also from queue.
	count++;
	//cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
	block[ temp_i ].setXY(grid[k], x, y, true);
	y = y + step;
      }//while
      x=nx+1; y=ny+1;//reset
      x = x - step;
      while( (count < pipo_layer) && (x > 0) && (!myqueue.empty()) ) {
	temp_i = myqueue.front(); // get one io from queue
	myqueue.pop(); // delete it also from queue.
	count++;
	//cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
	block[ temp_i ].setXY(grid[k], x, y, true);
	x = x - step;
      }//while
      x=0; y=ny+1;//reset
      y = y - step;
      while( (count < pipo_layer) && (y > 0) && (!myqueue.empty()) ) {
	temp_i = myqueue.front(); // get one io from queue
	myqueue.pop(); // delete it also from queue.
	count++;
	//cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
	block[ temp_i ].setXY(grid[k], x, y, true);
	y = y - step;
      }//while
    }//while ( !myqueue.empty() )

    // At this moment myqueue should be empty.
  }//for (k=0;k<num_layers;k++)

  printf("\nDone assigning locations to io-pads, version 01."); 
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::assign_locations_to_io_pads_on_layer(int layer_id)
{
  // Naively assigns io pads to locations around the chip 
  // equidistantly, on layer_id.
  int i=0,temp_i=0;
  int x=0,y=0;
  int step=0,count=0,pipo_layer=0;
  int total_num_lio = 2*(nx + ny); // Total number of io pads on a layer.
  queue<int> myqueue; 


  if(num_layers == 1)
    pipo_layer = num_p_inputs + num_p_outputs;
  else
    pipo_layer = pi_balance[layer_id] + po_balance[layer_id];
  cout<<"\n pipo_layer="<<pipo_layer;


  if( pipo_layer > 0) {
    if(total_num_lio <= pipo_layer)
      step = 1;
    else {
      step = int ( floor (double(total_num_lio)/double(pipo_layer)) );
      // Do a correction of it:
      if( step > (nx/3) ) step = step / 2;
      if( step >= nx ) step = nx - 1;
    }
  }
  else {
    printf("\nThere are no io-pads to assign on layer= %d.", layer_id);
    // Set all clb, though to the center of this layer and then
    // return only.
    for (i=0;i<num_blocks;i++)
      if( block[i].layer == layer_id )
	block[ i ].setXY(grid[layer_id], ((nx+1)/2),((ny+1)/2), true);
    return; // Exit routine.
  }


  count = 0;//reset

  // -- First put all io pads on current layer in a queue.
  for (i=0;i<num_blocks;i++)
    if( block[i].layer == layer_id ) {
      if( (block[i].type == INPAD)||(block[i].type == OUTPAD) ) {
	myqueue.push( i );
	//cout<<"\n put in queue  "<<i;
      }
      else {
	// Assign center as location for all clb to start
	// with all in the center.
	block[ i ].setXY(grid[layer_id], ((nx+1)/2),((ny+1)/2), true);
	//printf(" %d,%d",block[ i ].x,block[ i ].y);
      }
    }//if

  // -- Now, take all from queue and place them "in-circle"
  // on the layer's margins.  If circled more than once
  // io pads will be congested. 
  while ( !myqueue.empty() ) {
    x=0; y=0;//reset
    x = x + step;
    while( (count < pipo_layer) && (x <= nx) && (!myqueue.empty()) ) {
      temp_i = myqueue.front(); // get one io from queue
      myqueue.pop(); // delete it also from queue.
      count++;
      //cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
      block[ temp_i ].setXY(grid[layer_id], x, y, true);
      x = x + step;
    }//while
    x=nx+1; y=0;//reset
    y = y + step;
    while( (count < pipo_layer) && (y <= ny) && (!myqueue.empty()) ) {
      temp_i = myqueue.front(); // get one io from queue
      myqueue.pop(); // delete it also from queue.
      count++;
      //cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
      block[ temp_i ].setXY(grid[layer_id], x, y, true);
      y = y + step;
    }//while
    x=nx+1; y=ny+1;//reset
    x = x - step;
    while( (count < pipo_layer) && (x > 0) && (!myqueue.empty()) ) {
      temp_i = myqueue.front(); // get one io from queue
      myqueue.pop(); // delete it also from queue.
      count++;
      //cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
      block[ temp_i ].setXY(grid[layer_id], x, y, true);
      x = x - step;
    }//while
    x=0; y=ny+1;//reset
    y = y - step;
    while( (count < pipo_layer) && (y > 0) && (!myqueue.empty()) ) {
      temp_i = myqueue.front(); // get one io from queue
      myqueue.pop(); // delete it also from queue.
      count++;
      //cout<<" "<<temp_i<<" "<<x<<","<<y<<" ";
      block[ temp_i ].setXY(grid[layer_id], x, y, true);
      y = y - step;
    }//while
  }//while ( !myqueue.empty() )

  // At this moment myqueue should be empty.
  printf("\nDone assigning locations to io-pads on layer= %d.", layer_id); 
}//eof
// ======================================================================= //
// ======================================================================= //
int s_fpga3d::find_new_constraining_location_for_block(int oldx,int oldy,
					   int *newx,int *newy,int layer_id)
{
  // Search by increasing x and/or y (to be under the 2D bounding
  // box in the layer above)
  // Returns 1 if found, 0 otherwise.
  punct p(oldx,oldy);
  punct temp_p(0,0); 
  int xtemp=0,ytemp=0;
  int A[nx+1][ny+1];
  int i=0,j=0;
  for (i=0;i<=nx;++i) 
    for (j=0;j<=ny;++j)
      A[i][j]=0;

  queue<punct> frontwave; 
  frontwave.push( p );
  A[oldx][oldy]=1;

  while( !frontwave.empty() ) {
    temp_p = frontwave.front();
    frontwave.pop(); // delete it also from queue.
    // Look at all legal neighbors of this point on grid. If not empty add
    // to queue.  If empty record location and return.
    xtemp = temp_p.x; 
    ytemp = temp_p.y;
    if( ( (xtemp)>0 ) && ( (ytemp+1)<=ny ) ) // --- top
      if ( A[xtemp][ytemp+1] == 0 )
	if( grid[layer_id][xtemp][ytemp+1] > 0 )
	  {
	    p.x = xtemp;
	    p.y = ytemp+1;
	    frontwave.push( p );
	    A[xtemp][ytemp+1] = 1;
	  }
	else 
	  {
	    *newx = xtemp;
	    *newy = ytemp+1; return 1;
	  }
    if( ( (xtemp+1)<=nx ) && ( (ytemp+1)<=ny ) ) // --- top-right
      if ( A[xtemp+1][ytemp+1] == 0 )
	if( grid[layer_id][xtemp+1][ytemp+1] > 0 )
	  {
	    p.x = xtemp+1;
	    p.y = ytemp+1;
	    frontwave.push( p );
	    A[xtemp+1][ytemp+1] = 1;
	  }
	else 
	  {
	    *newx = xtemp+1;
	    *newy = ytemp+1; return 1;
	  }
    if( ( (xtemp+1)<=nx ) && ( (ytemp)<=ny ) ) // --- right
      if ( A[xtemp+1][ytemp] == 0 )
	if( grid[layer_id][xtemp+1][ytemp] > 0 )
	  {
	    p.x = xtemp+1;
	    p.y = ytemp;
	    frontwave.push( p );
	    A[xtemp+1][ytemp] = 1;
	  }
	else 
	  {
	    *newx = xtemp+1;
	    *newy = ytemp; return 1;
	  }
  }//while
  // If not found legal location return the starting location.
  *newx = oldx;
  *newy = oldy;
  return 0; // Not found empty location ?
}//eof
// ======================================================================= //

// ======================================================================= //

// ======================================================================= //
void s_fpga3d::read_LUT_delay_file(void)
{
  // Reads in the lut delay generated using vpr by POngstorn.
  // Done only once.
  // Delay matrix gotten from VPR tool. x axis (horizontal) is Delta-x.
  // y axis (vertical) is Delta-y. (x,y) position in the matrix gives
  // the delay of a routed net with two terminals whose bounding-box
  // is Delta-x wide Delta-y height.
  // EXAMPLE of delay file gotten from VPR:
  // 2 2
  // 0.000000e+000 2.190484e-009 
  // 2.863942e-009 2.863942e-009
  ifstream locstream("DelayLUT.txt");//local stream
  int no_rows,no_columns;
  int i=0,j=0,k=0;
  printf("\nReading Delay LUT.");
  locstream >> no_rows >> no_columns;
  printf(" DELAY_LUT[%d][%d] ",no_rows,no_columns);
  DELAY_LUT.resize(no_rows);
  for(k=0;k<no_columns;++k)
    {
      DELAY_LUT[k].resize(no_columns);
    }
  for(i=0;i<no_rows;i++)
    for(j=0;j<no_columns;j++)
      locstream >> DELAY_LUT[i][j];
  printf("\nDone reading Delay LUT."); 
}//eof
// ======================================================================= //


// ======================================================================= //
//
// Next, some functions for debugging purposes.
//
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::printf_grid_matrix_occupancy(int layer_id)
{
  // This is for debugging purposes only.  Dumps out
  // grid[layer_ud] as a matrix of numbers showing num
  // of blocks at each x,y.
  printf("\n Occupancy matrix grid:\n");
  int i=0,j=0;
  for (j=(ny+1);j>=0;--j)
    {
      for (i=0;i<=(nx+1);++i)
	{
	  if( (i==0)||(i==(nx+1))||(j==0)||(j==(ny+1)) )
	    {
	      // i.e., Borders where only io pads are placed.
	      if ( grid[layer_id][i][j] > 0 )
		printf(" %d", grid[layer_id][i][j]);
	      else
		printf(" *");
	    }
	  else
	    printf(" %d", grid[layer_id][i][j]);
	}
      printf("\n");
    }
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::printf_grid_stats_just_after_part(int layer_id)
{
  // This is for debugging purposes only.  Dumps out
  // some statistics about occupancy, max congestion
  // (i.e., max num of modules on a clb)
  int i=0,j=0;
  int tot_num_modules=0;
  int tot_num_empty_clbs=0;
  int max_occup=0;
  for (j=ny;j>0;--j)
    {
      for (i=1;i<=nx;++i) { 
	tot_num_modules = tot_num_modules + grid[layer_id][i][j];
	if ( grid[layer_id][i][j] > max_occup )
	  max_occup = grid[layer_id][i][j];
	if ( grid[layer_id][i][j] == 0 )
	  tot_num_empty_clbs++;
	}
    }//for
  printf("After rec quad-part of layer %d:",layer_id);
  printf("\n Total num of placed modules =%d",tot_num_modules);
  printf("\n Num of occupied clbs =%d from a total of %d",
	 (nx*ny - tot_num_empty_clbs), (nx*ny));
  printf("\n Max occupancy of a clb = %d",max_occup);
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::printf_grid_1st_most_critical_path(int layer_id)
{
  // This is for debugging purposes only.  Currently, 
  // layer_id not used.
  int i=0,j=0;
  int blk_id=0,level_id=0;
  int A[nx+2][ny+2];
  for (j=(ny+1);j>=0;--j)
    for (i=0;i<=(nx+1);++i)
      A[i][j]=0;

  // Go visit all nodes along 1st path.
  j=0;//1st path.
  level_id=0;
  for(list<int>::iterator it_list8=kmp->Paths()[j].begin();
      it_list8 != kmp->Paths()[j].end();it_list8++) {
    if ( (*it_list8) != -1 ) //sourceS is not a valid vertex
      {
	blk_id = tt_hg->verts[ (*it_list8) ].blockID;
	//printf(" %d %d,%d ",blk_id,block[blk_id].x,block[blk_id].y);
	level_id++;
	A[block[blk_id].x][block[blk_id].y] = level_id;
      }//if
  }//for

  printf("\n Critical path on grid:\n");
  for (j=(ny+1);j>=0;--j) {
    for (i=0;i<=(nx+1);++i) {
      if (A[i][j] > 0) printf(" %d", A[i][j]);
      else printf(" _");
    }
    printf("\n");
  }
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::print_to_file_blocks_locations(char outFileName[])
{
  // Debugging purposes.
  int i=0;

  char *fname;
  fname=outFileName;
  ofstream mystream(fname, ios::app);
  if(!mystream) { cout << "Abort: Error opening file."; exit(0); }

  mystream<<endl;
  for (i=0;i<num_blocks;i++) 
    {
      mystream<<i<<"  "<<block[i].x<<" "<<block[i].y<<endl;
    }
  mystream.close();
}//eof
// ======================================================================= //
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::print_to_file_criticalities_of_all_nets(char outFileName[])
{
  // Dumps out criticalities of all nets - in their order - one in
  // every other line.  This file is gonna be used as input file
  // into PRGUI garphics interface developed by James in summer 2004.
  int i=0;
  char *fname;
  fname=outFileName;
  ofstream mystream(fname);
  if(!mystream) { cout << "Abort: Error opening fileprint_to_file_criticalities_of_all_nets() ."; exit(0); }

  // Find the maximum among all net weights.
  double max_weight = -1;
  vector<IdWeightPair> ord_weights;
  for (i=0;i<num_nets;i++) {
    if (is_global[i] == false) 
      { 
	if ( net[i].weight > max_weight ) 
	  max_weight = net[i].weight;
	// Should be there because I put in inside
	// Put_net_delay_info_into_timing_graph_02() function...
      }//if
  }//for

  // Devide all by this maximum in order to normilize everything to 1.
  for (i=0;i<num_nets;i++) {
    if (is_global[i] == false) 
      { 
	net[i].weight = net[i].weight / max_weight;
      }//if
  }//for

  // Print to file criticalities of all nets.
  // First line has number of nets which is the number of lines that
  // follow with a criticality value.
  mystream<< num_nets <<endl;
  for (i=0;i<num_nets;i++) {
    if (is_global[i] == false) 
      { 
	mystream<< net[i].weight <<endl;
      }//if
  }//for

  mystream.close();
}//eof
// ======================================================================= //
