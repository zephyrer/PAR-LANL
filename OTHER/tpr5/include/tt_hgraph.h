#ifndef _TT_HGRAPH_
#define _TT_HGRAPH_

// ======================================================================= //
// Cristinel Ababei 10/11/2003
// ======================================================================= //

#include <queue>
#include <vector>
#include <map>

#include "hmetisInterface.h"
#include "tt_kmost.h"

using namespace std;

// ======================================================================= //

// Next two constants used in tt_hgraph.cpp
#define EDGE_WEIGHT_COMPUTATION_SCHEME 0 
#define NORMALIZING_MAX_EDGE_CRITICALITY 12
#define BIAS_AMOUNT_FOR_WEIGHTS_ON_KMOST 24
// FIXED_DELAY means that the vertex in the timing graph is a ipin of
// some block (all edges in tim graph from ipins to opins of same block
// have constant/fixed delay by the num of series of LUTs.  All these nets
// have fanout one). VAR_DELAY says that the net driven by this vertex is 
// a net driven by a opin of some block and fanouts to other blocks. 
enum timing_graph_delay_type {FIXED_DELAY=1, VAR_DELAY=0};

// ======================================================================= //
class bpelement
{ 
// branch_point_element; used to build "bpl"
// ID
// toID
public:
  bpelement(){}
  int ID;
  int toID;
  double Weight;
  friend bool operator<(bpelement a,bpelement b){ return a.Weight<b.Weight; }
  friend bool operator==(bpelement a,bpelement b){
    if(a.Weight==b.Weight) return true;
    else return false; }
};
// ======================================================================= //
class IdWeightPair
{ 
// Used for ordering generic weights
//
// Weight:  weight after which sorting will be done; of a vector of 
//          these IdWeightPair
public:
  IdWeightPair(){}
  int ID;
  double Weight;
  friend bool operator<(IdWeightPair a,IdWeightPair b) 
    { return a.Weight<b.Weight; }
  friend bool operator==(IdWeightPair a,IdWeightPair b){
    if(a.Weight==b.Weight) return true;
    else return false; }
};
// ======================================================================= //
//class Element
// It is now in ipr_types.h.  I moved its definition there to have it
// for fanin and fanout lists creation of blocks
// ======================================================================= //
class Vertex
{
  // Used for building the embedded timing graph.
  // This is a node in the timing graph and basically mirros a subblock
  // of a block.  When the fpga architecture has a single LUT (i.e., 
  // subblock) in each CLB, then each vertex mirrors also a block itself.
  //  
  // type:  CLB, INPAD or OUTPAD.  CLB means that this vertex mirrors a
  //        subblock inside a CLB, which may contain more subblocks,
  //        therefore more "mirror" vertices.
  // delay:  Subbblock (LUT) delay itself
  // x,y,z:  Coordinates on placement
  // ID:  Index (id) of this vertex
  // netID:  ID of net in vector nets that this vertex drives as source.
  //         PO do not drive any net, so netID has no meaning for POs.
  // blockID:  block index inside "block" of the fpga3d class.  The
  //           timing graph has verts all subblocks of "fg3" (the
  //           global fpga variable) and this blockID maintains the
  //           mirroring (i.e., embeds tt_hgraph) with fg3. More verts
  //           can have same blockID because they are subblocks of same
  //           block.
  // netID_fpga3d:  Index (i.e., id) of corresponding net
  //                among net[] of fpga3d object which is driven by this 
  //                vertex.
  // is_on_kmost:  Flag which tells me if this vertex is on any of the 
  //               kmost critical paths.  This flag is used when imposing
  //               constraints during rec-part at layers 1,2,3... "1"
  //               means it is on kmost, "0" it is not.
  // delay_type:  driven net by this vertex is internal to a block, has 
  //              fixed_delay vertex drives a net with fanouts other
  //              blocks, delay_variable given by place and route.
  // block_ptr:  Pointer
  // vmiu:  Delay accumulated up to the out of vertex, including vertex
  //        delay itself.
  // Crit:  Criticality of this vertex is criticality of driven net.
  // tokens:  Keeps track of how many proccessed fanins/outs "so-far" to
  //          know when a gate is ready to be processed durin STA and such.
  // arr,req,slack:  Static Timing Analysis (STA) purposes.
  // max_delay_to_sink:  kmost calculations.
  // ordFin,ordFout:  kmost calculations.
  // 
  enum e_block_types type;
  double delay;
  int x,y,z;
 public:
  Vertex();
  int ID;
  int netID;
  int blockID;
  int netID_fpga3d;
  int is_on_kmost;
  int FF; // If this vertex "mirrors" an opin of a block
  // and this opin is sent thru a FF (latch) I neet this "FF" 
  // flag to know to "split" it into PO and PI for timing-analysis.

  enum timing_graph_delay_type delay_type;
  map<int,Element> fINverts;
  map<int,Element> fOUTverts;
  vector<IdWeightPair> ordFin,ordFout;
  double vmiu;
  double Crit;
  int tokens; 
  double arr,req,slack;
  double max_delay_to_sink;
  // gets:
  int getX() { return x; }
  int getY() { return y; }  
  int getZ() { return z; }
  double Delay() { return delay; }
  enum e_block_types Type() { return type; }
  // sets:
  void setX(int f_x) {x=f_x;}
  void setY(int f_y) {y=f_y;}
  void setZ(int f_z) {z=f_z;}
  void setDelay(double f_delay) {delay=f_delay;};
  void setType(enum e_block_types f_type) {type=f_type;};
  // others:
  void addElementInFanin(int VertexId);
  void addElementInFanout(int VertexId);
};
// ======================================================================= //
class Net
{
  // Used for building the embedded timing graph.  It has different use than
  // s_net class inside ipr_types.h.
  // ID:  Index (id) of this Net; valid within tt_hgraph only.
  // netID_fpga3d:  Index (i.e., id) of corresponding net
  //                among net[] of fpga3d object.  only inter-clb nets
  //                have counterpart in the timing graph.
  //                Those which are intra-clb, do not have couterparts
  //                in the fpga3d objeact.  In this case netID_fpga3d=-1. 
  // delay_type:  same as for the driving vertex of this net.
  //              A net which is internal to a block, has 
  //              fixed_delay.  A net with fanouts other
  //              blocks has variable delay given by place and route.
  // weight_was_biased:  Flag needed during edge-weight update.  Set to one
  //             when the edge-weight is updated first time during the
  //             process of looking at all edges on kmost critical paths.
  // verts:  Pointers to verts[] of tt_hgraph. First element is the source,
  //         and the rest are sinks of this net.
  // weight:  General purpose. It can be the criticality of the driving 
  //          vertex.
  // delay:  delay (FIXED OR VAR)
public:
  Net();
  int ID;
  int netID_fpga3d;
  enum timing_graph_delay_type delay_type;
  int weight_was_biased;
  vector<Vertex*> verts;
  double weight;
  double delay;
};
// ======================================================================= //

// ======================================================================= //
class tt_hgraph
{
  // This class implements the embedded timing graph used for static timing
  // analysis of the circuit "inside" the fpga3d. It has a number or vertices
  // equal to the num of pis + pos + total_num_of_subbblocks_in_all_blocks.
  //
  // NumVerts:  number of cells in tt_hgraph.
  // NumNets:  number of nets in tt_hgraph.
  // K:  Number of critical paths to work with.  Should be set thru
  //     command line argumenst.
  // verts:  map of vertices.
  // nets:  vector of nets.
  // flag:  multi purpose flag -- debugging and experiments.
  // sourceS,sinkT:   kmost source and sink.
  // 
public:
  int NumVerts;
  int NumNets;
  int K;
  map<int,Vertex> verts;
  vector<Net> nets;
  int flag;
  Vertex sourceS, sinkT;
  tt_hgraph();
  tt_hgraph(int f_NumNets);
  
  // Basic functions with bodies in tt_hgraph.cpp
  void addVertex(Vertex t_vertex);
  void addVertex( Vertex t_vertex, int imposed_id );
  void addNet(int id);
  void BuildNets(); // Not really necessary because I can consider as net 
                    // each driving gate together with its FanOut list!
  // STA related functions.
  void Put_net_delay_info_into_timing_graph_02(float **net_delay);
  void Dump_delays_of_all_source_sink_pairs( char outFileName[] );
  double Get_Static_Delay();//Gets max among all POs
  void Compute_Static_DelayForPOs();// STA
  void ComputeArrivalTimesFromPIsToPOs();
  void ComputeArrivalTimesFromPIsToPOs_ProcessVertex(int vertex_id);
  void ComputeArrivalTimesFromPIsToPOs_ProcessVertex( int vertex_id,
	      queue<int> * frontwave );
  void ComputeRequiredTimesFromPOsToPIs();
  void ComputeRequiredTimesFromPOsToPIs_ProcessVertex(int vertex_id,
	      queue<int> * frontwave );

  void BiasSlack();
  void UpDateSlack();
  void SortSlacksAndPrintThemInFileInNonIncreasingOrder(char outFileName[]);
  void Print_tt_hgraph(char outFileName[]);

  // Function used for k-most critical paths stuff.
  void Add_Source_S_Sink_T(void);
  void Build_ordFin_ordFout();
  void Sort_ordFin_ordFout();
  void Compute_max_delay_to_sink_FromPOsToPIs();
  void Compute_max_delay_to_sink_FromPOsToPIs_ProcessVertex(
		      int vertex_id, queue<int> * frontwave );

  void PathEnumeration(char outFileName[]);

  void NormalizeAllPathsDelaysWithRespectTo( double norm );
  double FindDelayOfNewFirstMostCriticalPath();
  double FindNewDelayForOldPath(int index, KmostPaths *f_kmpl);
  void Empty_Paths_Inside_kmp();
  void PathEnumerationDuringRecursion(void);
  void UpdateInfoInside_ordFin_ordFout();
  void DumpKPathsAndVerticesPartitions(char outFileName[]);
  // Functions used during recursive quad-partitioning of layers.
  void Bias_edge_weights_on_kmost_paths(void);
  // GUI functions
  void Dump_KPaths( char outFileName[],int how_many );

};
// ======================================================================= //

#endif




