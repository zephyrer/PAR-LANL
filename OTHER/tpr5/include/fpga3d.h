#ifndef _FPGA3D_
#define _FPGA3D_

// ======================================================================= //
// Cristinel Ababei 09/27/2003
// ======================================================================= //
// ======================================================================= //
// Here the fpga3d super-class is declared. 
// GRAPH NELIST:  we work with a "s_block" class which will form the graph to 
//                be placed - each element clb can have only one output for
//                CLBs with only one LUT or may have more outputs, 
//                depending on the architecture. "s_net" is self-
//                explanatory. 
// PHYSICAL ARCHITECTURE:  We have a "s_clb" class which will form an array 
//                that represents the physical fpga layers. A clb 
//                contains one subblock or more depending on the 
//                architecture. 
// ======================================================================= //
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <vector>

#include "util.h"
#include "ipr_types.h"
#include "hmetisInterface.h"
#include "tt_hgraph.h"


using namespace std;

// ======================================================================= //
// ============ Defines and types local - used in read_arch.c ============ //

#define NUM_REQUIRED 9   // Number of parameters that are always required.
#define NUM_DETAILED 10  // Number needed only if detailed routing used
#define NUM_TIMING 8     // Number needed only if timing analysis used.

#define DETAILED_START NUM_REQUIRED
#define TIMING_START (NUM_REQUIRED + NUM_DETAILED)

// Total Number of different parameters in arch file
#define NUMINP (NUM_REQUIRED + NUM_DETAILED + NUM_TIMING)
// ======================================================================= //



// Used for recursive partitioning.
typedef vector<vector<int> > grid_matrix;

// ======================================================================= //
class s_fpga3d
{
  // Top-level class.  Owns graph of blocks and nets to be placed and mapped
  // to the physical fpga modeled by an array of clbs.
 public:


  // --------------------------------------------------------------------- //
  //                              SECTION 0
  // Added during development
  // --------------------------------------------------------------------- //
  // -vertical_delay_same:  says if all vertical connections have same
  //                        delay during place-level delay estimation.
  // -vertical_uwl:  is the "unit" wire-length of an inter-layer
  //                 via and it is an integer num of the length of a
  //                 wire-segment of one; i.e., dist between 2 clbs.
  // their default values is 1
  int vertical_delay_same;
  int vertical_uwl;

  // final to be dumped data
  s_final_data final_data;
  // VIA_DELAY used for delay-estimations at place-level.
  double VIA_DELAY;
  // num_layers:  number of layers for placement in 3D
  int num_layers;
  // balance[0..num_layers-1]:  stores info about number of blocks in 
  // any layer
  int balance[20];// max number of layers is now 20
  grid_matrix grid[20];
  // After partitioning into layers, store info about no of clb, pi, po
  // in each layer in following members.
  int pi_balance[20];
  int po_balance[20];
  int clb_balance[20];//no of clbs in each layer

  // metis:  Generic interface for HMetis partitioning engine. May add
  //         a little to run-time but it's safer this way
  MetisIntfc * metis;
  // DELAY_LUT:  look up table with delay used for delay estimation
  // during placement.  Will be populated reading from a file once,
  // file created with vpr by Pongstorn.
  vector<vector<double> > DELAY_LUT;
  // K:  Number of critical paths to work with.  Should be set tru
  //     command line argumenst.  It is mirrored in timing graph also.
  int K;

  // Partitioning of block into layers
  void init_hmetisInterface(void);
  void setting_condition_hmetis(void);
  void partitioning_to_layers(void);
  void get_hmetis_results_into_clbs(void);
  void print_hmetis_result(void);
  // Building timing graph for STA
  tt_hgraph * create_timing_tt_hgraph_01( t_subblock_data subblock_data );
  tt_hgraph * create_timing_tt_hgraph_02(
		   t_subblock_data subblock_data,t_timing_inf timing_inf);
  int get_id_top_level_of_subblock_driving_opin (
	       int blk_opin, int block_id,
	       t_subblock_data *subblock_data_ptr );
  int get_id_top_level_of_subblock_driven_by_ipin (
	       int blk_ipin, int block_id,
	       t_subblock_data *subblock_data_ptr );
  int finish_building_fanin_fanouts_of_verts_dueto_this_block_01 (
	       int block_id,
	       t_subblock_data *subblock_data_ptr,
               tt_hgraph *g);
  void rec_finish_building_fanin_fanouts_of_verts_dueto_this_block_02 (
	       int block_id,t_subblock_data *subblock_data_ptr,tt_hgraph *g,
               int subblk_id,int fixed_delay_up_to_now,
	       int sink_opin_id_top_level,t_timing_inf timing_inf );
  // Recursive partitioning of all layers top to bottom.
  // Their bodies is in "tt_place.cpp".
  void timing_kmost_driven_part_based_place(void);
  void printf_grid_matrix_occupancy(int layer_id);
  void printf_grid_1st_most_critical_path(int layer_id);
  void printf_grid_stats_just_after_part(int layer_id);
  void Update_Edge_Weights(void);
  void Import_edge_weights_from_tim_graph_to_fpga3d(void);
  void Assign_delay_to_cut_wires(void);
  void Compute_bounding_boxes_of_nets_fpga3d(void);
  double get_3D_WL(void);
  // cristinel.ababei
  double get_some_stats(void);
  void dump_out_final_data(char *net_file);

  void read_LUT_delay_file(void);
  void constrain_terminals_in_lower_layers(int net_id,int layer_id);
  void assign_locations_to_io_pads_on_all_layers_version01(void);
  void assign_locations_to_io_pads_on_layer(int layer_id);
  void print_to_file_blocks_locations(char outFileName[]);
  void print_to_file_criticalities_of_all_nets(char outFileName[]);
  int find_new_constraining_location_for_block(int oldx,int oldy,
				 int *newx,int *newy,int layer_id);


  // --------------------------------------------------------------------- //
  //                              SECTION I
  // Variables (which were globals in vpr) are now members of class s_fpga3d.
  // Most of them are populated by functions from SECTION II, which are
  // parsing the command line and input files. In this section functions 
  // that implement the actual place & route algos are put here. 
  // --------------------------------------------------------------------- //


  // ================ Netlist to be mapped stuff ================ //
  // num_p_inputs:  Number of INPADs
  // num_p_outputs:  Number of OUTPADs
  // total_num_subblocks:  Total number of subblocks. IN/OUT PAD's are 
  //                       counted as having/being an unique subblock
  //                       which is the block itseself. It is still
  //                       counted because I get in this way the total
  //                       num of vertices in tt_hgraph
  int num_nets, num_blocks;
  int num_p_inputs, num_p_outputs, num_clbs, num_globals;
  int total_num_subblocks;
  vector<s_block> block;
  vector<s_net> net;
  bool *is_global;         // FALSE if a net is normal, TRUE if it is  /
                           // global. Global signals are not routed.   


  // ================ Physical architecture stuff =============== //

  int nx, ny, io_rat, pins_per_clb;

  /* Pinloc[0..3][0..pins_per_clb-1].  For each pin pinloc[0..3][i] is 1 if    *
   * pin[i] exists on that side of the clb. See vpr_types.h for correspondence *
   * between the first index and the clb side.                                 */

  int **pinloc;  

  int *clb_pin_class;  /* clb_pin_class[0..pins_per_clb-1].  Gives the class  *
			* number of each pin on a clb.                        */

  /* TRUE if this is a global clb pin -- an input pin to which the netlist can *
   * connect global signals, but which does not connect into the normal        *
   * routing via muxes etc.  Marking pins like this (only clocks in my work)   *
   * stops them from screwing up the input switch pattern in the rr_graph      *
   * generator and from creating extra switches that the area model would      *
   * count.                                                                    */

  bool *is_global_clb_pin;     // [0..pins_per_clb-1].
  
  s_class *class_inf;   /* class_inf[0..num_class-1].  Provides   *
				* information on all available classes.  */
  
  int num_class;       // Number of different classes.  
  
  int *chan_width_x, *chan_width_y;   // [0..ny] and [0..nx] respectively
  int *chan_width_z; // [0..1]: All zchan have same width = vias_per_chan.
  // Meaning that each channel x can have its own no of tracks (i.e., Width).
  // Depending on the .arch file.
  
  s_clb ***clb;   // Physical block list, which used to be 2D
  //vector<<s_clb> > clb;   // Physical block list
  


  // ================ Structures defining the routing =============== //

  // [0..num_nets-1] of linked list start pointers.  Define the routing.
  
  s_trace **trace_head, **trace_tail;
  


  // ======= Structures defining the FPGA routing architecture ====== //

  int num_rr_nodes;// rr_nodes onto layers
  // cristinel.ababei
  // I add also a member to keep track of all rr_nodes introduced by 
  // vertical vias.
  int num_rr_nodes_vias;
  // This variable tells how many rr_nodes are due to layers only, excluding
  // vias.  For 2D it is the same as num_rr_nodes.  In other words:
  // num_rr_nodes = num_rr_nodes_horizontal + num_rr_nodes_vias
  int num_rr_nodes_horizontal;
  t_rr_node *rr_node; // [0..num_rr_nodes-1]
  
  int num_rr_indexed_data;
  t_rr_indexed_data *rr_indexed_data;         /* [0..num_rr_indexed_data-1] */
  
  /* Gives the rr_node indices of net terminals.    */
  
  int **net_rr_terminals;                  /* [0..num_nets-1][0..num_pins-1]. */
  
  /* Gives information about all the switch types                      *
   * (part of routing architecture, but loaded in read_arch.c          */
  
  s_switch_inf *switch_inf;      /* [0..det_routing_arch.num_switch-1] */
  
  /* Stores the SOURCE and SINK nodes of all CLBs (not valid for pads).     */
  
  int **rr_clb_source;          /* [0..num_blocks-1][0..num_class-1]*/
  


  // ======= Functions embedding "algorithms" for fpga3d ====== //

  s_fpga3d(); // constr





  // --------------------------------------------------------------------- //
  //                              SECTION II
  // Variables and functions that should implement "s_parser" class only.
  // However, because most of these functions populate members of s_fpga3d
  // I eventually put them here too. They should be in a separate class, but
  // in this case I should somehow give acces to them to members of s_fpga3d.
  // In current form there is no separation between parsing and main
  // algos working on data structures populated directly during parsing.
  // This is what happens when you try to port a c code to c++: you have
  // to "partition" the bunch of globals in c to classes - with min
  // communication - in c++.
  // --------------------------------------------------------------------- //


  // ======== Variables used in read_netlist.c ============ //

  // Temporary storage used during parsing.
   int *num_driver, *temp_num_pins;
   struct s_hash **hash_table;
   int temp_block_storage;

  // Used for memory chunking of everything except subblock data.
   int chunk_bytes_avail;
   char *chunk_next_avail_mem;
  
  // Subblock data can be accessed anywhere within this module.  Pointers to 
  // the main subblock data structures are passed back to the rest of the    
  // program through the "subblock_data_ptr" structure passed to read_net.     

   int max_subblocks_per_block;
   int subblock_lut_size;
   t_subblock **subblock_inf;
   int *num_subblocks_per_block;

  // The subblock data is put in its own "chunk" so it can be freed without  
  // hosing the other netlist data.                                          

   int ch_subblock_bytes_avail;
   char *ch_subblock_next_avail_mem;
   s_linked_vptr *ch_subblock_head_ptr;



  // ======== Variables used in read_arch.c ============ //

  int isread[NUMINP];
  char *names[NUMINP];


  // ============ Subroutines in parser.c ============== //




  // Parse input circuit and architecture
  void  get_input (
	    char *net_file, 
	    char *arch_file, 
	    enum pfreq place_freq,
	    int num_regions, 
	    float aspect_ratio, bool user_sized,
	    enum e_route_type route_type, 
	    s_det_routing_arch  *det_routing_arch, 
	    t_segment_inf **segment_inf_ptr,
	    t_segment_inf **via_inf_ptr,
	    t_timing_inf *timing_inf_ptr, 
	    t_subblock_data *subblock_data_ptr,
	    t_chan_width_dist *chan_width_dist_ptr,
	    // Next, added argumenst for the sake of "init_arch".
	    char *place_file );


  // Parse the command line to get the input and output files and options.
  void parse_command (int argc, char *argv[], char *net_file, 
		      char *arch_file, char *place_file, char *route_file, 
		      enum e_operation *operation, float *aspect_ratio,  
		      bool *full_stats, bool *user_sized,
		      bool *verify_binary_search, int *gr_automode, 
		      bool *show_graphics, 
		      s_annealing_sched *annealing_sched,
		      s_placer_opts *placer_opts, 
		      s_router_opts *router_opts, 
		      bool *timing_analysis_enabled, 
		      float *constant_net_delay);

  // Returns the value in argv[iarg+1]
  int read_int_option (int argc, char *argv[], int iarg);
  // Returns the value in argv[iarg+1]
  float read_float_option (int argc, char *argv[], int iarg);
  


  // ======== Subroutines in print_netlist.c =========== //


  void print_netlist (char *foutput, char *net_file, 
		      t_subblock_data subblock_data);
  void print_pinnum (FILE *fp, int pinnum);



  // ======== Subroutines in read_netlist.c ============ //


  void read_net (char *net_file, t_subblock_data *subblock_data_ptr);

  void init_parse(int doall);
  char *get_tok(char *buf, int doall, FILE *fp_net);
  int get_pin_number (char *ptr); 

  void load_subblock_array (int doall, FILE *fp_net, char *temp_buf, 
			    int num_subblocks, int bnum);
 
  void set_subblock_count (int bnum, int num_subblocks); 
  char *parse_subblocks (int doall, FILE *fp_net, char *buf, int bnum); 
  char *add_clb (int doall, FILE *fp_net, char *buf);
  //void add_io (int doall, int block_type, FILE *fp_net, char *buf);
  void add_io (int doall, enum e_block_types block_type, 
	       FILE *fp_net, char *buf) ;


  void parse_name_and_pinlist (int doall, FILE *fp_net, char *buf);

  void add_global (int doall, FILE *fp_net, char *buf);

  int add_net (char *ptr, enum e_pin_type type, int bnum, int blk_pnum, 
	       int doall); 

  void free_parse (void);
 


  // ======== Subroutines in read_arch.c =============== //


  // Most of them related to reading the architecture file.
  // Could you think that they were so many ?
  
  void  read_arch (
       char *arch_file, enum e_route_type route_type,
       struct s_det_routing_arch *det_routing_arch, 
       t_segment_inf **segment_inf_ptr,
       t_segment_inf **via_inf_ptr,
       t_timing_inf *timing_inf_ptr, 
       t_subblock_data *subblock_data_ptr, 
       t_chan_width_dist *chan_width_dist_ptr);

  void countpass (
    FILE *fp_arch, 
    enum e_route_type route_type, 
    t_segment_inf **segment_inf_ptr,
    t_segment_inf **via_inf_ptr,//cristinel
    struct s_det_routing_arch 
    *det_routing_arch_ptr, 
    t_timing_inf *timing_inf_ptr);

  int get_class (FILE *fp_arch, char *buf);

  void get_pin (char *ptr, int pinnum, enum e_pin_type type, 
		FILE *fp_arch, char *buf);

  enum e_Fc_type get_Fc_type (char *ptr, FILE *fp_arch, char *buf);
  enum e_switch_block_type get_switch_block_type (FILE *fp_arch,
						  char *buf); 
 
  void get_T_subblock (FILE *fp_arch, char *buf, t_T_subblock 
		       *T_subblock); 
  void get_segment_inf (FILE *fp_arch, char *buf,
			t_segment_inf *seg_ptr, int num_switch, 
			enum e_route_type route_type);
  // cristinel.ababei 
  void get_via_inf (
	 FILE *fp_arch, char *buf, 
	 t_segment_inf *seg_ptr, 
         int num_switch, enum e_route_type route_type);

  void get_switch_inf (FILE *fp_arch, char *buf, int num_switch, 
		       enum e_route_type route_type); 

  void load_global_segment_and_switch (
	    struct s_det_routing_arch * det_routing_arch,
	    t_segment_inf *segment_inf,
	    t_segment_inf *via_inf,
	    t_timing_inf  *timing_inf);

  void load_extra_switch_types ( s_det_routing_arch *
				det_routing_arch, t_timing_inf *timing_inf); 
  
  void check_keyword (FILE *fp, char *buf, const char *keyword); 
  char *get_middle_token (FILE *fp, char *buf); 

  char *get_last_token (FILE *fp, char *buf); 
  int get_int (char *ptr, int inp_num, FILE *fp_arch, char *buf, 
	       int min_val);
  float get_one_float (char *ptr, int inp_num, float low_lim,
		       float upp_lim, FILE *fp_arch, char *buf); 
  float get_float (char *ptr, int inp_num, float llim, float ulim,
		   FILE *fp_arch, char *buf); 

  void get_chan (char *ptr, t_chan *chan, int inp_num, FILE *fp_arch, 
		 char *buf); 
  void check_arch (
	char *arch_file, enum e_route_type route_type,
        struct s_det_routing_arch det_routing_arch, 
	t_segment_inf *segment_inf,
	t_segment_inf *via_inf,
        t_timing_inf timing_inf, 
	int max_subblocks_per_block, 
        t_chan_width_dist chan_width_dist);

  void print_arch (
       char *arch_file, 
       enum e_route_type route_type,
       struct s_det_routing_arch det_routing_arch, 
       t_segment_inf *segment_inf,    
       t_segment_inf *via_inf, // cristinel.ababei
       t_timing_inf timing_inf, 
       t_subblock_data subblock_data,
       t_chan_width_dist chan_width_dist);

  void init_arch (
	float aspect_ratio,
	bool user_sized,
	enum pfreq place_freq,
	// New arguments.
	char *place_file );
  void read_and_set_nx_ny_from_place_file (
	char *place_file );

  void fill_arch (void);





  // --------------------------------------------------------------------- //
  //                              SECTION III
  // Checks 
  // --------------------------------------------------------------------- //


  // ======== Variables used in check_netlist.c ============ //

  void check_netlist (t_subblock_data *subblock_data_ptr, int *num_driver);

  int check_connections_to_global_clb_pins (int inet);
  
  int check_clb_conn (int iblk, int num_conn);
  
  void check_for_multiple_sink_connections ();
  
  int check_for_duplicate_block_names ();
  
  int get_num_conn (int bnum);
  
  int check_subblocks (int iblk, t_subblock_data *subblock_data_ptr,
		       int *num_uses_of_clb_pin, int *num_uses_of_sblk_opin); 
  
  int check_subblock_pin (int clb_pin, int min_val, int max_val,
			  enum e_pin_type pin_type, int iblk, 
			  int isubblk, t_subblock 
			  *subblock_inf);
  
  int check_internal_subblock_connections (t_subblock_data *subblock_data_ptr,
	 int iblk, int *num_uses_of_sblk_opin); 

  int check_clb_to_subblock_connections (int iblk, t_subblock
         *subblock_inf, int num_subblocks, int *num_uses_of_clb_pin);






  // --------------------------------------------------------------------- //
  //                              SECTION IV
  // Prints, Reads
  // --------------------------------------------------------------------- //
  // these functions used to be in read_place.h:
  void parse_placement_file(char *place_file,char *net_file,char *arch_file);
  void print_place(char *place_file, char *net_file, char *arch_file);
  void read_user_pad_loc(char *pad_loc_file);
  void dump_clbs (void); // For debugging
  // Utils of the above:
  int get_subblock (int i, int j, int k, int bnum); 
  void read_place_header (FILE *fp, char *net_file, char *arch_file, 
				 char *buf); 

  void read_place (
       char *place_file, char *net_file, char *arch_file,
       struct s_placer_opts placer_opts, 
       struct s_router_opts router_opts,
       t_chan_width_dist chan_width_dist, 
       struct s_det_routing_arch det_routing_arch, 
       t_segment_inf *segment_inf, 
       t_timing_inf timing_inf,
       t_subblock_data *subblock_data_ptr);
  void check_place ();



  // --------------------------------------------------------------------- //
  //                              SECTION V
  // Routing graph construction
  // --------------------------------------------------------------------- //
  // =================== Functions, which are in rr_graph.cpp:
  int ***get_rr_node_indices();
  int get_nodes_per_chan();
  void build_rr_graph (
         enum e_route_type route_type, 
	 struct s_det_routing_arch det_routing_arch, 
	 t_segment_inf *segment_inf,
 	 t_segment_inf *via_inf,
         t_timing_inf timing_inf, 
         enum e_base_cost_type base_cost_type);
 
  void free_rr_graph_internals (enum e_route_type route_type, 
	 struct s_det_routing_arch
         det_routing_arch, t_segment_inf *segment_inf, t_timing_inf 
	 timing_inf, enum e_base_cost_type base_cost_type);

  // cristinel.ababei
  void alloc_and_load_rr_graph (
	   int ***rr_node_indices, 
	   int ***clb_opin_to_tracks, struct s_ivec **tracks_to_clb_ipin,
	   int **pads_to_tracks, struct s_ivec *tracks_to_pads, 
	   int Fc_output, int Fc_input, int Fc_pad, 
	   int nodes_per_chan, int vias_per_zchan,
	   enum e_route_type route_type, 
	   struct s_det_routing_arch det_routing_arch, 
	   t_seg_details *seg_details_x, 
	   t_seg_details *seg_details_y, 
	   t_seg_details *seg_details_z); 

  void free_rr_graph (void);
  void alloc_net_rr_terminals (void);
  void load_net_rr_terminals (int ***rr_node_indices, //obs
			      int nodes_per_chan);
  void alloc_and_load_rr_clb_source (int ***rr_node_indices,//obs
          int nodes_per_chan);
  int which_io_block (int iblk); 
  void build_rr_clb (int ***rr_node_indices, int Fc_output, int ***
	 clb_opin_to_tracks, int nodes_per_chan, int i,int j,int k,
         int delayless_switch, t_seg_details *seg_details_x, 
         t_seg_details *seg_details_y);
  void build_rr_pads (int ***rr_node_indices, int Fc_pad, int 
          **pads_to_tracks, int nodes_per_chan, int i,int j,int k, int 
          delayless_switch, t_seg_details *seg_details_x, t_seg_details 
          *seg_details_y);
  // cristinel.ababei
  void build_rr_zchan (
          int ***rr_node_indices, 
	  enum e_route_type route_type, 
	  int i,int j,int k, 
	  int nodes_per_chan, 
	  int vias_per_zchan,
	  enum e_switch_block_type switch_block_type, 
	  t_seg_details *seg_details_x, 
	  t_seg_details *seg_details_y,
	  t_seg_details *seg_details_z,
	  int cost_index_offset);
void build_rr_zchan_version_02 ( //For the case of mult_vert_segm.
          int ***rr_node_indices, 
	  enum e_route_type route_type, 
	  int i,int j,int k, 
	  int nodes_per_chan, 
	  int vias_per_zchan,
	  enum e_switch_block_type switch_block_type, 
	  t_seg_details *seg_details_x, 
	  t_seg_details *seg_details_y,
	  t_seg_details *seg_details_z,
	  int cost_index_offset);
 
  void build_rr_xchan (int ***rr_node_indices, enum e_route_type
          route_type, struct s_ivec **tracks_to_clb_ipin, struct s_ivec *
          tracks_to_pads, int i,int j,int k, 
	  int nodes_per_chan, int vias_per_zchan, enum 
          e_switch_block_type switch_block_type, int wire_to_ipin_switch,
          t_seg_details *seg_details_x, 
          t_seg_details *seg_details_y,
          t_seg_details *seg_details_z,
          int cost_index_offset); 
  void build_rr_ychan (int ***rr_node_indices, enum e_route_type
          route_type, struct s_ivec **tracks_to_clb_ipin, struct s_ivec *
          tracks_to_pads, int i,int j,int k, 
	  int nodes_per_chan, int vias_per_zchan, enum
          e_switch_block_type switch_block_type, int wire_to_ipin_switch,
          t_seg_details *seg_details_x, 
          t_seg_details *seg_details_y,
          t_seg_details *seg_details_z,
          int cost_index_offset);
  void alloc_and_load_edges_and_switches (int inode, int num_edges, 
          t_linked_edge *edge_list_head);
  int ***alloc_and_load_clb_pin_to_tracks (enum e_pin_type pin_type,
      int nodes_per_chan, int Fc, bool perturb_switch_pattern); 
  void load_uniform_switch_pattern (int ***tracks_connected_to_pin, 
         int num_phys_pins, int *pin_num_ordering, int *side_ordering, 
         int nodes_per_chan, int Fc, float step_size);
  void load_perturbed_switch_pattern (int ***tracks_connected_to_pin, 
         int num_phys_pins, int *pin_num_ordering, int *side_ordering, 
         int nodes_per_chan, int Fc, float step_size);
  void check_all_tracks_reach_pins (int ***tracks_connected_to_pin,
         int nodes_per_chan, int Fc, enum e_pin_type ipin_or_opin);
  s_ivec **alloc_and_load_tracks_to_clb_ipin (int nodes_per_chan,
	 int Fc, int ***clb_ipin_to_tracks); 
  int track_side (int clb_side); 
  int **alloc_and_load_pads_to_tracks (int nodes_per_chan, int Fc_pad); 
  s_ivec *alloc_and_load_tracks_to_pads (int **pads_to_tracks, 
         int nodes_per_chan, int Fc_pad); 
  void dump_rr_graph (char *file_name);
  void print_rr_node (FILE *fp, int inode);
  void print_rr_indexed_data (FILE *fp, int index);


  // Global variables shared only by the rr_* modules.
  // Initially, declared in rr_graph2.h.
  // [0..num_rr_nodes-1].  TRUE if a node is already listed in the edges array 
  // that's being constructed.  This allows me to ensure that there are never  
  // duplicate edges (two edges between the same thing).                      
  bool *rr_edge_done;   // [0..num_rr_nodes-1].  Used to keep track  
			// of whether or not a node has been put in 
			// an edge list yet. 
                       
  t_linked_edge *free_edge_list_head; // Start of linked list of free   
				      // edge_list elements (for speed). 


  // =================== Functions, which are in rr_graph2.c:

  // Variables local to this module
  // Two arrays below give the rr_node_index of the channel segment at 
  // (k,i,j,track) for fast index lookup.                             
  int ****chanx_rr_indices; //[0..num_layers-1][1..nx][0..ny][0..nodes_per_chan-1] 
  int ****chany_rr_indices; //[0..num_layers-1][0..nx][1..ny][0..nodes_per_chan-1] 

  // cristinel.ababei
  int ****chanz_rr_indices; //[0..num_layers][0..nx][0..ny][0..nodes_per_chan-1] 
 
 
  // cristinel.ababei
  int alloc_and_load_chanz_rr_indices (
     // t_seg_details *seg_details_z, // NOT used currently. 
     // int zchan_density,            // NOT used currently.
     int vias_per_zchan,
     int start_index);
  int alloc_and_load_chanz_rr_indices_version_02 ( 
                t_seg_details *seg_details_z, // NOT used currently. 
	        // int zchan_density,            // NOT used currently.
	        int vias_per_zchan,
		int start_index );


  int load_chanx_rr_indices (t_seg_details *seg_details_x, int 
           nodes_per_chan, int start_index, int i,int j,int k); 
  int load_chany_rr_indices (t_seg_details *seg_details_y, int 
           nodes_per_chan, int start_index, int i,int j,int k); 
  void get_switch_type (bool is_from_sbox, bool is_to_sbox, 
         short from_node_switch, short to_node_switch, short switch_types[2]); 

  void free_seg_details (t_seg_details *seg_details, int nodes_per_chan);
  void dump_seg_details (t_seg_details *seg_details, int nodes_per_chan, char 
        *fname);
  int get_closest_seg_start (
	       t_seg_details *seg_details, int itrack, int seg_num,
	       int chan_num); 
  int get_closest_seg_start_z (
  	       t_seg_details *seg_details, // Is "seg_details_z" in this case.
	       int itrack, 
	       int current_k); // This should go "along k"

  void get_sanity_check_of_placement(void);

  // cristinel.ababei
  t_seg_details *alloc_and_load_seg_details (
                      int nodes_per_chan,
                      int vias_per_zchan,
		      t_segment_inf *segment_inf, 
		      int num_seg_types, 
		      int num_via_types,
		      int max_len,
		      t_rr_type chan_type ); 

  int get_clb_opin_connections (int ***clb_opin_to_tracks, int ipin, int i, int
        j, int k, int Fc_output, t_seg_details *seg_details_x, t_seg_details
        *seg_details_y, t_linked_edge **edge_list_ptr, int nodes_per_chan, int
        **rr_node_indices); 
  int get_pad_opin_connections (int **pads_to_tracks, int ipad, int i, int j,
        int k, int Fc_pad, t_seg_details *seg_details_x, t_seg_details 
        *seg_details_y, t_linked_edge **edge_list_ptr, int nodes_per_chan, int
        **rr_node_indices);
  bool is_cbox (int seg_num, int chan_num, int itrack, t_seg_details 
	       *seg_details);

  // cristinel.ababei
  int ***alloc_and_load_rr_node_indices (
     int nodes_per_clb, int nodes_per_pad, 
     int nodes_per_chan, int vias_per_zchan,
     int num_via_types, // Num of different vias in arch file.
     t_seg_details *seg_details_x,
     t_seg_details *seg_details_y,
     t_seg_details *seg_details_z); 

  void free_rr_node_indices (int ***rr_node_indices);
  int get_rr_node_index (int i,int j,int k, t_rr_type rr_type, 
			 int ioff, int nodes_per_chan, 
			 int **rr_node_indices);
  int get_seg_end (t_seg_details *seg_details, int itrack, int seg_start, int
       chan_num, int max_end);
  int get_seg_end_z (
		 t_seg_details *seg_details, // Is "seg_details_z" in this case.
		 int itrack, 
		 int seg_start, 
		 int max_end); // ie, num_layers in this case.

  int get_xtrack_to_clb_ipin_edges (int tr_istart, int tr_iend, int tr_j,
       int itrack, int iside, t_linked_edge **edge_list_ptr, struct s_ivec
       **tracks_to_clb_ipin, int nodes_per_chan, int **rr_node_indices,
       t_seg_details *seg_details_x, int wire_to_ipin_switch,int k); 
  int get_ytrack_to_clb_ipin_edges (int tr_jstart, int tr_jend, int tr_i,
       int itrack, int iside, t_linked_edge **edge_list_ptr, struct s_ivec
       **tracks_to_clb_ipin, int nodes_per_chan, int **rr_node_indices,
       t_seg_details *seg_details_y, int wire_to_ipin_switch,int k); 
  int get_xtrack_to_pad_edges (int tr_istart, int tr_iend, int tr_j, int pad_j,
        int itrack, t_linked_edge **edge_list_ptr, struct s_ivec
        *tracks_to_pads, int nodes_per_chan, int **rr_node_indices,
        t_seg_details *seg_details_x, int wire_to_ipin_switch,int k); 
  int get_ytrack_to_pad_edges (int tr_jstart, int tr_jend, int tr_i, int pad_i,
        int itrack, t_linked_edge **edge_list_ptr, struct s_ivec
        *tracks_to_pads, int nodes_per_chan, int **rr_node_indices,
        t_seg_details *seg_details_y, int wire_to_ipin_switch,int k);
  // cristinel.ababei
  int get_xtrack_to_ztracks (
               int from_istart, 
	       int from_iend, int from_j, int from_track, int to_j, 
	       t_linked_edge **edge_list_ptr, 
	       int nodes_per_chan,
	       int vias_per_zchan,
	       int **rr_node_indices, 
	       t_seg_details *seg_details_x, 
	       t_seg_details *seg_details_z, 
	       enum e_switch_block_type switch_block_type, int k);
  int get_ytrack_to_ztracks ( 
                   int from_jstart,
		   int from_jend, int from_i, int from_track, int to_i, 
		   t_linked_edge **edge_list_ptr, 
		   int nodes_per_chan,
		   int vias_per_zchan,
		   int **rr_node_indices, 
		   t_seg_details *seg_details_y, 
		   t_seg_details *seg_details_z, 
		   enum e_switch_block_type switch_block_type,int k);
  // 3D fpga arch only length 1 ver.seg.
  int get_ztrack_to__x_y_z__tracks ( 
               int i, int j, // location
	       int k,// layer saying between which layers this track is
	       int from_track,// track in z-chan [0..vias_per_zchan-1]
	       t_linked_edge **edge_list_ptr, 
	       int nodes_per_chan,
	       int vias_per_zchan,
	       int **rr_node_indices, 
	       t_seg_details *seg_details_x,
 	       t_seg_details *seg_details_y,
	       t_seg_details *seg_details_z, 
	       enum e_switch_block_type switch_block_type ); 
  // 3D fpga arch mixed (multiple interlayer lengths) ver.seg.
  int get_ztrack_to__x_y_z__tracks_version_02 ( 
               int i,int j, // location
	       int from_kstart,// layer where this vert segm starts
	       int from_kend,  // layer where this vert segm ends
	       int from_track, // track in z-chan [0..vias_per_zchan-1]
	       t_linked_edge **edge_list_ptr, 
	       int nodes_per_chan,
	       int vias_per_zchan,
	       int **rr_node_indices, 
	       t_seg_details *seg_details_x,
 	       t_seg_details *seg_details_y,
	       t_seg_details *seg_details_z, 
	       enum e_switch_block_type switch_block_type );


  int get_xtrack_to_ytracks (int from_istart, int from_iend, int from_j, int
       from_track, int to_j, t_linked_edge **edge_list_ptr, int nodes_per_chan,
       int **rr_node_indices, t_seg_details *seg_details_x, t_seg_details
       *seg_details_y, enum e_switch_block_type switch_block_type,int k); 
  int get_ytrack_to_xtracks (int from_jstart, int from_jend, int from_i, int
       from_track, int to_i, t_linked_edge **edge_list_ptr, int nodes_per_chan,
       int **rr_node_indices, t_seg_details *seg_details_x, t_seg_details
       *seg_details_y, enum e_switch_block_type switch_block_type,int k); 
  int get_xtrack_to_xtrack (int from_i, int j, int from_track, int to_i,
       t_linked_edge **edge_list_ptr, int nodes_per_chan, int **rr_node_indices,
       t_seg_details *seg_details_x, enum e_switch_block_type 
       switch_block_type,int k); 
  int get_ytrack_to_ytrack (int i, int from_j, int from_track, int to_j,
       t_linked_edge **edge_list_ptr, int nodes_per_chan, int **rr_node_indices,
       t_seg_details *seg_details_y, enum e_switch_block_type 
       switch_block_type,int k); 
  bool is_sbox (int seg_num, int chan_num, int itrack, t_seg_details
        *seg_details, bool above_or_right);
 
  // =================== Functions, which are in rr_graph_sbox.cpp:
  // [0..3][0..3][0..nodes_per_chan-1].  Structure below is indexed as:       
  // [from_side][to_side][from_track].  That yields an integer vector (ivec)  
  // of the tracks to which from_track connects in the proper to_location.    
  // For simple switch boxes this is overkill, but it will allow complicated  
  // switch boxes with Fs > 3, etc. without trouble.
                          
  s_ivec ***switch_block_conn;  


  int get_simple_switch_block_track (enum e_side from_side, enum e_side
             to_side, int from_track, enum e_switch_block_type
             switch_block_type, int nodes_per_chan); 
  // cristinel.ababei
  enum e_side get_sbox_side_x_to_z (
	     int get_i,  int get_j,  int get_k,  t_rr_type get_type,
	     int comp_i, int comp_j, int comp_k, t_rr_type comp_type );
  enum e_side get_sbox_side_y_to_z (
	     int get_i,  int get_j,  int get_k,  t_rr_type get_type,
	     int comp_i, int comp_j, int comp_k, t_rr_type comp_type ); 
  enum e_side get_sbox_side_z_to__x_y (
	     int get_i,  int get_j,  int get_k,  t_rr_type get_type,
	     int comp_i, int comp_j, int comp_k, t_rr_type comp_type );

  enum e_side get_sbox_side (int get_i, int get_j, t_rr_type get_type,
             int comp_i, int comp_j); 
  // cristinel.ababei
  void alloc_and_load_switch_block_conn (
      int nodes_per_chan,
      int vias_per_zchan,
      enum e_switch_block_type switch_block_type); 

  void free_switch_block_conn (int nodes_per_chan); 
  s_ivec get_switch_box_tracks ( 
      int from_i, int from_j, int from_track, t_rr_type from_type,//eg. CHANX, 
      int to_i, int to_j, t_rr_type to_type,      //eg. CHANY
      enum e_switch_block_type switch_block_type, // [  ?  ] NOT used  
      int nodes_per_chan );                       // [  ?  ] NOT used  
  // cristinel.ababei
  s_ivec get_switch_box_tracks_z_to__x_y (
        int from_i,int from_j,// Loc. of CHANZ.
 	int from_k,           // Identifies zchan
	int from_track,       // Track inside z-chan
	t_rr_type from_type,  // CHANZ
	int to_i,int to_j,    // Which x or y chan; eg. (ABOVE, LEFT x-chan)
 	int to_k,             // "k" of chan x or y
	t_rr_type to_type,    // CHANX or CHANY
	enum e_switch_block_type switch_block_type, // [  ?  ] NOT used  
	int nodes_per_chan,                         // [  ?  ] NOT used  
	int vias_per_zchan );                       // [  ?  ] NOT used  
  s_ivec get_switch_box_tracks_x_to_z (
	int from_i,int from_j,// Loc. on CHANX during mersului.
 	int from_k,           // Layer in which xchan lies
	int from_track,       // Track inside xchan
	t_rr_type from_type,  // CHANX
	int to_i,int to_j,    // Which z-chan location
 	int to_k,             // Identifies zchan
	t_rr_type to_type);   // CHANZ
  s_ivec get_switch_box_tracks_y_to_z (
	int from_i,int from_j,// Loc. on CHANY during mersului.
 	int from_k,           // Layer in which ychan lies
	int from_track,       // Track inside ychan
	t_rr_type from_type,  // CHANY
	int to_i,int to_j,    // Which z-chan location
 	int to_k,             // Identifies zchan
	t_rr_type to_type);   // CHANZ



  // =================== Functions, which are in rr_graph_timing_params.cpp:
  void add_rr_graph_C_from_switches (float C_ipin_cblock);

  // =================== Functions, which are in rr_graph_indexed_data.cpp:
  // Local Subroutines:
  void load_rr_indexed_data_base_costs (int nodes_per_chan, 
       int *** rr_node_indices,//obs
       enum e_base_cost_type base_cost_type, int 
       wire_to_ipin_switch);
  float get_delay_normalization_fac (int nodes_per_chan, 
				     int *** rr_node_indices);//obs
  float get_average_opin_delay (int ***rr_node_indices, //obs
				int nodes_per_chan);
  void load_rr_indexed_data_T_values (int index_start, int
             num_indices_to_load, t_rr_type rr_type, int nodes_per_chan,
	     int ***rr_node_indices, //obs
             t_segment_inf *segment_inf);
  // Subroutines:
  void alloc_and_load_rr_indexed_data (t_segment_inf *segment_inf,
        int num_segment, int ***rr_node_indices, //obs
	int nodes_per_chan,
        int wire_to_ipin_switch, enum e_base_cost_type base_cost_type);
 
  // =================== Functions, which are in rr_graph_check.cpp:
  // Local Subroutines:
  bool rr_node_is_global_clb_ipin (int inode);
  void check_pass_transistors (int from_node); 
  // Subroutines:
  void check_rr_graph (enum e_route_type route_type, int num_switch);
  void check_node (int inode, enum e_route_type route_type); 

  // =================== Functions, which are in rr_graph_util.cpp:
  t_linked_edge *insert_in_edge_list (t_linked_edge *head, int edge, short
      iswitch, t_linked_edge **free_list_head_ptr); 
  void free_edge_list_hard (t_linked_edge **edge_list_head_ptr); 
  void free_linked_edge_soft (t_linked_edge *edge_ptr, t_linked_edge 
      **free_list_head_ptr);
  int seg_index_of_cblock (t_rr_type from_rr_type, int to_node); 
  int seg_index_of_sblock (int from_node, int to_node); 



  // --------------------------------------------------------------------- //
  //                              SECTION VI
  // --------------------------------------------------------------------- //

  // ======================= Routing itself ========================


  // ================ Functions, which are in route_breadth_first.cpp:
  // Local Subroutines:
  bool breadth_first_route_net (int inet, 
				float bend_cost, float bend_cost_between_layers); 
  void breadth_first_expand_trace_segment (s_trace *start_ptr,
        int remaining_connections_to_sink); 
  void breadth_first_expand_neighbours (int inode, float pcost, int inet,
          float bend_cost,float bend_cost_between_layers); 
  void breadth_first_add_source_to_heap (int inet); 
  // Subroutines:
  bool try_breadth_first_route (s_router_opts router_opts, 
				t_ivec **clb_opins_used_locally); 


  // ================ Functions, which are in route_tree_timing.cpp:

  // Array below allows mapping from any rr_node to any rt_node currently in   
  //  the rt_tree.                                                             
  t_rt_node **rr_node_to_rt_node ;   /* [0..num_rr_nodes-1] */
  // Frees lists for fast addition and deletion of nodes and edges. */
  t_rt_node *rt_node_free_list ;
  t_linked_rt_edge *rt_edge_free_list ;

  // Local Subroutines:
  t_rt_node *alloc_rt_node (void); 
  void free_rt_node (t_rt_node *rt_node);
  t_linked_rt_edge *alloc_linked_rt_edge (void);
  void free_linked_rt_edge (t_linked_rt_edge *rt_edge);
  t_rt_node *add_path_to_route_tree (s_heap *hptr, t_rt_node
        **sink_rt_node_ptr);
  void load_new_path_R_upstream (t_rt_node *start_of_new_path_rt_node);
  t_rt_node *update_unbuffered_ancestors_C_downstream (t_rt_node
            *start_of_new_path_rt_node);
  void load_rt_subtree_Tdel (t_rt_node *subtree_rt_root, float Tarrival); 
  // Subroutines:
  void alloc_route_tree_timing_structs (void);
  void free_route_tree_timing_structs (void);
  t_rt_node *init_route_tree_to_source (int inet);
  void free_route_tree (t_rt_node *rt_node);
  t_rt_node *update_route_tree (s_heap *hptr); 
  void update_net_delays_from_route_tree (float *net_delay, t_rt_node 
					  **rt_node_of_sink, int inet);




  // ================ Functions, which are in route_common.cpp.

  // ================ Variables shared by all route modules.
  t_rr_node_route_inf *rr_node_route_inf;       // [0..num_rr_nodes-1] 
  s_bb *route_bb; // [0..num_nets-1]. Limits area in which each  
                         // net must be routed.    
                     
  // ================ Static variables local to route_common.cpp
  s_heap **heap;  /* Indexed from [1..heap_size] */
  int heap_size;   /* Number of slots in the heap array */
  int heap_tail;   /* Index of first unused slot in the heap array */
  // For managing my own list of currently free heap data structures.     */
  s_heap *heap_free_head; 
  // For managing my own list of currently free trace data structures.    */
  s_trace *trace_free_head;
  s_linked_f_pointer *rr_modified_head;
  s_linked_f_pointer *linked_f_pointer_free_head;

  // Local Subroutines:
  void free_trace_data (s_trace *tptr);
  void load_route_bb (int bb_factor); 
  s_trace *alloc_trace_data (void); 
  void add_to_heap (s_heap *hptr);
  s_heap *alloc_heap_data (void);
  s_linked_f_pointer *alloc_linked_f_pointer (void);
  t_ivec **alloc_and_load_clb_opins_used_locally (t_subblock_data 
						  subblock_data);
  void adjust_one_rr_occ_and_pcost (int inode, int add_or_sub, float
				    pres_fac);
  // Subroutines:
  void pathfinder_update_one_cost (s_trace *route_segment_start,
				   int add_or_sub, float pres_fac);
  void pathfinder_update_cost (float pres_fac, float acc_fac);
  s_trace *update_traceback (s_heap *hptr, int inet); 
  void reset_path_costs (void); 
  float get_rr_cong_cost (int inode); 
  void mark_ends (int inet); 
  void node_to_heap (int inode, float cost, int prev_node, int prev_edge,
		     float backward_path_cost, float R_upstream); 
  void free_traceback (int inet); 
  void add_to_mod_list (float *fptr); 
  s_heap *get_heap_head (void); 
  void empty_heap (void); 
  void free_heap_data (s_heap *hptr); 
  void invalidate_heap_entries (int sink_node, int ipin_node);
  void init_route_structs (int bb_factor);
  void free_rr_node_route_structs (void);
  void alloc_and_load_rr_node_route_structs (void);
  void free_trace_structs(void);
  void reserve_locally_used_opins (float pres_fac, bool rip_up_local_opins,
				   t_ivec **clb_opins_used_locally);

  bool try_route (int width_fac, int vertical_width_fac, 
		  struct s_router_opts router_opts, 
		  struct s_det_routing_arch det_routing_arch, 
		  t_segment_inf *segment_inf,
		  t_segment_inf *via_inf,
		  t_timing_inf timing_inf, 
		  float **net_slack, 
		  float **net_delay,
		  t_chan_width_dist chan_width_dist, 
		  t_ivec **clb_opins_used_locally);

  bool feasible_routing (void);
  t_ivec **alloc_route_structs (t_subblock_data subblock_data);
  void free_route_structs (t_ivec **clb_opins_used_locally);
  s_trace **alloc_saved_routing (t_ivec **clb_opins_used_locally,
         t_ivec ***saved_clb_opins_used_locally_ptr);
  void free_saved_routing (s_trace **best_routing, t_ivec
        **saved_clb_opins_used_locally);
  void save_routing (s_trace **best_routing, t_ivec
        **clb_opins_used_locally, t_ivec **saved_clb_opins_used_locally);
  void restore_routing (s_trace **best_routing, t_ivec
       **clb_opins_used_locally, t_ivec **saved_clb_opins_used_locally);
  void get_serial_num (void);
  void print_route (char *name);



  // --------------------------------------------------------------------- //
  //                              SECTION VII
  // --------------------------------------------------------------------- //

  // ================ Functions, which are in route_timing.cpp.
  // Local Subroutines:
  int get_max_pins_per_net (void);
  void add_route_tree_to_heap (t_rt_node *rt_node, int target_node,
       float target_criticality, float astar_fac); 
  void timing_driven_expand_neighbours (s_heap *current, int inet,
          float bend_cost, float criticality_fac, int target_node, float
          astar_fac); 
  float get_timing_driven_expected_cost (int inode, int target_node,
              float criticality_fac, float R_upstream); 
  int get_expected_segs_to_target (int inode, int target_node, int *
                num_segs_ortho_dir_ptr);
  void update_rr_base_costs (int inet, float largest_criticality); 
  void timing_driven_check_net_delays (float **net_delay); 
  // Subroutines:
  bool try_timing_driven_route ( s_router_opts router_opts, float
       **net_slack, float **net_delay, t_ivec **clb_opins_used_locally);
  bool timing_driven_route_net (int inet, float pres_fac, float
       max_criticality, float criticality_exp, float astar_fac, float
       bend_cost, float *net_slack, float *pin_criticality, int *sink_order,
       t_rt_node **rt_node_of_sink, float T_crit, float *net_delay);
  void alloc_timing_driven_route_structs (float **pin_criticality_ptr,
            int **sink_order_ptr, t_rt_node ***rt_node_of_sink_ptr); 
  void free_timing_driven_route_structs (float *pin_criticality, int
            *sink_order, t_rt_node **rt_node_of_sink);

  // ================ Functions, which are in route_timing.cpp.
  // Local Subroutines:
  float comp_width (t_chan *chan, float x, float separation);
  // Subroutines:
  void place_and_route(enum e_operation operation, s_placer_opts
   placer_opts, char *place_file, char *net_file, char *arch_file,
   char *route_file, bool full_stats, bool verify_binary_search,
   s_annealing_sched annealing_sched, s_router_opts router_opts,
   s_det_routing_arch det_routing_arch, 
   t_segment_inf *segment_inf, 
   t_segment_inf *via_inf,
   t_timing_inf timing_inf, t_subblock_data
   *subblock_data_ptr, t_chan_width_dist chan_width_dist);
  void calculate_VIA_DELAY(t_segment_inf *via_inf);

  void init_chan (int cfactor, int vertical_width_fac,//3D
		  t_chan_width_dist chan_width_dist);
  int binary_search_route (s_placer_opts
      placer_opts, char *place_file, char *net_file, char *arch_file,
      char *route_file, bool full_stats, bool verify_binary_search,
      s_annealing_sched annealing_sched, s_router_opts
      router_opts, s_det_routing_arch det_routing_arch,
      t_segment_inf *segment_inf,
      t_segment_inf *via_inf,
      t_timing_inf timing_inf, 
      t_subblock_data *subblock_data_ptr,
      t_chan_width_dist chan_width_dist,
      int vertical_width_fac); 


  // ================ Functions, which are in heapsort.cpp.
  // Local Subroutines:
  void add_to_sort_heap (int *heap, float *sort_values, int index, 
       int heap_tail);
  int get_top_of_heap_index (int *heap, float *sort_values, int
          heap_tail); 
  // Subroutines:
  void heapsort (int *sort_index, float *sort_values, int nelem);


  // ================ Functions, which are in path_delay.cpp.

  // Variables for "chunking" the tedge memory.  If the head pointer is NULL, 
  // no timing graph exists now.                                              
  s_linked_vptr *tedge_ch_list_head;
  int tedge_ch_bytes_avail;
  char *tedge_ch_next_avail;

  // Local Subroutines:
  int alloc_and_load_pin_mappings (int ***block_pin_to_tnode_ptr,
            int ****sblk_pin_to_tnode_ptr, t_subblock_data subblock_data,
            int **num_uses_of_sblk_opin); 
  void free_pin_mappings (int **block_pin_to_tnode, int
          ***sblk_pin_to_tnode, int *num_subblocks_per_block);
  void alloc_and_load_fanout_counts (int ***num_uses_of_clb_ipin_ptr,
          int ***num_uses_of_sblk_opin_ptr, t_subblock_data subblock_data); 
  void free_fanout_counts (int **num_uses_of_clb_ipin, int
             **num_uses_of_sblk_opin); 
  float **alloc_net_slack (void); 
  void compute_net_slacks (float **net_slack);
  void alloc_and_load_tnodes_and_net_mapping (int **num_uses_of_clb_ipin,
          int **num_uses_of_sblk_opin, int **block_pin_to_tnode, int ***
          sblk_pin_to_tnode, t_subblock_data subblock_data, t_timing_inf
          timing_inf); 
  void build_clb_tnodes (int iblk, int *n_uses_of_clb_ipin, int
        **block_pin_to_tnode, int **sub_pin_to_tnode, int subblock_lut_size,
        int num_subs, t_subblock *sub_inf, float T_clb_ipin_to_sblk_ipin,
        int *next_clb_ipin_edge); 
  void build_subblock_tnodes (int *n_uses_of_sblk_opin,
          int *blk_pin_to_tnode, int **sub_pin_to_tnode,
          int subblock_lut_size, int num_subs, t_subblock *sub_inf,
          float T_sblk_opin_to_sblk_ipin, float T_sblk_opin_to_clb_opin,
          t_T_subblock *T_subblock, int *next_sblk_opin_edge, int iblk); 
  void build_ipad_tnodes (int iblk, int **block_pin_to_tnode, float
            T_ipad, int *num_subblocks_per_block, t_subblock
            **subblock_inf);
  bool is_global_clock (int iblk, int *num_subblocks_per_block,
     t_subblock **subblock_inf);
  void build_opad_tnodes (int *blk_pin_to_tnode, float T_opad, int iblk);
  void build_block_output_tnode (int inode, int iblk, int ipin,
            int **block_pin_to_tnode); 
  // Subroutines:
  float **alloc_and_load_timing_graph (t_timing_inf timing_inf, t_subblock_data
       subblock_data); 
  t_linked_int *allocate_and_load_critical_path (void);
  void load_timing_graph_net_delays (float **net_delay);
  float load_net_slack (float **net_slack, float target_cycle_time); 
  void free_timing_graph (float **net_slack);
  void free_subblock_data (t_subblock_data *subblock_data_ptr);
  void print_timing_graph (char *fname);
  void print_net_slack (char *fname, float **net_slack); 
  void print_critical_path (char *fname);
  void get_tnode_block_and_output_net (int inode, int *iblk_ptr, int *inet_ptr);
  void do_constant_net_delay_timing_analysis (t_timing_inf timing_inf,
        t_subblock_data subblock_data, float constant_net_delay_value);


  // ================ Functions, which are in path_delay.cpp.

  // Variable shared only amongst path_delay modules.
  t_tnode *tnode;                    /* [0..num_tnodes - 1] */
  t_tnode_descript *tnode_descript;  /* [0..num_tnodes - 1] */
  int num_tnodes;    /* Number of nodes in the timing graph */
  // [0..num_nets - 1].  Gives the index of the tnode that drives each net. */
  int *net_to_driver_tnode;  
  // [0..num__tnode_levels - 1].  Count and list of tnodes at each level of    *
  // the timing graph, to make breadth-first searches easier.                  */
  s_ivec *tnodes_at_level;
  int num_tnode_levels;     /* Number of levels in the timing graph. */

  // Local Subroutines:
  int *alloc_and_load_tnode_fanin_and_check_edges (int *num_sinks_ptr); 
  // Subroutines:
  int alloc_and_load_timing_graph_levels (void); 
  void check_timing_graph (int num_const_gen, int num_ff, int num_sinks); 
  float print_critical_path_node (FILE *fp, t_linked_int *critical_path_node);


  // ================ Functions, which are in net_delay.cpp.
  // Local Subroutines:
  t_rc_node *alloc_and_load_rc_tree (int inet, t_rc_node
       **rc_node_free_list_ptr, t_linked_rc_edge **rc_edge_free_list_ptr,
       t_linked_rc_ptr *rr_node_to_rc_node);
  void add_to_rc_tree (t_rc_node *parent_rc, t_rc_node *child_rc, short
         iswitch, int inode, t_linked_rc_edge **rc_edge_free_list_ptr);
  t_rc_node *alloc_rc_node (t_rc_node **rc_node_free_list_ptr);
  void free_rc_node (t_rc_node *rc_node, t_rc_node
           **rc_node_free_list_ptr);
  t_linked_rc_edge *alloc_linked_rc_edge (t_linked_rc_edge
             **rc_edge_free_list_ptr);
  void free_linked_rc_edge (t_linked_rc_edge *rc_edge, t_linked_rc_edge
             **rc_edge_free_list_ptr);
  float load_rc_tree_C (t_rc_node *rc_node);
  void load_rc_tree_T (t_rc_node *rc_node, float T_arrival);
  void load_one_net_delay (float **net_delay, int inet, t_linked_rc_ptr
              *rr_node_to_rc_node);
  void load_one_constant_net_delay (float **net_delay, int inet, float 
            delay_value);
  void free_rc_tree (t_rc_node *rc_root, t_rc_node
           **rc_node_free_list_ptr, t_linked_rc_edge **rc_edge_free_list_ptr);
  void reset_rr_node_to_rc_node (t_linked_rc_ptr *rr_node_to_rc_node, int
				 inet);
  void free_rc_node_free_list (t_rc_node *rc_node_free_list);
  void free_rc_edge_free_list (t_linked_rc_edge *rc_edge_free_list); 

  // Subroutines:
  float **alloc_net_delay (s_linked_vptr **chunk_list_head_ptr); 
  void free_net_delay (float **net_delay, s_linked_vptr 
            **chunk_list_head_ptr); 
  void load_net_delay_from_routing (float **net_delay);
  void load_constant_net_delay (float **net_delay, float delay_value); 
  void print_net_delay (float **net_delay, char *fname);

  // ================ Functions, which are in check_route.cpp.
  // Local Subroutines:
  void check_node_and_range (int inode, enum e_route_type route_type);
  void check_source (int inode, int inet);
  void check_sink (int inode, int inet, bool *pin_done); 
  void check_switch (s_trace *tptr, int num_switch); 
  bool check_adjacent (int from_node, int to_node);
  int pin_and_chan_adjacent (int pin_node, int chan_node);
  int chanx_chany_adjacent (int chanx_node, int chany_node);

  // cristinel.ababei
  int chanx_chanz_adjacent (int chanx_node, int chanz_node); 
  int chany_chanz_adjacent (int chany_node, int chanz_node);
  int chanz_chanz_adjacent (int chanz1_node,int chanz2_node);

  void reset_flags (int inet, bool *connected_to_route);
  void recompute_occupancy_from_scratch (t_ivec **clb_opins_used_locally);
  void check_locally_used_clb_opins (t_ivec **clb_opins_used_locally,
				     enum e_route_type route_type);
  // Subroutines:
  void check_route (enum e_route_type route_type, int num_switch,
		    t_ivec **clb_opins_used_locally);



  // ================ Functions, which are in stats.cpp.
  // Local Subroutines:
  void load_channel_occupancies (
	   int ***chanx_occ, int ***chany_occ, int ***chanz_occ); 
  void get_num_bends_and_length (int inet, int *bends, int *length, 
				 int *segments); 
  void get_length_and_bends_stats (void);
  void get_channel_occupancy_stats (void);
  // Subroutines:
  void routing_stats (bool full_stats, enum e_route_type route_type, 
         int num_switch, t_segment_inf *segment_inf, int num_segment, 
         float R_minW_nmos, float R_minW_pmos, bool timing_analysis_enabled,
         float **net_slack, float **net_delay);
  void print_wirelen_prob_dist (void); 
  void print_lambda (void);

  // ================ Functions, which are in rr_graph_area.cpp.
  // Local Subroutines:
  float get_cblock_trans (int *num_inputs_to_cblock, int
           max_inputs_to_cblock, float trans_cblock_to_lblock_buf,
           float trans_sram_bit); 
  float *alloc_and_load_unsharable_switch_trans (int num_switch,
         float trans_sram_bit, float R_minW_nmos); 
  float *alloc_and_load_sharable_switch_trans (int num_switch,
         float trans_sram_bit, float R_minW_nmos, float R_minW_pmos);
  float trans_per_buf (float Rbuf, float R_minW_nmos, float R_minW_pmos); 
  float trans_per_mux (int num_inputs, float trans_sram_bit); 
  float trans_per_R (float Rtrans, float R_minW_trans); 
  // Subroutines:
  void count_routing_transistors (int num_switch, float R_minW_nmos,
            float R_minW_pmos);


  // ================ Functions, which are in rr_graph_area.cpp.
  // Local Subroutines:

  // Subroutines:
  void get_segment_usage_stats (int num_segment, t_segment_inf *segment_inf); 




  // --------------------------------------------------------------------- //
  //                              SECTION VIII
  // --------------------------------------------------------------------- //
  // This is related to the generation of lookup tables for delay
  // estimation.


  //the delta arrays are used to contain the best case routing delay 
  //between different locations on the FPGA. 
  /*---
  float **delta_inpad_to_clb;
  float **delta_clb_to_clb;
  float **delta_clb_to_clb_2;// For cases when sink is above source.
  float **delta_clb_to_outpad;
  float **delta_inpad_to_outpad;
  ---*/


  // Other Global Arrays 
  // I could have allocated these as local variables, and passed them all 
  // around, but was too lazy, since this is a small file, it should not  
  // be a big problem 
  float **net_delay;
  float **net_slack;
  float *pin_criticality;
  int *sink_order;
  t_rt_node **rt_node_of_sink;

  t_ivec **clb_opins_used_locally;

  FILE *lookup_dump; //if debugging mode is on, print out to 
		     //the file defined in DUMPFILE 


  // ================ Functions, which are in timing_place_lookup.cpp.
  // Subroutines:
  void compute_delay_lookup_tables(s_router_opts router_opts, 
			   s_det_routing_arch det_routing_arch, 
			   t_segment_inf *segment_inf,
			   t_segment_inf *via_inf,  
			   t_timing_inf timing_inf, 
			   t_chan_width_dist chan_width_dist, 
                           t_subblock_data subblock_data); 
  void free_place_lookup_structs(void);
  // Local Subroutines:  

void alloc_net(void);

void alloc_block(void);

void alloc_and_assign_internal_structures(
		     vector<s_net> *original_net, 
		     vector<s_block> *original_block, 
		     int *original_num_nets, 
		     int *original_num_blocks);
void free_and_reset_internal_structures(
			   vector<s_net> original_net, 
			   vector<s_block> original_block, 
			   int original_num_nets, 
			   int original_num_blocks);

void setup_chan_width (s_router_opts router_opts,
		       t_chan_width_dist chan_width_dist);

void alloc_routing_structs(
		   s_router_opts router_opts, 
		   s_det_routing_arch det_routing_arch, 
		   t_segment_inf *segment_inf, 
		   t_segment_inf *via_inf,
		   t_timing_inf timing_inf, 
		   t_subblock_data subblock_data);
void free_routing_structs (s_router_opts router_opts, 
			   s_det_routing_arch det_routing_arch, 
			   t_segment_inf *segment_inf, 
			   t_timing_inf timing_inf
			   );
void assign_locations (enum e_block_types source_type, 
			      int source_x_loc, int source_y_loc, 
			      enum e_block_types sink_type,
			      int sink_x_loc, int sink_y_loc);
float assign_blocks_and_route_net (enum e_block_types source_type, 
				   int source_x_loc, int source_y_loc, 
				   enum e_block_types sink_type,
				   int sink_x_loc, int sink_y_loc,
				   s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf);

void alloc_delta_arrays(void);

void free_delta_arrays(void);

void generic_compute_matrix(float ***matrix_ptr, 
				   enum e_block_types source_type, 
				   enum e_block_types sink_type, int source_x, 
				   int source_y, int start_x,  
				   int end_x, int start_y, int end_y, 
				   s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf);
void compute_delta_clb_to_clb (s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf, int longest_length);
void compute_delta_clb_to_clb_2 (
		       s_router_opts router_opts,
		       s_det_routing_arch det_routing_arch, 
		       t_segment_inf *segment_inf, 
		       t_timing_inf timing_inf, int longest_length);
void compute_delta_inpad_to_clb (s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf);
void compute_delta_clb_to_outpad(s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf);
void compute_delta_inpad_to_outpad(s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf);
void compute_delta_arrays (s_router_opts router_opts,
				   s_det_routing_arch det_routing_arch, 
				   t_segment_inf *segment_inf, 
				   t_timing_inf timing_inf, int longest_length);
int get_first_pin(enum e_pin_type pintype);
int get_longest_segment_length(s_det_routing_arch det_routing_arch,
				      t_segment_inf *segment_inf);
void init_occ(void);
 void print_array(float **array_to_print, int x1, int x2, int y1, int y2);
 void print_array_2(float **array_to_print, int x1, int x2, int y1, int y2);






};//eoc
// ======================================================================= //
#endif
