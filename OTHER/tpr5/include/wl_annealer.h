#ifndef _WL_ANNEALER_
#define _WL_ANNEALER_
// ======================================================================= //
// Cristinel Ababei 11/14/2003
// ======================================================================= //
#include <vector>

#include "fpga3d.h"

using namespace std;

// ======================================================================= //
// This is my own annealer, which has as primary objective wl
// improvement.  I developed/adapted it from some code from the Internet
// found on some class's web-page at cmu.

class wl_annealer
{
  s_fpga3d *fg;
  int layer_id;

 public:
  wl_annealer(s_fpga3d *g,int layer_id);

  int tX,tY;
  int temp_Ux,temp_Uy;
  int temp_Vx,temp_Vy;
  vector<int> id2block;
  int magic;
  double range,rstep;
  s_block *U,*V,*Z;
  bool fix;

  // Utils.
  double cost_move_one_cell(s_block * b);
  bool anneal_move_one_cell(double temp);
  void run_wl_anneal_move_one_cell(int seed,double cool,
				   int moves,double current);
};

// ======================================================================= //
#endif
