#ifndef _PARSER_
#define _PARSER_

#include <ctype.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


// ======================================================================= //
/*
class s_parser
{

  // Currently not supported and not used. Should contain everything related
  // to parsing input files. All this I moved inside s_fpga3d because too
  // many functions directly work on members of s_fpga3d and access by making
  // two friend classes would have been virtually the same thing as moving
  // all in the same class.

public:
  s_parser();
  ~s_parser();

};//eoc
*/
// ======================================================================= //
#endif
