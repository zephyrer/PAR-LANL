#ifndef _PART_REC_
#define _PART_REC_
// ======================================================================= //
// Cristinel Ababei 10/21/2003
//
// Here are classes used for implementation of recursive partitioning
// of layers.  
//
// ======================================================================= //

#include <vector>

#include "fpga3d.h"

using namespace std;

// ======================================================================= //

class punct {
 public:
  punct(int x,int y):x(x),y(y){};
  int x;int y;
};
// ======================================================================= //

class Partition
{
  // metis:  hmetis interface.
  // quad_section_level: quad_section_level=0 for Partition of top-level 
  //                     hgraph.  =1 for all four Partitions resulted from 
  //                     first quad-section.  =2 for all 16 Partitions.  Etc.
  // 
 public:
  Partition(int blX,int blY,int trX,int trY);
  Partition();

  int blX,blY,trX,trY;

  MetisIntfc * metis;
  vector<s_block*> blocks;
  int quad_section_level;
  
  // Utilities.
  void init_hmetisInterface_Partition(s_fpga3d *fg,int layer_id);
  void init_hmetisInterface_Partition_with_terminal_propagation(
				       s_fpga3d *fg,int layer_id);
  double quad_partitioning(void);
  double cost_local(s_fpga3d *fg);
  void assignXY(vector<vector<int> > &grid,
		int option);
  void setXY(vector<vector<int> > &grid,
	     int blX,int blY,int trX,int trY,int layer_id);

  // Others.
  int midX() { return (blX+trX)/2; }
  int midY() { return (blY+trY)/2; }
  void impose_loc_constr_upon_lower_layers(s_fpga3d *fg,int layer_id);
  void remove_overlaps_in_layer(s_fpga3d *fg,int layer_id);
  int find_new_location_for_block(s_fpga3d *fg,int id,
				  int *newx,int *newy,int layer_id);

};
// ======================================================================= //

class Part_Rec
{
  // fg:  Top-level s_fpga3d object.  Transfered here for
  //      easy access.
  // layer_id:  Layer number for which rec partitioning is done.
  // 

 public:
  s_fpga3d *fg;
  int layer_id;

  Part_Rec(s_fpga3d *g, int layer_id);
  void run_rec(void);
};

// ======================================================================= //
#endif
