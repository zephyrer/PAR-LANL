
#include <time.h>
#include <iostream>
#include <stdio.h>

#include "util.h"
#include "ipr_types.h"
#include "fpga3d.h"


using namespace std;

extern tt_hgraph * tt_hg;


// ======================================================================= //


// =================== Subroutine definitions ============================ //


// ======================================================================= //
void s_fpga3d::place_and_route(enum e_operation operation, 
   s_placer_opts placer_opts, char *place_file, char *net_file, 
   char *arch_file, char *route_file, bool full_stats,
   bool verify_binary_search, 
   s_annealing_sched   annealing_sched, 
   s_router_opts       router_opts, 
   s_det_routing_arch  det_routing_arch, 
   t_segment_inf      *segment_inf,
   t_segment_inf      *via_inf, 
   t_timing_inf        timing_inf, 
   t_subblock_data    *subblock_data_ptr, 
   t_chan_width_dist   chan_width_dist) 
{
// This routine controls the overall placement and routing of a circuit.
  clock_t start_time_route, finish_time_route;
  double duration_route=0;
  double detailed_route_delay=0;
  double place_3D_WL=0,place_Static_Delay=0,place_vert_fraction=0;

  char msg[BUFSIZE];
  int width_fac=0;
  bool success=0;
  float **net_delay=0, **net_slack=0;
  s_linked_vptr *net_delay_chunk_list_head=0;
  t_ivec **clb_opins_used_locally;  // [0..num_blocks-1][0..num_class-1]
  
  int vertical_width_fac=0;    
  t_chan chan_z_dist = chan_width_dist.chan_z_dist;


  // -----------------------------------------------------------------------
  // --- A ---
  // -----------------------------------------------------------------------
  // Place: (timing-driven) partitioning-based placement. 
  // Place by recursive quad-partitioning.

  // First build delay-look-up tables - For delay estimation purposes
  // at place-level.
  calculate_VIA_DELAY(via_inf);

  /*---
  // cristinel.ababei  Next routine call does not compute correctly
  // the delay lookup table - it has a bug or something - so I 
  // generated all lookup tables in vpr and stored them hard.
  // see them in fpga3d.cpp
  compute_delay_lookup_tables(
	   router_opts, det_routing_arch, 
	   segment_inf, via_inf, 
	   timing_inf, chan_width_dist, *subblock_data_ptr);
   ---*/

  if (placer_opts.place_freq == PLACE_NEVER) {
     read_place (place_file, net_file, arch_file,
		 placer_opts, router_opts,
		 chan_width_dist, det_routing_arch, 
		 segment_inf, 
		 timing_inf,
		 subblock_data_ptr);
    // Dump out some results stuff.
    Update_Edge_Weights();
    //printf_grid_1st_most_critical_path(0);
    tt_hg->Compute_Static_DelayForPOs();
    cout<<"\n ------------------------------------------------------";
    cout<<"\n Stats of input placement:";
    cout<<"\n ------------------------------------------------------";
    place_3D_WL = get_3D_WL();
    place_Static_Delay = tt_hg->Get_Static_Delay();
    final_data.wl_place = place_3D_WL;
    final_data.delay_place = place_Static_Delay;
    final_data.run_time_place = 0;

    cout<<"\n 3D WL="<<place_3D_WL
	<<"   Place-run-time="<<0;
    cout<<"\n Delay estimated after placement="<<place_Static_Delay<<endl;
    place_vert_fraction = get_some_stats();
    final_data.fraction_z = place_vert_fraction;
    //tt_hg->Print_tt_hgraph( "dbg_tt_graph.echo" );
  }//if
  
  else if (placer_opts.place_freq == PLACE_ONCE) {
    timing_kmost_driven_part_based_place();// Part-based place.
    print_place(place_file, net_file, arch_file);
  }
 
  else if (placer_opts.place_freq == PLACE_ALWAYS &&
	   router_opts.fixed_channel_width != NO_FIXED_CHANNEL_WIDTH) {
    // oBs:  Currently this branch never entered - cristia.
    // For delay estimation purposes.
    placer_opts.place_chan_width = router_opts.fixed_channel_width;
    timing_kmost_driven_part_based_place();// Part-based place.
    print_place(place_file, net_file, arch_file);
  }
 

  fflush (stdout);
  if (operation == PLACE_ONLY)
    return;

  // -----------------------------------------------------------------------
  // --- B ---
  // -----------------------------------------------------------------------
  // Route: using 3D routing tool.
  // Binary search over channel width required?
  // --- a ---
  vertical_width_fac = 1*(int)chan_z_dist.peak; // Initial.

  if (router_opts.fixed_channel_width == NO_FIXED_CHANNEL_WIDTH) {
    width_fac = binary_search_route (placer_opts, place_file,
	   net_file, arch_file, route_file, full_stats, verify_binary_search,
           annealing_sched, router_opts, det_routing_arch, 
	   segment_inf, via_inf,
           timing_inf, subblock_data_ptr, chan_width_dist, 
	   vertical_width_fac);
    return;
  }//if
  // Only need to route (or try to route) once. 
  // --- b ---
  else { 

    width_fac = router_opts.fixed_channel_width;
    
    // Allocate the major routing structures.
    clb_opins_used_locally = alloc_route_structs(*subblock_data_ptr); 
 

    // If below:
    /*---
    if (timing_inf.timing_analysis_enabled) {
      net_slack = alloc_and_load_timing_graph (timing_inf, *subblock_data_ptr); 
      net_delay = alloc_net_delay (&net_delay_chunk_list_head);
    }
    else {
      net_delay = NULL;    // Defensive coding. 
      net_slack = NULL;
    }
    ---*/
    // is replaced by:
    net_delay = alloc_net_delay (&net_delay_chunk_list_head);

    // Only needed to build timing graph and clb_opins_used_locally 
    free_subblock_data (subblock_data_ptr);

    // cristinel.ababei:  Now, after all routing data-sructures were
    // allocated and loaded (populated), here the ROUTING comes!

    start_time_route = clock();
    success = try_route (width_fac, vertical_width_fac,
			 router_opts, det_routing_arch, 
			 segment_inf, via_inf, 
			 timing_inf, 
			 net_slack, net_delay, 
			 chan_width_dist,
			 clb_opins_used_locally);
    finish_time_route = clock();
    duration_route = (double)(finish_time_route - 
			      start_time_route) / CLOCKS_PER_SEC;
    final_data.run_time_route = duration_route;

  }//else
  // -----------------------------------------------------------------------



  // Routing is done now.



  // -----------------------------------------------------------------------
  if (success == false) {
    printf ("Circuit is unrouteable with a channel width factor of %d.\n\n",
	    width_fac);
    sprintf (msg,"Routing failed with a channel width factor of %d.  ILLEGAL "
	     "routing shown.", width_fac);
  }
  else {
    printf("\n ------------------------------------------------------");
    printf("\n Right after successful routing:");
    printf("\n ------------------------------------------------------");
    

    check_route (router_opts.route_type, det_routing_arch.num_switch,
		 clb_opins_used_locally);

    get_serial_num ();

    if(num_layers > 1) { // 3D
      printf("Circuit successfully routed with a horizontal channel width-"
	     "factor of %d and a vertical channel width-factor of %d.\n\n",
	     width_fac, chan_width_z[0]);
      final_data.hcw = width_fac;
      final_data.vcw = chan_width_z[0];
    }
    else {             // 2D
      printf("Circuit successfully routed with a channel width factor of %d."
	     "\n\n", width_fac);
      final_data.hcw = width_fac;
      final_data.vcw = 0;
    }



    routing_stats (full_stats, router_opts.route_type,
	   det_routing_arch.num_switch, segment_inf,
	   det_routing_arch.num_segment, det_routing_arch.R_minW_nmos,
	   det_routing_arch.R_minW_pmos, timing_inf.timing_analysis_enabled,
	   net_slack, net_delay);



    tt_hg->Put_net_delay_info_into_timing_graph_02( net_delay );
    tt_hg->Compute_Static_DelayForPOs();
    detailed_route_delay = tt_hg->Get_Static_Delay();
    final_data.delay_route = detailed_route_delay;
    cout<<"\n Delay after considering detailed routing delay = "
	<<detailed_route_delay<<"    Route-run-time="
	<<duration_route<<endl;


    //tt_hg->Dump_delays_of_all_source_sink_pairs( "dbg_route_delay.echo" );
    print_route (route_file);
    //tt_hg->Dump_KPaths( "dbg_paths.echo", 10 ); // Dumps file with paths for GUI
    //print_to_file_criticalities_of_all_nets("dbg_net_criticalities.echo");// Dumps file with net criticalities for GUI

    
    //#ifdef PRINT_SINK_DELAYS
    //print_sink_delays("Routing_Sink_Delays.echo");
    //#endif
    
    sprintf(msg,"Routing succeeded with a channel width factor of %d.",
	    width_fac);
  }//if else
  // -----------------------------------------------------------------------
 


  if (timing_inf.timing_analysis_enabled) {
    free_timing_graph (net_slack);
    free_net_delay (net_delay, &net_delay_chunk_list_head);
  }


  free_route_structs (clb_opins_used_locally);
  fflush (stdout);
  // -----------------------------------------------------------------------
}//eof


// ======================================================================= //
// ======================================================================= //


void s_fpga3d::calculate_VIA_DELAY(t_segment_inf *via_inf) 
{
  // I compute it here separately, and made VIA_DELAY a member of 
  // fpga class in order not to transmit "t_segment_inf *via_inf"
  // as argument recursively too many times...

  // Via delay calculation as Elmore delay of a series of
  // one switch and one wire segment of length one.
  // ASSUMPTION: I assume switch with index "0" is the 
  // buffered one, which it is in the case of all my experiments...
  VIA_DELAY = 
    // Metal wire of length one or as dictated by "vertical_uwl".
    0.5 * via_inf[ 0 ].Rmetal *  via_inf[ 0 ].Cmetal * vertical_uwl +
    // R swicth contribution.
    switch_inf[ 0 ].R * via_inf[ 0 ].Cmetal +
    // Intrinsic switch delay.
    switch_inf[ 0 ].Tdel; 
  /*---
  cout<<"\n VIA_DELAY = 0.5*Rmetal*Cmetal + R*Cmetal + Tdel"; 
  cout<<"\n via_inf[ 0 ].Rmetal="<<via_inf[ 0 ].Rmetal;
  cout<<"\n via_inf[ 0 ].Cmetal="<<via_inf[ 0 ].Cmetal; 
  cout<<"\n switch_inf[ 0 ].R="<<switch_inf[ 0 ].R;
  cout<<"\n via_inf[ 0 ].Cmetal="<<via_inf[ 0 ].Cmetal;
  cout<<"\n switch_inf[ 0 ].Tdel="<<switch_inf[ 0 ].Tdel;
  ---*/

}//eof


// ======================================================================= //


// ======================================================================= //


void s_fpga3d::dump_out_final_data(char *net_file) 
{
// Dumps out final_data for ease of collecting data later on.

    printf("\nFinal data for %s\n",net_file);

    printf("\nPlace-level estimations:");
    printf("\n       Delay     \tWL\tRun_time\tFrac_nets_span_more_layers.");
    cout<<"\nFinal: "<<final_data.delay_place;
    cout<<"\t"<<final_data.wl_place;
    cout<<"\t"<<final_data.run_time_place;
    cout<<"\t"<<final_data.fraction_z;

    printf("\n\nRoute-level actual computations:");
    printf("\n       Delay     \tWL\tRun_time\tHCW\tVCW\tAvg_net_length"
	   "\tOptimistic_route_area.");
    cout<<"\nFinal: "<< final_data.delay_route;
    cout<<"\t"<< final_data.wl_route;
    cout<<"\t"<< final_data.run_time_route;
    cout<<"\t"<< final_data.hcw;
    cout<<"\t"<< final_data.vcw;
    cout<<"\t"<< final_data.avg_net_length;
    cout<<"\t"<< final_data.optimistic_rr_area<<endl<<endl;

}//eof


// ======================================================================= //
// ======================================================================= //


void s_fpga3d::init_chan (int cfactor, int vertical_width_fac, 
			  t_chan_width_dist chan_width_dist) 
{
// Assigns widths to channels (in tracks).  Minimum one track           
// per channel.  io channels are io_rat * maximum in interior           
// tracks wide.  The channel distributions read from the architecture  
// file are scaled by cfactor.                                        

 float x=0, separation=0, chan_width_io=0;
 int nio=0, i=0;
 t_chan chan_x_dist, chan_y_dist, chan_z_dist;

 chan_width_io = chan_width_dist.chan_width_io;
 chan_x_dist = chan_width_dist.chan_x_dist;
 chan_y_dist = chan_width_dist.chan_y_dist;

 chan_z_dist = chan_width_dist.chan_z_dist;// [  ?  ]

 // cristinel.ababei
 // z channels have width 1, hypothetically speaking...  
 // for GLOBAL routing and width "vias_per_zchan" for
 // DETAILED routing.
 // Same number of vias in all z channels.  This info is stored
 // in ".peak" of the data structure describing the distribution
 // of channels z... for convinience. 
 // zchan must be declared as uniform in architecture file, for now.
 // If vertical_width_fac != (int)chan_z_dist.peak, then 
 // vertical_width_fac
 for (i=0;i<9;i++)
   chan_width_z[i] = vertical_width_fac;
 
 printf("\n | horizontal-channel-width | = %d.",cfactor); 
 // Basically nodes_per_chan.
 printf("\n | vertical-channel-width | = %d.",chan_width_z[0]);
 // Basically vias_per_zchan.

 // io channel widths

 nio = (int) floor (cfactor*chan_width_io + 0.5);
 if (nio == 0) nio = 1;   // No zero width channels 

 chan_width_x[0] = chan_width_x[ny] = nio;
 chan_width_y[0] = chan_width_y[nx] = nio;


 if (ny > 1) {
   separation = 1./(ny-2.); // Norm. distance between two channels.
   x = 0.;    // This avoids div by zero if ny = 2.
    chan_width_x[1] = (int) floor (cfactor*comp_width(&chan_x_dist, x,
                   separation) + 0.5);

    // No zero width channels
    chan_width_x[1] = max(chan_width_x[1],1);

    for (i=1;i<ny-1;i++) {
       x = (float) i/((float) (ny-2.));
       chan_width_x[i+1] = (int) floor (cfactor*comp_width(&chan_x_dist, x,
                   separation) + 0.5);
       chan_width_x[i+1] = max(chan_width_x[i+1],1);
    }
 }   
 

 if (nx > 1) {
   separation = 1./(nx-2.); // Norm. distance between two channels.
   x = 0.;    // Avoids div by zero if nx = 2. 
    chan_width_y[1] = (int) floor (cfactor*comp_width(&chan_y_dist, x,
                 separation) + 0.5);
 
    chan_width_y[1] = max(chan_width_y[1],1);
 
    for (i=1;i<nx-1;i++) {
       x = (float) i/((float) (nx-2.));
       chan_width_y[i+1] = (int) floor (cfactor*comp_width(&chan_y_dist, x,
                 separation) + 0.5);
       chan_width_y[i+1] = max(chan_width_y[i+1],1);
    }
 }


 
#ifdef VERBOSE
    printf("\nchan_width_x:\n");
    for (i=0;i<=ny;i++)
       printf("%d  ",chan_width_x[i]);
    printf("\n\nchan_width_y:\n");
    for (i=0;i<=nx;i++)
       printf("%d  ",chan_width_y[i]);
    printf("\n\n");
#endif
 
}//eof
// ======================================================================= // 
 
float s_fpga3d::comp_width (t_chan *chan, float x, float separation) 
{
 
/* Return the relative channel density.  *chan points to a channel   *
 * functional description data structure, and x is the distance      *   
 * (between 0 and 1) we are across the chip.  separation is the      *   
 * distance between two channels, in the 0 to 1 coordinate system.   */
 
 float val;
 
 switch (chan->type) {
 
 case UNIFORM:
    val = chan->peak;
    break;
     
 case GAUSSIAN:
    val = (x - chan->xpeak)*(x - chan->xpeak)/(2*chan->width*
        chan->width);
    val = chan->peak*exp(-val);
    val += chan->dc;
    break;
     
 case PULSE:
    val = (float) fabs((double)(x - chan->xpeak));
    if (val > chan->width/2.) {
       val = 0;
    }
    else {
       val = chan->peak;
    }
    val += chan->dc;
    break;
     
 case DELTA:
    val = x - chan->xpeak;
    if (val > -separation / 2. && val <= separation / 2.)
       val = chan->peak;
    else
       val = 0.;
    val += chan->dc;
    break;
     
 default:
    printf("Error in comp_width:  Unknown channel type %d.\n",chan->type);
    exit (1);
    break;
 }   
 
 return(val);
}//eof
// ======================================================================= //


// ======================================================================= //
int s_fpga3d::binary_search_route (struct s_placer_opts
      placer_opts, char *place_file, char *net_file, char *arch_file,
      char *route_file, bool full_stats, bool verify_binary_search,
      struct s_annealing_sched annealing_sched, struct s_router_opts
      router_opts, struct s_det_routing_arch det_routing_arch,
      t_segment_inf *segment_inf, 
      t_segment_inf *via_inf,				   
      t_timing_inf timing_inf, t_subblock_data
      *subblock_data_ptr, t_chan_width_dist chan_width_dist,
      int vertical_width_fac) 
{
// This routine performs a binary search to find the minimum number of     
// tracks per channel required to successfully route a circuit, and returns
// that minimum width_fac.                                                 
  clock_t start_time_route, finish_time_route;
  double duration_route=0;
  double detailed_route_delay=0;
  start_time_route = clock();

  s_trace **best_routing=0;  // Saves the best routing found so far.
  int current=0,current_z=0, low=0, high=0, final=0;
  bool success=0, prev_success=0, prev2_success=0;
  char msg[BUFSIZE];
  float **net_delay=0, **net_slack=0;
  s_linked_vptr *net_delay_chunk_list_head=0;
  
  t_ivec **clb_opins_used_locally, **saved_clb_opins_used_locally; 
  // [0..num_blocks-1][0..num_class-1] 
  
 
  // Allocate the major routing structures.
  clb_opins_used_locally = alloc_route_structs (*subblock_data_ptr); 
  best_routing = alloc_saved_routing (clb_opins_used_locally, 
				      &saved_clb_opins_used_locally);
  
  // If below:
  /*---
    if (timing_inf.timing_analysis_enabled) {
    net_slack = alloc_and_load_timing_graph (timing_inf, *subblock_data_ptr); 
    net_delay = alloc_net_delay (&net_delay_chunk_list_head);
    }
    else {
    net_delay = NULL;    // Defensive coding. 
    net_slack = NULL;
    }
    ---*/
  // is replaced by:
  net_delay = alloc_net_delay (&net_delay_chunk_list_head);
  
  // Only needed to build timing graph and clb_opins_used_locally 
  free_subblock_data (subblock_data_ptr);
  
  
  current_z = vertical_width_fac; // Initial vias_per_zchan.
  current = 2 * pins_per_clb;  // Binary search part.
  low = high = -1;
  final = -1;
  
 
  int contor=0;
  // ----------------------------------------------------------------------
  while ( (final == -1)&&(contor < 4) ) {
    fflush (stdout);
#ifdef VERBOSE
    printf ("low, high, current %d %d %d\n",low,high,current);
#endif
 
    // Check if the channel width is huge to avoid overflow.  Assume the 
    // circuit is unroutable with the current router options if we're    
    // going to overflow.                                                
 
    if (current > MAX_CHANNEL_WIDTH) {
       printf("This circuit appears to be unroutable with the current "
         "router options.\n");
       printf("Aborting routing procedure.\n");
       exit (1);
    }

    // cristinel.ababei:  Now, after all routing data-sructures were
    // allocated and loaded (populated), here the ROUTING comes!
    success = try_route (current, current_z, 
			 router_opts, det_routing_arch, 
			 segment_inf, via_inf, 
			 timing_inf, 
			 net_slack, net_delay, 
			 chan_width_dist,
                         clb_opins_used_locally);

    if (success) {
       high = current;
 
       // Save routing in case it is best.
       save_routing (best_routing, clb_opins_used_locally,
                     saved_clb_opins_used_locally);   

       if ((high - low) <= 1)
	 final = high;
 
       if (low != -1) {
	 current = (high+low)/2;
       } 
       else {
	 current = high/2;   // haven't found lower bound yet.
       } 
    }
 
    else {   // last route not successful.
      low = current;
      if (high != -1) {

	if ((high - low) <= 1)
	  final = high;
	
	current = (high+low)/2;
      } 
      else {
	current = low*2;  // Haven't found upper bound yet.
	contor = contor + 1; // Vezi de cite ori merg in sus.
      } 
    }


    // Vezi daca nu s-a gasit upper bound...
    if ( contor > 2 ) {
      // Increment z channel width.
      current_z = current_z + 1;
      current = 2 * pins_per_clb;//Restart searching for the new num of vias.
      contor = 0;//Reset contor.
      if ( current_z > 10 ) {
	printf(" :( This circuit appears to be unroutable with the current "
	       "router options and even after incrementing vias_per_zchan "
	       "up to 10, which is ridiculously big.\n");
	printf("Aborting routing procedure.\n");
	exit (1);
      }
    }//if

  }//while
  // ----------------------------------------------------------------------




 // The binary search above occasionally does not find the minimum    
 // routeable channel width.  Sometimes a circuit that will not route  
 // in 19 channels will route in 18, due to router flukiness.  If        
 // verify_binary_search is set, the code below will ensure that FPGAs 
 // with channel widths of final-2 and final-3 wil not route             
 // successfully.  If one does route successfully, the router keeps    
 // trying smaller channel widths until two in a row (e.g. 8 and 9)    
 // fail.                                                              
 // OBS: cristia: I disabled this thing for now. 
 // ...
 // ...
 


 // Restore the best routing, and 
 // the best channel widths for final drawing and statistics output.

 init_chan (final, current_z, chan_width_dist);

 free_rr_graph ();
 build_rr_graph (router_opts.route_type, det_routing_arch, 
		 segment_inf, via_inf,
                 timing_inf, router_opts.base_cost_type);
 free_rr_graph_internals (router_opts.route_type, det_routing_arch, segment_inf,
                 timing_inf, router_opts.base_cost_type);

 restore_routing (best_routing, clb_opins_used_locally, 
                  saved_clb_opins_used_locally);

 // -----------------------------------------------------------------------
 printf("\n ------------------------------------------------------");
 printf("\n Right after successful routing:");
 printf("\n ------------------------------------------------------");

 finish_time_route = clock();
 duration_route = (double)(finish_time_route - 
			   start_time_route) / CLOCKS_PER_SEC;
 final_data.run_time_route = duration_route;

    
 check_route (router_opts.route_type, det_routing_arch.num_switch,
	      clb_opins_used_locally);
 get_serial_num ();
 printf("Best routing used a channel width factor of %d and "
	"vertical CW of %d.\n\n",final,current_z);
 final_data.hcw = final;
 final_data.vcw = current_z;

 routing_stats (full_stats, router_opts.route_type,
		det_routing_arch.num_switch, segment_inf,
		det_routing_arch.num_segment, det_routing_arch.R_minW_nmos,
		det_routing_arch.R_minW_pmos, timing_inf.timing_analysis_enabled,
		net_slack, net_delay);


 tt_hg->Put_net_delay_info_into_timing_graph_02( net_delay );
 tt_hg->Compute_Static_DelayForPOs();
 detailed_route_delay = tt_hg->Get_Static_Delay();
 final_data.delay_route = detailed_route_delay;
 cout<<"\n Delay after considering detailed routing delay = "
     <<detailed_route_delay<<"    Route-run-time="
     <<duration_route<<endl;


 print_route (route_file);
 //tt_hg->Dump_KPaths( "dbg_paths.echo", 10 );// dumps file with paths for GUI 
   
 // -----------------------------------------------------------------------
 

 if (timing_inf.timing_analysis_enabled) {
   free_timing_graph (net_slack);
   free_net_delay (net_delay, &net_delay_chunk_list_head);
 }
 

 free_route_structs (clb_opins_used_locally);
 fflush (stdout);
 
 return (final);
}//eof
// ======================================================================= //
