// ======================================================================= //
// Cristinel Ababei 10/21/2003
// ======================================================================= //

#include <iostream>
#include <set>
#include <queue>
#include <algorithm>

#include "wl_annealer.h"
#include "part_rec.h"

using namespace std;

extern s_fpga3d * fg3;
extern tt_hgraph * tt_hg;

// ======================================================================= //

Partition::Partition(int blX,int blY,int trX,int trY):
  blX(blX),blY(blY),trX(trX),trY(trY) {}
Partition::Partition() {}
// ======================================================================= //

Part_Rec::Part_Rec(s_fpga3d *fg,int layer_id):fg(fg),layer_id(layer_id)
{
  // block to id, id to block
}//eof
// ======================================================================= //

void Part_Rec::run_rec(void)
{
  printf("\nDoing quad-partitioning of layer %d.",layer_id);

  // --- Right before we do partitioning we assign naively
  // but deterministically locations to all io pads in this layer.
  // oBs:  I moved this in timing_kmost_driven_part_based_place().
  // See tt_place.cpp.

  // --- Account for kmost paths and bring first all edge weights 
  // into fpga3d object.  Once before starting any new layer. 

  fg->Update_Edge_Weights();
  //fg->printf_grid_1st_most_critical_path(0);

  int i=0,j=0;
  int quad=-1;
 
  queue<Partition> q;
  // 1st Partition contains the entire top-level layer chip.
  Partition p(1,1,fg->nx,fg->ny);
  p.quad_section_level=0; 
  // Fill in the Partition with all blocks in the circuit in current layer.
  for (i=0;i<fg->num_blocks;i++) 
    {
      if ( (fg->block[i].layer == layer_id) && 
	   (fg->block[i].type == CLB) )
	{
	  p.blocks.push_back( &fg->block[i] );
	  //printf(" %d",fg->block[i].ID);
	}
    }//for
  // Get the top-level Partition of this layer in queue.
  q.push(p);

  int previous_quad_level=0;//keeps track(s) for updating stuff
  int current_quad_level =0;
  
  // --- Here it goes the recursive quad-partitioning.
  int x=0;
  while ( !q.empty() ) {
    // Pick 1st Partition from queue.
    Partition &pr = q.front(); 
    //
    /*---   
    if( pr.quad_section_level > 0 ) // was: pr.blocks.size() > 12
      pr.init_hmetisInterface_Partition_with_terminal_propagation(fg,layer_id);
    else
    ---*/
      pr.init_hmetisInterface_Partition(fg,layer_id);

    //
    pr.quad_partitioning();
    //
    // Create an array of 4 Partitions.  Initially all 4 
    // Partitions have same corners of their parent Partition.
    Partition p[4]; 
    for (j=0; j<4; ++j) { 
      p[j].blX = pr.blX;
      p[j].blY = pr.blY;
      p[j].trX = pr.trX;
      p[j].trY = pr.trY;
    }
    // Get the quad-partitioning result into the 4 Partitions
    for (j=0; j<pr.blocks.size(); ++j) {
      quad = pr.metis->getModPartition( pr.blocks[j]->ID );//0,1,2, or 3
      // Store in blocks of local Partition(s) p the address of
      // fg.block corresponding to "quad" (i.e., build local/recursive
      // Partitions possibly to be added in queue).
      p[quad].blocks.push_back( &fg->block[ pr.blocks[j]->ID ] );
      //cout<<"  "<<pr.blocks[j]->ID<<" "<<p[quad].midX()<<","<<p[quad].midY();
      // Set locations in order not to decrement from 0,0 with setXY.
      //fg->block[pr.blocks[j]->ID].setXY(fg->grid[layer_id],
      //   p[quad].midX(),p[quad].midY()); 
    }

    // Go thru all 24 permutations of the four partitions.
    // Compute cost for each permutation and keep the best.
    // 2 3
    // 0 1
    // This is done only for layer 0.  Layers 1,2,3,... do not
    // support this because interfers with the constraint propagation
    // as nodes, which are fixed for kind-of-terminal-allignment. 
    // For now it is done though and will over-power the constraints
    // propagation in favor of smaller wl.
    int v[4] = { 0,1,2,3 };
    double cost=INT_MAX,newcost=0;
    
    // Also when I am using terminal propagation the next loop
    // is not done even for layer 0 because I want to keep
    // nodes assigned to partitions guided by terminal propagation.
    //if (layer_id == 0) { 
      for (int x0=0;x0<4;++x0) {
	// Asign as x,y position on the top-level grid of "fg" 
	// for all verts in partition 0
	p[0].assignXY(fg->grid[layer_id],x0); 
	for (int x1=0;x1<4;++x1) {
	  if (x1==x0) continue;
	  p[1].assignXY(fg->grid[layer_id],x1);
	  for (int x2=0;x2<4;++x2) {
	    if ((x2==x0) || (x2==x1)) continue;
	    p[2].assignXY(fg->grid[layer_id],x2);
	    for (int x3=0;x3<4;++x3) {
	      if ((x3==x1) || (x3==x2) || (x3==x0)) continue;
	      p[3].assignXY(fg->grid[layer_id],x3);
	      newcost = p[0].cost_local(fg) + p[1].cost_local(fg) + 
		        p[2].cost_local(fg) + p[3].cost_local(fg);
	      //printf(" %f.2",cost);
	      if (cost > newcost)
		{
		  cost = newcost;
		  //printf(" %f.2",cost);
		  v[0]=x0; v[1]=x1; v[2]=x2; v[3]=x3;
		}
	    }
	  }
	}
      }//for
    //}//if 
    
     
    // Now after finding the best locations for the 4 partitions
    // set their blX,blY,trX,trY and assign all their blocks in the center
    // of the area defined by these coordinates
    // 2 3
    // 0 1
    bool Xeven = (pr.trX-pr.blX)&1,Yeven = (pr.trY-pr.blY)&1;
    int L[4][4] = {{pr.blX,pr.blY,pr.midX(),pr.midY()},// 0
		   {pr.midX()+(Xeven?1:0),pr.blY,pr.trX,pr.midY()},// 1
		   {pr.blX,pr.midY()+(Yeven?1:0),pr.midX(),pr.trY}, // 2
		   {pr.midX()+(Xeven?1:0),pr.midY()+(Yeven?1:0),pr.trX,pr.trY}};
    p[0].setXY(fg->grid[layer_id],
	       L[v[0]][0],L[v[0]][1],L[v[0]][2],L[v[0]][3],layer_id);
    p[1].setXY(fg->grid[layer_id],
	       L[v[1]][0],L[v[1]][1],L[v[1]][2],L[v[1]][3],layer_id);
    p[2].setXY(fg->grid[layer_id],
	       L[v[2]][0],L[v[2]][1],L[v[2]][2],L[v[2]][3],layer_id);
    p[3].setXY(fg->grid[layer_id],
	       L[v[3]][0],L[v[3]][1],L[v[3]][2],L[v[3]][3],layer_id);
    // Check and see whether we have to add any of the 4 partitions. 
    for (int i=0;i<4;++i)
      if ((p[i].blocks.size()>3) && 
	  ((p[i].trX-p[i].blX+1)*(p[i].trY-p[i].blY+1)>2))
	{
	  // Set quadrisection level of ready to append Partition.
	  p[i].quad_section_level = pr.quad_section_level + 1;
	  q.push(p[i]);
	}
      else 
	{
	  // Set x,y of the three or less clbs as corners
	  // of their Partition. 
	  for (int k=0; k<p[i].blocks.size(); ++k) {
	    //---> if(p[i].blocks.size() >= 4) 
	    //---> printf(" +++ stop rec for size %d +++ ",p[i].blocks.size());
	    if (k == 0)
	      fg->block[p[i].blocks[k]->ID].setXY(fg->grid[layer_id],
					    p[i].blX, p[i].blY, true);
	    if (k == 1)
	      fg->block[p[i].blocks[k]->ID].setXY(fg->grid[layer_id],
					    p[i].blX+1, p[i].blY+1, true);
	    if (k == 2)
	      fg->block[p[i].blocks[k]->ID].setXY(fg->grid[layer_id],
					    p[i].blX, p[i].blY+1, true);
	    if (k == 3)
	      fg->block[p[i].blocks[k]->ID].setXY(fg->grid[layer_id],
					    p[i].blX+1, p[i].blY, true);
	  }//for
	}// if else
    

             
    // Update kmost slack-based criticalities or not.
    // Do it only once at the end of every quad-level.
    current_quad_level = pr.quad_section_level + 1;
    
    if( current_quad_level != previous_quad_level )
      {
	previous_quad_level = current_quad_level;
	
	fg->Update_Edge_Weights();

	//fg->printf_grid_1st_most_critical_path(0);
      } 
    
    
    q.pop();
  }//while(!q.empty())


  /*--- 
  // --- Do low-temp SA at this layer for wl improvement.
  wl_annealer wl_anneal(fg,layer_id);
  wl_anneal.run_wl_anneal_move_one_cell(
	      time(NULL),0.99,(fg->num_blocks),0.05);
  ---*/


  // --- Throw an overlap removal.
  p.remove_overlaps_in_layer(fg,layer_id);


  // After every layer is partitioned, use the placement result
  // to propagate constraints (nodes as fixed or free) to layers
  // beneath it only.  In this way the whole place is constructive
  // one with constraints propagation guided by the kmost critical
  // paths.
  // "p" is 1st Partition containing the whole layer.
  
  if ( layer_id < (fg->num_layers-1) )
    p.impose_loc_constr_upon_lower_layers(fg,layer_id);
  


  // Some statistics for visual debugging.
  fg->printf_grid_matrix_occupancy(layer_id);
  fg->printf_grid_stats_just_after_part(layer_id);
  printf("\nDone rec quad-partitioning at layer %d", layer_id);

}//eof
// ======================================================================= //



// ======================================================================= //



// ======================================================================= //
void Partition::init_hmetisInterface_Partition(s_fpga3d *fg,int layer_id)
{
  // Used for transfering nodes and nets to hmetis interface; i.e.,
  // preparation for calling hmetis partitioner.
  // Instantiate MetisIntfc class and initialize the interface, i.e.
  // introduce into the interface all modules and then all nets with 
  // their connectivity.  Also, here, we set the net weights.
  int i=0,j=0,k=0,max_pin=0,valid_terms=0;
  vector<int> block_ids;
  map<int,int> pot_nets;//potential set of nets

  //printf("\nDoing init_hmetisInterface_Partition.");
  // Preparatory stuff.
  for (i=0;i<blocks.size();i++) 
    block_ids.push_back(blocks[i]->ID);
  for (i=0;i<blocks.size();i++) 
    {
      if ((blocks[i]->type == INPAD) || (blocks[i]->type == OUTPAD))
	max_pin = 1;
      else
	max_pin = fg->pins_per_clb;
      for (j=0;j<max_pin;j++)
	{
	  if ( blocks[i]->nets[j] != OPEN )
	    {
	      // Net which touches a block in this Partition.
	      // See if it has at least two blocks in this Partition.
	      valid_terms=0;
	      for (k=0;k<fg->net[blocks[i]->nets[j]].num_pins;k++)
		{
		  if (find(block_ids.begin(),block_ids.end(),
			   fg->net[blocks[i]->nets[j]].blocks[k]) != 
		           block_ids.end())
		    valid_terms++;
		}
	      // Add this net as local net with
	      // terminals in current Partition only if it has at 
	      // least two local terminals.
	      if (valid_terms >= 2)
		{ 
		  pot_nets[blocks[i]->nets[j]] = valid_terms;
		  //printf(" %d %d ",blocks[i]->nets[j],valid_terms); 
		}
	    }//if
	}//for
    }//for

  metis = new MetisIntfc(block_ids.size(),pot_nets.size());
  // Add modules.
  int lx=0,ly=0,midx=0,midy=0;
  for (i=0;i<blocks.size();i++) {
    metis->AddModule( blocks[i]->ID );//ID of block in top-level s_fpga3d
    //printf(" %d",blocks[i]->ID);
    metis->setModWeight( blocks[i]->ID, 1 );
    // Here, if it on layer 0, first one to be rec-quad-part
    // just tell hmetis the node is free to move.  If at layer 1,2,...
    // then look and see if it's a constrained node due to placement
    // at higher layers.
    if (layer_id == 0)
      metis->setModPartition( blocks[i]->ID, -1 );//Free to move
    else {
      if (blocks[i]->locked == 1) {
	// clb locked on layer 1,2,... needs to be fixed.
	// Find out partition in which it has to be fixed.
	//
	lx = blocks[i]->x; midx = midX();
	ly = blocks[i]->y; midy = midY();
	// Quadrants
	// 2 3
	// 0 1
	if( (lx <= midx) && (ly <= midy) ) { // --- Quadrant 0
	  metis->setModPartition( blocks[i]->ID, 0 );//fix it
	  //cout<<" "<<0;
	}
	if( (lx > midx) && (ly <= midy) ) { // --- Quadrant 1
	  metis->setModPartition( blocks[i]->ID, 1 );//fix it
	  //cout<<" "<<1;
	}
	if( (lx <= midx) && (ly > midy) ) { // --- Quadrant 2
	  metis->setModPartition( blocks[i]->ID, 2 );//fix it
	  //cout<<" "<<2;
	}
	if( (lx > midx) && (ly > midy) ) { // --- Quadrant 3
	  metis->setModPartition( blocks[i]->ID, 3 );//fix it
	  //cout<<" "<<3;
	}
      }
      else
	metis->setModPartition( blocks[i]->ID, -1 );//Free to move
    }//else
  }//for
  
  // Add connectivity information.
  i=0;//reset
  for (map<int,int>::iterator i1=pot_nets.begin();
       i1!=pot_nets.end();i1++)
    {
      j=0;//reset
      int * mods = new int[ i1->second + 1 ];
      for (k=0;k<fg->net[i1->first].num_pins;k++)
	{
	  if (find(block_ids.begin(),block_ids.end(),
		   fg->net[i1->first].blocks[k]) != block_ids.end())
	    {
	      mods[j] = fg->net[i1->first].blocks[k]; //Add this net-terminal
	      j++;
	    }
	}
      
      metis->AddNet( i, i1->second, mods );
      //metis->setNetWeight( i, ceil(fg->net[i1->first].weight) );
      metis->setNetWeight( i, 1 );
      //cout<<" "<<ceil(fg->net[i1->first].weight);
      delete mods;
      i++;
    }//for
 

}//eof
// ======================================================================= //

// ======================================================================= //
void Partition::impose_loc_constr_upon_lower_layers(s_fpga3d *fg,int layer_id)
{
  // Here, we work on propagation of location constraints due to
  // layers already placed.  Terminals of nets with some terminals
  // already placed in this layer are locked to locations right
  // beneath or as close as possible.  In this way a 3d bounding box
  // minimization is attempted.  These locked locations appear as
  // fixed nodes to hmetis.  This is kind of terminal allignment in 3d.
  // Only nets whose sources are on kmost critical paths
  // are considered because they are most critical.
  int i=0,j=0,k=0,max_pin=0;
  int id_source_clb=0;
  int count_constr=0;
  // --- Unlock first all clbs.
  for (i=0;i<fg->num_blocks;i++) 
    fg->block[i].locked = 0;
  // --- Visit all blocks in this layer and look for critical clbs
  // as sources of critical nets.
  for (i=0;i<blocks.size();i++) { // 1
    // If this block is on kmost paths.
    if (blocks[i]->is_on_kmost == 1) { // 2
      // Only pi or clb can be source of a net.
      // However, pi should not be constrained, because their locations
      // were fixed to the margins.
      if ( blocks[i]->type == CLB ) { // 3

	max_pin = fg->pins_per_clb;
	//cout<<" "<<max_pin;
	// Look now only at nets touching this block and
	// whose source block/clb is exactly this block.  Only in 
	// this case do constraining of terminals of that net.
	for (j=0;j<max_pin;j++) // 4
	  {
	    if (blocks[i]->nets[j] != OPEN)
	      {
		// See if the net "touching" this clb has its source
		// current block, if so constrain its terminals in lower
		// layers.
		id_source_clb = fg->net[blocks[i]->nets[j]].blocks[0];
		//cout<<" "<<id_source_clb;
		if( blocks[i]->ID == id_source_clb )
		  // Here we go.  So, let's constrain all
		  // its terminals in lower layers.
		  {
		    count_constr++;
		    fg->constrain_terminals_in_lower_layers(
			      blocks[i]->nets[j],// net id
			      layer_id);// "from" layer
		  }//if
	      }
	  }// 4
      }// 3
    }// 2
  }//1

  printf("\n Num of constrained nets with possible terminals in layers"
	 " below layer %d is = %d ",layer_id,count_constr);
}//eof
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::constrain_terminals_in_lower_layers(int net_id,int layer_id)
{
  // Input:  net_id and current layer.  We have to fix/constrain
  // locations of terminals on critical nets.  The net has its source
  // on kmost paths and terminals in layers beneath the current one
  // will be fixed in a clb below the 2D bounding-box-in-current-layer
  // of this net.  These constraints will appear as fixed nodes for hmetis.
  //
  //    _____*   layer 0
  //   /     /
  //  /     /
  // *_____* 
  // |     |
  // |     |
  // *     |     layer 1
  //       |
  //       *     layer 2
  int j=0,k=0;
  int term_layer=0;
  // --- Get the 2D bounding box in current layer of this net.
  int loc_minx=INT_MAX,loc_miny=INT_MAX;
  int loc_maxx=-1,loc_maxy=-1;
  int newx=0,newy=0;
  for (j=0;j<net[net_id].num_pins;j++) {//go thru all terminals
    if ( block[ net[net_id].blocks[j] ].layer == layer_id ) {
      // Look only at terminals in current layer_id.
      if(block[net[net_id].blocks[j]].x < loc_minx) loc_minx = 
				block[net[net_id].blocks[j]].x;
      if(block[net[net_id].blocks[j]].y < loc_miny) loc_miny = 
				block[net[net_id].blocks[j]].y;
      if(block[net[net_id].blocks[j]].x > loc_maxx) loc_maxx = 
				block[net[net_id].blocks[j]].x;
      if(block[net[net_id].blocks[j]].y > loc_maxy) loc_maxy = 
				block[net[net_id].blocks[j]].y;
    }//if
  }//for
  // --- Look again at all terminals and constrain those in layers below.
  for (k=0;k<net[net_id].num_pins;k++) {
    term_layer = block[ net[net_id].blocks[k] ].layer;
    if( ( term_layer > layer_id ) &&
	( block[ net[net_id].blocks[k] ].locked == 0) )
      // This means that this terminal in on a layer below
      // the current one.  So, we have to lock/fix its clb
      // "beneath" the 2d-bb of this net in current layer,
      // only if it was not already constrained because it was
      // a terminal on a critial net already processed
      // by constraining.
      // In other words, do a kind of allignment.
      {
	if ( block[ net[net_id].blocks[k] ].type == CLB )
	  // Constrain only CLBs.  do not touch io pads.
	  {
	    block[ net[net_id].blocks[k] ].locked = 1;//lock it
	    // Need to revise next two lines because I can fix more 
	    // terminals on same locations.
	    if( grid[term_layer][loc_minx][loc_miny] == 0) {
	      // If the clb is empty/available fix it here
	      block[ net[net_id].blocks[k] ].setXY(grid[term_layer],
						   loc_minx, loc_miny, true);
	      //cout<<" "<<net[net_id].blocks[k]<<" "<<loc_minx<<","<<loc_miny<<" ";
	    }
	    else {
	      // If the location is not available look for the 
	      // closest available by increasing x and/or y within
	      // legal limits.
	      find_new_constraining_location_for_block(loc_minx, loc_miny,
						       &newx, &newy, term_layer);
	      block[ net[net_id].blocks[k] ].setXY(grid[term_layer],
						   newx, newy, true);
	      //cout<<" "<<net[net_id].blocks[k]<<" "<<newx<<","<<newy<<" ";
	    }
	  }//if
      }//if CLB
  }//for
}//eof
// ======================================================================= //
// ======================================================================= //
void Partition::remove_overlaps_in_layer(s_fpga3d *fg,int layer_id)
{
  // Overlap removal:  go and move first critical clbs
  // to closest empty place.  This is first phase.  In second phase do
  // the same thing with left noncritical modules. 
  int i=0,j=0,k=0,max_pin=0;
  int newx=0,newy=0;
  int moved_non_crit_blocks=1,moved_crit_blocks=1;
  // --- PHASE I 
  // Distribute in cele mai apropiate empty locations mai intii
  // modules care NU sint on kmost critical paths.  In felul asta
  // cele critical sint lasate sa fie mutate ptr. overlap removal
  // la sfirsit in felul asta disturbindule cit mai putin.
  moved_non_crit_blocks=1;
  while (moved_non_crit_blocks == 1) {// should loop about three times only
    moved_non_crit_blocks = 0;//reset
    for (i=0;i<blocks.size();i++) { // 1
      // Check if location of this block is > 1, i.e., it
      // is crowded.  If so move it to closest empty clb.
      if (fg->grid[layer_id][blocks[i]->x][blocks[i]->y] > 1) { // 2
	// If this block is not on kmost paths.
	if (blocks[i]->is_on_kmost != 1) { // 3
	  // Find new location cautind "in cerc" in jurul
	  // vechii location.
	  find_new_location_for_block(fg,i,&newx,&newy,layer_id);
	  if ( !(newx>0 && newx<=fg->nx) ||
	       !(newy>0 && newy<=fg->ny) ) {
	    printf("Aborting:  remove_overlaps_in_layer returned"
		   " new location outside 1,nx 1,ny.\n");
	    exit(1);
	  }
	  // Move it to new location.
	  blocks[i]->setXY(fg->grid[layer_id],newx,newy,true);
	  moved_non_crit_blocks = 1;
	}// 3
      }// 2
    }//for
  }//while
  // --- PHASE II
  // Go now and move if necessary blocks on critical paths
  // within a similar loop.
  moved_crit_blocks=1;
  while (moved_crit_blocks == 1) {
    moved_crit_blocks = 0;//reset
    for (i=0;i<blocks.size();i++) { // 1
      // Check if location of this block is > 1, i.e., it
      // is crowded.  If so move it to closest empty clb.
      if (fg->grid[layer_id][blocks[i]->x][blocks[i]->y] > 1) { // 2
	// This block should be on kmost paths.
	if (blocks[i]->is_on_kmost == 1) { // 3
	  // Find new location cautind "in cerc" in jurul
	  // vechii location.
	  find_new_location_for_block(fg,i,&newx,&newy,layer_id);
	  if ( !(newx>0 && newx<=fg->nx) ||
	       !(newy>0 && newy<=fg->ny) ) {
	    printf("Aborting:  remove_overlaps_in_layer returned"
		   " new location outside 1,nx 1,ny.\n");
	    exit(1);
	  }
	  // Move it to new location.
	  blocks[i]->setXY(fg->grid[layer_id],newx,newy,true);
	  moved_crit_blocks = 1;
	}// 3
      }// 2
    }//for
  }//while
  // At this time this layer should not have overlaps.
  printf("\nDone overlap removal on layer= %d",layer_id); 
}//eof
// ======================================================================= //
// ======================================================================= //
double Partition::quad_partitioning(void)
{
  //
  //
  metis->Partition( 4 ); 
}//eof
// ======================================================================= //
// ======================================================================= //
double Partition::cost_local(s_fpga3d *fg)
{
  //
  //
  int j=0,k=0,max_pin=0;
  set<s_net*> unets;
  for (vector<s_block*>::iterator v=blocks.begin();v!=blocks.end();v++)
    {
      if ((*v)->type == INPAD || (*v)->type == OUTPAD)
	max_pin = 1;
      else
	max_pin = fg->pins_per_clb;
      
      for (j=0;j<max_pin;j++)
	{
	  if ( (*v)->nets[j] != OPEN )
	    unets.insert( &fg->net[(*v)->nets[j]] );
	}
    }//for
  
  double cost = 0;
  for (set<s_net*>::iterator n = unets.begin();n != unets.end();n++)
    cost+=(*n)->cost2D(fg);
  //printf(" %.2f",cost);
  return cost;
}//eof
// ======================================================================= //
// ======================================================================= //
void Partition::assignXY(vector<vector<int> > &grid,
			 int option)
{
  // This routine used for locations x,y assignment during
  // rec part to explore the best - from WL point of view -
  // topological arrangement of the four Partitions.
  // 2 3 OR 0 2 OR etc.
  // 0 1    3 1
  int X,Y;
  bool Xeven = (trX-blX)&1,Yeven = (trY-blY)&1;
  switch(option)
    {
    case 0:
      X = (blX+midX())/2;
      Y = (blY+midY())/2;
      break;
    case 1:
      X = (midX()+trX+(Xeven?1:0))/2;
      Y = (blY+midY())/2;
      break;
    case 2:
      X = (blX+midX())/2;
      Y = (midY()+trY+(Yeven?1:0))/2;
      break;
    case 3:
      X = (midX()+trX+(Xeven?1:0))/2;
      Y = (midY()+trY+(Yeven?1:0))/2;
      break;
    }
  //printf(" %d %d ",X,Y);
  for (vector<s_block*>::iterator v=blocks.begin();v!=blocks.end();v++)
    (*v)->setXY( grid, X, Y ); 
}//eof
// ======================================================================= //
// ======================================================================= //
void Partition::setXY(vector<vector<int> > &grid,
		      int blX,int blY,int trX,int trY,int layer_id)
{
  // Center coordinates of this Partition is made x,y of all blocks
  // contain inside Partition.
  Partition::blX = blX;
  Partition::blY = blY;
  Partition::trX = trX;
  Partition::trY = trY;
  int midx = midX();
  int midy = midY();
  //printf(" %d %d ",midx,midy);
  for (vector<s_block*>::iterator v=blocks.begin();v!=blocks.end();v++)
    if ( layer_id == 0 )
      (*v)->setXY( grid, midx, midy );
    else
      if ( (*v)->locked == 0 ) {
	// Set coordinates in the middle only if it's not fixed/locked,
	// otherwise leave it where it is.
	(*v)->setXY( grid, midx, midy );
      }
  //else {
  //cout<<" fix:"<<(*v)->x<<","<<(*v)->y<<" ";
  //  }
      
}//eof
// ======================================================================= //
// ======================================================================= //
void s_block::setXY(vector<vector<int> > &grid,int x,int y,bool dec)
{
  // Increments "grid" at the location dictated by x,y.  This is
  // kind of virtual placement of this block at x,y on the grid.
  // Old location may be decremented as dictated by "dec".
  // Sanity check.
  //if ((s_block::x == 0) && (s_block::y == 0) && dec)
  //{
  //  printf("\nAbort:  Error, trying to decrement grid[0][0].");
  //  exit(1);
  //}
  //printf(" %d %d ",s_block::x,s_block::y);
  if ( s_block::type == CLB ) { // Applies for clbs only.
    // When I am left with 3 or less nodes in a Partition
    // I stop rec partitioning and assign locations x,y or
    // x+1,y or x,y+1 or x+1,y+1.  However, this may place a
    // block on nx+1 or ny+1 where only IO can be. 
    // This is a boundary "situation" which I correct 
    // at the expense of placing more nodes at same location,
    // which will be fixed during overlap removal.
    if (x==fg3->nx+1) {
      x = x - 1;
      printf("Adjusted x due to boundary situation during rec_part.\n");
    }
    if (y==fg3->ny+1) {
      y = y - 1;
      printf("Adjusted y due to boundary situation during rec_part.\n");
    }
  }
  if (dec) --grid[s_block::x][s_block::y];
  s_block::x = x;
  s_block::y = y;
  grid[x][y]++;
}//eof
// ======================================================================= //
// ======================================================================= //
double s_net::cost2D(s_fpga3d *fg)
{
  // Returns 1/2 (half) perimeter bounding box.
  int blX=INT_MAX,blY=INT_MAX,trX=-1,trY=-1;
  for (int i=0;i<num_pins;i++)
    {
      blX = min( fg->block[blocks[i]].x, blX );
      trX = max( fg->block[blocks[i]].x, trX );
      blY = min( fg->block[blocks[i]].y, blY );
      trY = max( fg->block[blocks[i]].y, trY );
    }//for
  return double( (trX-blX + trY-blY) ) ;
}//eof
// ======================================================================= //
// ======================================================================= //
double s_net::cost3D(s_fpga3d *fg)
{
  // Returns 1/2 (half) perimeter of the 3D bounding box.
  // When num_layers=1, z coordinate has no effect and
  // this function returns cost2D.
  int blX=INT_MAX,blY=INT_MAX,trX=-1,trY=-1;
  int maxZ=INT_MAX,minZ=-1;
  for (int i=0;i<num_pins;i++)
    {
      blX = min( fg->block[blocks[i]].x, blX );
      trX = max( fg->block[blocks[i]].x, trX );
      blY = min( fg->block[blocks[i]].y, blY );
      trY = max( fg->block[blocks[i]].y, trY );
      minZ = min( fg->block[blocks[i]].z, minZ );
      maxZ = max( fg->block[blocks[i]].z, maxZ );
    }//for
  return double( (trX-blX + trY-blY) + (maxZ - minZ) ) ;
}//eof
// ======================================================================= //
// ======================================================================= //
int Partition::find_new_location_for_block(s_fpga3d *fg,int id,
					   int *newx,int *newy,int layer_id)
{
  // Search in circle for the closest empty clb.
  // Returns 1 if found, 0 otherwise.
  int x0=blocks[id]->x;  
  int y0=blocks[id]->y;
  punct p(x0,y0);
  punct temp_p(0,0); 
  int temp_id=0;
  int xtemp=0,ytemp=0;
  int A[fg->nx+1][fg->ny+1];
  int i=0,j=0;
  for (i=0;i<=fg->nx;++i) 
    for (j=0;j<=fg->ny;++j)
      A[i][j]=0;

  queue<punct> frontwave; 
  frontwave.push( p ); // put in queue the congested-block itself.
  A[x0][y0]=1;

  while( !frontwave.empty() ) {
    temp_p = frontwave.front();
    frontwave.pop(); // delete it also from queue.
    // Look at all legal neighbors of this point on grid. If not empty add
    // to queue.  If empty record location and return.
    xtemp = temp_p.x; 
    ytemp = temp_p.y;
    if( ( (xtemp-1)>0 ) && ( (ytemp+1)<=fg->ny ) ) // --- 1
      if ( A[xtemp-1][ytemp+1] == 0 )
	if( fg->grid[layer_id][xtemp-1][ytemp+1] > 0 )
	  {
	    p.x = xtemp-1;
	    p.y = ytemp+1;
	    frontwave.push( p );
	    A[xtemp-1][ytemp+1] = 1;
	  }
	else 
	  {
	    *newx = xtemp-1;
	    *newy = ytemp+1; return 1;
	  }
    if( ( (xtemp)>0 ) && ( (ytemp+1)<=fg->ny ) ) // --- 2
      if ( A[xtemp][ytemp+1] == 0 )
	if( fg->grid[layer_id][xtemp][ytemp+1] > 0 )
	  {
	    p.x = xtemp;
	    p.y = ytemp+1;
	    frontwave.push( p );
	    A[xtemp][ytemp+1] = 1;
	  }
	else 
	  {
	    *newx = xtemp;
	    *newy = ytemp+1; return 1;
	  }
    if( ( (xtemp+1)<=fg->nx ) && ( (ytemp+1)<=fg->ny ) ) // --- 3
      if ( A[xtemp+1][ytemp+1] == 0 )
	if( fg->grid[layer_id][xtemp+1][ytemp+1] > 0 )
	  {
	    p.x = xtemp+1;
	    p.y = ytemp+1;
	    frontwave.push( p );
	    A[xtemp+1][ytemp+1] = 1;
	  }
	else 
	  {
	    *newx = xtemp+1;
	    *newy = ytemp+1; return 1;
	  }
    if( ( (xtemp+1)<=fg->nx ) && ( (ytemp)<=fg->ny ) ) // --- 4
      if ( A[xtemp+1][ytemp] == 0 )
	if( fg->grid[layer_id][xtemp+1][ytemp] > 0 )
	  {
	    p.x = xtemp+1;
	    p.y = ytemp;
	    frontwave.push( p );
	    A[xtemp+1][ytemp] = 1;
	  }
	else 
	  {
	    *newx = xtemp+1;
	    *newy = ytemp; return 1;
	  }
    if( ( (xtemp+1)<=fg->nx ) && ( (ytemp-1)>0 ) ) // --- 5
      if ( A[xtemp+1][ytemp-1] == 0 )
	if( fg->grid[layer_id][xtemp+1][ytemp-1] > 0 )
	  {
	    p.x = xtemp+1;
	    p.y = ytemp-1;
	    frontwave.push( p );
	    A[xtemp+1][ytemp-1] = 1;
	  }
	else 
	  {
	    *newx = xtemp+1;
	    *newy = ytemp-1; return 1;
	  }
    if( ( (xtemp)>0 ) && ( (ytemp-1)>0) ) // --- 6
      if ( A[xtemp][ytemp-1] == 0 )
	if( fg->grid[layer_id][xtemp][ytemp-1] > 0 )
	  {
	    p.x = xtemp;
	    p.y = ytemp-1;
	    frontwave.push( p );
	    A[xtemp][ytemp-1] = 1;
	  }
	else 
	  {
	    *newx = xtemp;
	    *newy = ytemp-1; return 1;
	  }
    if( ( (xtemp-1)>0 ) && ( (ytemp-1)>0 ) ) // --- 7
      if ( A[xtemp-1][ytemp-1] == 0 )
	if( fg->grid[layer_id][xtemp-1][ytemp-1] > 0 )
	  {
	    p.x = xtemp-1;
	    p.y = ytemp-1;
	    frontwave.push( p );
	    A[xtemp-1][ytemp-1] = 1;
	  }
	else 
	  {
	    *newx = xtemp-1;
	    *newy = ytemp-1; return 1;
	  }
    if( ( (xtemp-1)>0 ) && ( (ytemp)<=fg->ny ) ) // --- 8
      if ( A[xtemp-1][ytemp] == 0 )
	if( fg->grid[layer_id][xtemp-1][ytemp] > 0 )
	  {
	    p.x = xtemp-1;
	    p.y = ytemp;
	    frontwave.push( p );
	    A[xtemp-1][ytemp] = 1;
	  }
	else 
	  {
	    *newx = xtemp-1;
	    *newy = ytemp; return 1;
	  }
  }//while
  return 0; // Not found empty location ?
}//eof
// ======================================================================= //


// ======================================================================= //


// ======================================================================= //
void Partition::init_hmetisInterface_Partition_with_terminal_propagation(
						s_fpga3d *fg,int layer_id)
{
  // Basically same as init_hmetisInterface_Partition but
  // here I also do terminal propagation starting with terminals
  // and then recursively inserting single dummy and fixed nodes 
  // in each partition.
  // Dummy and fixed  nodes will have id's as:
  // -3 -4
  // -1 -2
  //

  int i=0,j=0,k=0,max_pin=0,valid_terms=0;
  vector<int> block_ids;
  map<int,int> pot_nets;//potential set of nets

  //printf("\nDoing init_hmetisInterface_Partition.");
  // --- 1 ---
  // Preparatory stuff.
  for (i=0;i<blocks.size();i++) 
    block_ids.push_back(blocks[i]->ID);
  for (i=0;i<blocks.size();i++) 
    {
      if ((blocks[i]->type == INPAD) || (blocks[i]->type == OUTPAD))
	max_pin = 1;
      else
	max_pin = fg->pins_per_clb;
      for (j=0;j<max_pin;j++)
	{
	  if ( blocks[i]->nets[j] != OPEN )
	    {
	      // Net which touches a block in this Partition.
	      // See if it has at least two blocks in this Partition.
	      valid_terms=0;
	      for (k=0;k<fg->net[blocks[i]->nets[j]].num_pins;k++)
		{
		  if (find(block_ids.begin(),block_ids.end(),
			   fg->net[blocks[i]->nets[j]].blocks[k]) != 
		           block_ids.end())
		    valid_terms++;
		}
	      // Add this net as local net with
	      // terminals in current Partition only if it has at 
	      // least two local terminals.
	      if (valid_terms >= 2)
		{ 
		  pot_nets[blocks[i]->nets[j]] = valid_terms;
		  //printf(" %d %d ",blocks[i]->nets[j],valid_terms); 
		}
	    }//if
	}//for
    }//for

  // --- 2 ---
  // Num of nets is with 4 more to account for the nets involving
  // the fixed dummy nodes for terminal propagation.
  metis = new MetisIntfc( (block_ids.size() + 4),(pot_nets.size() + 4) );
  // Add modules.
  int lx=0,ly=0,midx=0,midy=0;
  for (i=0;i<blocks.size();i++) {
    metis->AddModule( blocks[i]->ID );//ID of block in top-level s_fpga3d
    //printf(" %d",blocks[i]->ID);
    metis->setModWeight( blocks[i]->ID, 1 );
    // Here, if it on layer 0, first one to be rec-quad-part
    // just tell hmetis the node is free to move.  If at layer 1,2,...
    // then look and see if it's a constrained node due to placement
    // at higher layers.
    if (layer_id == 0)
      metis->setModPartition( blocks[i]->ID, -1 );//Free to move
    else {
      if (blocks[i]->locked == 1) {
	// clb locked on layer 1,2,... needs to be fixed.
	// Find out partition in which it has to be fixed.
	//
	lx = blocks[i]->x; midx = midX();
	ly = blocks[i]->y; midy = midY();
	// Quadrants
	// 2 3
	// 0 1
	if( (lx <= midx) && (ly <= midy) ) { // --- Quadrant 0
	  metis->setModPartition( blocks[i]->ID, 0 );//fix it
	  //cout<<" "<<0;
	}
	if( (lx > midx) && (ly <= midy) ) { // --- Quadrant 1
	  metis->setModPartition( blocks[i]->ID, 1 );//fix it
	  //cout<<" "<<1;
	}
	if( (lx <= midx) && (ly > midy) ) { // --- Quadrant 2
	  metis->setModPartition( blocks[i]->ID, 2 );//fix it
	  //cout<<" "<<2;
	}
	if( (lx > midx) && (ly > midy) ) { // --- Quadrant 3
	  metis->setModPartition( blocks[i]->ID, 3 );//fix it
	  //cout<<" "<<3;
	}
      }
      else
	metis->setModPartition( blocks[i]->ID, -1 );//Free to move
    }//else
  }//for
  // --- 2 --- Add four dummy nodes with id's as:
  // -3 -4
  // -1 -2 
  // and fix them in partitions 0,1,2,3
  for (i=0;i<=3;i++) {
    metis->AddModule( -(1+i) );//-1,-2,-3,-4
    metis->setModWeight( -(1+i), 1 );
    metis->setModPartition( -(1+i), i );//fix it
  }//for
  // --- 3 ---
  // --- Add connectivity information about the "normal" nets.
  i=0;//reset
  for (map<int,int>::iterator i1=pot_nets.begin();
       i1!=pot_nets.end();i1++)
    {
      j=0;//reset
      int * mods = new int[ i1->second + 1 ];
      for (k=0;k<fg->net[i1->first].num_pins;k++)
	{
	  if (find(block_ids.begin(),block_ids.end(),
		   fg->net[i1->first].blocks[k]) != block_ids.end())
	    {
	      mods[j] = fg->net[i1->first].blocks[k]; //Add this net-terminal
	      j++;
	    }
	}
      
      metis->AddNet( i, i1->second, mods );
      //metis->setNetWeight( i, ceil(fg->net[i1->first].weight) );
      metis->setNetWeight( i, 1 );
      //cout<<" "<<ceil(fg->net[i1->first].weight);
      delete mods;
      i++;
    }//for
  // --- Add connectivity information about the "ab-normal" nets,
  // those involving the dummy nodes for terminal propagation.
  // --- 4 ---
  int j1=0,j2=0,j3=0,j4=0,s=0;
  int is_in=0;
  int * mods1 = new int[ 1000 ];
  int * mods2 = new int[ 1000 ];
  int * mods3 = new int[ 1000 ];
  int * mods4 = new int[ 1000 ];
  mods1[ j1 ] = -1; j1++;
  mods2[ j2 ] = -2; j2++;
  mods3[ j3 ] = -3; j3++;
  mods4[ j4 ] = -4; j4++;
  int loc_x=0,loc_y=0;
  int midlex = midX(); 
  int midley = midY();
  //cout<<"\n "<<blX<<","<<blY<<" \n";
  //cout<<"\n "<<trX<<","<<trY<<" \n";
  for (i=0;i<blocks.size();i++) 
    {
      if ((blocks[i]->type == INPAD)||(blocks[i]->type == OUTPAD))
	max_pin = 1;
      else
	max_pin = fg->pins_per_clb;
      for (j=0;j<max_pin;j++)
	{
	  if ( blocks[i]->nets[j] != OPEN )
	    {
	      // "this" is current net touching current node in Partition  
	      // Net of fpga3d which touches a block in this Partition.
	      // See if it has terminals outside current Partition.
	      // If so, see the location of the external terminals 
	      // relative to center of current partition:
	      //       |
	      //   -3  |  -4 o----------o
	      //       |
	      // ----------------
	      //       |
	      //   -1  |  -2
	      //       |
	      // Replace connection to external terminal with connection
	      // to appropriate dummy node.

	      // So, visit all terminals of this discovered net.
	      for (k=0;k<fg->net[blocks[i]->nets[j]].num_pins;k++)
		{
		  loc_x = 
		    fg->block[ fg->net[blocks[i]->nets[j]].blocks[k] ].x;
		  loc_y = 
		    fg->block[ fg->net[blocks[i]->nets[j]].blocks[k] ].y;
		  //cout<<" "<<loc_x<<","<<loc_y<<" ";
		  // If terminal is outside Partition
		  if( ((loc_x<blX)||(loc_x>trX)) ||
		      ((loc_y<blY)||(loc_y>trY)) ) {
		    //cout<<" "<<loc_x<<","<<loc_y<<" ";
		    
		    // ---------------------------------------------------------
		    
		    if( (loc_x <= midlex)&&(loc_y <= midley) ) { // --- "-1" --- 
		      // Add it only if not already added.
		      // See if added:
		      s=0;is_in=0;
		      while( s < j1 ) {
			if( mods1[s] == blocks[i]->ID ) is_in=1;
			s++;
		      }
		      if( is_in == 0 ) {
			mods1[ j1 ] = blocks[i]->ID;// add term from curr Partition
			//cout<<"  "<<blocks[i]->ID<<" "<<-1;
			j1++;
		      }
		    }//if
		    
		    // ---------------------------------------------------------

		    if( (loc_x > midlex)&&(loc_y <= midley) ) { // --- "-2" --- 
		      // Add it only if not already added.
		      // See if added:
		      s=0;is_in=0;
		      while( s < j2 ) {
			if( mods2[s] == blocks[i]->ID ) is_in=1;
			s++;
		      }		      
		      if( is_in == 0 ) {
			mods2[ j2 ] = blocks[i]->ID;// add term from curr Partition
			//cout<<"  "<<blocks[i]->ID<<" "<<-2;
			j2++;
		      }
		    }//if
		    
		    // ---------------------------------------------------------
		    
		    if( (loc_x <= midlex)&&(loc_y > midley) ) { // --- "-3" --- 
		      // Add it only if not already added.
		      // See if added:
		      s=0;is_in=0;
		      while( s < j3 ) {
			if( mods3[s] == blocks[i]->ID ) is_in=1;
			s++;
		      }		      
		      if( is_in == 0 ) {		    
			mods3[ j3 ] = blocks[i]->ID;// add term from curr Partition
			//cout<<"  "<<blocks[i]->ID<<" "<<-3;
			j3++;
		      }
		    }//if
		    
		    // ---------------------------------------------------------
		    
		    if( (loc_x > midlex)&&(loc_y > midley) ) { // --- "-4" --- 
		      // Add it only if not already added.
		      // See if added:
		      s=0;is_in=0;
		      while( s < j4 ) {
			if( mods4[s] == blocks[i]->ID ) is_in=1;
			s++;
		      }		      
		      if( is_in == 0 ) {		    
			mods4[ j4 ] = blocks[i]->ID;// add term from curr Partition
			//cout<<"  "<<blocks[i]->ID<<" "<<-4;
			j4++;
		      }
		    }//if
		    
		    // ---------------------------------------------------------
		  }//if
		}//for
	    }//if
	}//for
    }//for
  int num_nor_nets = pot_nets.size();
  metis->AddNet( num_nor_nets, (j1-1), mods1 );
  metis->setNetWeight( num_nor_nets, 20 );
  num_nor_nets++;
  metis->AddNet( num_nor_nets, (j2-1), mods2 );
  metis->setNetWeight( num_nor_nets, 20 );
  num_nor_nets++;
  metis->AddNet( num_nor_nets, (j3-1), mods3 );
  metis->setNetWeight( num_nor_nets, 20 );
  num_nor_nets++;
  metis->AddNet( num_nor_nets, (j4-1), mods4 );
  metis->setNetWeight( num_nor_nets, 20 );
  delete mods1;
  delete mods2; 
  delete mods3;
  delete mods4;
}//eof
// ======================================================================= //
