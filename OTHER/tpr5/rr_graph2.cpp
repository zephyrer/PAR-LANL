
#include <stdio.h>
#include <assert.h>
#include <iostream>

#include "util.h"
#include "ipr_types.h"
#include "fpga3d.h"


using namespace std;

// ======================================================================= //


// =================== Subroutine definitions ============================ //

// ======================================================================= //
t_seg_details *s_fpga3d::alloc_and_load_seg_details (
                      int nodes_per_chan,
                      int vias_per_zchan,
		      t_segment_inf *segment_inf,
		      int num_seg_types, 
		      int num_via_types,
		      int max_len,
		      t_rr_type chan_type ) 
{
// Allocates and loads the seg_details data structure.  Max_len gives the   
// maximum length of a segment (dimension of array).  The code below tries  
// to:                                                                      
// (1) stagger the start points of segments of the same type evenly;        
// (2) spread out the limited number of connection boxes or switch boxes    
//     evenly along the length of a segment, starting at the segment ends;  
// (3) stagger the connection and switch boxes on different long lines,     
//     as they will not be staggered by different segment start points.     
  int i=0, j=0;
  int tracks_left=0, next_track=0, ntracks=0, itrack=0, length=0, index=0;
  int wire_switch=0, opin_switch=0, num_cb=0, num_sb=0;
  float frac_left=0, start_step=0, sb_step=0, cb_step=0;
  float cb_off_step=0, sb_off_step=0;
  t_seg_details *seg_details;
  bool longline;


if ( (chan_type == CHANX) || (chan_type == CHANY) )  
{
// =================================================================
 seg_details = (t_seg_details *) my_malloc (nodes_per_chan * sizeof 
                    (t_seg_details));

 tracks_left = nodes_per_chan;// Start with all possible first.
 next_track = 0;
 frac_left = 1.;

 // Browseaza toate segment-types.
 for (i=0;i<num_seg_types;i++) {
    if (frac_left <= 0.) {
       ntracks = 0;
    }
    else {
       ntracks = nint (segment_inf[i].frequency / frac_left * tracks_left);
    }
    frac_left -= segment_inf[i].frequency;
    tracks_left -= ntracks;
    
    printf(" segment_type=%d ntracks=%d ",i,ntracks);
    if (ntracks == 0) 
       continue;      /* No tracks of this length.  Avoid divide by 0, etc. */

    longline = segment_inf[i].longline;
 
    if (!longline) {  
       length = min (segment_inf[i].length, max_len);
       start_step = (float) length / (float) ntracks;
    }
    else {                                 /* Is a long line. */
       length = max_len;
       start_step = 0.;
    }

    // Number of connection boxes and switch boxes.
    num_cb = nint (length * segment_inf[i].frac_cb);
    num_sb = nint ((length + 1.) * segment_inf[i].frac_sb);

/* Distribute connection boxes, with two endpoints covered first.           */

    if (!longline) {
       if (num_cb > 1) 
          cb_step = (float) (length-1) / (float) (num_cb - 1);
       else
          cb_step = 0.;
    }

/* Treat longlines differently.  Distribute C and S blocks as if the        *
 * longlines were circular (i.e., don't cover both endpoints first).  This  *
 * is important, as the rotation of the C and S blocks from line to line    *
 * would cause stupidities otherwise.                                       */

    else {   
       if (num_cb > 0)
          cb_step = (float) length / (float) num_cb;
       else
          cb_step = 0.;
    }


/* Now spread out switch boxes in the same way.     */

    if (!longline) {
       if (num_sb > 1) 
          sb_step = (float) length / (float) (num_sb - 1);
       else
          sb_step = 0.;
    }

    else {    /* Is a longline */
       if (num_sb > 0) 
          sb_step = (float) (length + 1) / (float) num_sb;
       else
          sb_step = 0;
    }

    if (!longline) {
       cb_off_step = 0.;
       sb_off_step = 0.;
    }

    else {                                 /* Is a long line. */
       cb_off_step = cb_step / (float) ntracks;
       sb_off_step = sb_step / (float) ntracks;
    }

    // segment_inf[i] a venit ca argument in aceasta functie.
    // wire_switch:  Index of the switch type that connects other wires *to* 
    //               this segment.
    wire_switch = segment_inf[i].wire_switch;
    opin_switch = segment_inf[i].opin_switch;
 
    // Browseaza toate tracks cu acest tip de segment.
    for (itrack=0;itrack<ntracks;itrack++) {
       seg_details[next_track].length = length;
       seg_details[next_track].longline = longline;
 
       seg_details[next_track].start = nint (itrack * start_step) % length + 1;
 
       seg_details[next_track].cb = (bool *) my_calloc (length, 
                           sizeof (bool));

       seg_details[next_track].sb = (bool *) my_calloc ((length+1),
                            sizeof (bool));

       // Mergind de unde incepe acest track-wire-segment si pina unde se
       // termina seteaza true or false daca are switch box or connection 
       // on it.
       for (j=0;j<num_cb;j++) {
          index = nint (j * cb_step + itrack * cb_off_step) % length;
          seg_details[next_track].cb[index] = true;
       }    

       // [  ?  ] cristinel.ababei
       for (j=0;j<num_sb;j++) {
          index = nint (j * sb_step + itrack * sb_off_step) % (length + 1);
          seg_details[next_track].sb[index] = true;
       }    
 
       seg_details[next_track].Rmetal = segment_inf[i].Rmetal;
       seg_details[next_track].Cmetal = segment_inf[i].Cmetal;

       seg_details[next_track].wire_switch = wire_switch;
       seg_details[next_track].opin_switch = opin_switch;

       seg_details[next_track].index = i;

       next_track++;
    }
 }   /* End for each segment type. */
 
 assert (tracks_left == 0);
 assert (next_track == nodes_per_chan);
 return (seg_details);
// =================================================================
} 
 else // i.e., CHANZ
{
// =================================================================
// cristinel.ababei

  if ( ( num_via_types == 1 ) || // num_via_types a venit ca argument,
                                 // and via *should* be of length 1.
       ( num_layers <= 2 ) ) {   // Because anyway for 2 layers I can have
                                 // only vias of length one, irrespective
                                 // of what .arch says.  So in this case
                                 // num_layers is more powerfull for decission. 
    // ARCHITECTURE -- I -- SINGLE VERTICAL VIAS
    // ---------------------------------------------------------------
    // ---> So, 1st perform a sanity check.
    if ( segment_inf[0].length > 1 ) {
      printf("Error in alloc_and_load_seg_details: "
	     " arch file dictates a single type of via with "
	     " length=%d.  Expected length 1 only in this case.\n",
	     segment_inf[0].length);
      exit (1);
    }//if

    seg_details = (t_seg_details *) my_malloc ((vias_per_zchan+1)*sizeof 
					       (t_seg_details));

    next_track = 0;
    // Browseaza toate via-types.
    //  OBS:  num_via_types = 1 for simplicity!
    for (i=0;i<1;i++) { // No matter actually what "num_via_types" is from .arch
                        // file, because this builds only vias with length 1
                        // and this is the only type in the case of this global
                        // if branch.
      
      // There are vias_per_zchan tracks of a single type.
      ntracks = vias_per_zchan; // Asta e ce modific in .arch file!
      
      length = 1;
      
      // Number of connection boxes and switch boxes.
      num_cb = 0; 
      num_sb = 2; 
      
      sb_step = 1;
      
      // segment_inf[i] a venit ca argument in aceasta functie.
      // wire_switch:  Index of the switch type that connects other wires *to* 
      //               this segment.
      wire_switch = segment_inf[i].wire_switch;// Only one via type; i=0
      opin_switch = segment_inf[i].opin_switch;// Only one via type; i=0
      
      // Browseaza toate tracks cu acest tip de segment, care e de 
      // fapt un singur type.
      for (itrack=0;itrack<ntracks;itrack++) {
	seg_details[next_track].length = length;
	seg_details[next_track].longline = false;
	
	// start:  index at which a segment starts in channel 0.
	seg_details[next_track].start = 0;
	seg_details[next_track].cb = 0;
	seg_details[next_track].sb = (bool *) my_calloc ((length+1),// 2
							 sizeof (bool));
	
	for (j=0;j<num_sb;j++) {
	  seg_details[next_track].sb[j] = true;
	}    
	
	seg_details[next_track].Rmetal = segment_inf[i].Rmetal;// i=0
	seg_details[next_track].Cmetal = segment_inf[i].Cmetal;// i=0
	
	// wire_switch:  Index of the switch type that connects other wires *to*
	//          this segment.                                             
	// opin_switch:  Index of the switch type that connects output pins (OPINs)
	//               *to* this segment.  
	seg_details[next_track].wire_switch = wire_switch;// All buffered???
	seg_details[next_track].opin_switch = opin_switch;
	
	seg_details[next_track].index = i;// i=0
	
	next_track++;
      }
    }// End for each via type.
  }// if ( num_via_types == 1 ) {
  // ---------------------------------------------------------------
  else { 
    // ARCHITECTURE -- II -- MULTIPLE VERTICAL VIAS
    // cristinel.ababei: I will play only with vertical length
    // 1, 2 and longvia
    // ---------------------------------------------------------------
    // oBs:  Because here we are in if branch for CHANZ "segment_inf"
    // has to be transmited via arguments with the meaning for channel z
    // Deci atentie aici. Also, "max_len", which in this case comes as
    // argument and is num_layers.

    seg_details = (t_seg_details *) my_malloc ((vias_per_zchan)*sizeof 
					       (t_seg_details));

    tracks_left = vias_per_zchan;// Start with all possible first.
    next_track = 0; // Will count all tracks [0..vias_per_zchan-1].
    frac_left = 1.;

    // Browseaza toate *via-types*: { 1, 2, longvia }
    for (i=0;i<num_via_types;i++) { 
      if (frac_left <= 0.) {
	ntracks = 0;
      }
      else {
	ntracks = nint (segment_inf[i].frequency / frac_left * tracks_left);
	// eg, | 0.4/1.0 * 5 | = 2
      }
      frac_left   -= segment_inf[i].frequency;
      tracks_left -= ntracks;

      printf(" via_type=%d ntracks=%d ",i,ntracks);
      if (ntracks == 0) 
	continue; // No tracks of this length. Avoid divide by 0, etc.

      longline = segment_inf[i].longline;
 
      if (!longline) {  
	length = min (segment_inf[i].length, max_len);
	start_step = (float) length / (float) ntracks;
	//cout<<" start_step="<<start_step;
	// eg, 2.0 / 2.0 = 1
      }
      else {                                 // Is a long line.
	length = max_len; // ie, num_layers.
	start_step = 0.;
      }

      // Number of connection boxes and switch boxes.
      num_cb = 0;
      num_sb = nint ((length + 1.) * segment_inf[i].frac_sb);
      // eg, (3+1)*1.0 = 4.0


      //  a)  No connection boxes to distribute in case of vertical vias,
      // therefore:
      cb_step = 0.;
      //  b)  Now spread out switch boxes.
      if (!longline) {
	if (num_sb > 1) 
          sb_step = (float) length / (float) (num_sb - 1);
	else
          sb_step = 0.;
      }
      else {                                 // Is a longvia.
	if (num_sb > 0) 
          sb_step = (float) (length + 1) / (float) num_sb;
	// eg, (3+1)/4.0 = 1.0
	else
          sb_step = 0;
      }

      if (!longline) {
	cb_off_step = 0.;
	sb_off_step = 0.;
      }
      else {                                     // Is a long via.
	cb_off_step = cb_step / (float) ntracks;
	sb_off_step = sb_step / (float) ntracks;
	// eg, 1.0 / 2.0 = 0.5                   [ ? ]
      }


      // segment_inf[i] a venit ca argument in aceasta functie.
      // wire_switch:  Index of the switch type that connects other wires *to* 
      //               this segment.
      wire_switch = segment_inf[i].wire_switch;
      opin_switch = segment_inf[i].opin_switch;


      // Browseaza toate tracks cu acest tip de segment.
      for (itrack=0;itrack<ntracks;itrack++) {
	seg_details[next_track].length = length;
	seg_details[next_track].longline = longline;// True for longvia only.

	seg_details[next_track].start = nint (itrack * start_step) % length;
	//printf(" s=%d",seg_details[next_track].start);
	// longvia:  
	// start_step=0. --> .start=0  because z index starts from zero.
	// all other vias, for example:  
	// start_step=1. --> .start=0, 1, etc.
	// si in felul asta as putea avea length 2 tracks starting from 
	// 0,0,1,1,and 2 totaling 5 tracks of this sort.
	// oBs:  asta e un fel de ofset cu care incep tracks de un anumit fel
	// incepind cu de la zero si urcind.
	seg_details[next_track].cb = 0; // No connection-boxes on vertical vias.
	seg_details[next_track].sb = (bool *) my_calloc ((length+1),
							 sizeof (bool));
	
	// Mergind de unde incepe acest track-wire-segment si pina unde se
	// termina seteaza true or false daca are switch box on it.
	for (j=0;j<num_sb;j++) {
	  index = nint (j * sb_step + itrack * sb_off_step) % (length + 1);
	  seg_details[next_track].sb[index] = true;
	}   
	
	seg_details[next_track].Rmetal = segment_inf[i].Rmetal;
	seg_details[next_track].Cmetal = segment_inf[i].Cmetal;

	// wire_switch:  Index of the switch type that connects other wires *to*
	//          this segment.                                             
	// opin_switch:  Index of the switch type that connects output pins (OPINs)
	//               *to* this segment.  
	seg_details[next_track].wire_switch = wire_switch;// All buffered???
	seg_details[next_track].opin_switch = opin_switch;
	
	seg_details[next_track].index = i;
	
	next_track++;
	//printf(" %d",next_track);
      }//for(itrack=0;
    }// End for each via type.
    

  }// if else
  // ---------------------------------------------------------------



  assert (next_track == vias_per_zchan);
  return (seg_details);
// =================================================================
}//if ( (chan_type == CHANX) || (chan_type == CHANY) ) else 




}//eof
// ======================================================================= //

 

 
void s_fpga3d::free_seg_details (t_seg_details *seg_details, int nodes_per_chan) 
{
// Frees all the memory allocated to an array of seg_details structures.
  int i=0;

  for (i=0;i<nodes_per_chan;i++) {
    free (seg_details[i].cb);
    free (seg_details[i].sb);
  }
  free (seg_details);
}//eof
// ======================================================================= //

void s_fpga3d::dump_seg_details (t_seg_details *seg_details, 
				 int nodes_per_chan, char *fname) {

/* Dumps out an array of seg_details structures to file fname.  Used only   *
 * for debugging.                                                           */

 FILE *fp;
 int i, j;

 fp = my_fopen (fname, "w", 0);

 for (i=0;i<nodes_per_chan;i++) {
    fprintf (fp, "Track: %d.\n", i);
    fprintf (fp, "Length: %d,  Start: %d,  Long line: %d  wire_switch: %d  "
        "opin_switch: %d.\n", seg_details[i].length, seg_details[i].start, 
        seg_details[i].longline, seg_details[i].wire_switch, 
        seg_details[i].opin_switch);

    fprintf (fp, "Rmetal: %g  Cmetal: %g\n", seg_details[i].Rmetal, 
        seg_details[i].Cmetal);

    fprintf (fp, "cb list: ");
    for (j=0;j<seg_details[i].length;j++)
       fprintf (fp, "%d ", seg_details[i].cb[j]);

    fprintf (fp, "\nsb list: ");
    for (j=0;j<=seg_details[i].length;j++)
       fprintf (fp, "%d ", seg_details[i].sb[j]);

    fprintf (fp, "\n\n");
 } 

 fclose (fp);
}
// ======================================================================= //

int s_fpga3d::get_closest_seg_start (t_seg_details *seg_details, 
				     int itrack, int seg_num,
				     int chan_num) 
{
// Returns the segment number at which the segment this track lies on 
// started.                                                           

 int closest_start, length, start;

 if (seg_details[itrack].longline) {
    closest_start = 1;
 }
 else {
    length = seg_details[itrack].length;
    start = seg_details[itrack].start;

    // Start is guaranteed to be between 1 and length.  Hence adding length to
    // the quantity in brackets below guarantees it will be nonnegative.
    closest_start = seg_num - (seg_num + chan_num - start + length) % length;
    closest_start = max (closest_start, 1);
 }

 return (closest_start);
}//end
// ======================================================================= //


int s_fpga3d::get_clb_opin_connections (int ***clb_opin_to_tracks, int ipin, 
	int i, int j, int k, 
	int Fc_output, t_seg_details *seg_details_x, t_seg_details 
        *seg_details_y, t_linked_edge **edge_list_ptr, int nodes_per_chan, int 
        **rr_node_indices) 
{
  // Returns the number of tracks to which clb opin #ipin at (i,j) connects.
  // Also stores the nodes to which this pin connects in the linked list    
  // pointed to by *edge_list_ptr.                                            

 int iside, num_conn, iconn, itrack, tr_j, tr_i, to_node;
 t_rr_type to_rr_type;
 bool cbox_exists;
 t_linked_edge *edge_list_head;// Will process edge_list_ptr received by ref
 t_seg_details *seg_details;

 edge_list_head = *edge_list_ptr;
 num_conn = 0;

 for (iside=0;iside<=3;iside++) {  
    if (clb_opin_to_tracks[ipin][iside][0] != OPEN) {
 
/* This track may be at a different (i,j) location than the clb.  Tracks *
 * above and to the right of a clb are at the same (i,j) -- the channels *
 * on the other two sides "belong" to different clbs.                    */
 
       if (iside == BOTTOM)
          tr_j = j-1;
       else
          tr_j = j;
 
       if (iside == LEFT)
          tr_i = i-1;
       else
          tr_i = i;
 
       if (iside == LEFT || iside == RIGHT) {
          to_rr_type = CHANY;
          seg_details = seg_details_y;
       }
       else {
          to_rr_type = CHANX;
          seg_details = seg_details_x;
       }
                 
       for (iconn=0;iconn<Fc_output;iconn++) {
          itrack = clb_opin_to_tracks[ipin][iside][iconn]; 
 
          if (to_rr_type == CHANX) 
             cbox_exists = is_cbox (tr_i, tr_j, itrack, seg_details);
          else
             cbox_exists = is_cbox (tr_j, tr_i, itrack, seg_details);

          if (cbox_exists) {
             to_node = get_rr_node_index (tr_i,tr_j,k, to_rr_type, itrack,
                           nodes_per_chan, rr_node_indices);
             edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                       seg_details[itrack].opin_switch, &free_edge_list_head);
             num_conn++;
          }
       }
           
    }
 }//for

 if (num_conn == 0) {
    printf ("Error:  clb output pin %d at (%d,%d,%d) does not connect to any "
            "tracks.\n", ipin, i,j,k);
    exit (1);
 }

 *edge_list_ptr = edge_list_head;
 return (num_conn);
}//eof
// ======================================================================= //

int s_fpga3d::get_pad_opin_connections (int **pads_to_tracks, int ipad, 
	int i, int j, int k, 
        int Fc_pad, t_seg_details *seg_details_x, t_seg_details *seg_details_y, 
        t_linked_edge **edge_list_ptr, int nodes_per_chan, int 
        **rr_node_indices) {

/* Returns the number of tracks to which the pad opin at (i,j) connects.     *
 * If edge_ptr isn't NULL, it also loads the edge array.                     */

 int chan_i, chan_j, iconn, itrack, num_conn, to_node;
 t_rr_type chan_type;
 bool cbox_exists;
 t_linked_edge *edge_list_head;
 t_seg_details *seg_details;

/* Find location of adjacent channel. */

 if (j == 0) {                 /* Bottom row of pads. */
    chan_i = i;
    chan_j = j;
    chan_type = CHANX;
    seg_details = seg_details_x;
 }
 else if (j == ny+1) {
    chan_i = i;
    chan_j = j-1;
    chan_type = CHANX;
    seg_details = seg_details_x;
 }
 else if (i == 0) {
    chan_i = i;
    chan_j = j;
    chan_type = CHANY;
    seg_details = seg_details_y;
 }
 else if (i == nx+1) {
    chan_i = i-1;
    chan_j = j;
    chan_type = CHANY;
    seg_details = seg_details_y;
 }
 else {
    printf ("Error in get_pad_opin_connections:  requested IO block at "
            "(%d,%d,%d) does not exist.\n", i,j,k);
    exit (1);
 }

 edge_list_head = *edge_list_ptr;
 num_conn = 0;

 for (iconn=0;iconn<Fc_pad;iconn++) {
    itrack = pads_to_tracks[ipad][iconn];

    if (chan_type == CHANX)
       cbox_exists = is_cbox (chan_i, chan_j, itrack, seg_details);
    else
       cbox_exists = is_cbox (chan_j, chan_i, itrack, seg_details);

    if (cbox_exists) {
       to_node = get_rr_node_index (chan_i,chan_j,k, chan_type, itrack,
             nodes_per_chan, rr_node_indices);
       edge_list_head = insert_in_edge_list (edge_list_head, to_node, 
                       seg_details[itrack].opin_switch, &free_edge_list_head);
       num_conn++;
    }
 } 

 if (num_conn == 0) {
    printf ("Error:  INPAD %d at (%d,%d,%d) does not connect to any "
            "tracks.\n", ipad, i,j,k);
    exit (1);
 }

 *edge_list_ptr = edge_list_head;
 return (num_conn);
}
// ======================================================================= //

bool s_fpga3d::is_cbox (int seg_num, int chan_num, int itrack, t_seg_details 
       *seg_details) {

/* Returns 1 (true) if the track segment at this segment and channel         *
 * location with track number itrack contains a connection box, 0 otherwise. */

 int seg_offset, start, length;

 length = seg_details[itrack].length;
 start = seg_details[itrack].start;

 seg_offset = (seg_num + chan_num - start + length) % length;
 return (seg_details[itrack].cb[seg_offset]);
}
// ======================================================================= //

int ***s_fpga3d::alloc_and_load_rr_node_indices (
         int nodes_per_clb, int nodes_per_pad, 
	 int nodes_per_chan, int vias_per_zchan,
	 int num_via_types, // Num of different vias in arch file.
	 t_seg_details *seg_details_x,
	 t_seg_details *seg_details_y,
	 t_seg_details *seg_details_z ) 
{
 //
 // Allocates and loads all the structures needed for fast lookups of the   
 // index of an rr_node.  
 // rr_node_indices is a matrix containing the index  
 // of the *first* rr_node at a given (i,j) location.  
 // The chanx_rr_indices and chany_rr_indices data structures give 
 // the  rr_node  index for each track in each (i,j,k) channel segment.
 // "k" is the layer identifier.                                 
 
 int index, i=0,j=0,k=0;
 int ***rr_node_indices; // Will be returned 

 rr_node_indices = (int ***) alloc_matrix3 (0,num_layers, 0,nx+1, 0,ny+1, 
					    sizeof (int));
 

 // cristinel.ababei
 // Question: Shall I use a single "rr_node_indices" to count rr_node's
 // in all layers, or I should declare one for each layer separately?
 // For now I use a single one.
 // In this case then these data structures, chanx_rr_indices[k] and 
 // chany_rr_indices[k] would contain different info for different 
 // layers).

 // Next reset is done only once to count from zero (starting with rr_node
 // on first layer) and continuing for the rest of layers.
 index = 0;


 // Inside these for loops is basically the place where rr_node_indices
 // - for all k layers - are set. 
 for (k=0;k<num_layers;k++) { // cristinel.ababei


   chanx_rr_indices[k] = (int ***) alloc_matrix3 (1,nx, 0,ny, 0,nodes_per_chan - 1,
						  sizeof (int)); 
   chany_rr_indices[k] = (int ***) alloc_matrix3 (0,nx, 1,ny, 0,nodes_per_chan - 1,
						  sizeof (int)); 
   // chanx_rr_indices, chany_rr_indices: give rr_node  index for 
   // each track in each (i,j,k) channel segment for quick access
   for (i=0;i<=nx+1;i++) { 
     for (j=0;j<=ny+1;j++) {


       // 1st time is zero and will all the time be used as some sort of
       // "offset" or "bias" starting at which rr_nodes due to clb at
       // location i,j are enumerated.
       rr_node_indices[k][i][j] = index;
       // OBS: rr_node_indices[k][i][j] vor contine numere de genul 0 7 15...
       // ptr ca index de fapt sweeps all rr_nodes created for all clbs 
       // (each clb is represented by nodes_per_clb) insa numai "inceputul"
       // unei secvente de rr_node (coresp to clb and its channels) este stored
       // in rr_node_indices[k][i][j];  then there are also created
       // rr_nodes for channels x and y at the location i,j.
       
       if (clb[i][j][k].type == CLB) { 
	 index += nodes_per_clb; 
	 index = load_chanx_rr_indices (seg_details_x, nodes_per_chan, index, 
					i,j,k);//see this function below
	 index = load_chany_rr_indices (seg_details_y, nodes_per_chan, index,
					i,j,k);
       } 
       
       else if (clb[i][j][k].type == IO) {  
	 index += nodes_per_pad;

	 if (j == 0)    // Bottom row
	   index = load_chanx_rr_indices (seg_details_x, nodes_per_chan, index, 
					  i,j,k);
	 
	 if (i == 0)    // Leftmost column
	   index = load_chany_rr_indices (seg_details_y, nodes_per_chan, index,
					  i,j,k);
       } 
       
       else if (clb[i][j][k].type != ILLEGAL) {
	 printf("Error in alloc_and_load_rr_node_indices.  Unexpected clb"
		" type.\n");
	 exit (1);
       } 


     }
   }
 }//for (k=0;k<num_layers;k++)


 num_rr_nodes_horizontal = index;


 
 // cristinel.ababei
 // At this time all indices of all rr_nodes corresponding to clbs
 // and their channels x,y are enumerated.  Next I enumerate all indices
 // for vertical channels (i.e., chanz) as a continuation of
 // enumerating all rr_nodes of the 3d architecture.
 if ( num_layers > 1 ) {
   if ( (num_via_types == 1) ||    // Vias only of length one.
	(num_layers <= 2) )        // Because anyway for 2 layers I can have
                                   // only vias of length one, irrespective
                                   // of what .arch says.  So in this case
                                   // num_layers is more powerfull for decission. 
     index = alloc_and_load_chanz_rr_indices (vias_per_zchan, index); 
   else                          // Mixed vias.
     index = alloc_and_load_chanz_rr_indices_version_02 (
		  seg_details_z, vias_per_zchan, index); 
 }
 


 // VERY IMPORTANT: now we know how many rr_nodes are in total
 // so we set num_rr_nodes
 num_rr_nodes = index;
 num_rr_nodes_vias =  num_rr_nodes - num_rr_nodes_horizontal;
 printf(" | rr_nodes | = %d.\n",num_rr_nodes);
 //printf("Done alloc_and_load_rr_node_indices.\n");

 return (rr_node_indices);
}//eof

// ======================================================================= //
// ======================================================================= //

int s_fpga3d::alloc_and_load_chanz_rr_indices ( 
                // t_seg_details *seg_details_z, // NOT used currently. 
	        // int zchan_density,            // NOT used currently.
	        int vias_per_zchan,
		int start_index )
{
// OBS: this routine should be called once only if num_layers > 1.
//
// cristinel.ababei
// Loads the chanz_rr_indices array for all vertical connections/vias.
// Vias are only between two consecutive layers, for now.
// A channel "z" is a bunch of "vias_per_zchan" vertical wires between 
// two switch-boxes situated in two different-and-adjacent layers.
// The number of vias in a z-chan should be varied from 1 up
// by means of    vias_per_zchan    parameter!
// Symplifying RULE: Vias (when less than the number of tracks
// in channels, as it will be the case) will connect to first tracks.
// Also, the density of z-chan's should be varied by a global variable; 
// in order to have more sparsity vertically; we could have z-chan
// only the 1st, the 4th, the 7th etc. rows and columns of clb's
// because anyway (technologically speaking) too many vertical vias are
// not possible, due to their big pitch!
// Channels z exist *only* between switch-boxes, which have neighbor clbs in
// all four corners.  In other words first channel z will be between
// switch-boxes located at i=1,j=1.  
// There is no channel z at i=0,j=any or i=any,j=0 or i=nx,j=any or i=any,j=ny.
// Switch-boxes on layer 0 and L-1 are having vias going below and above
// (asuming counting from top-to-bottom) while switch-boxes in layers 
// 1..L-2 have vias going both below and above.

  int i=0,j=0,k=0;
  int rr_index=0,itrack=0;
  rr_index = start_index; // Will count all rr_nodes for all vias.


  chanz_rr_indices = (int ****) my_calloc (num_layers, sizeof(int ***));


  for (k=0;k<(num_layers-1);k++) { /*  0..L-2  */

    chanz_rr_indices[k] = 
      (int ***) alloc_matrix3 (0,nx, 0,ny, 0,vias_per_zchan, sizeof(int)); 

    for (i=1; /* not from 0 */ i<=nx-1; /* not till nx+1 */ i++) { 
      for (j=1; /* not from 0 */ j<=ny-1; /* not till ny+1 */ j++) {

	
	// At "location" i,j between layers k,k+1 we have a number 
	// of vias_per_zchan vias, which is smaller than the number
	// of tracks in x-chan and y-chan's.  Keep that in mind!
	for (itrack=0;itrack<vias_per_zchan;itrack++) {
	  // ------------------------------------------
	  chanz_rr_indices[k][i][j][itrack] = rr_index;
	  rr_index++;
	  // ------------------------------------------
	}//for

	
      }//for
    }//for
  }//for
  
  //printf("Done alloc_and_load_chanz_rr_indices.\n");
  return (rr_index);
}//eof

// ======================================================================= //
// ======================================================================= //

int s_fpga3d::load_chanx_rr_indices (t_seg_details *seg_details_x, int 
           nodes_per_chan, int start_index, int i,int j,int k) 
{
// Loads the chanx_rr_indices array for all track segments starting at
// (i,j,k), assuming the first segment found has index start_index.  It also 
// returns the next free index (i.e. the index for the next rr_node).     
  int rr_index=0, istart=0, iend=0, iseg=0, itrack=0;

  rr_index = start_index; // will be worked on and returned

  for (itrack=0;itrack<nodes_per_chan;itrack++) {
    istart = get_closest_seg_start (seg_details_x, itrack, i, j);
    
    /* Don't do anything if this isn't the start of the segment. */
    
    if (istart != i)
      continue;
   
    iend = get_seg_end (seg_details_x, itrack, istart, j, nx);
    
    for (iseg=istart;iseg<=iend;iseg++) 
      chanx_rr_indices[k][iseg][j][itrack] = rr_index;//cristia: added [k]
    
    rr_index++;
  }//for
  
 return (rr_index);
}//eof
// ======================================================================= //

int s_fpga3d::load_chany_rr_indices (t_seg_details *seg_details_y, int 
           nodes_per_chan, int start_index, int i,int j,int k) 
{
 
/* Loads the chany_rr_indices array for all track segments starting at     *
 * (i,j), assuming the first segment found has index start_index.  It also *
 * returns the next free index (i.e. the index for the next rr_node).      */
 
 int rr_index, jstart, jend, jseg, itrack;

 rr_index = start_index;
 
 for (itrack=0;itrack<nodes_per_chan;itrack++) {
    jstart = get_closest_seg_start (seg_details_y, itrack, j, i);
 
    /* Don't do anything if this isn't the start of the segment. */
  
    if (jstart != j)
       continue; 
    
    jend = get_seg_end (seg_details_y, itrack, jstart, i, ny);
    
    for (jseg=jstart;jseg<=jend;jseg++) 
       chany_rr_indices[k][i][jseg][itrack] = rr_index;//cristia: added [k]  
 
    rr_index++; 
 } 
  
 return (rr_index); 
}//eof

// ======================================================================= //

void s_fpga3d::free_rr_node_indices (int ***rr_node_indices) 
{
// Frees all the rr_node_indices structures allocated for fast index 
// computations.  
  int k=0;

  free_matrix3 (rr_node_indices, 0,num_layers, 0,nx+1, 0, sizeof (int));

  for (k=0;k<num_layers;k++) { // cristinel.ababei
    free_matrix3 (chanx_rr_indices[k], 1,nx, 0,ny, 0, sizeof (int));
    free_matrix3 (chany_rr_indices[k], 0,nx, 1,ny, 0, sizeof (int));
  }

  for (k=0;k<(num_layers-1);k++) { // 0..L-2 
    free_matrix3 (chanz_rr_indices[k], 0,nx, 0,ny, 0, sizeof (int));
  }

}//eof

// ======================================================================= //

int s_fpga3d::get_rr_node_index (int i,int j,int k, 
				 t_rr_type rr_type, 
				 int ioff, 
				 int nodes_per_chan, 
				 int **rr_node_indices) 
{
// OBS: "rr_node_indices" is of a particular "k".  This is the way
// layer information is transmitted also during this inquiry;  apart from 
// argument itself k...
//
//  Returns the index of the specified routing resource node.  (i,j,k) are   
//  the location within the FPGA, rr_type specifies the type of resource,    
//  and ioff gives the number of this resource.  
//
//  ioff is the class number, pin number or track number, 
//  depending on what type of resource this is.    
//  All ioffs start at 0 and go up to pins_per_clb-1 or the equivalent. 
//  The order within a clb is:  SOURCEs + SINKs (num_class of them); IPINs,  
//  and OPINs (pins_per_clb of them); CHANX; and CHANY (nodes_per_chan of    
//  each).  For (i,j,k) locations that point at pads the order is:  io_rat   
//  occurances of SOURCE, SINK, OPIN, IPIN (one for each pad), then one      
//  associated channel (if there is a channel at (i,j,k)).  All IO pads are  
//  bidirectional, so while each will be used only as an INPAD or as an      
//  OUTPAD, all the switches necessary to do both must be in each pad.       
//                                                                           
//  Note that for segments (CHANX and CHANY) of length > 1, the segment is   
//  given an rr_index based on the (i,j) location at which it starts (i.e.   
//  lowest (i,j) location at which this segment exists).                     
//  This routine also performs error checking to make sure the node in       
//  question exists.

 int index=0,iclass=0;

 // printf(" %d", k);
 assert (ioff >= 0);
 assert (i >= 0 && i < nx + 2);
 assert (j >= 0 && j < ny + 2);
 assert (k >= 0 && k < num_layers);

 index = rr_node_indices[i][j];  // Start (offset so to speak) of that block.
 

 switch (clb[i][j][k].type) {
 
 case CLB:
    switch (rr_type) {
 
    case SOURCE:
       assert (ioff < num_class);
       assert (class_inf[ioff].type == DRIVER);
 
       index += ioff;
       return (index);
  
    case SINK:
       assert (ioff < num_class);
       assert (class_inf[ioff].type == RECEIVER);
  
       index += ioff;
       return (index);
 
    case OPIN:
       assert (ioff < pins_per_clb);
       iclass = clb_pin_class[ioff];
       assert (class_inf[iclass].type == DRIVER);
 
       index += num_class + ioff;
       return (index);
 
    case IPIN:
       assert (ioff < pins_per_clb);
       iclass = clb_pin_class[ioff];
       assert (class_inf[iclass].type == RECEIVER);
 
       index += num_class + ioff;
       return (index);
 
    case CHANX:
       assert (ioff < nodes_per_chan);
       index = chanx_rr_indices[k][i][j][ioff];
       return (index);
 
    case CHANY:
       assert (ioff < nodes_per_chan);
       index = chany_rr_indices[k][i][j][ioff];
       return (index);


       // cristinel.ababei
       // Next, we look at a node corresponding to a via.  The thing
       // works similarly to CHANX based on which 
       // alloc_and_load_chanz_rr_indices() was written.
       // Here, we should change things if density/distribution of vias
       // is different.
    case CHANZ:
      assert (ioff < nodes_per_chan); 
      // nodes_per_chan is actually vias_per_zchan as via argument.
      // ioff is the track number in this case.
      index = chanz_rr_indices[k][i][j][ioff];
      return (index);

 
    default:
       printf ("Error:  Bad rr_node passed to get_rr_node_index.\n"
               "Request for type %d number %d at (%d,%d,%d).\n", rr_type,
               ioff, i,j,k);
       exit (1);
    }
    break;
 
 case IO:
    switch (rr_type) {
 
    case SOURCE:
       assert (ioff < io_rat);
       index += 4 * ioff;
       return (index);
 
    case SINK:
       assert (ioff < io_rat);
       index += 4 * ioff + 1;
       return (index);
 
    case OPIN:
       assert (ioff < io_rat);
       index += 4 * ioff + 2;
       return (index);
 
    case IPIN:
       assert (ioff < io_rat);
       index += 4 * ioff + 3;
       return (index);
 
    case CHANX:
       assert (ioff < nodes_per_chan);
       assert (j == 0);                 // Only one with a channel.
       index = chanx_rr_indices[k][i][j][ioff];
       return (index);
 
    case CHANY:
       assert (ioff < nodes_per_chan);
       assert (i == 0);                 // Only one with a channel.
       index = chany_rr_indices[k][i][j][ioff];
       return (index);
 
    default:
       printf ("Error:  Bad rr_node passed to get_rr_node_index.\n"
               "Request for type %d number %d at (%d,%d,%d).\n", rr_type,
               ioff, i,j,k);
       exit (1);
    }
    break;
 
 default:
    printf("Error in get_rr_node_index:  unexpected block type (%d) at "
           "(%d,%d,%d).\nrr_type: %d.\n", clb[i][j][k].type, i,j,k, rr_type);
    exit (1);
 }
}//eof
// ======================================================================= //

int s_fpga3d::get_seg_end (t_seg_details *seg_details, 
			   int itrack, int seg_start, int
			   chan_num, int max_end) 
{
// Returns the segment number (coordinate along the channel) at which this 
// segment ends.  For a segment spanning clbs from 1 to 4, seg_end is 4.   
// max_end is the maximum dimension of the FPGA in this direction --       
// either nx or ny.                                                        
  int seg_end, length, norm_start;

  length = seg_details[itrack].length;
  
  if (seg_start != 1) {
    seg_end = min (seg_start + length - 1, max_end);
  }
  else if (seg_details[itrack].longline) {
    seg_end = max_end;
  }
  else {        /* Short segment, at the left edge of array. */
    norm_start = seg_details[itrack].start;
    seg_end = length - (length + 1 + chan_num - norm_start) % length;
  }
  
  return (seg_end);
}//eof
// ======================================================================= //

int s_fpga3d::get_xtrack_to_clb_ipin_edges (int tr_istart, 
					    int tr_iend, int tr_j,
       int itrack, int iside, t_linked_edge **edge_list_ptr, struct s_ivec 
       **tracks_to_clb_ipin, int nodes_per_chan, int **rr_node_indices, 
       t_seg_details *seg_details_x, int wire_to_ipin_switch,int k) 
{
/* This routine counts how many connections should be made from this segment *
 * to the clbs above (TOP) or below (BOTTOM) it.   It also adds them to the  *
 * edge linked list and updates edge_list_ptr.                               */

 int clb_j, iconn, num_conn, max_conn, i, ipin, to_node;
 t_linked_edge *edge_list_head;

/* Side is from the *track's* perspective */

 if (iside == BOTTOM) {
    clb_j = tr_j;
 }
 else if (iside == TOP) {
    clb_j = tr_j + 1;
 }
 else {  
    printf ("Error in get_xtrack_to_clb_ipin_edges:  Unknown iside: %d.\n",
            iside);
    exit (1);
 }

 edge_list_head = *edge_list_ptr;
 num_conn = 0;
 max_conn = tracks_to_clb_ipin[itrack][iside].nelem;

 for (i=tr_istart;i<=tr_iend;i++) {
    if (is_cbox (i, tr_j, itrack, seg_details_x)) {
       for (iconn=0;iconn<max_conn;iconn++) {
          ipin = tracks_to_clb_ipin[itrack][iside].list[iconn];
          to_node = get_rr_node_index (i,clb_j,k, 
			  IPIN, ipin, nodes_per_chan, rr_node_indices);
          edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                    wire_to_ipin_switch, &free_edge_list_head);
       }
       num_conn += max_conn;
    }
 }

 *edge_list_ptr = edge_list_head;
 return (num_conn);
}
// ======================================================================= //

int s_fpga3d::get_ytrack_to_clb_ipin_edges (int tr_jstart, 
					    int tr_jend, int tr_i,
       int itrack, int iside, t_linked_edge **edge_list_ptr, struct s_ivec
       **tracks_to_clb_ipin, int nodes_per_chan, int **rr_node_indices,
       t_seg_details *seg_details_y, int wire_to_ipin_switch,int k) 
{
/* This routine counts how many connections should be made from this segment *
 * to the clbs to the LEFT or RIGHT of it.  It also adds them to the edge    *
 * linked list and updates edge_list_ptr.                                    */

 int clb_i, iconn, num_conn, max_conn, j, ipin, to_node;
 t_linked_edge *edge_list_head;

/* Side is from the *track's* perspective */

 if (iside == LEFT) {  
    clb_i = tr_i;
 }
 else if (iside == RIGHT) {
    clb_i = tr_i + 1;   
 }
 else {  
    printf ("Error in get_ytrack_to_clb_ipin_edges:  Unknown iside: %d.\n",
            iside);
    exit (1);
 }
 
 edge_list_head = *edge_list_ptr;
 num_conn = 0;   
 max_conn = tracks_to_clb_ipin[itrack][iside].nelem;
 
 for (j=tr_jstart;j<=tr_jend;j++) {
    if (is_cbox (j, tr_i, itrack, seg_details_y)) {
       for (iconn=0;iconn<max_conn;iconn++) {
          ipin = tracks_to_clb_ipin[itrack][iside].list[iconn];
          to_node = get_rr_node_index (clb_i,j,k, 
		       IPIN, ipin, nodes_per_chan, rr_node_indices);
          edge_list_head = insert_in_edge_list (edge_list_head, to_node, 
                        wire_to_ipin_switch, &free_edge_list_head);
       }
       num_conn += max_conn;
    }   
 }
 
 *edge_list_ptr = edge_list_head;
 return (num_conn); 
}
// ======================================================================= //

int s_fpga3d::get_xtrack_to_pad_edges (int tr_istart, int tr_iend, 
				       int tr_j, int pad_j,
        int itrack, t_linked_edge **edge_list_ptr, struct s_ivec 
        *tracks_to_pads, int nodes_per_chan, int **rr_node_indices, 
        t_seg_details *seg_details_x, int wire_to_ipin_switch,int k) 
{
/* This routine counts how many connections should be made from this segment *
 * to the row of pads above or below it.  It also adds these edges to the    *
 * edge list and updates edge_list_ptr to point to the new list head.        */

 int iconn, ipad, to_node, num_conn, max_conn, i;
 t_linked_edge *edge_list_head;

 edge_list_head = *edge_list_ptr;
 num_conn = 0;
 max_conn = tracks_to_pads[itrack].nelem;

 for (i=tr_istart;i<=tr_iend;i++) {
    if (is_cbox (i, tr_j, itrack, seg_details_x)) {
       for (iconn=0;iconn<max_conn;iconn++) {
          ipad = tracks_to_pads[itrack].list[iconn];
          to_node = get_rr_node_index (i,pad_j,k,
                        IPIN, ipad, nodes_per_chan, rr_node_indices);
          edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                           wire_to_ipin_switch, &free_edge_list_head);
       }
       num_conn += max_conn; 
    }
 }
 
 *edge_list_ptr = edge_list_head;
 return (num_conn);
}
// ======================================================================= //

int s_fpga3d::get_ytrack_to_pad_edges (int tr_jstart, int tr_jend, 
				       int tr_i, int pad_i, 
        int itrack, t_linked_edge **edge_list_ptr, struct s_ivec 
        *tracks_to_pads, int nodes_per_chan, int **rr_node_indices, 
        t_seg_details *seg_details_y, int wire_to_ipin_switch,int k) 
{
/* This routine counts how many connections should be made from this segment *
 * to the row of pads to the left or right of it.  Additionally, if rr_edges *
 * isn't NULL, it will also load the edges array.  Make sure rr_edges points *
 * at the next free spot to put an edge in this case.                        */

 int iconn, ipad, to_node, num_conn, max_conn, j;
 t_linked_edge *edge_list_head;

 edge_list_head = *edge_list_ptr;
 num_conn = 0;
 max_conn = tracks_to_pads[itrack].nelem;

 for (j=tr_jstart;j<=tr_jend;j++) {
    if (is_cbox (j, tr_i, itrack, seg_details_y)) {
       for (iconn=0;iconn<tracks_to_pads[itrack].nelem;iconn++) {
          ipad = tracks_to_pads[itrack].list[iconn];
          to_node = get_rr_node_index (pad_i,j,k, 
			IPIN, ipad, nodes_per_chan, rr_node_indices);
          edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                           wire_to_ipin_switch, &free_edge_list_head);
       } 
       num_conn += max_conn;
    }  
 }
 
 *edge_list_ptr = edge_list_head; 
 return (num_conn);
}//eof


// ======================================================================= //
// ======================================================================= //
// ======================================================================= //


int s_fpga3d::get_xtrack_to_ztracks ( 
               int from_istart, 
	       int from_iend, int from_j, int from_track, int to_j, 
	       t_linked_edge **edge_list_ptr, 
	       int nodes_per_chan,
	       int vias_per_zchan,
	       int **rr_node_indices, 
	       t_seg_details *seg_details_x, 
	       t_seg_details *seg_details_z, 
	       enum e_switch_block_type switch_block_type, int k) 
{
// cristinel.ababei
// 
// oBs: from_j == to_j
//
/* Counts how many connections should be made from this segment to the      *
 * z-segments in the adjacent channels at to_j.  It returns the number of   *
 * connections, and updates edge_list_ptr to point at the head of the       *
 * (extended) linked list giving the nodes to which this segment connects   *
 * and the switch type used to connect to each.                             *
 *                                                                          *
 * An edge is added from this segment to a z-segment (ie, via) if:          *
 * (1) this segment should have a switch box at that location, or           *
 * (2) the z-segment to which it would connect has a switch box, and the    *
 *     switch type of that z-segment is unbuffered (bidirectional pass      *
 *     transistor).                                                         *
 *                                                                          *
 * If the switch in each direction is a pass transistor (unbuffered), both  *
 * switches are marked as being of the types of the larger (lower R) pass   *
 * transistor.  Note that this code implicitly assumes the routing is       *
 * bidirectional (a switch in each direction).                              */


 int num_conn=0, to_node=0, i=0, to_track=0, iconn=0;
 int from_node_switch, to_node_switch;
 short switch_types[2];
 bool is_x_sbox, is_z_sbox;
 t_linked_edge *edge_list_head;
 struct s_ivec conn_tracks;

 num_conn = 0;
 edge_list_head = *edge_list_ptr;// List where edges (1 or 2) are added.


// Recall:  this is the type of switch to use on switches that go *to* this
// node (i.e. the output side is on the from_node).                        

 from_node_switch = seg_details_x[from_track].wire_switch;




 for (i=from_istart;i<=from_iend;i++) { 
   // Acum mergem de-a lungul wire-segmentului de la "from_start"
   // till "from_end" si for every location ma uit to see whether 
   // there should be a switch "on" this segment and if so I have to "look" at 
   // possible connections to tracks (wire-segments) in z channels.


   
   //  ===>  Diagonal connection to above (from xchan to zchan) 
   if ( (k > 0) && 
	(i > 0) && (i < nx) ) { // Look only there are xchan's.
     // ----------------------------------------------------------
     // Next returns true if the specified segment has a switch box at specified  
     // location, false otherwise.  The switch box is from the specified segment 
     // to the left (for chanx) or below (for chany) unless above_or_right is    
     // true.  This is a bad way of implementing the whole VPR: to check all
     // the time this... It would dramatically speed it up if whole
     // routing resources would be embodied in some sort of graph from which to 
     // extract this info directly.
     is_x_sbox = is_sbox (i,from_j, from_track, seg_details_x, false);
   
     // Next function returns a vector of the tracks to which from_track 
     // at (from_i,from_j) x-chan should connect at (k-1) z-chan.

       
     conn_tracks = // Deci astea sint conexiuni in interiorul aceluiasi s-b.
       get_switch_box_tracks_x_to_z (
			      i,from_j,  // Loc. on CHANX during mersului.
			      k,         // Layer in which xchan lies
			      from_track,// Inside xchan
			      CHANX, 
			      i,from_j,  // Which z-chan location
			      k-1,       // Identifies zchan
			      CHANZ ); 

  
 
     // For all the tracks we connect to in that z-channel ... 
     // For the simplest/standard switch-box a track connects only to its 
     // counterpart track.  Hence next "for" loop will be gone thru once.
     for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       // ASUMPTION:  I will simplify the problem by assuming that
       // if track from x-chan connects to track in z-chan, then
       // it's bidirectional.  So,
       is_z_sbox = is_x_sbox;
   
       to_node_switch = seg_details_z[to_track].wire_switch;

    
       // [ 1 ] Generally: 
       // This routine looks at whether the from_node and to_node want a switch,  
       // and what type of switch is used to connect *to* each type of node       
       // (from_node_switch and to_node_switch).  It decides what type of switch, 
       // if any, should be used to go from from_node to to_node.  If no switch   
       // should be inserted (i.e. no connection), it returns OPEN.  Its returned 
       // values are in the switch_types array.  It needs to return an array      
       // because one topology (a buffer in the forward direction and a pass      
       // transistor in the backward direction) results in *two* switches.
       // [ 2 ] Particularly:
       // If there is connection from x-chan track to the z-chan track 
       // then it is bidirectional and therefore I insert two edges.
       // Therefore, I replace:
       // get_switch_type (  is_x_sbox, is_z_sbox, 
       //                    from_node_switch, to_node_switch, 
       //                    switch_types  );
       // With:
       if ( is_x_sbox ) {
	 switch_types[0] = to_node_switch;
	 //switch_types[1] = from_node_switch;
	 switch_types[1] = OPEN;
	 //printf(" %d %d",switch_types[0],switch_types[1]);
       }
       else {
	 switch_types[0] = OPEN;
	 switch_types[1] = OPEN;
       }
       // Which translates into connection in both directions.

       
       if (switch_types[0] != OPEN) {
	 to_node = get_rr_node_index (i,from_j,
				      k-1,// pt ca asa s-au generat indices.
				      CHANZ, to_track, 
				      vias_per_zchan, 
				      rr_node_indices);
	 //printf(" %d",to_node);
	 if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	   num_conn++;
	   rr_edge_done[to_node] = true;
	   edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[0], &free_edge_list_head);
	   
	   if (switch_types[1] != OPEN) {   // A second edge.
	     edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	     num_conn++;
	   }
	 }
       }//if
     }//for (iconn=0;...
     // ----------------------------------------------------------
   }//if
   

   
   //  ===>  Diagonal connection to below (from xchan to zchan) 
   if ( (k < num_layers-1) &&
	(i > 0) && (i < nx) ) { // Look only there are xchan's.
     // ----------------------------------------------------------
     is_x_sbox = is_sbox (i, from_j, from_track, seg_details_x, true);

     // Next function returns a vector of the tracks to which from_track 
     // at (from_i, from_j) should connect at (to_i, to_j).
     conn_tracks =
       get_switch_box_tracks_x_to_z (
			      i,from_j,  // Loc. on CHANX during mersului.
			      k,         // Layer in which xchan lies
			      from_track,// Inside xchan
			      CHANX, 
			      i,from_j,  // Which z-chan location
			      k,         // Identifies zchan
			      CHANZ ); 
  
     // For all the tracks we connect to in that channel ... 
     for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       // ASUMPTION:  I will simplify the problem by assuming that
       // if track from x-chan connects to track in z-chan, then
       // it's bidirectional.  So,
       is_z_sbox = is_x_sbox;
       
       to_node_switch = seg_details_z[to_track].wire_switch;

       // If there is connection from x-chan track to the z-chan track 
       // then it is bidirectional and therefore I insert two edges.
       // Therefore, I replace:
       // get_switch_type (  is_x_sbox, is_z_sbox, 
       //                    from_node_switch, to_node_switch, 
       //                    switch_types  );
       // With:
       if ( is_x_sbox ) {
	 switch_types[0] = to_node_switch;
	 //switch_types[1] = from_node_switch;
	 switch_types[1] = OPEN;
       }
       else {
	 switch_types[0] = OPEN;
	 switch_types[1] = OPEN;
       }
       // Which translates into connection in both directions.
       
       if (switch_types[0] != OPEN) {
	 to_node = get_rr_node_index (i,to_j,
				      k,// pt ca asa s-au generat indices.
				      CHANZ, to_track, 
				      vias_per_zchan, 
				      rr_node_indices);

	 if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	   num_conn++;
	   rr_edge_done[to_node] = true;
	   edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[0], &free_edge_list_head);
	   
	   if (switch_types[1] != OPEN) {   // A second edge.
	     edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	     num_conn++;
	   }
	 }
       }//if
     }//for (iconn=0;...
    // ----------------------------------------------------------
   }//if
   

 }// for (i=from_istart;i<=from_iend;i++) ---> End for length of segment.
 

 *edge_list_ptr = edge_list_head;
 //printf(" %d",num_conn);
 return (num_conn);
 // At this time we have added some rr_node idices (coresp. to vias)
 // to the fanout list with connections of the wire-segment 
 // characterized by: begins at (from_istart,from_j) is located
 // on track "from_track" inside the channel x and extends till "to_j".
 // Note that this "mers de-a lungul lui x-chan" is in same spirit
 // as vias indices were "counted".
 // Question:  Could we do the adding of rr_node indices in fanout list 
 // of vias connections to x-chan in this function as well and NOT
 // with another function z-->x ?  It would save some run time...
 // and trouble.  
}//eof


// ======================================================================= //
// ======================================================================= //
// ======================================================================= //

int s_fpga3d::get_ytrack_to_ztracks (
                   int from_jstart,
		   int from_jend, int from_i, int from_track, int to_i, 
		   t_linked_edge **edge_list_ptr, 
		   int nodes_per_chan,
		   int vias_per_zchan,
		   int **rr_node_indices, 
		   t_seg_details *seg_details_y, 
		   t_seg_details *seg_details_z, 
		   enum e_switch_block_type switch_block_type,int k) 
{
// OBS:  Very similar to the above function.
//

 int num_conn, to_node, j, to_track, iconn;
 int from_node_switch, to_node_switch;
 short switch_types[2];
 bool is_z_sbox, is_y_sbox;
 t_linked_edge *edge_list_head;
 struct s_ivec conn_tracks;

 num_conn = 0;
 edge_list_head = *edge_list_ptr;// List where edges (1 or 2) are added.


// Recall:  this is the type of switch to use on switches that go *to* this
// node (i.e. the output side is on the from_node).                        

 from_node_switch = seg_details_y[from_track].wire_switch;


 for (j=from_jstart;j<=from_jend;j++) {
   // Acum mergem de-a lungul wire-segmentului de la "from_start"
   // till "from_end" si for every location ma uit to see whether 
   // there should be a switch "on" this segment and if so I have to "look" at 
   // possible connections to tracks (wire-segments) in z channels.

   

   //  ===>  Diagonal connection to above (from xchan to zchan) 
   if ( (k > 0) &&
	(j > 0) && (j < ny) ) { // Look only there are ychan's.
     // ----------------------------------------------------------
     is_y_sbox = is_sbox (j,from_i, from_track, seg_details_y, false);
     // Next function returns a vector of the tracks to which from_track 
     // at (from_i, from_j) should connect at (to_i, to_j).
     conn_tracks =
       get_switch_box_tracks_y_to_z (
			      from_i,j,  // Loc. on CHANY during mersului.
			      k,         // Layer in which ychan lies
			      from_track,// Inside ychan
			      CHANY, 
			      from_i,j,  // Which z-chan location
			      k-1,       // Identifies zchan
			      CHANZ ); 

      // For all the tracks we connect to in that z-channel ... 
     for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       // ASUMPTION:  I will simplify the problem by assuming that
       // if track from y-chan connects to track in z-chan, then
       // it's bidirectional.  So,
       is_z_sbox = is_y_sbox;
       
       to_node_switch = seg_details_z[to_track].wire_switch;

       // See comments in previous function in this file.
       // [ 2 ] Particularly:
       // If there is connection from y-chan track to the z-chan track 
       // then it is bidirectional and therefore I insert two edges.
       // Therefore, I replace:
       // get_switch_type ( is_y_sbox, is_z_sbox, 
       //                   from_node_switch, to_node_switch,
       //                   switch_types );
       // With:
       if ( is_y_sbox ) {
	 switch_types[0] = to_node_switch;
	 //switch_types[1] = from_node_switch;
	 switch_types[1] = OPEN;
	 //printf(" %d %d",switch_types[0],switch_types[1]);
       }
       else {
	 switch_types[0] = OPEN;
	 switch_types[1] = OPEN;
       }
       // Which translates into connection in both directions.
       
       if (switch_types[0] != OPEN) {
	 to_node = get_rr_node_index (from_i,j,
				      k-1,// pt ca asa s-au generat indices.
				      CHANZ, to_track, 
				      vias_per_zchan, 
				      rr_node_indices);
	 //printf(" %d",to_node);
	 if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	   num_conn++;
	   rr_edge_done[to_node] = true;
	   edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[0], &free_edge_list_head);
	   
	   if (switch_types[1] != OPEN) {   // A second edge.
	     edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	     num_conn++;
	   }
	 }
       }//if
     }//for (iconn=0;...
     // ----------------------------------------------------------
   }//if


   //  ===>  Diagonal connection to below (from xchan to zchan)
   if ( (k < num_layers-1) &&
	(j > 0) && (j < ny) ) { // Look only there are ychan's.
     // ----------------------------------------------------------
     is_y_sbox = is_sbox ( j,from_i, from_track, seg_details_y, true);
     conn_tracks =
       get_switch_box_tracks_y_to_z (
			      from_i,j,  // Loc. on CHANY during mersului.
			      k,         // Layer in which ychan lies
			      from_track,// Inside ychan
			      CHANY, 
			      from_i,j,  // Which z-chan location
			      k,       // Identifies zchan
			      CHANZ); 

     // For all the tracks we connect to in that channel ... 
     for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       is_z_sbox = is_y_sbox;
       
       to_node_switch = seg_details_z[to_track].wire_switch;


       // See comments in previous function in this file.
       // [ 2 ] Particularly:
       // If there is connection from y-chan track to the z-chan track 
       // then it is bidirectional and therefore I insert two edges.
       if ( is_y_sbox ) {
	 switch_types[0] = to_node_switch;
	 //switch_types[1] = from_node_switch;
	 switch_types[1] = OPEN;
       }
       else {
	 switch_types[0] = OPEN;
	 switch_types[1] = OPEN;
       }
       // Which translates into connection in both directions.
   
       if (switch_types[0] != OPEN) {
	 to_node = get_rr_node_index (to_i,j,
				      k,// pt ca asa s-au generat indices.
				      CHANZ, to_track, 
				      vias_per_zchan, 
				      rr_node_indices);

	 if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	   num_conn++;
	   rr_edge_done[to_node] = true;
	   edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[0], &free_edge_list_head);
	   
	   if (switch_types[1] != OPEN) {   // A second edge.
	     edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	     num_conn++;
	   }
	 }
       }//if
     }//for (iconn=0;...
    // ----------------------------------------------------------
   }//if

 }// for (j=from_jstart;j<=from_jend;j++)  ---> End for length of segment.

 
 *edge_list_ptr = edge_list_head;
 return (num_conn);
}//eof



// ======================================================================= //
// ======================================================================= //
// ======================================================================= //



int s_fpga3d::get_ztrack_to__x_y_z__tracks ( 
               int i, int j, // location
	       int k,// layer saying between which layers this track is
	       int from_track,// track in z-chan [0..vias_per_zchan-1]
	       t_linked_edge **edge_list_ptr, 
	       int nodes_per_chan,
	       int vias_per_zchan,
	       int **rr_node_indices, 
	       t_seg_details *seg_details_x,
 	       t_seg_details *seg_details_y,
	       t_seg_details *seg_details_z, 
	       enum e_switch_block_type switch_block_type ) 
{
// cristinel.ababei
// Asta este ptr cind vertical vias are only between two adjacent layers.
// Goes in the spirit of alloc_and_load_chanz_rr_indices().
// This function should be called for every via/track
// of all z-chan's between any two layers.

 int num_conn=0, iconn=0;
 int to_node=0, to_track=0; 
 int from_node_switch=0, to_node_switch=0;

 short switch_types[2];
 bool is_x_sbox, is_y_sbox, is_z_sbox;

 t_linked_edge *edge_list_head;
 struct s_ivec conn_tracks;

 num_conn = 0;
 edge_list_head = *edge_list_ptr;


 // from_track may be 0..vias_per_zchan-1
 from_node_switch = seg_details_z[from_track].wire_switch;

 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 // ABOVE, x-chan's
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to x-chan at location (ABOVE,LEFT)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 // Of course, all tracks in z-chan's have switches at their both endings.
if ( k >= 0) {

  
  // I look and see if the corresponding track from xchan 
  // does connect (ie, requires) a switch-box here and only
  // in that case I connect the vertical via to it.  This is because
  // I can have a certain type of segment with connections 
  // NOT at every switch-box.
  //is_z_sbox = true;
  is_z_sbox = is_sbox (i,j, from_track, seg_details_x, true);
  

 // Next function returns a vector of the tracks to which from_track 
 // at (k-1) z-chan should connect at (i,j) x-chan.
 // Normally should be a single value for simplest switch-box.

 conn_tracks = // Deci astea sint conexiuni in interiorul aceluiasi s-b.
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j,       //(ABOVE, LEFT x-chan); --> "to"
			      k,       // Identifies layer of chan x/y
			      CHANX,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 
 // For all the tracks we connect to in this x-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_x_sbox = is_z_sbox;
       
   to_node_switch = seg_details_x[to_track].wire_switch;
   // If there is connection from z-chan track to the x-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_x_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
     //printf(" %d %d",switch_types[0],switch_types[1]);
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }

   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j, //(ABOVE, LEFT x-chan)
				  k, // pt ca asa s-au generat indices.
				  CHANX, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);

     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			switch_types[0], &free_edge_list_head);
   
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                          switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;...
 
 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to x-chan at location (ABOVE,RIGHT)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 //is_z_sbox = true;
 is_z_sbox = is_sbox (i,j, from_track, seg_details_x, true);

 conn_tracks = 
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i+1,j,     //(ABOVE, RIGHT x-chan); --> "to"
			      k,       // Identifies layer of chan x/y
			      CHANX,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this x-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_x_sbox = is_z_sbox;
   to_node_switch = seg_details_x[to_track].wire_switch;
   // If there is connection from z-chan track to the x-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_x_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i+1,j, //(ABOVE, RIGHT x-chan)
				  k,   // pt ca asa s-au generat indices.
				  CHANX, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;...
 
 // ----------------------------------------------------------
 // ABOVE, y-chan's
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to y-chan at location (ABOVE,BOTTOM)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 //is_z_sbox = true;
 is_z_sbox = is_sbox (j,i, from_track, seg_details_y, true);
 
 conn_tracks = 
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j,       //(ABOVE, BOTTOM y-chan); --> "to"
			      k,       // Identifies layer of chan x/y
			      CHANY,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this y-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_y_sbox = is_z_sbox;
   to_node_switch = seg_details_y[to_track].wire_switch;
   // If there is connection from z-chan track to the y-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_y_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j, //(ABOVE, BOTTOM y-chan)
				  k, // pt ca asa s-au generat indices.
				  CHANY, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;... 

 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to y-chan at location (ABOVE,TOP)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 //is_z_sbox = true;
 is_z_sbox = is_sbox (j,i, from_track, seg_details_y, true);

 conn_tracks = 
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j+1,     //(ABOVE, TOP y-chan); --> "to"
			      k,       // Identifies layer of chan x/y
			      CHANY,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this y-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_y_sbox = is_z_sbox;
   to_node_switch = seg_details_y[to_track].wire_switch;
   // If there is connection from z-chan track to the y-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_y_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j+1, //(ABOVE, TOP y-chan)
				  k, // pt ca asa s-au generat indices.
				  CHANY, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;... 
 // ----------------------------------------------------------


}// if ( k >= 0)




 // At this time all 4 (four) connections of current via in current
 // z-chan to layer above, were added to edge_list_head.
 // Let's also put also the 4 (four) from layer below.
 // OBS:  num_conn saaga continues... :o)




 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 // BELOW, x-chan's
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to x-chan at location (BELOW,LEFT)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 // Of course, all tracks in z-chan's have switches at their both endings.
if ( k < num_layers-1 ) {


  //is_z_sbox = true;
  is_z_sbox = is_sbox (i,j, from_track, seg_details_x, false);

 // Next function returns a vector of the tracks to which from_track 
 // at (k) z-chan should connect at (i,j) x-chan.
 // Normally should be a single value for simplest switch-box.
 conn_tracks = // Deci astea sint conexiuni in interiorul aceluiasi s-b.
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j,       //(BELOW, LEFT x-chan); --> "to"
			      k+1,         // Identifies layer of chan x/y
			      CHANX,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this x-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_x_sbox = is_z_sbox;
       
   to_node_switch = seg_details_x[to_track].wire_switch;
   // If there is connection from z-chan track to the x-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_x_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j, //(BELOW, LEFT x-chan)
				  k+1, // pt ca asa s-au generat indices.
				  CHANX, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;...

 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to x-chan at location (BELOW,RIGHT)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 //is_z_sbox = true;
 is_z_sbox = is_sbox (i,j, from_track, seg_details_x, false);

 conn_tracks = 
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i+1,j,     //(BELOW, RIGHT x-chan); --> "to"
			      k+1,         // Identifies layer of chan x/y
			      CHANX,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this x-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_x_sbox = is_z_sbox;
   to_node_switch = seg_details_x[to_track].wire_switch;
   // If there is connection from z-chan track to the x-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_x_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i+1,j, //(BELOW, RIGHT x-chan)
				  k+1,   // pt ca asa s-au generat indices.
				  CHANX, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;... 


 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 // BELOW, y-chan's
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to y-chan at location (BELOW,BOTTOM)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 //is_z_sbox = true;
 is_z_sbox = is_sbox (j,i, from_track, seg_details_y, false);

 conn_tracks = 
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j,       //(BELOW, BOTTOM y-chan); --> "to"
			      k+1,         // Identifies layer of chan x/y
			      CHANY,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this y-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_y_sbox = is_z_sbox;
   to_node_switch = seg_details_y[to_track].wire_switch;
   // If there is connection from z-chan track to the y-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_y_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j, //(BELOW, BOTTOM y-chan)
				  k+1, // pt ca asa s-au generat indices.
				  CHANY, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;... 

 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 //  ===>  Diagonal connection to y-chan at location (BELOW,TOP)
 // from z-chan from this via/track (z-chan is between layers
 // given thru k).
 // ----------------------------------------------------------
 //is_z_sbox = true;
 is_z_sbox = is_sbox (j,i, from_track, seg_details_y, false);

 conn_tracks = 
   get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j+1,     //(BELOW, TOP y-chan); --> "to"
			      k+1,         // Identifies layer of chan x/y
			      CHANY,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 // For all the tracks we connect to in this y-chan ... 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
   to_track = conn_tracks.list[iconn];
   is_y_sbox = is_z_sbox;
   to_node_switch = seg_details_y[to_track].wire_switch;
   // If there is connection from z-chan track to the y-chan track 
   // then it is bidirectional and therefore I insert two edges.
   if ( is_y_sbox ) {
     switch_types[0] = to_node_switch;
     //switch_types[1] = from_node_switch;
     switch_types[1] = OPEN;
   }
   else {
     switch_types[0] = OPEN;
     switch_types[1] = OPEN;
   }
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j+1, //(BELOW, TOP y-chan)
				  k+1, // pt ca asa s-au generat indices.
				  CHANY, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//for (iconn=0;... 
 // ----------------------------------------------------------


}//if ( k < num_layers-1 )
 // ----------------------------------------------------------





 // At this time all 4+4 (eight) connections of current via in current
 // z-chan to layer above AND below, were added to edge_list_head.
 // Let's also put also the 1 OR 2 from layers above and/or below
 // as connections from z-chan to z-chan.
 // OBS:  num_conn saaga continues... :o)





 // ----------------------------------------------------------

 // ----------------------------------------------------------
 //printf(" %d",num_conn);
 // ----------------------------------------------------------
 //  ===>  Vertical connection to above (from zchan to zchan)
 // ----------------------------------------------------------

 if ( k > 0) {
   // We connect a track in z-chan with its couterpart only.
   to_track = from_track;
   //to_node_switch = seg_details_z[to_track].wire_switch;
   to_node_switch = from_node_switch;

   // Connection from z-chan track to the z-chan track 
   // is bidirectional and therefore I insert two edges.
   //if ( vertical_delay_same == 0 ) {
     // This is the case with architecture with vias only
     // between adjacent layers.  In this case I make/force z-track
     // to z-track switch connection with a pass-transistor
     // in order to be able to verify place-level estimation
     // in similar case. 
   //  switch_types[0] = 1; // By the way I know the architecture
     // is;  this is un-buffered switch
   //}
   //else {
     // All other cases connections are fully buffered.
     switch_types[0] = to_node_switch;
     //}
   //switch_types[1] = from_node_switch;
   switch_types[1] = OPEN;
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j, //(ABOVE z-chan)
				  k-1, // pt ca asa s-au generat indices.
				  CHANZ, to_track, 
				  vias_per_zchan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//if

 // ----------------------------------------------------------
 //  ===>  Vertical connection to below (from zchan to zchan)
 // ----------------------------------------------------------

 if ( k < num_layers-2 ) {// Atentie si intelegere aici!
   // We connect a track in z-chan with its couterpart only.
   to_track = from_track;
   //to_node_switch = seg_details_z[to_track].wire_switch;
   to_node_switch = from_node_switch;

   // Connection from z-chan track to the z-chan track 
   // is bidirectional and therefore I insert two edges.   
   //if ( vertical_delay_same == 0 ) {
     // This is the case with architecture with vias only
     // between adjacent layers.  In this case I make/force z-track
     // to z-track switch connection with a pass-transistor
     // in order to be able to verify place-level estimation
     // in similar case. 
   //  switch_types[0] = 1; // By the way I know the architecture
     // is;  this is un-buffered switch
   //}
   //else {
     // All other cases connections are fully buffered.
     switch_types[0] = to_node_switch;
     //}
   //switch_types[1] = from_node_switch;
   switch_types[1] = OPEN;
   
   if (switch_types[0] != OPEN) {
     to_node = get_rr_node_index (i,j, //(BELOW z-chan)
				  k+1, // pt ca asa s-au generat indices.
				  CHANZ, to_track, 
				  vias_per_zchan, 
				  rr_node_indices);
     if (!rr_edge_done[to_node]) {   // Not a repeat edge.
       num_conn++;
       rr_edge_done[to_node] = true;
       edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
       if (switch_types[1] != OPEN) {   // A second edge.
	 edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	 num_conn++;
       }
     }
   }//if
 }//if

 // ----------------------------------------------------------
 // ----------------------------------------------------------

 

 *edge_list_ptr = edge_list_head;
 //printf(" %d |",num_conn);
 return (num_conn);
}//eof



// ======================================================================= //
// ======================================================================= //
// ======================================================================= //


int s_fpga3d::get_xtrack_to_ytracks ( 
	 int from_istart, int from_iend, 
	 int from_j, int from_track, int to_j, 
	 t_linked_edge **edge_list_ptr, // The ONE populated in here! 
	 int nodes_per_chan,
	 int **rr_node_indices, 
	 t_seg_details *seg_details_x, t_seg_details *seg_details_y, 
	 enum e_switch_block_type switch_block_type, int k) 
{
/* Counts how many connections should be made from this segment to the y-   *
 * segments in the adjacent channels at to_j.  It returns the number of     *
 * connections, and updates edge_list_ptr to point at the head of the       *
 * (extended) linked list giving the nodes to which this segment connects   *
 * and the switch type used to connect to each.                             *
 *                                                                          *
 * An edge is added from this segment to a y-segment if:                    *
 * (1) this segment should have a switch box at that location, or           *
 * (2) the y-segment to which it would connect has a switch box, and the    *
 *     switch type of that y-segment is unbuffered (bidirectional pass      *
 *     transistor).                                                         *
 *                                                                          *
 * If the switch in each direction is a pass transistor (unbuffered), both  *
 * switches are marked as being of the types of the larger (lower R) pass   *
 * transistor.  Note that this code implicitly assumes the routing is       *
 * bidirectional (a switch in each direction).                              */


 int num_conn, to_node, i, to_track, iconn;
 int from_node_switch, to_node_switch;
 short switch_types[2];
 bool is_x_sbox, is_y_sbox, yconn_to_above;
 t_linked_edge *edge_list_head;
 struct s_ivec conn_tracks;

 num_conn = 0;
 edge_list_head = *edge_list_ptr;// List where edges (1 or 2) are added

 if (to_j <= from_j) 
    yconn_to_above = true;  /* The connection goes up from ychan to xchan. */
 else
    yconn_to_above = false;

/* Recall:  this is the type of switch to use on switches that go *to* this *
 * node (i.e. the output side is on the from_node).                         */

 from_node_switch = seg_details_x[from_track].wire_switch;

/* For each unit-length piece of wire in the segment, I add the things it    *
 * would connect to diagonally to the right and diagonally to the left of    *
 * it.  This is important -- for some switch boxes, the connection from      *
 * (i,j) to (i,j+1) and the connection from (i+1,j) to (i,j+1), for example, *
 * would lead to connections to different tracks in the (i,j+1) channel.  In *
 * this case, the code below will connect to ALL the tracks that two unit    *
 * length segments at (i,j) and (i+1,j) would have connected to in (i,j+1).  */



 for (i=from_istart;i<=from_iend;i++) { 
   // Acum mergem de-a lungul wire-segmentului si for every location
   // ma uit to see whether there should be a switch and if so
   // I have to "look" at possible connections to tracks (wire-segments)
   // in y channels.


   /*  ===>  Diagonal connection to left (from xchan to ychan) */

   // Next returns true if the specified segment has a switch box at specified  
   // location, false otherwise.  The switch box is from the specified segment 
   // to the left (for chanx) or below (for chany) unless above_or_right is    
   // true.                                                                    
   is_x_sbox = 
     is_sbox (i, from_j, from_track, seg_details_x, false);//TRUE/FALSE & sweep i
   // Next function returns a vector of the tracks to which from_track 
   // at (from_i, from_j) should connect at (to_i,to_j).
   // For simple switch-box returns just a number == connection
   // between one track to another only on diff. sides of a switch-box.
   conn_tracks = get_switch_box_tracks (i,from_j, from_track, CHANX, 
					i-1,to_j, CHANY, 
					switch_block_type, nodes_per_chan);


   /* For all the tracks we connect to in that channel ... */
   // For the simplest/standard switch-box a track connects only to its 
   // counterpart track.  Hence next "for" loop will be gone thru once.

    for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       // [  ?  ] Is this like a double-check; because I know already that
       // the track from x-chan is connected to a track in y-chan - that
       // is why "iconn" was added in the conn_tracks list, right? 
       // I see, in one direction I might connect with a type of switch
       // while in the other direction with a different type...
       is_y_sbox = 
	 is_sbox (to_j, i-1, to_track, seg_details_y, yconn_to_above);//TRUE/FALSE
       to_node_switch = seg_details_y[to_track].wire_switch;
    

       // This routine looks at whether the from_node and to_node want a switch,  
       // and what type of switch is used to connect *to* each type of node       
       // (from_node_switch and to_node_switch).  It decides what type of switch, 
       // if any, should be used to go from from_node to to_node.  If no switch   
       // should be inserted (i.e. no connection), it returns OPEN.  Its returned 
       // values are in the switch_types array.  It needs to return an array      
       // because one topology (a buffer in the forward direction and a pass      
       // transistor in the backward direction) results in *two* switches. 
       // Next will populate switch_types[0] and [1].  This will say
       // connection or not in one direction, the other or both.
       get_switch_type (is_x_sbox, is_y_sbox, from_node_switch, to_node_switch, 
                        switch_types);// This the one pupulated.

       if (switch_types[0] != OPEN) {
	 //printf(" %d",switch_types[0]);
	 to_node = get_rr_node_index (i-1,to_j,k, CHANY, to_track, 
                  nodes_per_chan, rr_node_indices);

	 if (!rr_edge_done[to_node]) {   /* Not a repeat edge. */
	   num_conn++;
	   rr_edge_done[to_node] = true;
	   edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[0], &free_edge_list_head);

	   if (switch_types[1] != OPEN) {   /* A second edge. */
	     //printf(" %d",switch_types[1]);
	     edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			  switch_types[1], &free_edge_list_head);
	     num_conn++;
	   }
	 }
       }
    }//for
    

    /*  ===>  Diagonal connection to right (from xchan to ychan) */

    is_x_sbox = 
      is_sbox (i, from_j, from_track, seg_details_x, true);
    conn_tracks = get_switch_box_tracks (i,from_j, from_track, CHANX,
					 i,to_j, CHANY, 
					 switch_block_type, nodes_per_chan);

    /* For all the tracks we connect to in that channel ... */
    // Normally, for simple switch-boxes is only one...
    for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       is_y_sbox = is_sbox (to_j, i, to_track, seg_details_y, yconn_to_above);
       to_node_switch = seg_details_y[to_track].wire_switch;
       
       get_switch_type (is_x_sbox, is_y_sbox, from_node_switch, to_node_switch,
                        switch_types);

       if (switch_types[0] != OPEN) {
          to_node = get_rr_node_index (i,to_j,k, CHANY, to_track, 
                  nodes_per_chan, rr_node_indices);

          if (!rr_edge_done[to_node]) {   /* Not a repeat edge. */
             num_conn++;
             rr_edge_done[to_node] = true;
             edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                             switch_types[0], &free_edge_list_head);

             if (switch_types[1] != OPEN) {    /* A second edge */
                edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                             switch_types[1], &free_edge_list_head);
                num_conn++;
             }
          }
       }
    }

 }// for (i=from_istart;i<=from_iend;i++) ---> End for length of segment.
 
 *edge_list_ptr = edge_list_head;
 return (num_conn);
}//eof
// ======================================================================= //

int s_fpga3d::get_ytrack_to_xtracks (int from_jstart,
				     int from_jend, int from_i, int 
       from_track, int to_i, t_linked_edge **edge_list_ptr, int nodes_per_chan, 
       int **rr_node_indices, t_seg_details *seg_details_x, t_seg_details 
       *seg_details_y, enum e_switch_block_type switch_block_type,int k) 
{
/* Counts how many connections should be made from this segment to the x-   *
 * segments in the adjacent channels at to_i.  It returns the number of     *
 * connections, and updates edge_list_ptr to point at the head of the       *
 * (extended) linked list giving the nodes to which this segment connects   *
 * and the switch type used to connect to each.                             *
 *                                                                          *
 * An edge is added from this segment to an x-segment if:                   *
 * (1) this segment should have a switch box at that location, or           *
 * (2) the x-segment to which it would connect has a switch box, and the    *
 *     switch type of that x-segment is unbuffered (bidirectional pass      *
 *     transistor).                                                         *
 *                                                                          *
 * If the switch in each direction is a pass transistor (unbuffered), both  *
 * switches are marked as being of the types of the larger (lower R) pass   *
 * transistor.  Note that this code implicitly assumes the routing is       *
 * bidirectional (a switch in each direction).                              */

 
 int num_conn, to_node, j, to_track, iconn;
 int from_node_switch, to_node_switch;
 short switch_types[2];
 bool is_x_sbox, is_y_sbox, xconn_to_right;
 t_linked_edge *edge_list_head;
 struct s_ivec conn_tracks;

 num_conn = 0;
 edge_list_head = *edge_list_ptr;

 if (to_i <= from_i) 
    xconn_to_right = true;  /* The connection goes RIGHT from xchan to ychan */
 else
    xconn_to_right = false;

/* Recall:  this is the type of switch to use on switches that go *to* this *
 * node (i.e. the output side is on the from_node).                         */

 from_node_switch = seg_details_y[from_track].wire_switch;

/* For each unit-length piece of wire in the segment, I add the things it    *
 * would connect to diagonally above and diagonally below it.  This is       *
 * important -- for some switch boxes, the connection from (i,j) to (i+1,j)  *
 * and the connection from (i,j+1) to (i+1,j), for example, would lead to    *
 * connections to different tracks in the (i+1,j) channel.  In this case,    *
 * the code below will connect to ALL the tracks that two unit length        *
 * segments at (i,j) and (i,j+1) would have connected to in (i+1,j).         */
 
 for (j=from_jstart;j<=from_jend;j++) {
   
    /* Diagonal connection to below (from ychan to xchan) */

    is_y_sbox = is_sbox (j, from_i, from_track, seg_details_y, false);

    conn_tracks = get_switch_box_tracks (from_i, j, from_track, CHANY, to_i,
               j-1, CHANX, switch_block_type, nodes_per_chan);

    /* For all the tracks we connect to in that channel ... */

    for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
       to_track = conn_tracks.list[iconn];
       is_x_sbox = is_sbox (to_i, j-1, to_track, seg_details_x, xconn_to_right);
       to_node_switch = seg_details_x[to_track].wire_switch;
     
       get_switch_type (is_y_sbox, is_x_sbox, from_node_switch, to_node_switch,
                        switch_types);

       if (switch_types[0] != OPEN) { 
          to_node = get_rr_node_index (to_i,j-1,k, CHANX, to_track, 
                    nodes_per_chan, rr_node_indices);

          if (!rr_edge_done[to_node]) {    /* Not a repeat edge. */
             num_conn++; 
             rr_edge_done[to_node] = true;
             edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                                switch_types[0], &free_edge_list_head);

             if (switch_types[1] != OPEN) {   /* A second edge. */
                edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                                   switch_types[1], &free_edge_list_head);
                num_conn++;
             }
          }
       }   
    }

    /* Diagonal connection to above (from ychan to xchan) */

    is_y_sbox = is_sbox (j, from_i, from_track, seg_details_y, true);

    conn_tracks = get_switch_box_tracks (from_i, j, from_track, CHANY, to_i,
               j, CHANX, switch_block_type, nodes_per_chan);

    /* For all the tracks we connect to in that channel ... */ 

    for (iconn=0;iconn<conn_tracks.nelem;iconn++) { 
       to_track = conn_tracks.list[iconn];
       is_x_sbox = is_sbox (to_i, j, to_track, seg_details_x, xconn_to_right);
       to_node_switch = seg_details_x[to_track].wire_switch;

       get_switch_type (is_y_sbox, is_x_sbox, from_node_switch, to_node_switch,
                        switch_types);

       if (switch_types[0] != OPEN) { 
          to_node = get_rr_node_index (to_i,j,k, CHANX, to_track, 
                    nodes_per_chan, rr_node_indices);

          if (!rr_edge_done[to_node]) {    /* Not a repeat edge. */
             num_conn++; 
             rr_edge_done[to_node] = true;
             edge_list_head = insert_in_edge_list (edge_list_head, to_node, 
                            switch_types[0], &free_edge_list_head);

             if (switch_types[1] != OPEN) {  /* A second edge */
                edge_list_head = insert_in_edge_list (edge_list_head, to_node, 
                               switch_types[1], &free_edge_list_head);
                num_conn++;
             }
          }
       }   
    }

 }  /* End for length of segment. */
    
 
 *edge_list_ptr = edge_list_head;
 return (num_conn);
}


// ======================================================================= //


int s_fpga3d::get_xtrack_to_xtrack (int from_i, int j, 
				    int from_track, int to_i, 
       t_linked_edge **edge_list_ptr, int nodes_per_chan, int 
       **rr_node_indices, t_seg_details *seg_details_x, enum 
       e_switch_block_type switch_block_type,int k) 
{
// Returns the number of edges between the specified channel segments.      
// Also updates edge_list_ptr to point at the new (extended) linked list    
// of edges and switch types.
//
// cristinel.ababei
// Deci links between a track (wire-segment) within current
// channel and tracks in "continuation" adjacent channels. For x(y)
//  channels these links are to the left(bottom)/right(right) x(y) channel.  
// These links with afferent edges being created, are made based on the 
// switch-box information.

 
 bool is_from_sbox, is_to_sbox, from_goes_right, to_goes_right;
 int to_track, to_node, iconn, num_conn;
 int from_node_switch, to_node_switch;
 short switch_types[2];
 struct s_ivec conn_tracks;

 if (from_i < to_i) {
    from_goes_right = true;
    to_goes_right = false;
 }
 else {
    from_goes_right = false;
    to_goes_right = true;
 }

 num_conn = 0;
 from_node_switch = seg_details_x[from_track].wire_switch;

 is_from_sbox = is_sbox (from_i, j, from_track, seg_details_x, from_goes_right);

 // Next function returns a vector of the tracks to which from_track 
 // at (from_i, from_j) should connect at (to_i, to_j).
 //
 // cristinel.ababei:  What returns will also contain vertical tracks!?
 //
 conn_tracks = get_switch_box_tracks (from_i, j, from_track, CHANX, to_i,
            j, CHANX, switch_block_type, nodes_per_chan);


 /* For all the tracks we connect to in that channel ... */
 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
    to_track = conn_tracks.list[iconn];
    is_to_sbox = is_sbox (to_i, j, to_track, seg_details_x, to_goes_right);
    to_node_switch = seg_details_x[to_track].wire_switch;
  
    get_switch_type (is_from_sbox, is_to_sbox, from_node_switch,
                     to_node_switch, switch_types);
 
    if (switch_types[0] != OPEN) {
       to_node = get_rr_node_index (to_i,j,k, CHANX, to_track, nodes_per_chan,
               rr_node_indices);

       /* No need to check for repeats with the current switch boxes. */

       *edge_list_ptr = insert_in_edge_list (*edge_list_ptr, to_node, 
                        switch_types[0], &free_edge_list_head);
       num_conn++;

       if (switch_types[1] != OPEN) {
          *edge_list_ptr = insert_in_edge_list (*edge_list_ptr, to_node, 
                           switch_types[1], &free_edge_list_head);
          num_conn++;
       }
    }
 }       
 
 return (num_conn);
}//eof
// ======================================================================= //

int s_fpga3d::get_ytrack_to_ytrack (int i, int from_j, int from_track,
				    int to_j,
       t_linked_edge **edge_list_ptr, int nodes_per_chan, int 
       **rr_node_indices, t_seg_details *seg_details_y, enum 
       e_switch_block_type switch_block_type,int k) 
{
// Returns the number of edges between the specified channel segments.     
// Also updates edge_list_ptr to point at the new (extended) linked list   
// of edges and switch types.                                              

 
 bool is_from_sbox, is_to_sbox, from_goes_up, to_goes_up; 
 int to_track, to_node, iconn, num_conn;
 int from_node_switch, to_node_switch;
 short switch_types[2];
 struct s_ivec conn_tracks;

if (from_j < to_j) { 
    from_goes_up = true; 
    to_goes_up = false; 
 } 
 else {
    from_goes_up = false; 
    to_goes_up = true; 
 } 

 num_conn = 0;
 from_node_switch = seg_details_y[from_track].wire_switch;
 
 is_from_sbox = is_sbox (from_j, i, from_track, seg_details_y, from_goes_up); 
 conn_tracks = get_switch_box_tracks (i, from_j, from_track, CHANY, i, to_j, 
             CHANY, switch_block_type, nodes_per_chan);
 
 /* For all the tracks we connect to in that channel ... */
 
 for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
    to_track = conn_tracks.list[iconn];
    is_to_sbox = is_sbox (to_j, i, to_track, seg_details_y, to_goes_up); 
    to_node_switch = seg_details_y[to_track].wire_switch;
 
    get_switch_type (is_from_sbox, is_to_sbox, from_node_switch,
                     to_node_switch, switch_types);

    if (switch_types[0] != OPEN) { 
       to_node = get_rr_node_index (i,to_j,k, CHANY, to_track, nodes_per_chan,
                rr_node_indices); 

       /* No need to check for repeats with the current switch boxes. */

       *edge_list_ptr = insert_in_edge_list (*edge_list_ptr, to_node,
                        switch_types[0], &free_edge_list_head);
       num_conn++;

       if (switch_types[1] != OPEN) {
          *edge_list_ptr = insert_in_edge_list (*edge_list_ptr, to_node,
                           switch_types[1], &free_edge_list_head);
          num_conn++;
       }
    }
 }
 
 return (num_conn);
}
// ======================================================================= //

bool s_fpga3d::is_sbox (int seg_num, int chan_num, int itrack, 
			t_seg_details *seg_details, 
			bool above_or_right) 
{
// Returns true if the specified segment has a switch box at the specified  
// location, false otherwise.  The switch box is from the specified segment 
// to the left (for chanx) or below (for chany) unless above_or_right is    
// true.                                                                    

 int seg_offset, start, length;
 bool longline;
 
 length = seg_details[itrack].length;
 start = seg_details[itrack].start;
 longline = seg_details[itrack].longline;

 // NB: Periodicity is length for normal segments, length + 1 for long lines. 
 
 if (!longline) {
    seg_offset = (seg_num + chan_num - start + length) % length;
    seg_offset += above_or_right;    // Add one if conn. is above or to right 
 }

 else {    // Is a longline
    seg_offset = (seg_num + chan_num - start + above_or_right) % (length + 1);
 }

 return (seg_details[itrack].sb[seg_offset]);
}//eof
// ======================================================================= //

void s_fpga3d::get_switch_type ( 
                 bool is_from_sbox, bool is_to_sbox, 
		 short from_node_switch, short to_node_switch, 
		 short switch_types[2] )
{
// This routine looks at whether the from_node and to_node want a switch,  
// and what type of switch is used to connect *to* each type of node       
// (from_node_switch and to_node_switch).  It decides what type of switch, 
// if any, should be used to go from from_node to to_node.  If no switch   
// should be inserted (i.e. no connection), it returns OPEN.  Its returned 
// values are in the switch_types array.  It needs to return an array      
// because one topology (a buffer in the forward direction and a pass      
// transistor in the backward direction) results in *two* switches.        

// cristinel.ababei
// Next two will be populated by this function.  This will convey
// th einfo about what kind of connection between the two tracks is
// existing inside the switch-box: in one direction, both directions,
// or no connection.
  switch_types[0] = OPEN;  /* No switch */
  switch_types[1] = OPEN;


 if (!is_from_sbox && !is_to_sbox) {  /* No connection wanted in either dir */
    switch_types[0] = OPEN;
 }


 else if (is_from_sbox && !is_to_sbox) {  /* Only forward connection wanted */
    switch_types[0] = to_node_switch;  /* Type of switch to go *to* to_node */
 }

 
 else if (!is_from_sbox && is_to_sbox) {

/* Only backward connection desired.  We're deciding whether or not to put *
 * in the forward connection.  Put it in if the backward connection uses   *
 * a bidirectional (pass transistor) switch.  Remember that the backward   *
 * connection uses a from_node_switch type of switch.                      */

    if (switch_inf[from_node_switch].buffered == false) {
       switch_types[0] = from_node_switch;
    }
 }


 else {

/* Both a forward and a backward connection desired.  If the switch types   *
 * desired for the two connection are different, we have to reconcile them. */

    if (from_node_switch == to_node_switch) {
       switch_types[0] = to_node_switch;
    }
    else {    /* Different switch types.  Reconcile. */
       if (switch_inf[to_node_switch].buffered) {
          switch_types[0] = to_node_switch;

          if (switch_inf[from_node_switch].buffered == false) {

         /* Buffer in forward direction, pass transistor in backward.  Put *
          * in *two* edges.                                                */
              
             switch_types[1] = from_node_switch;
          }
       }
       else {   /* Forward connection is a pass transistor. */
          if (switch_inf[from_node_switch].buffered) {
             switch_types[0] = to_node_switch;
          }
          else {  

          /* Both forward and backward connections use pass transistors. *
           * use whichever one is larger, since you'll only physically   *
           * build one switch.                                           */
     
             if (switch_inf[to_node_switch].R < 
                            switch_inf[from_node_switch].R) {
                switch_types[0] = to_node_switch;
             }
             else if (switch_inf[from_node_switch].R < 
                            switch_inf[to_node_switch].R) {
                switch_types[0] = from_node_switch;
             }
             else {

             /* Two pass transistors have the same R, but are have different *
              * switch indices.  Use the one with lower index (arbitrarily), *
              * to ensure both switches are of the same type (since you can  *
              * only physically build one).  I'm being pretty dogmatic here. */

                if (to_node_switch < from_node_switch) {
                   switch_types[0] = to_node_switch;
                }
                else {
                   switch_types[0] = from_node_switch;
                }
             }
          }
       }
    }     /* End switch types are different */
 }   /* End both forward and backward connection desired. */
}//eof
// ======================================================================= //
