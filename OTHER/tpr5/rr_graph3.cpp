
#include <stdio.h>
#include <assert.h>
#include <iostream>

#include "util.h"
#include "ipr_types.h"
#include "fpga3d.h"


using namespace std;

// ======================================================================= //

// This rr_graph3.cpp contains stuff related to the implementation
// of the 3d routing tool which shall work with fpga architectures
// with multiple vertical wires.

// ======================================================================= //



// For the case of mult_vert_segm. 
int s_fpga3d::get_ztrack_to__x_y_z__tracks_version_02 ( 
               int i,int j, // location
	       int from_kstart,// layer where this vert segm starts
	       int from_kend,  // layer where this vert segm ends
	       int from_track, // track in z-chan [0..vias_per_zchan-1]
	       t_linked_edge **edge_list_ptr, 
	       int nodes_per_chan,
	       int vias_per_zchan,
	       int **rr_node_indices, 
	       t_seg_details *seg_details_x,
 	       t_seg_details *seg_details_y,
	       t_seg_details *seg_details_z, 
	       enum e_switch_block_type switch_block_type ) 
{
// cristinel.ababei
// (oBs: Goes in the spirit of alloc_and_load_chanz_rr_indices().)
// Counts how many connections should be made from this segment to the      
// x,y,(z)-segments in the adjacent channels at to_j.  It returns the number of 
// connections, and updates edge_list_ptr to point at the head of the       
// (extended) linked list giving the nodes to which this segment connects   
// and the switch type used to connect to each.                             
//                                                                          
// An edge is added from this segment to a x,y-segment (ie, via) if:          
// (1) this segment should have a switch box at that location, or           
// (2) the x,y-segment to which it would connect has a switch box, and the    
//     switch type of that x,y-segment is unbuffered (bidirectional pass      
//     transistor).                                                         
//                                                                          
// If the switch in each direction is a pass transistor (unbuffered), both  
// switches are marked as being of the types of the larger (lower R) pass   
// transistor.  Note that this code implicitly assumes the routing is       
// bidirectional (a switch in each direction).                              


  int num_conn=0, iconn=0, k=0;
  int to_node=0, to_track=0; 
  int from_node_switch=0, to_node_switch=0;
  
  short switch_types[2];
  bool is_x_sbox, is_y_sbox, is_z_sbox;

  t_linked_edge *edge_list_head;
  struct s_ivec conn_tracks;

  num_conn = 0;
  edge_list_head = *edge_list_ptr;


  // from_track may be 0..vias_per_zchan-1
  from_node_switch = seg_details_z[from_track].wire_switch;
  
  

 
  for (k=from_kstart;k<=from_kend;k++) {
    // ========================================================================
    // This is the BIG "for" loop over-here. 
    // Acum mergem de-a lungul wire-segmentului de la "from_start"
    // till "from_end" si for every location ma uit to see whether 
    // there should be a switch "on" this segment and if so I have to "look" at 
    // possible connections to tracks (wire-segments) in x,y channels.
    // Here it is slightly different the way I "look: at x,y channels: I keep
    // same "k" as opposed to just looking "above" and "below" as it was for
    // vertical-segment-of-length-one-only. 

    
    //printf(" %d",num_conn);
    // ----------------------------------------------------------
    // x-chan's
    // ----------------------------------------------------------
    //  ===>  Diagonal connection to x-chan at location (LEFT)
    // from z-chan from this via/track (z-chan is between layers
    // given thru k).
    // ----------------------------------------------------------
    // Of course, all tracks in z-chan's have switches at their both endings.
    if ( k >= 0) {

  
      // I look and see if the corresponding track from xchan 
      // does connect (ie, requires) a swicth-box here and only
      // in that case I connect the vertical via to it.  This is because
      // I can have a certain type of segment with connections 
      // NOT at every switch-box.
      //is_z_sbox = true;
      is_z_sbox = is_sbox (i,j, from_track, seg_details_z, false);
  

      // Next function returns a vector of the tracks to which from_track 
      // at (k-1) z-chan should connect at (i,j) x-chan.
      // Normally should be a single value for simplest switch-box.

      conn_tracks = // Deci astea sint conexiuni in interiorul aceluiasi s-b.
	get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j,       //(LEFT x-chan); --> "to"
			      k,         // Identifies layer of chan x/y
			      CHANX,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
 
      // For all the tracks we connect to in this x-chan ...
      // (which in case of the universal switch box we work with
      // is only one connection, so next *for* loop is traversed once only) 
      for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
	to_track = conn_tracks.list[iconn];
	is_x_sbox = is_z_sbox;
       
	to_node_switch = seg_details_x[to_track].wire_switch;
	// If there is connection from z-chan track to the x-chan track 
	// then it is bidirectional and therefore I insert two edges.
	if ( is_x_sbox ) {
	  switch_types[0] = to_node_switch;
	  //switch_types[1] = from_node_switch;
	  switch_types[1] = OPEN;
	  //printf(" %d %d",switch_types[0],switch_types[1]);
	}
	else {
	  switch_types[0] = OPEN;
	  switch_types[1] = OPEN;
	}

	if (switch_types[0] != OPEN) {
	  to_node = get_rr_node_index (i,j, //(LEFT x-chan)
				  k, // pt ca asa s-au generat indices.
				  CHANX, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
	  // Sanity check:
	  //if ( to_node == 0) printf(" >%d",to_node);

	  if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	    num_conn++;
	    rr_edge_done[to_node] = true;
	    edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
   
	    if (switch_types[1] != OPEN) {   // A second edge.
	      edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                          switch_types[1], &free_edge_list_head);
	      num_conn++;
	    }
	  }
	}//if
      }//for (iconn=0;...
 
      //printf(" %d",num_conn);
      // ----------------------------------------------------------
      //  ===>  Diagonal connection to x-chan at location (ABOVE,RIGHT)
      // from z-chan from this via/track (z-chan is between layers
      // given thru k).
      // ----------------------------------------------------------
      //is_z_sbox = true;
      is_z_sbox = is_sbox (i,j, from_track, seg_details_z, false);

      conn_tracks = 
	get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i+1,j,     //(RIGHT x-chan); --> "to"
			      k,         // Identifies layer of chan x/y
			      CHANX,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
      // For all the tracks we connect to in this x-chan ... 
      for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
	to_track = conn_tracks.list[iconn];
	is_x_sbox = is_z_sbox;
	to_node_switch = seg_details_x[to_track].wire_switch;
	// If there is connection from z-chan track to the x-chan track 
	// then it is bidirectional and therefore I insert two edges.
	if ( is_x_sbox ) {
	  switch_types[0] = to_node_switch;
	  //switch_types[1] = from_node_switch;
	  switch_types[1] = OPEN;
	}
	else {
	  switch_types[0] = OPEN;
	  switch_types[1] = OPEN;
	}
	
	if (switch_types[0] != OPEN) {
	  to_node = get_rr_node_index (i+1,j, //(ABOVE, RIGHT x-chan)
				  k,   // pt ca asa s-au generat indices.
				  CHANX, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
	  //if ( to_node == 0) printf(" >>%d",to_node);

	  if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	    num_conn++;
	    rr_edge_done[to_node] = true;
	    edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
	    if (switch_types[1] != OPEN) {   // A second edge.
	      edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	      num_conn++;
	    }
	  }
	}//if
      }//for (iconn=0;...
 
      // ----------------------------------------------------------
      // y-chan's
      // ----------------------------------------------------------
      //  ===>  Diagonal connection to y-chan at location (ABOVE,BOTTOM)
      // from z-chan from this via/track (z-chan is between layers
      // given thru k).
      // ----------------------------------------------------------
      //is_z_sbox = true;
      is_z_sbox = is_sbox (j,i, from_track, seg_details_z, false);
 
      conn_tracks = 
	get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j,       //(BOTTOM y-chan); --> "to"
			      k,         // Identifies layer of chan x/y
			      CHANY,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
      // For all the tracks we connect to in this y-chan ... 
      for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
	to_track = conn_tracks.list[iconn];
	is_y_sbox = is_z_sbox;
	to_node_switch = seg_details_y[to_track].wire_switch;
	// If there is connection from z-chan track to the y-chan track 
	// then it is bidirectional and therefore I insert two edges.
	if ( is_y_sbox ) {
	  switch_types[0] = to_node_switch;
	  //switch_types[1] = from_node_switch;
	  switch_types[1] = OPEN;
	}
	else {
	  switch_types[0] = OPEN;
	  switch_types[1] = OPEN;
	}
	
	if (switch_types[0] != OPEN) {
	  to_node = get_rr_node_index (i,j, //(BOTTOM y-chan)
				  k, // pt ca asa s-au generat indices.
				  CHANY, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
	  //if ( to_node == 0) printf(" >>>%d",to_node);

	  if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	    num_conn++;
	    rr_edge_done[to_node] = true;
	    edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
	    if (switch_types[1] != OPEN) {   // A second edge.
	      edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	      num_conn++;
	    }
	  }
	}//if
      }//for (iconn=0;... 

      //printf(" %d",num_conn);
      // ----------------------------------------------------------
      //  ===>  Diagonal connection to y-chan at location (ABOVE,TOP)
      // from z-chan from this via/track (z-chan is between layers
      // given thru k).
      // ----------------------------------------------------------
      //is_z_sbox = true;
      is_z_sbox = is_sbox (j,i, from_track, seg_details_z, false);

      conn_tracks = 
	get_switch_box_tracks_z_to__x_y (
			      i,j,       // Loc of current z-chan; "from" -->
			      k,         // Identifies z-chan
			      from_track,// Track inside z-chan
			      CHANZ,
			      i,j+1,     //(TOP y-chan); --> "to"
			      k,         // Identifies layer of chan x/y
			      CHANY,
			      switch_block_type, 
			      nodes_per_chan,
			      vias_per_zchan );
      // For all the tracks we connect to in this y-chan ... 
      for (iconn=0;iconn<conn_tracks.nelem;iconn++) {
	to_track = conn_tracks.list[iconn];
	is_y_sbox = is_z_sbox;
	to_node_switch = seg_details_y[to_track].wire_switch;
	// If there is connection from z-chan track to the y-chan track 
	// then it is bidirectional and therefore I insert two edges.
	if ( is_y_sbox ) {
	  switch_types[0] = to_node_switch;
	  //switch_types[1] = from_node_switch;
	  switch_types[1] = OPEN;
	}
	else {
	  switch_types[0] = OPEN;
	  switch_types[1] = OPEN;
	}
   
	if (switch_types[0] != OPEN) {
	  to_node = get_rr_node_index (i,j+1, //(TOP y-chan)
				  k, // pt ca asa s-au generat indices.
				  CHANY, to_track, 
				  nodes_per_chan, 
				  rr_node_indices);
	  //if ( to_node == 0) printf(" >>>>%d",to_node);

	  if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	    num_conn++;
	    rr_edge_done[to_node] = true;
	    edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			     switch_types[0], &free_edge_list_head);
       
	    if (switch_types[1] != OPEN) {   // A second edge.
	      edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                            switch_types[1], &free_edge_list_head);
	      num_conn++;
	    }
	  }
	}//if
      }//for (iconn=0;... 
      // ----------------------------------------------------------


    }// if ( k >= 0 )
    // At this time all 4 (four) connections of current via in current
    // z-chan to layer x,y channels in layer "k", were added to 
    // edge_list_head.
    // Let's go for the nekt "k" and do this till whole length of this 
    // vertical via/segment is "walked"


  }// for (k=from_kstart;...
  // This was the BIG "for" loop over-here, after which I should have all
  // connections of this verticl segment to all x,y channels along its length.
  //      /
  //   --*--      fromk_start
  //    /|
  //     |
  //     |
  //     |/
  //   --*--     :possible connections
  //    /|
  //     |
  //     |
  //     |/
  //   --*--      fromk_end
  //    /
  //
  // ========================================================================

  

  // At this time all potential (fromk_end - fromk_start + 1)*4
  // connections of current via in current
  // z-chan to x,y layers, were added to edge_list_head.
  // Let's also put also the 1 OR 2 from layers above and/or below
  // as connections from z-chan to z-chan.



  // ----------------------------------------------------------
  
  // ----------------------------------------------------------
  //printf(" %d",num_conn);
  // ----------------------------------------------------------
  //  ===>  Vertical connection to above (from zchan to zchan)
  // ----------------------------------------------------------
  
  if ( from_kstart > 0) {
    // We connect a track in z-chan with its couterpart only.
    to_track = from_track;
    //to_node_switch = seg_details_z[to_track].wire_switch;
    to_node_switch = from_node_switch;
    
    // Connection from z-chan track to the z-chan track 
    // is bidirectional and therefore I insert two edges.
    switch_types[0] = to_node_switch;
    //switch_types[1] = from_node_switch;
    switch_types[1] = OPEN;
    
    if (switch_types[0] != OPEN) {
      to_node = get_rr_node_index (i,j, //(ABOVE z-chan)
			from_kstart - 1,//pt ca asa s-au generat indices.
			CHANZ, to_track, 
			vias_per_zchan, 
			rr_node_indices);
      //if ( to_node == 0) printf(" ^%d-%d-%d-%d",to_node,i,j,from_kstart - 1);

      if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	num_conn++;
	rr_edge_done[to_node] = true;
	edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			 switch_types[0], &free_edge_list_head);
	
	if (switch_types[1] != OPEN) {   // A second edge.
	  edge_list_head = insert_in_edge_list (edge_list_head, to_node,
                           switch_types[1], &free_edge_list_head);
	  num_conn++;
	}
      }
    }//if
  }//if
  
  // ----------------------------------------------------------
  //  ===>  Vertical connection to below (from zchan to zchan)
  // ----------------------------------------------------------

  if ( from_kend < num_layers-2 ) {// Atentie si intelegere aici!
    // We connect a track in z-chan with its couterpart only.
    to_track = from_track;
    //to_node_switch = seg_details_z[to_track].wire_switch;
    to_node_switch = from_node_switch;

    // Connection from z-chan track to the z-chan track 
    // is bidirectional and therefore I insert two edges.
    switch_types[0] = to_node_switch;
    //switch_types[1] = from_node_switch;
    switch_types[1] = OPEN;
    
    if (switch_types[0] != OPEN) {
      to_node = get_rr_node_index (i,j, //(BELOW z-chan)
			from_kend + 1,//pt ca asa s-au generat indices.
			CHANZ, to_track, 
			vias_per_zchan, 
			rr_node_indices);
      //if ( to_node == 0) printf(" ^^%d",to_node);

      if (!rr_edge_done[to_node]) {   // Not a repeat edge.
	num_conn++;
	rr_edge_done[to_node] = true;
	edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			 switch_types[0], &free_edge_list_head);
       
	if (switch_types[1] != OPEN) {   // A second edge.
	  edge_list_head = insert_in_edge_list (edge_list_head, to_node,
			   switch_types[1], &free_edge_list_head);
	  num_conn++;
	}
      }
    }//if
  }//if

  // ----------------------------------------------------------
  // ----------------------------------------------------------

 

  *edge_list_ptr = edge_list_head;
  //printf(" %d |",num_conn);
  return (num_conn);
}//eof



// ======================================================================= //

// ======================================================================= //



void s_fpga3d::build_rr_zchan_version_02 ( //For the case of mult_vert_segm.
          int ***rr_node_indices, 
	  enum e_route_type route_type, 
	  int i,int j,int k, 
	  int nodes_per_chan, 
	  int vias_per_zchan,
	  enum e_switch_block_type switch_block_type, 
	  t_seg_details *seg_details_x, 
	  t_seg_details *seg_details_y,
	  t_seg_details *seg_details_z,
	  int cost_index_offset) 
{
// cristinel.ababei
// Loads up (populates) all the routing resource nodes in the 
// z channels "starting" at (i,j,k).  These rr_nodes are already 
// minimally allocated and their indices also.  Now we populate
// them only.
// OBS:  To be called only for:
//       (k >= 0) && (k < num_layers-1) && 
//	 (i > 0) && (i < nx) && (j > 0) && (j < ny)
 
  int itrack=0,num_edges=0,inode=0,kstart=0,kend=0,length=0,seg_index=0;
  t_linked_edge *edge_list_head;



  for (itrack=0;itrack<vias_per_zchan; /* see it */ itrack++) {
    // ------------------------------------------

    kstart = get_closest_seg_start_z (seg_details_z, itrack, k);//START
    if (kstart != k)
      continue;        // Not the start of this vertical segment.
    kend = get_seg_end_z (seg_details_z, itrack, kstart, num_layers-1);//STOP
    //printf(" %d %d ",kstart,kend);
    edge_list_head = NULL;

    num_edges = get_ztrack_to__x_y_z__tracks_version_02 ( 
          i, j, // location
	  kstart, kend,// layers between which this track is
	  itrack,// track in z-chan [0..vias_per_zchan-1]
	  &edge_list_head,
	  nodes_per_chan,
	  vias_per_zchan, 
	  rr_node_indices[k],
	  seg_details_x, seg_details_y, seg_details_z, 
	  switch_block_type );


    
    inode = get_rr_node_index (i,j,k, CHANZ, itrack, vias_per_zchan, 
			       rr_node_indices[k]);
    
    // EDGES ? and SWITCHES ?
    // Tot ce-a fost pina acum a fost doar ptr. ca sa pun in edge_list_head
    // indices off rr_nodes la care acest "inode" fanouts...
    // So, edge_list_head is now created and used to convey the information
    // in order to actually create the routing graph edges!

    alloc_and_load_edges_and_switches (inode, num_edges, edge_list_head);


    // Edge arrays have now been built up.  Do everything else.

    seg_index = seg_details_z[itrack].index;
    rr_node[inode].cost_index = cost_index_offset // Came as argument.
                                + seg_index;
    rr_node[inode].occ = 0;

    if (route_type == DETAILED) 
      rr_node[inode].capacity = 1;
    else                                    // GLOBAL routing
      rr_node[inode].capacity = chan_width_z[0];//i.e., vias_per_zchan.

    rr_node[inode].xlow = i;
    rr_node[inode].xhigh = i;
    rr_node[inode].ylow = j;
    rr_node[inode].yhigh = j;
    rr_node[inode].zlow = kstart;
    rr_node[inode].zhigh = kend;

    // Currently vias span only between two layers.
    length = kend - kstart;
    //cout<<" "<<length;
    rr_node[inode].R = length * seg_details_z[itrack].Rmetal;
    rr_node[inode].C = length * seg_details_z[itrack].Cmetal;

    rr_node[inode].ptc_num = itrack;
    rr_node[inode].type = CHANZ;
    // ------------------------------------------
  }//for (itrack=0;itrack<vias_per_zchan;
  
  //printf("build_rr_zchan.\n");
}//eof



// ======================================================================= //

// ======================================================================= //

int s_fpga3d::alloc_and_load_chanz_rr_indices_version_02 ( 
                t_seg_details *seg_details_z, // NOT used currently. 
	        // int zchan_density,            // NOT used currently.
	        int vias_per_zchan,
		int start_index )
{
// oBs: this routine should be called once only if num_layers > 1.
//
// Loads the chanz_rr_indices array for all vertical connections/vias.
// In this version 02 we look at an architecture, which has vertical
// segments mixed:  Vias can be between two consecutive layers but also
// between 1st and 3rd, etc. depending how .arch specifies.  
// The frequency and type of vertical segments is dictated by the 
// architecture file, similarly to the horizontal case.
//
// Symplifying RULE: Vias (when less than the number of tracks
// in channels, as it will be the case) will connect to first tracks.
// Also, the density of z-chan's should be varied by a global variable; 
// in order to have more sparsity vertically; we could have z-chan
// only the 1st, the 4th, the 7th etc. rows and columns of clb's
// because anyway (technologically speaking) too many vertical vias are
// not possible, due to their big pitch!
// Channels z exist *only* between switch-boxes, which have neighbor clbs in
// all four corners.  In other words first channel z will be between
// switch-boxes located at i=1,j=1.  
// There is no channel z at i=0,j=any or i=any,j=0 or i=nx,j=any or i=any,j=ny.
// Switch-boxes on layer 0 and L-1 are having vias going below and above
// (asuming counting from top-to-bottom) while switch-boxes in layers 
// 1..L-2 have vias going both below and above.
  int i=0,j=0,k=0;
  int rr_index=0,itrack=0, kstart=0, kend=0, kseg=0;
  rr_index = start_index; // Will count all rr_nodes for all vias.


  // --- 1 Allocate first the whole thing for z channels.
  chanz_rr_indices = (int ****) my_calloc (num_layers, sizeof(int ***));
  for (k=0;k<(num_layers-1);k++) { /*  0..L-2  */
    chanz_rr_indices[k] = 
      (int ***) alloc_matrix3 (0,nx, 0,ny, 0,vias_per_zchan, sizeof(int)); 
  }


  // --- 2 Now generate indices for z channels.
  for (k=0;k<(num_layers-1);k++) { /*  0..L-2  */
    for (i=1; /* not from 0 */ i<=nx-1; /* not till nx+1 */ i++) { 
      for (j=1; /* not from 0 */ j<=ny-1; /* not till ny+1 */ j++) {

	
	// At "location" i,j between layers k,k+1 we have a number 
	// of vias_per_zchan vias, which is normally smaller than the number
	// of tracks in x-chan and y-chan's.  Keep that in mind!
	// oBs:  Deci urm if e un sort of
	// load_chanz_rr_indices()
	for (itrack=0;itrack<vias_per_zchan;itrack++) {
	  // ------------------------------------------
	  kstart = get_closest_seg_start_z (seg_details_z, itrack, k);//START
	  if (kstart != k)
	    continue;  //Don't do anything if this isn't the start of the via. 
	  kend = get_seg_end_z (seg_details_z, itrack, kstart, num_layers-1);//STOP
	  //printf(" [%d %d]-%d-%d-%d ",kstart,kend,i,j,k);

	  for (kseg=kstart;kseg<kend; /* see it: < and not <=  */ kseg++) {
	    chanz_rr_indices[kseg][i][j][itrack] = rr_index;
	  }

	  rr_index++;
	  // ------------------------------------------
	}//for

	
      }//for
    }//for
  }//for


  // ---> Let's throw in, a sanity check - to be on the safe side :) - 
  // to see that all possible (i,j,k) have a valid and reasonable 
  // rr_node_index.
  for (k=0;k<(num_layers-1);k++) { /*  0..L-2  */
    for (i=1; /* not from 0 */ i<=nx-1; /* not till nx+1 */ i++) { 
      for (j=1; /* not from 0 */ j<=ny-1; /* not till ny+1 */ j++) {
	for (itrack=0;itrack<vias_per_zchan;itrack++) {
	  if (chanz_rr_indices[k][i][j][itrack] <= 0) {
	    printf("Error in alloc_and_load_chanz_rr_indices_version_02: "
		   " rr_node at location i=%d j=%d k=%d has index %d "
		   " expected something positive.\n",i,j,k,
		   chanz_rr_indices[k][i][j][itrack]);
	    exit (1);
	  }
	}//for
      }//for
    }//for
  }//for

  
  //printf("Done alloc_and_load_chanz_rr_indices.\n");
  return (rr_index);
}//eof
// ======================================================================= //



// ======================================================================= //
int s_fpga3d::get_seg_end_z (
		 t_seg_details *seg_details, // Is "seg_details_z" in this case.
		 int itrack, 
		 int seg_start, 
		 int max_end) // ie, num_layers in this case.
{
// Returns the segment number (coordinate along the channel z) at which this 
// segment ends.  For a segment spanning clbs from 1 to 4, seg_end is 4.   
// max_end is the maximum dimension of the FPGA in this direction --       
// either nx or ny.                                                        
  int seg_end=0, length=0, norm_start=0;
  
  length = seg_details[itrack].length;
  
  if (seg_start != 0) {
    // E undeva in interiorul architecturii, nu incepe de la border.
    seg_end = min (seg_start + length, max_end);
  }
  else if (seg_details[itrack].longline) {
    seg_end = max_end;
  }
  else {       
    // Incepe chiar de la margine si merge pina unde .start
    // primul segment de lungime lenth on this itrack in channel 0;
    // sau este chiar o via normal care incepe de la 0.
    // start:  index at which a segment starts in channel 0.
    norm_start = seg_details[itrack].start;
    if ( norm_start > 0 )
      seg_end = norm_start; // Short segment, at the left edge of array.
    else
      seg_end = length; // Normal via starts at 0.
  }
  
  return (seg_end);
}//eof
// ======================================================================= //
// ======================================================================= //
int s_fpga3d::get_closest_seg_start_z (
  	       t_seg_details *seg_details, // Is "seg_details_z" in this case.
	       int itrack, 
	       int current_k) // This should go "along k"
{
// Returns where the segment lying on itrack starts along k direction.
//
// cristinel.ababei:  The original get_closest_seg_start(), from which this
// function I developed is redundant and therefore wrong.  In a correct 
// construction of segment_details .start IS (and HAS to be) the same as 
// "current_k"                                                           
  int closest_start=0, length=0, start=0;

  if (seg_details[itrack].longline) {
    closest_start = 0;
  }
  else {
    length = seg_details[itrack].length;
    start = seg_details[itrack].start;
    // length:  length of segment.
    // start:  index at which a segment starts in channel 0.      

    if (current_k > 0) {
      closest_start = current_k - ((current_k - start) % length);
      //cout<<" cs="<<closest_start;
      // deci poate incepe la "current_k" sau mai degraba ca sa zic asa, adica scad
      // an amount care ar ramine dupa ce aflu offset-ul ca in
      // alloc_and_load_seg_details() si l-as scade din length...
      closest_start = max (closest_start, 0);
    }
    else {
      closest_start = 0; 
      // At "k"=0 all segments normal or "fillings" start at 0.
    }
  }//if

 return (closest_start);
}//end
// ======================================================================= //
// ======================================================================= //
void s_fpga3d::get_sanity_check_of_placement(void)
{
// Looks at some stats of a read placement, which 
// is read with read_place() in order to be routed only.
// This is only some sanity check to see how many clb and ios
// are placed in each layer.
  int i=0;
  // 1st resets.
  for (i=0;i<num_layers;i++) 
    {
      balance[ i ] = 0;
      pi_balance[ i ] = 0;
      po_balance[ i ] = 0;
      clb_balance[ i ] = 0;
    }
  // 2nd counting.
  for (i=0;i<num_blocks;i++) 
    {
      balance[ block[i].z ]++; //keep track of how many blocks in layers
      if ( block[i].type == INPAD ) 
	pi_balance[ block[i].z ]++;//pi_balance member of s_fpga3d
      else
	if ( block[i].type == OUTPAD )
	  po_balance[ block[i].z ]++;
	else
	  clb_balance[ block[i].z ]++;
    }//for
  //3rd printing.
  printf(" Situation on each layer of the just read-in placement:");
  for (i=0;i<num_layers;i++) 
    {
      printf("\n === Layer %d: %d blocks - clb:%d pi:%d po:%d",
	     i,balance[i],clb_balance[i],pi_balance[i],po_balance[i]);
     }//for

}//eof
// ======================================================================= //

