// ======================================================================= //
// Cristinel Ababei 10/17/2003
// ======================================================================= //

#include <algorithm>
#include <iostream> 
#include <fstream> 

#include "tt_kmost.h"
#include "tt_hgraph.h"
#include "fpga3d.h"

using namespace std;

extern s_fpga3d * fg3;

// ======================================================================= //
// Top-level only k-most critical paths storage and manipulation.
extern KmostPaths *kmp; 

// ======================================================================= //


// ======================================================================= //
void tt_hgraph::Add_Source_S_Sink_T(void)
{
  // Called only once inside main() to add the source S and sink T for 
  // the top-level timing graph.
  // Create two new vertices S that fanouts to all PIs and T that has 
  // fanins all POs both vertices will have type=4 to distinguish them 
  // from the rest of the hgraph.  They will have indices -2 -1.

  int t_id1=0,t_id2=0;
  char S[]="S",T[]="T";

  //sinkT.setType( 4 );
  sinkT.setDelay( 0 );
  sinkT.ID = -2;
  //sourceS.setType( 4 );
  sourceS.setDelay( 0 );
  sourceS.ID = -1;
  // Build their fanin and fanout lists.
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++){
    // FF-driven vertices are split into PO+PI.  Hence, there will
    // be in my timing graph a path from sourceS-->sinkT.  This is ok
    // because this sort of path will never be enumerated
    // because sourceS is not put as fanin element of the vertex
    // but the vertex is put as fanoutelement for sourceS; 
    // similarly for sinkT

    if ( ( i1->second.Type() == INPAD ) ||
	 ( i1->second.FF == 1 ) )// Driven thru FF vertices 
                                 // represent "virtual" PO+PI nodes.

      {
	t_id1 = i1->second.ID;
	sourceS.addElementInFanout(t_id1);
	// Add it only in fanout of ourceS.  Not needed to add
	// sourceS in fanin of INPADs.
      }
    if ( ( i1->second.Type() == OUTPAD ) ||
	 ( i1->second.FF == 1 ) )// Driven thru FF vertices 
                                 // represent "virtual" PO+PI nodes.
      {
	t_id2 = i1->second.ID;
	sinkT.addElementInFanin(t_id2);
      }
  }//for
}//eof
// ======================================================================= //

// ======================================================================= //
void tt_hgraph::Build_ordFin_ordFout()
{
// Called only once in its actual form inside main(); for the 
// top-level hgraph.  During recursion ordFout and ordFin will have 
// only to be updated: done with another function  copy all fanin and 
// fanout maps into ordFin and ordFout vectors these vectors are 
// only for sorting purposes after the weight in IdWeightPair
// the price paid for these vectors is memory overhead.


  int t_id3=0,t_id4=0,t_id5=0;

  // --- the verts[] themselves
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++) {
    /*
    // ordFin not used actually
    if( (i1->second.fINverts.size() !=0)&&(i1->second.Type() != INPAD) )
    // that is because i might have PI_new (were FF in .map) that 
    // have fanin...
    {
    //i1->second.ordFin.resize( i1->second.fINverts.size() );
    int t_id1=i1->second.ID;
    for(map<int,Element>::iterator i2=i1->second.fINverts.begin();
    i2!=i1->second.fINverts.end();i2++)
    {
    int t_id2 =i2->second.ID;
    IdWeightPair temp_pair1;
    temp_pair1.ID =t_id2;
    temp_pair1.Weight = 0;
    // see Ghanta(DAC89) & Saleh(DAC91) for the above cost function for 
    // all SUCC or PRED of a vertex
    i1->second.ordFin.push_back(temp_pair1);
    }
    }
    */
    if(i1->second.fOUTverts.size() !=0) {
      t_id3 =i1->second.ID;
      for(map<int,Element>::iterator i3=i1->second.fOUTverts.begin();
	  i3!=i1->second.fOUTverts.end();i3++) {
	t_id4 =i3->second.ID;
	if(verts[t_id4].Type() != INPAD) 
	  // when going from PI->PO for STA we stop at flip-flops
	  // and we do not put in ordFout fanout elements, of verts 
	  // type=21, that go into flip-flops...
	  {
	    IdWeightPair temp_pair2;
	    temp_pair2.ID =t_id4;
	    temp_pair2.Weight = i3->second.miu + 
	      verts[t_id4].Delay() + verts[t_id4].max_delay_to_sink;
	    // printf(" %f",temp_pair2.Weight);
	    // See Ghanta(DAC89) & Saleh(DAC91) for the abobe cost 
	    // function for all SUCC or PRED of a vertex
	    i1->second.ordFout.push_back(temp_pair2);
	  }
      }
    }//if
  }//for
  // --- S (no neeed for T) vertices
  for(map<int,Element>::iterator i5=sourceS.fOUTverts.begin();
      i5!=sourceS.fOUTverts.end();i5++) {
    t_id5 =i5->second.ID;
    IdWeightPair temp_pair3;
    temp_pair3.ID =t_id5;
    temp_pair3.Weight = verts[t_id5].max_delay_to_sink;
    sourceS.ordFout.push_back(temp_pair3);
  }//for
  
}//eof
// ======================================================================= //

// ======================================================================= //
void tt_hgraph::Sort_ordFin_ordFout()
{
  // Sort vectors ordFin and ordFout vectors after the Weight inside their
  // IdWeightPair elements.  See Ghanta(DAC89) & Saleh(DAC91)
  
  // --- the verts[] themselves
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++)
    {
      // ordFin not used actually 
      //if( (i1->second.fINverts.size() !=0)&&(i1->second.Type() !=INPAD) )
      //{
      //  sort(i1->second.ordFin.begin(),i1->second.ordFin.end());
      //}
      if(i1->second.fOUTverts.size() !=0)
	{
	  sort(i1->second.ordFout.begin(),i1->second.ordFout.end());
	}
    }
  // --- the S and T vertices
  sort(sourceS.ordFout.begin(),sourceS.ordFout.end());
}//eof
// ======================================================================= //

// ======================================================================= //
void tt_hgraph::Compute_max_delay_to_sink_FromPOsToPIs()
{
  // Backward propagation to compute max_delay_to_sink time
  // needed for kmost algo.  Use frontwave queue.
  queue<int> frontwave; 
  int t_id1=0,t_id2=0;

  // Cleaning first.
  sourceS.max_delay_to_sink = 0;//for just in case
  for (map<int,Vertex>::iterator i0=verts.begin();i0!=verts.end();i0++)
    {
      i0->second.tokens = 0;
      i0->second.max_delay_to_sink = 0;
    }

  // === 1st, put in queue all POs and virtual POs as vertices
  // driven thru FFs.
  for (map<int,Element>::iterator i1=sinkT.fINverts.begin();
       i1!=sinkT.fINverts.end();i1++) {
    t_id1 =i1->second.ID;
    frontwave.push(t_id1);
    //printf("..%d", t_id1);
  }

  // === 2nd, backpropagates process all gates in circuit, i.e. 
  // until "frontwave" queue is empty
  while (!frontwave.empty()) {
    int ver_ID=frontwave.front(); // take it from front of FIFO queue
    frontwave.pop(); // delete it also 
    //reset it here and prepare it in this way for next time:
    verts[ver_ID].tokens=0; 
    //printf(" %d", ver_ID);
    Compute_max_delay_to_sink_FromPOsToPIs_ProcessVertex(ver_ID,&frontwave);
    // Note that new elements are added to queue inside ProcessVertex()!
  }//while

  // === 3rd,compute also the max_delay_to_sink of sourceS
  for(map<int,Element>::iterator i2=sourceS.fOUTverts.begin();
      i2!=sourceS.fOUTverts.end();i2++) {
    t_id2 =i2->second.ID;
    if( verts[t_id2].max_delay_to_sink > sourceS.max_delay_to_sink )
      {
	sourceS.max_delay_to_sink = verts[t_id2].max_delay_to_sink;
      }
  }//for

}//eof
// ======================================================================= //

// ======================================================================= //
void tt_hgraph::Compute_max_delay_to_sink_FromPOsToPIs_ProcessVertex( 
			    int vertex_id, queue<int> * frontwave ){
  //
  // Used in the previous function.
  double cur_max_delay_to_sink=0;
  Vertex *cur_vertex =&verts[vertex_id];
  if ( cur_vertex->Type() != INPAD ) {
    for(map<int,Element>::iterator i2=cur_vertex->fINverts.begin();
	i2!=cur_vertex->fINverts.end();i2++) {
      //cur_max_delay_to_sink = 
      // cur_vertex->max_delay_to_sink + cur_vertex->Delay() + 
      // verts[i2->second.ID].fOUTverts.begin()->second.miu;
      cur_max_delay_to_sink = 
	cur_vertex->max_delay_to_sink + cur_vertex->Delay() + 
	verts[i2->second.ID].fOUTverts[ vertex_id ].miu;

      if (cur_max_delay_to_sink > verts[i2->second.ID].max_delay_to_sink)
	{
	  verts[i2->second.ID].max_delay_to_sink =cur_max_delay_to_sink;
	  //printf(" %f",verts[i2->second.ID].max_delay_to_sink);
	}
      verts[i2->second.ID].tokens++;
      // Driven thru a FF vertices represent "virtual PI" stopping nodes,
      // and I do not add them to "frontwave" because I did it 
      // at the begining, when I added them together with real POs.
      if ( ( verts[i2->second.ID].tokens == 
	     verts[i2->second.ID].fOUTverts.size() ) &&
	   ( verts[i2->second.ID].FF != 1 ) )
	{
	  frontwave->push(i2->second.ID);
	}
    }//for
  }//if
}//eof
// ======================================================================= //
//
//
//
//
//
//
//
//
//
// ======================================================================= //
//
// Path Enumeration itself !  This is really big.
//
// ======================================================================= //
void tt_hgraph::PathEnumeration(char outFileName[])
{
  // Geee, now when I read it after more than one year it's so
  // messy.  How the heck I wrote it, I am not sure.  If you do not 
  // realy need to know exactly how it works, avoid reading
  // this function.
  // Path enumeration as in Saleh(DAC91): see it by all means to 
  // understand the code!!!
  // Only enumerates paths and also computes delays for each of them.
  //
  // OBS: In momentul de fata kmp stocheaza paths definite sau 
  // inclusind si nodurile de tip 31 care sint folosite doar ptr. 
  // STA purposes dar care nu sint trimise ca noduri ale
  // hg lui hmetis!
  // Chiar inainte de a scrie aceste paths in pathFile.txt voi delete 
  // nodurile de tip 31 care din fericire apar doar la sfirsitul unor paths!
  //
  
  //printf("\n Enter PathEnumeration()");

  int reached_sinkT=0;
  int enum_paths=0;
  double cost_function_1=0,cost_function_2=0,branch_slack=0,cur_path_delay=0;
  int next_vertex_id=0,branch_point=0;
  // To keep track whether I added any branch_point; if not then there 
  // is nothing in bpls[] and so there is nothing to add to next_delays
  //  after the path was enumerated!
  int added_any_bp=0;
  int couter_node_on_cur_path=0; // This will be used so that
  // for the first node on the path (not counting the sourceS) I shall
  // allow going on if sourceS enters directly into a FF-driven vertex.

  //----------------------------------------------------------------------
  kmp->setNoOfEnumPaths( 0 );

  //----------------------------------------------------------------------
  // array of K pointers to lists of IdWeightPair objects; each object 
  // in lists is a branch_point from Vertex with rank .ID and 
  // branch_slack stored in .Weight

  typedef list<bpelement> bpl;
  bpl * bpls;
  bpls=(bpl *) new bpl[K]; 
  // bpl == branch_points_list; stores all the branching points from 
  // the critical paths that were already enumerated in non-increasing 
  // order of their "branch_slack"

  //----------------------------------------------------------------------
  deque<IdWeightPair> next_delays; 
  // Deque that will be heapified to give me the max next_delay and 
  // the parent path from which I will branch in generating a new path.

  //----------------------------------------------------------------------
  /*---
    char *fname;
    fname=outFileName;
    ofstream mystream(fname, ios::app);
    if(!mystream) { cout << "error opening file in PrintHGraph()"; exit(0); }
    //mystream<<endl<<endl<<"level = "<<part_level;
    ---*/
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  // Reset flag is_on_kmost for all verts.  This flag is used during
  // rec quad-part for constr propagation downwords.
  for (map<int,Vertex>::iterator i0=verts.begin();i0!=verts.end();i0++)
    i0->second.is_on_kmost =0;

  //----------------------------------------------------------------------
  // Store the 1st most critical path; the easiest:
  // === 1st, check out the sourceS vertex

  kmp->setNoOfEnumPaths( (kmp->NoOfEnumPaths()+1) );
  // 1st vertex in 1st most critical path is sourceS with rank -1:
  kmp->Paths()[enum_paths].push_back(-1);
  couter_node_on_cur_path++;//1
  cur_path_delay =sourceS.Delay(); //i.e., 0
  vector<IdWeightPair>::reverse_iterator vec_it1 = sourceS.ordFout.rbegin();
  next_vertex_id =vec_it1->ID; //next vertex vertex id along critical path
  if (sourceS.fOUTverts.size()>1) {
    // Here it's ok to be fOUTverts instead of ordFout because by 
    // the way hg is build the two fOUTverts and ordFout will be 
    // the same. there is no PI into FF's.
    // fanout at least 2, i.e. there is branch_slack to compute 
    // and to store in bpl
    added_any_bp=1;
    // --- compute branch_slack and branch_point
    cost_function_1 =vec_it1->Weight;   vec_it1++;
    cost_function_2 =vec_it1->Weight;
    //---
    bpelement temp_pair;
    temp_pair.ID =-1; //branch_point means sourceS
    temp_pair.toID =vec_it1->ID;
    temp_pair.Weight =(cost_function_1 - cost_function_2);//branch_slack
    // Push_back now in bpl branch_point along 1st most critical path
    // and sort the list after the entire path was enumerated
    bpls[enum_paths].push_back(temp_pair); 
  }//if

  
  //----------------------------------------------------------------------
  // === 2nd,go along the path towards POs, sinkT and record the
  // path itself and branch_points
  reached_sinkT =0;
  while (reached_sinkT ==0) {

    // --- go on same way until reach sinkT
    // --- add vertex to path:
    kmp->Paths()[enum_paths].push_back( next_vertex_id );
    couter_node_on_cur_path++;//2,3,...
    // Set this vertex as belonging to at least one of the
    // kmost paths.
    if((next_vertex_id !=-1)&&(next_vertex_id !=-2))
      verts[next_vertex_id].is_on_kmost = 1;//Flag needed in rec quad-part.
    Vertex * cur_vertex =&verts[next_vertex_id];
    vector<IdWeightPair>::reverse_iterator vec_it2 = 
                                     cur_vertex->ordFout.rbegin();
    // --- see if reached the sinkT:
    if ( ( cur_vertex->fOUTverts.size() != 0 )  &&
	 ((cur_vertex->FF != 1)||(couter_node_on_cur_path <= 2)) )
      { // NOT driven thru FF vertex.
	next_vertex_id =vec_it2->ID; //update next_vertex_id for next step
	//printf(" %d", next_vertex_id);
	
	//cur_path_delay = cur_path_delay + cur_vertex->Delay() + 
	//                 cur_vertex->fOUTverts.begin()->second.miu;
	// above "cur_path_delay" replaced with the below for correct 
	// computation for dif delay for diff edges of same hedge
	
	cur_path_delay = cur_path_delay + cur_vertex->Delay() + 
	  cur_vertex->fOUTverts[ next_vertex_id ].miu;
	//next_vertex_id is the updated !!!
      }//if
    else 
      {                          // Driven thru FF vertex.  So stop.
	reached_sinkT =1;
	cur_path_delay = cur_path_delay + cur_vertex->Delay();
      }

    // --- see if we have any branch_point from this vertex,
    // but only if I have to continue with the path...
    if ( ( cur_vertex->ordFout.size() > 1 )  &&
	 ((cur_vertex->FF != 1)||(couter_node_on_cur_path <= 2)) )
      { // NOT driven thru FF vertex.
	// was fOUTverts but i corrected to ordFout not to go into FF's...
	// fanout at least 2, i.e. there is branch_slack to compute 
	// and to store in bpl branch-points towards inputs in flip-flops 
	// from nodes type=21 shall not be considered
	added_any_bp=1;
	// --- compute branch_slack and branch_point
	cost_function_1 =vec_it2->Weight; vec_it2++; // e correct caci 
	// lucrez cu ordFout
	cost_function_2 =vec_it2->Weight;
	// ---
	bpelement temp_pair1;
	temp_pair1.ID =cur_vertex->ID;//branch_point
	temp_pair1.toID =vec_it2->ID;
	temp_pair1.Weight =(cost_function_1 - cost_function_2);//branch_slack
	bpls[enum_paths].push_back(temp_pair1);
	// push_back now in bpl branch_point along 1st most critical path
	// and sort the list after the entire path was enumerated
      }//if
    
  }//while(reached_sinkT ==0)


  //----------------------------------------------------------------------
  kmp->Delays()[enum_paths] = cur_path_delay;


  //----------------------------------------------------------------------
  // Sort the bpls[enum_paths]:
  bpls[enum_paths].sort();

	
  //----------------------------------------------------------------------
  // Add corresponding element to next_delays and heapify it
  if ( added_any_bp == 1 ) {
    IdWeightPair temp_pair2;
    temp_pair2.ID =enum_paths;
    temp_pair2.Weight = (kmp->Delays()[enum_paths] -
			 bpls[enum_paths].begin()->Weight);//next_delay
    next_delays.push_back(temp_pair2);
  }


  //----------------------------------------------------------------------
  // Heapify next_delays (done only once)
  make_heap (next_delays.begin(), next_delays.end());

	
  //----------------------------------------------------------------------
  /*---
  // print to file the 1st most critical path and its delay as well 
  // as the branch_points_list:
  mystream<<endl<<" "<<enum_paths<<"-th most critical path delay:  "
  <<cur_path_delay<<endl;
  for(list<int>::iterator it_list=kmp->Paths()[enum_paths].begin();
  it_list!=kmp->Paths()[enum_paths].end();it_list++)
  mystream<<" "<<(*it_list);
  mystream<<endl<<" bpl: "<<endl;
  for(list<bpelement>::iterator i2=bpls[enum_paths].begin();
      i2!=bpls[enum_paths].end();i2++)
  {
  mystream<<"  "<<i2->ID<<"-"<<i2->toID<<"/"<<i2->Weight;
  }
  ---*/
  //----------------------------------------------------------------------


  //----------------------------------------------------------------------
  // At this point the 1st most critical path is recorded in paths[0] and
  // all branch points along it stored (together with their branch_slack)
  // in bpl. bpl is ordered dupa Weight by construction! 
  // now increment the enum_paths:
  enum_paths++;
  //----------------------------------------------------------------------
	
  //
  int parent_id=0,cur_bp_id=0,cur_bpto_id=0;
  //
  //----------------------------------------------------------------------
  // Next do path enumeration from Saleh(DAC91)
  while ((enum_paths < K)&&(!next_delays.empty())) {
    kmp->setNoOfEnumPaths( (kmp->NoOfEnumPaths()+1) );
    couter_node_on_cur_path++;//0 Reset.
    //---------------------------------------------------
    added_any_bp =0;
    //---------------------------------------------------
    cur_path_delay=0;//reset
    next_vertex_id=0;//reset
    cost_function_1=0; cost_function_2=0;//reset
    next_vertex_id=0;//reset
    //---------------------------------------------------
    // find the parent path from whose branch_point (i.e. 1st element 
    // in its bpl list) we will start generating curent path
    deque<IdWeightPair>::iterator pt=next_delays.begin();
    parent_id =pt->ID; // the 1st element in heap is all the time the largest
    // in terms of Weight, that I used to overload > and ==
    //---------------------------------------------------
    // eliminate element from heap next_delays
    pop_heap (next_delays.begin(), next_delays.end());
    next_delays.pop_back();
    //---------------------------------------------------
    // go and delete first element in list bpls[parent_id] and add to 
    // next_delays the new
    // 1st element of bpls[parent_id] if this exists (not emptied already)
    // but before deleting it get the branch_point ID and toID
    list<bpelement>::iterator finbpl=bpls[parent_id].begin();
    cur_bp_id =finbpl->ID;
    cur_bpto_id =finbpl->toID;
    bpls[parent_id].pop_front();
    //mystream<<endl<<(enum_paths+1)<<"  parent path: "<<(parent_id+1);
    //mystream<<"   branch_point: "<<cur_bp_id<<"   branch_to_point: "
    // <<cur_bpto_id;
    //printf("\n %d-th path, parent path: %d, branch-point: %d,"
    //"branch-to-point: %d\n",
    //(enum_paths+1),(parent_id+1),cur_bp_id,cur_bpto_id);
    if (!bpls[parent_id].empty()) {
      // if not empty then take again its first el and put in heap next_delays
      IdWeightPair temp_pair3;
      temp_pair3.ID =parent_id;
      temp_pair3.Weight = (kmp->Delays()[parent_id] - 
			   bpls[parent_id].begin()->Weight); //next_delay
      next_delays.push_back(temp_pair3);
      push_heap (next_delays.begin(), next_delays.end());
    }
    //---------------------------------------------------
    // trace-back the parent path in building curent path
    // this actually means copying the parent path till cur_bp_id
    if(cur_bp_id == -1) { // branch_point is chiar the sourceS
      kmp->Paths()[enum_paths].push_back(-1);
      couter_node_on_cur_path++;//1
      cur_path_delay =sourceS.Delay();
      // check to see if the branching_to vertex is the last one in SUCC
      // if it's not the last one then we have to add a branch_point 
      // for cur_bp_id for curent path enum_paths
      vector<IdWeightPair>::reverse_iterator vec_it3 =sourceS.ordFout.rend();
      vec_it3--;//bring it to point to the last valid object inside vector
      if ( vec_it3->ID != cur_bpto_id ) {
	added_any_bp =1;
	// - find the element in SUCC that has ID==cur_bpto_id
	while (vec_it3->ID != cur_bpto_id) vec_it3--;
	// -- create the element and add it as branch point for cur path
	cost_function_1 =vec_it3->Weight;   vec_it3++;
	cost_function_2 =vec_it3->Weight;
	// ---
	bpelement temp_pair4;
	temp_pair4.ID =-1;//branch_point means sourceS
	temp_pair4.toID =vec_it3->ID;
	temp_pair4.Weight =(cost_function_1 - cost_function_2);//branch_slack
	bpls[enum_paths].push_back(temp_pair4); 
      }
      //else: do nothing, i.e. there is no branch point anymore for sourceS
    }
    else { // branch_point is somewhere upstream
      //copy the parent path up to the cur_bp_id
      list<int>::iterator path_iter=kmp->Paths()[parent_id].begin();
      while ( (*path_iter) != cur_bp_id ) {
	kmp->Paths()[enum_paths].push_back(*path_iter);
	couter_node_on_cur_path++;//1,2,3...up_to branch point - 1.
	// Set this vertex as belonging to at least one of the
	// kmost paths.
	if((*path_iter !=-1)&&(*path_iter !=-2))
	  verts[*path_iter].is_on_kmost = 1;//Flag needed in rec quad-part.
	if ( (*path_iter) != -1 ) {
	  int local_present = (*path_iter);
	  path_iter++;
	  int local_future = (*path_iter);
	  
	  //cur_path_delay = cur_path_delay + verts[ local_present ].Delay() +
	  //                 verts[ local_present ].fOUTverts.begin()->second.miu;
	  // above "cur_path_delay" replaced with the below for correct 
	  // computation for dif delay  for diff edges of same hedge
	  cur_path_delay = cur_path_delay + verts[ local_present ].Delay() +
	    verts[ local_present ].fOUTverts[ local_future ].miu;
	}
	else
	  path_iter++;//sourceS has no delay, so only advance ++
	// we do not record for cur path any branch_point pe calea comuna pina 
	// la cur_bp_id starting with cur_bp_id however we look 
	// for branch_points	
	//path_iter++; //done above in computation of cur_path_delay !!! ababei 
      }
      //process the cur_bp_id itself:
      kmp->Paths()[enum_paths].push_back(cur_bp_id);
      couter_node_on_cur_path++;//branch point.
      // Set this vertex as belonging to at least one of the
      // kmost paths.
      if((cur_bp_id !=-1)&&(cur_bp_id !=-2))
	verts[cur_bp_id].is_on_kmost = 1;//Flag needed in rec quad-part.
      //cur_path_delay = cur_path_delay + verts[cur_bp_id].Delay() +
      //                 verts[cur_bp_id].fOUTverts.begin()->second.miu;
      // above "cur_path_delay" replaced with the below for correct 
      // computation for dif delay for diff edges of same hedge
      cur_path_delay = cur_path_delay + verts[cur_bp_id].Delay() +
	verts[cur_bp_id].fOUTverts[ cur_bpto_id ].miu;

      // check to see if the branching_to vertex is the last one in SUCC
      // if it's not the last one then we have to add a branch_point for
      //  cur_bp_id for curent path enum_paths
      vector<IdWeightPair>::reverse_iterator vec_it4 = 
	verts[cur_bp_id].ordFout.rend();
      vec_it4--;//bring it to point to the last valid object inside vector
      if ( vec_it4->ID != cur_bpto_id ) {
	added_any_bp =1;
	// - find the element in SUCC that has ID==cur_bpto_id
	while ( vec_it4->ID != cur_bpto_id ) vec_it4--;
	// -- create the element and add it as branch point for cur path
	cost_function_1 =vec_it4->Weight;   vec_it4++;
	cost_function_2 =vec_it4->Weight;
	// ---
	bpelement temp_pair5;
	temp_pair5.ID =cur_bp_id;//branch_point
	temp_pair5.toID =vec_it4->ID;
	temp_pair5.Weight =(cost_function_1 - cost_function_2);//branch_slack
	bpls[enum_paths].push_back(temp_pair5);
	// push_back now in bpl branch_point along enum_paths-th most 
	// critical pathand sort the list after the entire path was enumerated
      }
      //else: do nothing, i.e. there is no branch point anymore for 
      // cur_bp_id in the new path	
    }//else
    //---------------------------------------------------
    // at this point the common part from the parent path was copied in 
    // the new path and also the cur_bp_id was processed (in terms of 
    // delay and branch_points) irrespective of whether it is sourceS 
    // or somewhere upstream
    //---------------------------------------------------
    //
    next_vertex_id =cur_bpto_id;
    //---------------------------------------------------
    // trace-forward the curent path and also add branch_points 
    // accordingly in bpls[enum_paths]
    reached_sinkT =0;//reset
    while ( reached_sinkT == 0 ) {
      //go on same way until reach sinkT
      // --- add vertex to path:
      kmp->Paths()[enum_paths].push_back( next_vertex_id );
      couter_node_on_cur_path++;//2 if starts from sourceS or bigger.
      // Set this vertex as belonging to at least one of the
      // kmost paths.
      if((next_vertex_id !=-1)&&(next_vertex_id !=-2))
	verts[next_vertex_id].is_on_kmost = 1;//Flag needed in rec quad-part.
      Vertex * cur_vertex2 =&verts[next_vertex_id];
      vector<IdWeightPair>::reverse_iterator vec_it5 = 
	cur_vertex2->ordFout.rbegin();
      // --- see if reached the sinkT:
      if ( ( cur_vertex2->fOUTverts.size() != 0 )  &&
	   ((cur_vertex2->FF != 1)||(couter_node_on_cur_path <= 2)) )
	{// NOT driven thru FF vertex.
	  next_vertex_id =vec_it5->ID; //update next_vertex_id for next step
	  
	  //cur_path_delay = cur_path_delay + cur_vertex2->Delay() + 
	  //                 cur_vertex2->fOUTverts.begin()->second.miu;
	  // above "cur_path_delay" replaced with the below for correct 
	  // computation for dif delay for diff edges of same hedge
	  cur_path_delay = cur_path_delay + cur_vertex2->Delay() + 
	    cur_vertex2->fOUTverts[ next_vertex_id ].miu;
	  //next_vertex_id is the updated !!!
	}
      else 
	{
	  reached_sinkT =1;
	  cur_path_delay = cur_path_delay + cur_vertex2->Delay();
	}
      // --- see if we have any branch_point from this vertex
      // but only if I have to continue with the path...
      if ( ( cur_vertex2->ordFout.size() > 1 )  &&
	   ((cur_vertex2->FF != 1)||(couter_node_on_cur_path <= 2)) )
	{// NOT driven thru FF vertex.
	  // replaced fOUTverts with ordFout not to run into FF's...
	  // fanout at least 2, i.e. there is branch_slack to compute 
	  // and to store in bpl
	  added_any_bp =1;
	  // --- compute branch_slack and branch_point
	  cost_function_1 =vec_it5->Weight;   vec_it5++;
	  cost_function_2 =vec_it5->Weight;
	  // ---
	  bpelement temp_pair6;
	  temp_pair6.ID =cur_vertex2->ID;//branch_point
	  temp_pair6.toID =vec_it5->ID;
	  temp_pair6.Weight =(cost_function_1 - cost_function_2);//branch_slack
	  bpls[enum_paths].push_back(temp_pair6);
	  // push_back now in bpl branch_point along enum_paths-th most 
	  // critical path and sort the list after the entire path was enumerated
	}
    }//while(reached_sinkT ==0)
    //---------------------------------------------------
    //
    kmp->Delays()[enum_paths] =cur_path_delay;
    //---------------------------------------------------
    // sort the bpls[enum_paths]:
    bpls[enum_paths].sort();
    //---------------------------------------------------
    // now since the new path was enumerated place its first element in
    //  next_delays
    if ( added_any_bp == 1 ) {
      IdWeightPair temp_pair7;
      temp_pair7.ID =enum_paths;
      temp_pair7.Weight =(kmp->Delays()[enum_paths] - 
			  bpls[enum_paths].begin()->Weight); //next_delay
      next_delays.push_back(temp_pair7);
      push_heap (next_delays.begin(), next_delays.end());
    }
    //---------------------------------------------------
    //
    enum_paths++; //so,yet another path was added
  }//while(enum_paths < K)
  //----------------------------------------------------------------------


  //----------------------------------------------------------------------
  //printf("\n --------------------- kmp->Delays()[0] = %f ",kmp->Delays()[0]);

  //----------------------------------------------------------------------
  /*---
  // print to file the K most critical paths and its delay as well as 
  // the branch_points_list:
  mystream<<endl;
  for(int i=0;i<kmp->NoOfEnumPaths();i++)
  {
  mystream<<endl<<kmp->Delays()[i]<<" ";
  
  //mystream<<endl<<(i+1)<<"-th path:"<<endl<<"  "<<kmp->Delays()[i]<<"   ";
  //mystream<<endl;
  for(list<int>::iterator it_list1=kmp->Paths()[i].begin();
  it_list1!=kmp->Paths()[i].end();it_list1++)
  {
  //mystream.width(6);
  mystream<<" "<<(*it_list1);
  }
  //mystream<<endl;
  //int temp_id=0;
  //for(list<int>::iterator it_list2=kmp->Paths()[i].begin();
  // it_list2!=kmp->Paths()[i].end();it_list2++)
  //  {
  //    if((*it_list2) != -1)
  //	{
  //  mystream.width(6);
  //  temp_id = verts[(*it_list2)].block;
  //  printf(" %d",temp_id);
  //  mystream<<" "<<temp_id;
  //}
  //  else 
  //{
  //  mystream.width(6);
  //  mystream<<"NIL";
  //}
  //}
  //mystream<<endl<<"  bpl:   ";
  //for(list<bpelement>::iterator i2=bpls[i].begin();i2!=bpls[i].end();i2++)
  //  {
  //    mystream<<"  "<<i2->ID<<"/"<<i2->toID<<"/"<<i2->Weight;
  //  }
  }
  ---*/
  //----------------------------------------------------------------------


  //----------------------------------------------------------------------
  // Finally, (gee i thought it was never gonna end!) free all the memory 
  // used and allocated with new above
  // because you will repeat this process of finding kmost paths...
  //
  // free branch_point_lists associated to each of the K paths:
  for(int u=0;u<K;u++)
    while(!bpls[u].empty())	
      bpls[u].pop_front();
  // free the next_delays heap:
  while(!next_delays.empty())
    next_delays.pop_back();
  //----------------------------------------------------------------------
  //  printf("\n K=%d   Enumerated=%d",K,kmp->NoOfEnumPaths());
  //----------------------------------------------------------------------



  //----------------------------------------------------------------------
  /*---
  // print wire delays along most critical path
  // for debug purposes in the looping of top-level part only
  printf("\n kmp->Delays()[0]: %e",kmp->Delays()[0]);
  for(list<int>::iterator it_list8=kmp->Paths()[0].begin();
  it_list8 != kmp->Paths()[0].end();it_list8++)
  {
  if((*it_list8) != -1) // the sourceS is not a valid vertex
    cout<<" "<<verts[(*it_list8)].fOUTverts.begin()->second.miu;
  }
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ---*/
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  // Now go tru all vertices and for those with correspondent in 
  // fpga3d block[] set their correspondents as "in_on_kmost"=1.
  // Needed for constratints propagations to lower layers.
  for(map<int,Vertex>::iterator io=verts.begin();io!=verts.end();io++) 
    {
      if( (io->second.is_on_kmost == 1)&&
	  (io->second.netID_fpga3d != -1) )
	{
	  // This vertex has clb correspondent
	  fg3->block[io->second.blockID].is_on_kmost = 1;
	  //printf(" %d",io->second.blockID);
	}
    }//for
 
  //printf("\nDone PathEnumeration.");
}//eof
// ======================================================================= //
//
//
//
//
//
//
//
//
//
// ======================================================================= //
void tt_hgraph::NormalizeAllPathsDelaysWithRespectTo( double norm )
{
  double temp =0;
  double max_delay =kmp->Delays()[0];
  for(int i=0;i<kmp->NoOfEnumPaths();i++)
    {
      temp = (kmp->Delays()[i]/max_delay)*norm;
      kmp->Delays()[i] = temp;
    }
}//eof
// ======================================================================= //

// ======================================================================= //
double tt_hgraph::FindDelayOfNewFirstMostCriticalPath()
{
  double res=0;
  int next_vertex_id=0;
  int reached_sinkT=0;
  
  res =sourceS.Delay(); // i.e. 0
  vector<IdWeightPair>::reverse_iterator vec_it1 =sourceS.ordFout.rbegin();
  next_vertex_id =vec_it1->ID; // next vertex vertex id along critical path
  reached_sinkT =0;
  while(reached_sinkT ==0) 
    {   
      Vertex * cur_vertex =&verts[next_vertex_id];
      vector<IdWeightPair>::reverse_iterator vec_it2 = 
	                     cur_vertex->ordFout.rbegin();
      //---see if reached the sinkT:
      if(cur_vertex->fOUTverts.size() !=0){
	res = res + cur_vertex->Delay() + 
	  cur_vertex->fOUTverts.begin()->second.miu;
	next_vertex_id =vec_it2->ID; // update next_vertex_id for next step
      }
      else{
	reached_sinkT =1;
	res = res + cur_vertex->Delay();
      }
    }//while(reached_sinkT ==0)
  return res;
}//eof
// ======================================================================= //

// ======================================================================= //
double tt_hgraph::FindNewDelayForOldPath( int index, KmostPaths *f_kmpl )
{
  double res=0;
  
  for(list<int>::iterator it_list=f_kmpl->Paths()[index].begin();
      it_list!=f_kmpl->Paths()[index].end();it_list++)
    {
      int v = (*it_list);
      if(v != -1)
	{
	  Vertex * cur_vertex =&verts[v];
	  if(cur_vertex->fOUTverts.size() !=0)
	    res = res + cur_vertex->Delay() + 
	      cur_vertex->fOUTverts.begin()->second.miu;
	  else
	    res = res + cur_vertex->Delay();
	}
    }
  return res;
}//eof
// ======================================================================= //

// ======================================================================= //
void tt_hgraph::Empty_Paths_Inside_kmp()
{
  // Needed to re-enumerate all paths in the circuit
  // during the re-enumeration all kmp->Paths() will be created again
  //printf("\n   ...cleaning kmp->Paths...\n");
  for(int i=0;i<kmp->NoOfEnumPaths();i++)
    {
      kmp->Delays()[i] =0;
      while(!kmp->Paths()[i].empty())
	kmp->Paths()[i].pop_back();
    }
}//eof
// ======================================================================= //

// ======================================================================= //

// ======================================================================= //
void tt_hgraph::PathEnumerationDuringRecursion(void)
{
  // Path re-enumeration during recursions.
  // kmp has to be emptied first because now k-most paths may be 
  // different from those that were k-most at previous level

  Compute_max_delay_to_sink_FromPOsToPIs();
  UpdateInfoInside_ordFin_ordFout();
  Sort_ordFin_ordFout();
  // PrintHGraph_StuffRelatedTo_Kmost_Calculations( "dbg_cristi_ordfout" );
  Empty_Paths_Inside_kmp();
  PathEnumeration("dbg_reenum_paths.echo");

  //printf("\nDone R e E n u m e r a t i o n.");
}//eof
// ======================================================================= //

// ======================================================================= //
void tt_hgraph::UpdateInfoInside_ordFin_ordFout()
{
  // During recursion ordFout and ordFin will have only to be updated
  // because their creation took place inside Build_ordFin_ordFout() 
  // that was called only once

  // do nothing about ordFin this time (as opposed inside 
  // Build_ordFin_ordFout()) because it turned out that these ordFin 
  // were not really needed
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++) {
    if(i1->second.fOUTverts.size() != 0 ) {
      for(vector<IdWeightPair>::reverse_iterator vec_it1 = 
	    i1->second.ordFout.rbegin();
	    vec_it1!=i1->second.ordFout.rend();vec_it1++)
	{
	  int t_id1=vec_it1->ID;
	  vec_it1->Weight = i1->second.fOUTverts[t_id1].miu + 
	    verts[t_id1].Delay() + verts[t_id1].max_delay_to_sink;
  }   }   }
  // === S
  for(vector<IdWeightPair>::reverse_iterator vec_it2=sourceS.ordFout.rbegin();
      vec_it2!=sourceS.ordFout.rend();vec_it2++) {
    int t_id2 =vec_it2->ID;
    vec_it2->Weight=verts[t_id2].max_delay_to_sink;
  } 
}//eof
// ======================================================================= //

// ======================================================================= //

// ======================================================================= //
void tt_hgraph::DumpKPathsAndVerticesPartitions( char outFileName[] )
{
  // prints K paths and .block of each node on these paths
  // also K_j is printed
  // all these are from top-level hg

  char *fname;
  fname=outFileName;
  ofstream mystream(fname);
  if(!mystream) { cout << "error opening file in PrintHGraph()"; exit(0); }
  
  mystream<<endl;
  //mystream<<"Lev: "<<part_level;
  for(int j=0;j<kmp->NoOfEnumPaths();j++) { //kmp is global
    // ========================================================
    /*---
      mystream<<endl;
      for(list<int>::iterator it_list1=kmp->Paths()[j].begin();
      it_list1!=kmp->Paths()[j].end();it_list1++)
      {
      mystream.width(6);
      mystream<< (*it_list1 )<<" ";
      }
      ---*/
    // ========================================================
    mystream<<endl;
    for(list<int>::iterator it_list2=kmp->Paths()[j].begin();
	it_list2!=kmp->Paths()[j].end();it_list2++)
      {
	if((*it_list2) != -1)
	  {
	    mystream.width(6);
	    //mystream<< (verts[(*it_list2)].block) <<" ";
	  }
      }
    mystream<<"   Kj: "<< (kmp->K_j_edge()[j]); // cut edges along path!
    //mystream<<endl;
  }//for

  // if at lower level print also the fragments transmitted to only 
  // this local subgraph
  // these fragments are derived from top-level paths just printed above
  // 1st build the map used above to write inside pathFile.txt correct 
  // vertex indices: 
  IntMapType toplevelID2localID; 
  IntMapType localID2toplevelID; 
  int contor=0;
  map<int,Vertex>::iterator loc_i1=verts.begin();//top-level or local subhgraph
  while(loc_i1 != verts.end())
    {
      if(loc_i1->second.Type() !=31)
	{
	  toplevelID2localID[loc_i1->second.ID] =contor;
	  localID2toplevelID[contor] =loc_i1->second.ID;
	  contor++;
	}
      loc_i1++;
    }
  // 2nd do the work:
  mystream<<"\nFragments: "; 
  for(int j=0;j<kmp->NoOfEnumPaths();j++) { //kmp is global
    // ========================================================
    // for levels >= 2nd we work on subgraphs...
    mystream<<endl;
    /*---
    if(part_level >1) {
      if(kmp->No_of_verts_in_frag_i_in_path_j()[j].size() != 0) {
	list<int>::iterator verts_seq = 
	  kmp->Verts_in_frag_i_in_path_j()[j].begin();
	for(list<int>::iterator it_list3  = 
	      kmp->No_of_verts_in_frag_i_in_path_j()[j].begin();
	      it_list3 != kmp->No_of_verts_in_frag_i_in_path_j()[j].end();
	      it_list3++) {
	  int no_verts_for_cur_frag = (*it_list3);
	  for(int u=0; u<(no_verts_for_cur_frag -0);u++)
	    { 
	      mystream.width(6);
	      mystream<<( verts[ localID2toplevelID[(*verts_seq)] ].block )
		      <<" ";
	      verts_seq++;
	    }
	}
      }
    }//if(part_level >1)
    ---*/
    // ========================================================
  }//for
  
}//eof
// ======================================================================= //



// ======================================================================= //
// Next function prints out `some statistics after each K most critical 
// path update
/*---
void tt_hgraph::PrintSomeStatisticsAfterEachBipartitioning ( 
		       tt_hgraph * temp_hg, char outFileName[] ) 
{
  // prints after each bipartitioning
  // -- number K of paths that we work with
  // -- number of previous critical paths that remained critical
  // -- number of paths that were transmitted to this subgraph bipart
  // -- delay of most critical path before bipart
  // -- delay of most critical path after bipart
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  char *fname;
  fname=outFileName;
  ofstream mystream(fname, ios::app);
  if(!mystream) { cout << "error opening file in PrintHGraph()"; exit(0); }
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  mystream<<endl<<"K="<<kmp->NoOfEnumPaths()<<"  ";
  printf("\n K=%d ",kmp->NoOfEnumPaths());	 
  //---------------------------------------------------
  //---------------------------------------------------
  // create a local copy of kmp with delays normalized:
  KmostPaths *kmpl=new KmostPaths;
  ll * temp_paths=(ll *)new ll[kmp->NoOfEnumPaths()];
  kmpl->setPaths(temp_paths);
  double * temp_delays=(double*) new double[kmp->NoOfEnumPaths()];
  kmpl->setDelays(temp_delays);
  kmpl->setNoOfEnumPaths( kmp->NoOfEnumPaths() );
  for(int x=0;x<kmp->NoOfEnumPaths();x++)
    {
      kmpl->Delays()[x] =kmp->Delays()[x];
      for(list<int>::iterator it_list=kmp->Paths()[x].begin();
	  it_list!=kmp->Paths()[x].end();it_list++)
	kmpl->Paths()[x].push_back( (*it_list) );
    }
  //---------------------------------------------------
  // create an array with non -1 elements for all ranks that are 
  // verts in these paths
  map<int,int> prev_map;
  for(int x1=0;x1<kmpl->NoOfEnumPaths();x1++)
    {
      for(list<int>::iterator it1=kmpl->Paths()[x1].begin();
	  it1!=kmpl->Paths()[x1].end();it1++)	   
	if((*it1) != -1)
	  prev_map[(*it1)] = (*it1);
    }
  //---------------------------------------------------
  


  //temp_hBiPartitioning();



  //---------------------------------------------------
  // some cleaning:
  for(map<int,Vertex>::iterator i1=verts.begin();i1!=verts.end();i1++)
    i1->second.max_delay_to_sink =0;
  
  Compute_max_delay_to_sink_FromPOsToPIs();
  UpdateInfoInside_ordFin_ordFout();
  Sort_ordFin_ordFout();
  double new_max_delay = FindDelayOfNewFirstMostCriticalPath();
  //---------------------------------------------------
  //---------------------------------------------------
  // find delays and paths after partitioning
  // empty kmp and update it
  Empty_Paths_Inside_kmp();
  PathEnumeration(critFileName);
  //critFileName is global but nothing is written in it this call!
  //---------------------------------------------------
  //---------------------------------------------------	 
  mystream<<" Prev_D0="<<kmpl->Delays()[0]<<"  ";
  printf(" Prev_D0=%f ",kmpl->Delays()[0]);
  //---------------------------------------------------
  mystream<<" New_D0="<<kmp->Delays()[0]<<"  ";
  printf(" New_D0=%f ",kmp->Delays()[0]);
  //---------------------------------------------------
  //---------------------------------------------------
  // count how many of previous critical paths are still critical
  int no_of_prev_crit_still_crit=0;
  for(int x=0;x<kmpl->NoOfEnumPaths();x++)
    {
      // take paths one by one from local kmpl and check it againt any 
      // of those new in kmp to see
      // whether it is among them or not
      int found_prev_among_new=0;
      int y=0;
      while( (y<kmp->NoOfEnumPaths())&&(found_prev_among_new ==0) )
	{ 
	  // while not found prev path among new path or not checked 
	  //all new path keep looking
	  int identical_so_far=1;
	  list<int>::iterator it_local=kmpl->Paths()[x].begin();
	  //go thru previous kmpl
	  list<int>::iterator it_new=kmp->Paths()[y].begin();//go thru new kmp
	  while( (it_local!=kmpl->Paths()[x].end())&&
		 (it_new!=kmp->Paths()[y].end())&&(identical_so_far == 1) )
	    {
	      if( (*it_local) != (*it_new) )  identical_so_far=0;
	      it_local++;
	      it_new++;
	    }
	  if(identical_so_far == 1) 
	    // means that last two-path comparison ended by discovering 
	    //old path still critical
	    found_prev_among_new =1;
	  
	  y++;
	}
      if(found_prev_among_new ==1)
	no_of_prev_crit_still_crit++;
    }//for
  // at this time no_of_prev_crit_still_crit sould be equal to the number of 
  //prev critical paths that are still critical
  //---------------------------------------------------
  mystream<<" Remained_crit="<<no_of_prev_crit_still_crit<<"  ";
  printf(" Remained_crit=%d ",no_of_prev_crit_still_crit);
  //---------------------------------------------------
  //---------------------------------------------------
  // see how many verts were in K paths prior to call hmetis
  // and how many in after and how many remained common
  // create an array with non -1 elements for all ranks that are verts in 
  // these new paths
  map<int,int> new_map;
  for(int x2=0;x2<kmp->NoOfEnumPaths();x2++)
    {
      for(list<int>::iterator it2=kmp->Paths()[x2].begin();
	  it2!=kmp->Paths()[x2].end();it2++)
	if((*it2) != -1)
	  new_map[(*it2)] = (*it2);
    }
  int common_verts=0;
  int total_verts_prev=0;
  map<int,int>::iterator it_prev=prev_map.begin();
  map<int,int>::iterator it_new=new_map.begin();
  while( it_prev!=prev_map.end() )
    {
      if(it_prev->second != 0)
	if(new_map[it_prev->second] !=0) common_verts++;
      it_prev++;
    }
  it_prev=prev_map.begin();//reset
  while( it_prev!=prev_map.end() )
    {
      if(it_prev->second != 0)   total_verts_prev++;
      it_prev++;
    }
  mystream<<" Common_verts="<<common_verts<<"|"<<total_verts_prev;
  printf(" Common_verts=%d|%d ",common_verts, total_verts_prev);
  
  // de-allocate map memory
  
  //---------------------------------------------------
  
  
  //---------------------------------------------------
  
  
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
}//eof
---*/
// ======================================================================= //


// ======================================================================= //
void tt_hgraph::Dump_KPaths( char outFileName[],int how_many )
{
  // prints how_many paths out of K most critical paths 
  // and .block of each node on these paths.
  // how_many <= K always

  char *fname;
  fname=outFileName;
  ofstream mystream(fname);
  if(!mystream) {cout << "Error: Cannot open file in Dump_KPaths"; exit(0);}
  
  // print in first line number of paths to be dumped.
  // if required how_many is > K, dump all K paths
  mystream<<how_many<<endl;


  if ( how_many > kmp->NoOfEnumPaths() )
    how_many = kmp->NoOfEnumPaths();


  int temp_block_id=0;
  int counter=0;
  int curr_id=0, prev_id=-100;//used to skip any other vertex because
  // by the way tt_hg was built we have a vertex for ipin and one for opin 
  // along a path because both are nodes in timing graph.
  // Delay, Num of cells on path, Set of cell ids
  for (int i=0; i<how_many; i++)
    {

      mystream<<kmp->Delays()[i];
      // count how many useful blocks on path
      counter = 0; 
      curr_id=0; prev_id = -100; // reset
      for (list<int>::iterator it_list1=kmp->Paths()[i].begin();
	   it_list1!=kmp->Paths()[i].end();it_list1++)
	{
	  if ( (*it_list1) != -1 ) {
	    curr_id = verts[(*it_list1)].blockID;	    
	    if ( curr_id != prev_id ) {
	      counter = counter + 1;
	      prev_id = verts[(*it_list1)].blockID;
	    }
	  }
	}
      mystream<<" "<<counter;
      curr_id=0; prev_id = -100; // reset
      // write down all "counter" block ids
      for (list<int>::iterator it_list2=kmp->Paths()[i].begin();
	   it_list2!=kmp->Paths()[i].end();it_list2++)
	{
	  if ( (*it_list2) != -1 ) {
	    curr_id = verts[(*it_list2)].blockID;	    
	    if ( curr_id != prev_id ) {
	      temp_block_id = verts[(*it_list2)].blockID;
	      mystream<<" "<<temp_block_id;
	      prev_id = verts[(*it_list2)].blockID;
	    }
	  }
	}
      mystream<<endl;

    }// for

  mystream.close();
}//eof
// ======================================================================= //

