
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <iostream>

#include "util.h"
#include "ipr_types.h"
#include "fpga3d.h"


using namespace std;


// ======================================================================= //
// ================ Types and defines exported by rr_graph.cpp:
struct s_linked_vptr *rr_mem_chunk_list_head = NULL;   
// Used to free "chunked" memory.  If NULL, no rr_graph exists right now.

int chunk_bytes_avail = 0;
char *chunk_next_avail_mem = NULL;
// Status of current chunk being dished out by calls to my_chunk_malloc.

/*define a structure to keep track of the internal variables allocated inside */
/*build_rr_graph. This is required so that we can later free them in          */
/*free_rr_graph_internals. If this seems like a hack, it is. The original code*/
/*was able to do everything within one procedure, however since the timing    */
/*driven placer needs to continuously change the mappping of the nets to the  */
/*rr terminals, we need to keep the internal structures active until we finish*/
/*placement -Sandy Nov 10/98*/

struct s_rr_graph_internal_vars {
  int nodes_per_chan;
  int vias_per_zchan;
  int ***rr_node_indices;
  int **pads_to_tracks;
  struct s_ivec **tracks_to_clb_ipin;
  struct s_ivec *tracks_to_pads;
  int ***clb_opin_to_tracks;
  int ***clb_ipin_to_tracks;
  t_seg_details *seg_details_x;
  t_seg_details *seg_details_y;
  t_seg_details *seg_details_z;
};

struct s_rr_graph_internal_vars rr_graph_internal_vars;
// ======================================================================= //


// ======================================================================= //
// =================== Subroutine definitions ============================ //
// ======================================================================= //
int ***s_fpga3d::get_rr_node_indices()
{
  return (rr_graph_internal_vars.rr_node_indices);
}
// ======================================================================= //

int s_fpga3d::get_nodes_per_chan() 
{
  return (rr_graph_internal_vars.nodes_per_chan);
}
// ======================================================================= //

void s_fpga3d::build_rr_graph (
         enum e_route_type route_type, 
	 struct s_det_routing_arch det_routing_arch, 
	 t_segment_inf *segment_inf,
 	 t_segment_inf *via_inf,
         t_timing_inf timing_inf, 
         enum e_base_cost_type base_cost_type) 
{

  // Builds the routing resource graph needed for routing.  Does all the   
  // necessary allocation and initialization.  If route_type is DETAILED   
  // then the routing graph built is for detailed routing, otherwise it is 
  // for GLOBAL routing.                                                   

 int nodes_per_clb=0,  nodes_per_pad=0;
 int nodes_per_chan=0, vias_per_zchan=0;
 float Fc_ratio=0;
 bool perturb_ipin_switch_pattern;
 int ***rr_node_indices;
 int Fc_output=0, Fc_input=0, Fc_pad=0;
 int **pads_to_tracks;
 struct s_ivec **tracks_to_clb_ipin, *tracks_to_pads;
 int k=0;

 // [0..pins_per_clb-1][0..3][0..Fc_output-1].  List of tracks this pin 
 // connects to.  Second index is the side number (see pr.h).  If a pin 
 // is not an output or input, respectively, or doesn't connect to      
 // anything on this side, the [ipin][iside][0] element is OPEN.        

 int ***clb_opin_to_tracks, ***clb_ipin_to_tracks;

 t_seg_details *seg_details_x, *seg_details_y, *seg_details_z; 
 // The above are NOT "created" for each layer individually because they
 // would store exactly the same infor for all layers.  They will be
 // populated in alloc_and_load_rr_node_indices() in rr_graph2.cpp

 nodes_per_clb = num_class + pins_per_clb;
 nodes_per_pad = 4 * io_rat;    // SOURCE, SINK, OPIN, and IPIN

 if (route_type == GLOBAL) {
    nodes_per_chan = 1;
    vias_per_zchan = 1;
 }
 else {
   nodes_per_chan = chan_width_x[0];// Same for both xchan and ychan.
   vias_per_zchan = chan_width_z[0];//[  ?  ] Set where?
 }

 // ------------------------------------

 chanx_rr_indices = (int ****) my_calloc (num_layers+1, sizeof(int ***));
 chany_rr_indices = (int ****) my_calloc (num_layers+1, sizeof(int ***)); 
 // chanx_rr_indices, chany_rr_indices: give  rr_node  index for 
 // each track in each (i,j,k) channel segment.  The rest of them
 // will be allocated inside alloc_and_load_rr_node_indices() in rr_graph2.cpp
 // They will be heavily used inside get_rr_node_index().


   seg_details_x = alloc_and_load_seg_details (
		     nodes_per_chan, vias_per_zchan, segment_inf,// segment 
		     det_routing_arch.num_segment, 
		     det_routing_arch.num_via_types,
		     nx, CHANX);
   seg_details_y = alloc_and_load_seg_details (
		     nodes_per_chan, vias_per_zchan, segment_inf,// segment  
		     det_routing_arch.num_segment, 
		     det_routing_arch.num_via_types,
		     ny, CHANY);
   seg_details_z = alloc_and_load_seg_details (
		     nodes_per_chan, vias_per_zchan, via_inf,    // via 
		     det_routing_arch.num_segment, 
		     det_routing_arch.num_via_types,
		     num_layers-1, // max_len
		     CHANZ);


#ifdef DEBUG
   // cristia: next two files will contain info only for last k
   //dump_seg_details (seg_details_x, nodes_per_chan, "x.echo");
   //dump_seg_details (seg_details_y, nodes_per_chan, "y.echo");
#endif

   // To set up the routing graph I need to choose an order for the rr_indices. 
   // For each (i,j) slot in the FPGA, the index order is [source+sink]/pins/   
   // chanx/chany. The dummy source and sink are ordered by class number or pad 
   // number; the pins are ordered by pin number or pad number, and the channel 
   // segments are ordered by track number.  Track 1 is closest to the "owning" 
   // block; i.e. it is towards the left of y-chans and the bottom of x-chans.  
   // To maximize cache effectiveness, I put all the rr_nodes associated with  
   // a block together (this includes the "owned" channel segments).  I start   
   // at (0,0) (empty), go to (1,0), (2,0) ... (0,1) and so on.                 
   

   // NB:  Allocates data structures for fast index computations -- more than 
   // just rr_node_indices are allocated by this routine.  
   // cristinel.ababei                   
   // OBS: Will also do it for rr_nodes corresponding to vias.   
   rr_node_indices = 
     alloc_and_load_rr_node_indices (
		 nodes_per_clb, nodes_per_pad, 
		 nodes_per_chan, vias_per_zchan,
		 det_routing_arch.num_via_types, // Num of different vias.
		 seg_details_x, seg_details_y, seg_details_z);
  
 // ------------------------------------

 // Next thing was moved inside alloc_and_load_rr_node_indices() above
 //--->num_rr_nodes = num_layers * rr_node_indices[0][nx+1][ny+1];


 if (det_routing_arch.Fc_type == ABSOLUTE) {
    Fc_output = (int) min (det_routing_arch.Fc_output, nodes_per_chan);
    Fc_input = (int) min (det_routing_arch.Fc_input, nodes_per_chan);
    Fc_pad = (int) min (det_routing_arch.Fc_pad, nodes_per_chan);
 }
 else {    // FRACTIONAL
   // Make Fc_output round up so all tracks are driven when                    
   // Fc = W/(Nequivalent outputs), regardless of W.                           
   Fc_output = (int) ceil (nodes_per_chan * det_routing_arch.Fc_output - 0.005);
   //  Fc_output = nint (nodes_per_chan * det_routing_arch.Fc_output); 
   Fc_output = max (1, Fc_output);
   Fc_input = nint (nodes_per_chan * det_routing_arch.Fc_input);
   Fc_input = max (1, Fc_input);
   Fc_pad = nint (nodes_per_chan * det_routing_arch.Fc_pad);
   Fc_pad = max (1, Fc_pad);
 }


 // Next, allocate and load the switch_block_conn data structure.  
 // This structure lists which tracks connect to which at each 
 // switch-block.
 // Question:  Where is this data structure impacting the rr_graph
 // so that vertical vias will come into the picture?
 alloc_and_load_switch_block_conn (nodes_per_chan, vias_per_zchan, 
				   det_routing_arch.switch_block_type);

 clb_opin_to_tracks = alloc_and_load_clb_pin_to_tracks 
   (DRIVER, nodes_per_chan, Fc_output, false);


 // Perturb ipin switch pattern if it will line up perfectly with the output  
 // pin pattern, and there are not so many switches that this would cause 1   
 // track to connect to the same pin twice.                                   

 if (Fc_input > Fc_output) 
    Fc_ratio = (float) Fc_input / (float) Fc_output;
 else 
    Fc_ratio = (float) Fc_output / (float) Fc_input;

 if (Fc_input <= nodes_per_chan - 2 && fabs (Fc_ratio - nint (Fc_ratio)) < 
      .5 / (float) nodes_per_chan)
    perturb_ipin_switch_pattern = true;
 else
    perturb_ipin_switch_pattern = false;


 clb_ipin_to_tracks = alloc_and_load_clb_pin_to_tracks 
   (RECEIVER, nodes_per_chan, Fc_input, perturb_ipin_switch_pattern);

 tracks_to_clb_ipin = alloc_and_load_tracks_to_clb_ipin 
   (nodes_per_chan, Fc_input, clb_ipin_to_tracks);

 pads_to_tracks = alloc_and_load_pads_to_tracks (nodes_per_chan, Fc_pad);
 
 tracks_to_pads = alloc_and_load_tracks_to_pads (pads_to_tracks, 
          nodes_per_chan, Fc_pad);

 rr_edge_done = (bool *) my_calloc (num_rr_nodes, sizeof (bool));



 // Next function call uses "rr_node_indices" as ***
 // (see definition below in same file)
 // Here it comes the Big boy...
 alloc_and_load_rr_graph ( 
	      rr_node_indices, 
	      clb_opin_to_tracks, tracks_to_clb_ipin, 
	      pads_to_tracks, tracks_to_pads, 
	      Fc_output, Fc_input, Fc_pad, 
	      nodes_per_chan, vias_per_zchan,
	      route_type, 
	      det_routing_arch, 
	      seg_details_x, seg_details_y, seg_details_z);


 // Next is described in rr_graph_timing_params.cpp and ads 
 // capacitance information
 add_rr_graph_C_from_switches (timing_inf.C_ipin_cblock); 

 // Next function call uses "rr_node_indices" as ***
 // (see definition in rr_graph_indexed_data.cpp)
 alloc_and_load_rr_indexed_data (segment_inf, det_routing_arch.num_segment,
        rr_node_indices, nodes_per_chan, det_routing_arch.wire_to_ipin_switch,
        base_cost_type);

 // cristinel.ababei: this is commented out in normal functioning      
 //
 //  
 //dump_rr_graph ("dbg_rr_graph.echo");
 //
 check_rr_graph (route_type, det_routing_arch.num_switch);
 //
 //

 rr_graph_internal_vars.clb_opin_to_tracks = clb_opin_to_tracks;
 rr_graph_internal_vars.clb_ipin_to_tracks = clb_ipin_to_tracks;
 rr_graph_internal_vars.tracks_to_clb_ipin = tracks_to_clb_ipin;
 rr_graph_internal_vars.tracks_to_pads = tracks_to_pads;
 rr_graph_internal_vars.pads_to_tracks = pads_to_tracks;
 rr_graph_internal_vars.nodes_per_chan = nodes_per_chan;
 rr_graph_internal_vars.vias_per_zchan = vias_per_zchan;//3D
 rr_graph_internal_vars.seg_details_x = seg_details_x;
 rr_graph_internal_vars.seg_details_y = seg_details_y;
 rr_graph_internal_vars.seg_details_z = seg_details_z;//3D
 rr_graph_internal_vars.rr_node_indices = rr_node_indices;//cristia:  transfer

 //printf("Done build_rr_graph.\n");
}//eof


// ======================================================================= //


void s_fpga3d::free_rr_graph_internals (enum e_route_type route_type, 
	 struct s_det_routing_arch det_routing_arch, 
	 t_segment_inf *segment_inf, t_timing_inf timing_inf,
	 enum e_base_cost_type base_cost_type)
{
// Frees the variables that are internal to build_rr_graph.        
// This procedure should typically be called immediatley after     
// build_rr_graph, except for in the timing driven placer, which   
// is constantly modifying some of the rr_graph structures         
// cristinel.ababei:  Indeed it is called after build_rr_graph() is used.

 int nodes_per_chan=0;
 int vias_per_zchan=0;//3D
 int ***rr_node_indices;
 int **pads_to_tracks;
 struct s_ivec **tracks_to_clb_ipin, *tracks_to_pads;
 int ***clb_opin_to_tracks, ***clb_ipin_to_tracks;
 t_seg_details *seg_details_x, *seg_details_y;  /* [0 .. nodes_per_chan-1] */
 t_seg_details *seg_details_z;//3D

 clb_opin_to_tracks = rr_graph_internal_vars.clb_opin_to_tracks;
 clb_ipin_to_tracks = rr_graph_internal_vars.clb_ipin_to_tracks;
 tracks_to_clb_ipin = rr_graph_internal_vars.tracks_to_clb_ipin;
 tracks_to_pads = rr_graph_internal_vars.tracks_to_pads;
 pads_to_tracks = rr_graph_internal_vars.pads_to_tracks;
 nodes_per_chan = rr_graph_internal_vars.nodes_per_chan;
 vias_per_zchan = rr_graph_internal_vars.vias_per_zchan;//3D
 seg_details_x = rr_graph_internal_vars.seg_details_x;
 seg_details_y = rr_graph_internal_vars.seg_details_y;
 seg_details_z = rr_graph_internal_vars.seg_details_z;//3D

 rr_node_indices = rr_graph_internal_vars.rr_node_indices;


 free (rr_edge_done);
 free_matrix3 (clb_opin_to_tracks, 0, pins_per_clb-1, 0, 3, 0, sizeof(int));
 free_matrix3 (clb_ipin_to_tracks, 0, pins_per_clb-1, 0, 3, 0, sizeof(int));
 free_ivec_matrix (tracks_to_clb_ipin, 0, nodes_per_chan-1, 0, 3);
 free_ivec_vector (tracks_to_pads, 0, nodes_per_chan-1);
 free_matrix (pads_to_tracks, 0, io_rat-1, 0, sizeof(int));
 free_switch_block_conn (nodes_per_chan);//ok
 free_edge_list_hard (&free_edge_list_head);//ok
 free_seg_details (seg_details_x, nodes_per_chan);//ok
 free_seg_details (seg_details_y, nodes_per_chan);//ok
 free_seg_details (seg_details_z, vias_per_zchan);//3D, ok

 free_rr_node_indices (rr_node_indices);//ok
}//eof
// ======================================================================= //


void s_fpga3d::alloc_and_load_rr_graph (
	   int ***rr_node_indices, 
	   int ***clb_opin_to_tracks, struct s_ivec **tracks_to_clb_ipin,
	   int **pads_to_tracks, struct s_ivec *tracks_to_pads, 
	   int Fc_output, int Fc_input, int Fc_pad, 
	   int nodes_per_chan, int vias_per_zchan,
	   enum e_route_type route_type, 
	   struct s_det_routing_arch det_routing_arch, 
	   t_seg_details *seg_details_x, 
	   t_seg_details *seg_details_y, 
	   t_seg_details *seg_details_z) 
{
 // Does the actual work of allocating the rr_graph and filling all the 
 // appropriate values.  Everything up to this was just a prelude! 
 // So, this is the big boy.  

 int i=0,j=0,k=0;
 enum e_switch_block_type switch_block_type;
 int delayless_switch=0, wire_to_ipin_switch=0;
 int num_segment=0, num_via_types=0;


 // Allocate storage for the graph nodes.  Storage for the edges will be 
 // allocated as I fill in the graph.                                    

 if (rr_mem_chunk_list_head != NULL) {
    printf("Error in alloc_and_load_rr_graph:  rr_mem_chunk_list_head = %p.\n",
            rr_mem_chunk_list_head);
    printf("Expected NULL.  It appears an old rr_graph has not been freed.\n");
    exit (1);
 }


 alloc_net_rr_terminals ();
 load_net_rr_terminals (rr_node_indices, nodes_per_chan);
 // Store the SOURCE and SINK nodes of all CLBs (not valid for pads).
 alloc_and_load_rr_clb_source (rr_node_indices, nodes_per_chan);


 // cristia:  Next is one of most important allocations.
 // Last num_rr_nodes_vias out of  num_rr_nodes are due to chanz.
 rr_node = (t_rr_node *) my_malloc (num_rr_nodes * sizeof (t_rr_node));
 
 switch_block_type = det_routing_arch.switch_block_type;
 delayless_switch = det_routing_arch.delayless_switch;
 wire_to_ipin_switch = det_routing_arch.wire_to_ipin_switch;
 num_segment = det_routing_arch.num_segment;
 num_via_types = det_routing_arch.num_via_types;

 for (k=0;k<num_layers;k++) { 
   for (i=0;i<=nx+1;i++) {
     for (j=0;j<=ny+1;j++) {
       
       if (clb[i][j][k].type == CLB) {
	 
	 build_rr_clb (rr_node_indices, Fc_output, clb_opin_to_tracks, 
		       nodes_per_chan, i,j,k, delayless_switch, seg_details_x, 
		       seg_details_y);

	 		 
	 // cristinel.ababei
	 // "Enumerarea" rr_nodes coresp. to vias o facem dupa cum s-a facut
	 // construirea indicilor in alloc_and_load_chanz_rr_indices()
	 if ( (num_layers > 1) &&       // For 2D there is no z channels.
	      (k >= 0) && (k < num_layers-1) && 
	      (i > 0) && (i < nx) && (j > 0) && (j < ny) ) {// my if
	   //printf("\n num_via_types=%d",num_via_types);
	   //printf("  i=%d j=%d k=%d",i,j,k);
	   if ( (num_via_types == 1) || // Vias only of length one.
		(num_layers <= 2) )// Because anyway for 2 layers I can have
                                   // only vias of length one, irrespective
                                   // of what .arch says.  So in this case
                                   // num_layers is more powerfull for decission. 
	     build_rr_zchan (
		   rr_node_indices, 
		   route_type, 
		   i,j,k, 
		   nodes_per_chan, 
		   vias_per_zchan,
		   switch_block_type, 
		   seg_details_x, seg_details_y, seg_details_z,
		   //CHANX_COST_INDEX_START + 2*num_segment);//[  ?  ]
		   CHANX_COST_INDEX_START + num_segment);//[  ?  ]
	   else // Mixed vias.
	     build_rr_zchan_version_02 (
		   rr_node_indices, 
		   route_type, 
		   i,j,k, 
		   nodes_per_chan, 
		   vias_per_zchan,
		   switch_block_type, 
		   seg_details_x, seg_details_y, seg_details_z,
		   //CHANX_COST_INDEX_START + 2*num_segment);//[  ?  ]
		   CHANX_COST_INDEX_START + num_segment);//[  ?  ]
	 }// my if


	 	 
	 build_rr_xchan (rr_node_indices, route_type, tracks_to_clb_ipin, 
			 tracks_to_pads, i,j,k, 
			 nodes_per_chan, 
			 vias_per_zchan,
			 switch_block_type, 
			 wire_to_ipin_switch, 
			 seg_details_x, seg_details_y,seg_details_z,
			 CHANX_COST_INDEX_START);
	 
	 build_rr_ychan (rr_node_indices, route_type, tracks_to_clb_ipin, 
			 tracks_to_pads, i,j,k, 
			 nodes_per_chan, 
			 nodes_per_chan, 
			 switch_block_type, 
			 wire_to_ipin_switch, 
			 seg_details_x, seg_details_y, seg_details_z,
			 CHANX_COST_INDEX_START + num_segment);

	 
       }

       else if (clb[i][j][k].type == IO) {
	 
          build_rr_pads (rr_node_indices, Fc_pad, pads_to_tracks,
                  nodes_per_chan, i,j,k, delayless_switch, seg_details_x, 
                  seg_details_y);

          if (j == 0)  // Bottom-most channel. 
             build_rr_xchan (rr_node_indices, route_type, tracks_to_clb_ipin, 
			     tracks_to_pads, i,j,k, 
			     nodes_per_chan,
			     vias_per_zchan,
			     switch_block_type, 
			     wire_to_ipin_switch, 
			     seg_details_x, seg_details_y,seg_details_z,
			     CHANX_COST_INDEX_START);

          if (i == 0)  // Left-most channel.   
             build_rr_ychan (rr_node_indices, route_type, tracks_to_clb_ipin,
			     tracks_to_pads, i,j,k, 
			     nodes_per_chan, 
			     vias_per_zchan,
			     switch_block_type, 
			     wire_to_ipin_switch, 
			     seg_details_x, seg_details_y,seg_details_z, 
			     CHANX_COST_INDEX_START + num_segment);

       }

       else if (clb[i][j][k].type != ILLEGAL) {
          printf ("Error in alloc_and_load_rr_graph.\n"
                  "Block at (%d,%d,%d) has unknown type (%d).\n", 
		  i,j,k, clb[i][j][k].type);
	  exit (1);
       }

      }//for j
    }//for i
 }//for k

 //printf("Done alloc_and_load_rr_graph.\n");
}//eof


// ======================================================================= //


void s_fpga3d::free_rr_graph (void) 
{
// Frees all the routing graph data structures, if they have been       
// allocated.  I use rr_mem_chunk_list_head as a flag to indicate       
// whether or not the graph has been allocated -- if it is not NULL,    
// a routing graph exists and can be freed.  Hence, you can call this   
// routine even if you're not sure of whether a rr_graph exists or not. 

  if (rr_mem_chunk_list_head == NULL)   // Nothing to free.
    return;

  free_chunk_memory (rr_mem_chunk_list_head);  /* Frees ALL "chunked" data */
  rr_mem_chunk_list_head = NULL;               /* No chunks allocated now. */
  chunk_bytes_avail = 0;             /* 0 bytes left in current "chunk". */
  chunk_next_avail_mem = NULL;       /* No current chunk.                */
  
  // Before adding any more free calls here, be sure the data is NOT chunk 
  // allocated, as ALL the chunk allocated data is already free!           

  free (net_rr_terminals);//ok
  free (rr_node);//ok
  free (rr_indexed_data);// [  ?  ]
  free_matrix (rr_clb_source, 0, num_blocks-1, 0, sizeof(int));

  net_rr_terminals = NULL;
  rr_node = NULL;
  rr_indexed_data = NULL;
  rr_clb_source = NULL;
  printf("Done free_rr_graph.\n");
}//eof
// ======================================================================= //


void s_fpga3d::alloc_net_rr_terminals (void) 
{
  int inet;

  net_rr_terminals = (int **) my_malloc (num_nets * sizeof(int *));

  for (inet=0;inet<num_nets;inet++) {
    net_rr_terminals[inet] = (int *) my_chunk_malloc (net[inet].num_pins * 
       sizeof (int), &rr_mem_chunk_list_head, &chunk_bytes_avail,
       &chunk_next_avail_mem);
  }
  //printf("Done alloc_net_rr_terminals.\n");
}//eof
// ======================================================================= //


void s_fpga3d::load_net_rr_terminals (int ***rr_node_indices, 
        int nodes_per_chan) 
{
// Loads (i.e., populates) the net_rr_terminals data structure.  For each net 
// it stores the rr_node index of the SOURCE of the net and all the SINKs   
// of the net.  [0..num_nets-1][0..num_pins-1].  Entry [inet][pnum] stores  
// the rr_node index corresponding to the SOURCE (opin) or SINK (ipin) of pnum.  

 int inet, ipin, inode, iblk, blk_pin, iclass;
 int i=0,j=0,k=0;
 t_rr_type rr_type;


 for (inet=0;inet<num_nets;inet++) {
    
    rr_type = SOURCE;     /* First pin only */
    for (ipin=0;ipin<net[inet].num_pins;ipin++) {
       iblk = net[inet].blocks[ipin];
       i = block[iblk].x;
       j = block[iblk].y;
       k = block[iblk].z;

       if (clb[i][j][k].type == CLB) {
          blk_pin = net[inet].blk_pin[ipin];
          iclass = clb_pin_class[blk_pin];
       }
       else {
          iclass = which_io_block (iblk);
       }

       // When this function is called rr_node_indices were allocated
       // already and loaded (i.e., populated).
       inode = get_rr_node_index (i,j,k, rr_type, iclass, nodes_per_chan, 
                  rr_node_indices[k]);
       net_rr_terminals[inet][ipin] = inode;
 
       rr_type = SINK;    /* All pins after first are SINKs. */
    }
 }
 //printf("Done load_net_rr_terminals.\n");
}//eof
// ======================================================================= //


void s_fpga3d::alloc_and_load_rr_clb_source (int ***rr_node_indices,
          int nodes_per_chan) 
{
// Saves the rr_node corresponding to each SOURCE and SINK in each CLB      
// in the FPGA.  Currently only the SOURCE rr_node values are used, and     
// they are used only to reserve pins for locally used OPINs in the router. 
// [0..num_blocks-1][0..num_class-1].  The values for blocks that are pads  
// are NOT valid.

 int iblk, i,j,k, iclass, inode;
 t_rr_type rr_type;

 rr_clb_source = (int **) alloc_matrix (0, num_blocks-1, 0, num_class-1,
                  sizeof(int));

 for (iblk=0;iblk<num_blocks;iblk++) {
    for (iclass=0;iclass<num_class;iclass++) {
       if (block[iblk].type == CLB) {
          i = block[iblk].x;
          j = block[iblk].y;
          k = block[iblk].z;

          if (class_inf[iclass].type == DRIVER)
             rr_type = SOURCE;
          else
             rr_type = SINK;
 
          inode = get_rr_node_index (i,j,k, rr_type, iclass, nodes_per_chan,
               rr_node_indices[k]);
          rr_clb_source[iblk][iclass] = inode;
       }
       else {  /* IO Pad; don't need any data so set to OPEN (invalid) */
          rr_clb_source[iblk][iclass] = OPEN;
       }
    }
 }
 //printf("Done alloc_and_load_rr_clb_source.\n");
}//eof
// ======================================================================= //


int s_fpga3d::which_io_block (int iblk) 
{
// Returns the subblock (pad) number at which this block was placed.  
// iblk must be an IO block.                                                    

  int i=0,j=0,k=0;
  int ipad=0,ifound=0,test_blk=0;

 ifound = -1;
 i = block[iblk].x;
 j = block[iblk].y;
 k = block[iblk].z;

 if (block[iblk].type != INPAD && block[iblk].type != OUTPAD) {
    printf ("Error in which_io_block:  block %d is not an IO block.\n", iblk);
    printf ("Its coordinates are x=%d, y=%d, z=%d with type(clb)=%d "
	    "and with type(block)=%d.\n",i,j,k, clb[i][j][k].type,block[iblk].type);
    exit (1);
 }

 for (ipad=0;ipad<clb[i][j][k].occ;ipad++) {
    test_blk = clb[i][j][k].u.io_blocks[ipad];
    //printf(" %d",test_blk);
    if (test_blk == iblk) {
       ifound = ipad;
       break;
    }
 }

 if (ifound < 0) {
    printf ("Error in which_io_block:  block %d at location [%d,%d,%d]"
	    " not found in clb array at clb[][][].u.io_blocks[]. \n",
	    iblk,i,j,k);
    exit (1);
 }

 return (ifound);
}//eof
// ======================================================================= //


void s_fpga3d::build_rr_clb (int ***rr_node_indices, int Fc_output, int ***
       clb_opin_to_tracks, int nodes_per_chan, int i,int j,int k, int 
       delayless_switch, t_seg_details *seg_details_x, t_seg_details 
       *seg_details_y) 
{
  // OBS:  This function is called inside its "parent rutine" for each "k"
  //
  // Load up (i.e., populate) the rr_node structures for the clb at location
  // (i,j,k).  I both fill in fields that shouldn't change during the entire 
  // routing and initialize fields that will change to the proper starting value.

 int ipin, iclass, inode, pin_num, to_node, num_edges;
 t_linked_edge *edge_list_head;


/* SOURCES and SINKS first.   */
// These, constitute rr_nodes themeselves.  In some sense they represent
// the "center of the LUT" or the "CLB" itself... Then, these
// rr_nodes would connect through "edges" of the routing graph to pins of 
// this current CLB; pins (i.e., ipin or opin) which are rr_nodes 
// themselves too!

 for (iclass=0;iclass<num_class;iclass++) {
    if (class_inf[iclass].type == DRIVER) {    /* SOURCE */
       inode = get_rr_node_index (i,j,k, SOURCE, iclass, nodes_per_chan,
                 rr_node_indices[k]);

       num_edges = class_inf[iclass].num_pins;
       rr_node[inode].num_edges = num_edges;

       rr_node[inode].edges = (int *) my_chunk_malloc (num_edges * 
              sizeof (int), &rr_mem_chunk_list_head, &chunk_bytes_avail, 
              &chunk_next_avail_mem);

       rr_node[inode].switches = (short *) my_chunk_malloc (num_edges *
              sizeof (short), &rr_mem_chunk_list_head, &chunk_bytes_avail,
              &chunk_next_avail_mem);

       // cristinel.ababei:  Here the fanout list, with all rr_nodes to which
       // current SOURCE connects to, is created.
       for (ipin=0;ipin<class_inf[iclass].num_pins;ipin++) {
          pin_num = class_inf[iclass].pinlist[ipin];
          to_node = get_rr_node_index (i,j,k, OPIN, pin_num, nodes_per_chan,
                  rr_node_indices[k]);
          rr_node[inode].edges[ipin] = to_node;
          rr_node[inode].switches[ipin] = delayless_switch;
       }

       rr_node[inode].capacity = class_inf[iclass].num_pins;
       rr_node[inode].cost_index = SOURCE_COST_INDEX;
       rr_node[inode].type = SOURCE;
    }

    else {    /* SINK */
       inode = get_rr_node_index (i,j,k, SINK, iclass, nodes_per_chan,
                 rr_node_indices[k]);

/* Note:  To allow route throughs through clbs, change the lines below to  *
 * make an edge from the input SINK to the output SOURCE.  Do for just the *
 * special case of INPUTS = class 0 and OUTPUTS = class 1 and see what it  *
 * leads to.  If route throughs are allowed, you may want to increase the  *
 * base cost of OPINs and/or SOURCES so they aren't used excessively.      */

       rr_node[inode].num_edges = 0; 
       rr_node[inode].edges = NULL;
       rr_node[inode].switches = NULL;
       rr_node[inode].capacity = class_inf[iclass].num_pins;
       rr_node[inode].cost_index = SINK_COST_INDEX;
       rr_node[inode].type = SINK;
    }

/* Things common to both SOURCEs and SINKs.   */

    rr_node[inode].occ = 0;

    rr_node[inode].xlow = i;
    rr_node[inode].xhigh = i;
    rr_node[inode].ylow = j;
    rr_node[inode].yhigh = j;
    rr_node[inode].zlow = k;//cristia
    rr_node[inode].zhigh = k;
    rr_node[inode].R = 0;
    rr_node[inode].C = 0;

    rr_node[inode].ptc_num = iclass;
 }

/* Now do the pins.  */
// cristinel.ababei:  As I was saying, pins (ipin, opin) are rr_nodes
// themeselves, which will connect through edges to wire-segments 
// (or tracks) from the channels, tracks which themselves are rr_nodes
// as well.

 for (ipin=0;ipin<pins_per_clb;ipin++) {
    iclass = clb_pin_class[ipin];
    if (class_inf[iclass].type == DRIVER) {  /* OPIN */
       inode = get_rr_node_index (i,j,k, OPIN, ipin, nodes_per_chan,
                 rr_node_indices[k]);

       edge_list_head = NULL; //Will be set by next function call.
       // Next, get number of tracks to which clb opin #ipin at (i,j) connects.
       // Also, store the nodes to which this pin connects in the linked list    
       // pointed to by *edge_list_ptr.                                            
       num_edges = get_clb_opin_connections (clb_opin_to_tracks, ipin, i,j,k,
                   Fc_output, seg_details_x, seg_details_y, &edge_list_head, 
                   nodes_per_chan, rr_node_indices[k]);

       // EDGES ? and SWITCHES ?
       // And now create the fanout list of this current OPIN
       // as a sequence of rr_node id's, rr_nodes which are tracks in 
       // channel (wire-segments)
       alloc_and_load_edges_and_switches (inode, num_edges, edge_list_head);

       rr_node[inode].cost_index = OPIN_COST_INDEX;
       rr_node[inode].type = OPIN;
    }
    else {                                   /* IPIN */
       inode = get_rr_node_index (i,j,k, IPIN, ipin, nodes_per_chan,
                 rr_node_indices[k]);

       rr_node[inode].num_edges = 1;
       rr_node[inode].edges = (int *) my_chunk_malloc (sizeof(int), 
          &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem);

       rr_node[inode].switches = (short *) my_chunk_malloc (sizeof(short), 
          &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem);

       to_node = get_rr_node_index (i,j,k, SINK, iclass, nodes_per_chan,
                  rr_node_indices[k]);
       rr_node[inode].edges[0] = to_node;
       rr_node[inode].switches[0] = delayless_switch;

       rr_node[inode].cost_index = IPIN_COST_INDEX;
       rr_node[inode].type = IPIN;
    }

/* Things that are common to both OPINs and IPINs.  */

    rr_node[inode].capacity = 1;
    rr_node[inode].occ = 0;
 
    rr_node[inode].xlow = i;
    rr_node[inode].xhigh = i;
    rr_node[inode].ylow = j;
    rr_node[inode].yhigh = j;
    rr_node[inode].zlow = k;//cristia
    rr_node[inode].zhigh = k;
    rr_node[inode].C = 0;
    rr_node[inode].R = 0;
 
    rr_node[inode].ptc_num = ipin;
 }
 //printf("build_rr_clb.\n");
}//eof
// ======================================================================= //


void s_fpga3d::build_rr_pads (int ***rr_node_indices, int Fc_pad, int 
        **pads_to_tracks, int nodes_per_chan, int i,int j,int k, int 
        delayless_switch, t_seg_details *seg_details_x, t_seg_details 
        *seg_details_y) 
{
  // OBS: Very similar to build_rr_clb() above
  //
  // Load up the rr_node structures for the pads at location (i,j,k). I both
  // fill in fields that shouldn't change during the entire routing and     
  // initialize fields that will change to the proper starting value.       
  // Empty pad locations have their type set to EMPTY.                      

 int s_node, p_node, ipad, iloop;
 int inode, num_edges;
 t_linked_edge *edge_list_head;


/* Each pad contains both a SOURCE + OPIN, and a SINK + IPIN, since each  *
 * pad is bidirectional.  The fact that it will only be used as either an *
 * input or an output makes no difference.                                */

 for (ipad=0;ipad<io_rat;ipad++) {   /* For each pad at this (i,j) */

   /* Do SOURCE first.   */

    s_node = get_rr_node_index (i,j,k, SOURCE, ipad, nodes_per_chan,
             rr_node_indices[k]);
       
    rr_node[s_node].num_edges = 1;
    p_node = get_rr_node_index (i,j,k, OPIN, ipad, nodes_per_chan, 
              rr_node_indices[k]);

    rr_node[s_node].edges = (int *) my_chunk_malloc (sizeof(int), 
          &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem);
    rr_node[s_node].edges[0] = p_node;

    rr_node[s_node].switches = (short *) my_chunk_malloc (sizeof(short),
          &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem);
    rr_node[s_node].switches[0] = delayless_switch;
    
    rr_node[s_node].cost_index = SOURCE_COST_INDEX;
    rr_node[s_node].type = SOURCE;


   /* Now do OPIN */
       
    edge_list_head = NULL;
    num_edges = get_pad_opin_connections (pads_to_tracks, ipad, i,j,k, 
              Fc_pad, seg_details_x, seg_details_y, &edge_list_head, 
              nodes_per_chan, rr_node_indices[k]);
  
     // EDGES ? and SWITCHES ?
    alloc_and_load_edges_and_switches (p_node, num_edges, edge_list_head);
 
    rr_node[p_node].cost_index = OPIN_COST_INDEX;
    rr_node[p_node].type = OPIN;

   /* Code common to both SOURCE and OPIN. */

    inode = s_node;
    for (iloop=1;iloop<=2;iloop++) {    /* for both SOURCE or SINK and pin */
       rr_node[inode].occ = 0;
       rr_node[inode].capacity = 1;

       rr_node[inode].xlow = i;
       rr_node[inode].xhigh = i;
       rr_node[inode].ylow = j;
       rr_node[inode].yhigh = j;
       rr_node[inode].zlow = k;//cristia
       rr_node[inode].zhigh = k;
       rr_node[inode].R = 0.;
       rr_node[inode].C = 0.;

       rr_node[inode].ptc_num = ipad;

       inode = p_node;
    }


   /* Now do SINK */

    s_node = get_rr_node_index (i,j,k, SINK, ipad, nodes_per_chan,
             rr_node_indices[k]);

    rr_node[s_node].num_edges = 0;
    rr_node[s_node].edges = NULL;
    rr_node[s_node].switches = NULL;

    rr_node[s_node].cost_index = SINK_COST_INDEX;
    rr_node[s_node].type = SINK;


    /* Now do IPIN */
    
    p_node = get_rr_node_index (i,j,k, IPIN, ipad, nodes_per_chan,
             rr_node_indices[k]);

    rr_node[p_node].num_edges = 1;
    rr_node[p_node].edges = (int *) my_chunk_malloc (sizeof(int), 
          &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem);
    rr_node[p_node].edges[0] = s_node;

    rr_node[p_node].switches = (short *) my_chunk_malloc (sizeof(short), 
          &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem);
    rr_node[p_node].switches[0] = delayless_switch;

    rr_node[p_node].cost_index = IPIN_COST_INDEX;
    rr_node[p_node].type = IPIN;

  /* Code common to both SINK and IPIN. */

    inode = s_node;
    for (iloop=1;iloop<=2;iloop++) {    /* for both SOURCE or SINK and pin */
       rr_node[inode].occ = 0;
       rr_node[inode].capacity = 1;

       rr_node[inode].xlow = i;
       rr_node[inode].xhigh = i;
       rr_node[inode].ylow = j;
       rr_node[inode].yhigh = j;
       rr_node[inode].zlow = k;//cristia
       rr_node[inode].zhigh = k;
       rr_node[inode].R = 0.;
       rr_node[inode].C = 0.;

       rr_node[inode].ptc_num = ipad;

       inode = p_node;
    }

 }   /* End for each pad. */
 //printf("build_rr_pads.\n");
}//eof



// ======================================================================= //
// ======================================================================= //
// ======================================================================= //



void s_fpga3d::build_rr_zchan (
          int ***rr_node_indices, 
	  enum e_route_type route_type, 
	  int i,int j,int k, 
	  int nodes_per_chan, 
	  int vias_per_zchan,
	  enum e_switch_block_type switch_block_type, 
	  t_seg_details *seg_details_x, 
	  t_seg_details *seg_details_y,
	  t_seg_details *seg_details_z,
	  int cost_index_offset) 
{
// cristinel.ababei
// Loads up (populates) all the routing resource nodes in the 
// z channels "starting" at (i,j,k).  These rr_nodes are already 
// minimally allocated and their indices also.  Now we populate
// them only.
// OBS:  To be called only for:
//       (k >= 0) && (k < num_layers-1) && 
//	 (i > 0) && (i < nx) && (j > 0) && (j < ny)
 
  int itrack=0,num_edges=0,inode=0,istart=0,iend=0,length=0,seg_index=0;
  t_linked_edge *edge_list_head;


  for (itrack=0;itrack<vias_per_zchan; // see it  
       itrack++) {
    // ------------------------------------------
    edge_list_head = NULL;

    num_edges = get_ztrack_to__x_y_z__tracks ( 
               i, j, // location
	       k,// layer saying between which layers this track is
	       itrack,// track in z-chan [0..vias_per_zchan-1]
	       &edge_list_head,
	       nodes_per_chan,
	       vias_per_zchan, 
	       rr_node_indices[k],
	       seg_details_x, seg_details_y, seg_details_z, 
	       switch_block_type );


    inode = get_rr_node_index (i,j,k, CHANZ, itrack, vias_per_zchan, 
			       rr_node_indices[k]);
    // EDGES ? and SWITCHES ?
    // Tot ce-a fost pina acum a fost doar ptr. ca sa pun in edge_list_head
    // indices off rr_nodes la care acest "inode" fanouts...
    // So, edge_list_head is now created and used to convey the information
    // in order to actually create the routing graph edges!

    alloc_and_load_edges_and_switches (inode, num_edges, edge_list_head);


    // Edge arrays have now been built up.  Do everything else.

    seg_index = seg_details_z[itrack].index;
    rr_node[inode].cost_index = cost_index_offset + seg_index;//[  ?  ]
    rr_node[inode].occ = 0;

    if (route_type == DETAILED) 
      rr_node[inode].capacity = 1;
    else                                    // GLOBAL routing
      rr_node[inode].capacity = chan_width_z[0];//i.e., vias_per_zchan.

    rr_node[inode].xlow = i;
    rr_node[inode].xhigh = i;
    rr_node[inode].ylow = j;
    rr_node[inode].yhigh = j;
    rr_node[inode].zlow = k;
    rr_node[inode].zhigh = k + 1;

    // Currently vias span only between two layers.
    length = seg_details_z[itrack].length; // ie, 1 for now.
    //cout<<" "<<length;

    rr_node[inode].R = length * seg_details_z[itrack].Rmetal;//[  ?  ]
    rr_node[inode].C = length * seg_details_z[itrack].Cmetal;//[  ?  ]
    //cout<<" "<<rr_node[inode].C;
    rr_node[inode].ptc_num = itrack;
    rr_node[inode].type = CHANZ;
    // ------------------------------------------
  }//for (itrack=0;itrack<vias_per_zchan;
  
  //printf("build_rr_zchan.\n");
}//eof



// ======================================================================= //
// ======================================================================= //
// ======================================================================= //



void s_fpga3d::build_rr_xchan (int ***rr_node_indices, enum e_route_type 
      route_type, struct s_ivec **tracks_to_clb_ipin, struct s_ivec *
      tracks_to_pads, int i,int j,int k, 
      int nodes_per_chan, int vias_per_zchan, 
      enum e_switch_block_type switch_block_type, int wire_to_ipin_switch, 
      t_seg_details *seg_details_x, 
      t_seg_details *seg_details_y,
      t_seg_details *seg_details_z,
      int cost_index_offset) 
{
// Chemata for all i,j,k pairs.  Deci in fiecare caz se va crea
// un "edge_list_head" and popula cu indices of rr_nodes to 
// connect to.
// Loads up (populates) all the routing resource nodes in the 
// x-directed channel segments starting at (i,j,k).   
  
  int itrack=0,num_edges=0,inode=0,istart=0,iend=0,length=0,seg_index=0;
  t_linked_edge *edge_list_head;

 for (itrack=0;itrack<nodes_per_chan;itrack++) {
   istart = get_closest_seg_start (seg_details_x, itrack,i,j);//Same for all k?
    if (istart != i)
      continue;        // Not the start of this segment.

    iend = get_seg_end (seg_details_x, itrack, istart, j, nx);
    edge_list_head = NULL;


// First count number of edges and put the edges in a linked list.
// which is [ edge_list_head ] used to convey all the information
// about the connectivity of this channel's segments!

    if (j == 0) {                   /* Between bottom pads and clbs.  */
       num_edges = get_xtrack_to_clb_ipin_edges (istart, iend, j, itrack, TOP, 
                   &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                   rr_node_indices[k], 
		   seg_details_x, wire_to_ipin_switch,  k);

       num_edges += get_xtrack_to_pad_edges (istart, iend, j, j, itrack, 
                    &edge_list_head, tracks_to_pads, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, wire_to_ipin_switch,  k);

       /* Only channels above exist. */

       num_edges += get_xtrack_to_ytracks (istart, iend, j, itrack, j+1,
                    &edge_list_head, nodes_per_chan, 
		    rr_node_indices[k], 
                    seg_details_x,  seg_details_y, switch_block_type,  k); 
    }

    else if (j == ny) {           /* Between top clbs and pads. */
       num_edges = get_xtrack_to_clb_ipin_edges (istart, iend, j, itrack, 
                   BOTTOM, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                   rr_node_indices[k],
                   seg_details_x, wire_to_ipin_switch,  k);

       num_edges += get_xtrack_to_pad_edges (istart, iend, j, j+1, itrack, 
                    &edge_list_head, tracks_to_pads, nodes_per_chan, 
                    rr_node_indices[k],
                    seg_details_x, wire_to_ipin_switch,  k);

       /* Only channels below exist. */

       num_edges += get_xtrack_to_ytracks (istart, iend, j, itrack, j,
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x,  seg_details_y, switch_block_type,  k); 
    }

    else {                          /* Between two rows of clbs. */
       num_edges = get_xtrack_to_clb_ipin_edges (istart, iend, j, itrack, 
                   BOTTOM, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                   rr_node_indices[k],
                   seg_details_x, wire_to_ipin_switch,  k);

       num_edges += get_xtrack_to_clb_ipin_edges (istart, iend, j, itrack, 
                    TOP, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                    rr_node_indices[k],
                    seg_details_x, wire_to_ipin_switch,  k);

       /* Channels above and below both exist. */

       num_edges += get_xtrack_to_ytracks (istart, iend, j, itrack, j+1,
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x,  seg_details_y, switch_block_type,  k); 

       num_edges += get_xtrack_to_ytracks (istart, iend, j, itrack, j,
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x,  seg_details_y, switch_block_type,  k); 

       // cristinel.ababei
       // Question: Should I only allow x tracks to connect to z-chan
       // or only to y-chan to z-chan or all possible connections?
       // This should be a varying factor and see how performance 
       // changes.
       // Next should be called only for:
       //  if ( (k >= 0) && (k < num_layers-1) && 
       //       //(i > 0) && (i < nx) && 
       //       //It will be tested inside function
       //	(j > 0) && (j < ny) )
       if (num_layers > 1)
	 num_edges += get_xtrack_to_ztracks (istart, iend, j, itrack, j,
                    &edge_list_head, 
		    nodes_per_chan, vias_per_zchan,
                    rr_node_indices[k], 
                    seg_details_x, 
		    seg_details_z, 
		    switch_block_type,  k); 

    }
    
    if (istart != 1) {   /* x-chan to left exists. */
       num_edges += get_xtrack_to_xtrack (istart, j, itrack, istart - 1, 
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, switch_block_type,  k); 
    }

    if (iend != nx) {      /* x-chan to right exists. */
       num_edges += get_xtrack_to_xtrack (iend, j, itrack, iend + 1, 
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, switch_block_type,  k); 
    }


    inode = get_rr_node_index (i,j,k, CHANX, itrack, nodes_per_chan, 
             rr_node_indices[k]);

    // EDGES ? and SWITCHES ?
    // Tot ce-a fost pina acum a fost doar ptr. ca sa pun in edge_list_head
    // indices off rr_nodes la care acest "inode" fanout...
    // So, edge_list_head is now created and used to convey the information
    // in order to actually create the routing graph edges!
    alloc_and_load_edges_and_switches (inode, num_edges, edge_list_head);

// Edge arrays have now been built up.  Do everything else.

    seg_index = seg_details_x[itrack].index;
    rr_node[inode].cost_index = cost_index_offset + seg_index;
    rr_node[inode].occ = 0;

    if (route_type == DETAILED) 
       rr_node[inode].capacity = 1;
    else                                    // GLOBAL routing
       rr_node[inode].capacity = chan_width_x[j];
 
    rr_node[inode].xlow = istart;
    rr_node[inode].xhigh = iend;
    rr_node[inode].ylow = j;
    rr_node[inode].yhigh = j;
    rr_node[inode].zlow = k;//cristia
    rr_node[inode].zhigh = k;

    length = iend - istart + 1;
    rr_node[inode].R = length * seg_details_x[itrack].Rmetal;
    rr_node[inode].C = length * seg_details_x[itrack].Cmetal;

    rr_node[inode].ptc_num = itrack;
    rr_node[inode].type = CHANX;

 }// for (itrack=0;itrack<nodes_per_chan;itrack++)

 //printf("build_rr_xchan.\n");
}//eof
// ======================================================================= //


void s_fpga3d::build_rr_ychan (int ***rr_node_indices, enum e_route_type 
      route_type, struct s_ivec **tracks_to_clb_ipin, struct s_ivec *
      tracks_to_pads, int i,int j,int k, 
      int nodes_per_chan, int vias_per_zchan, 
      enum  e_switch_block_type switch_block_type,
      int wire_to_ipin_switch,
      t_seg_details *seg_details_x, 
      t_seg_details *seg_details_y,
      t_seg_details *seg_details_z,
      int cost_index_offset) 
{
 // OBS: Very similar to build_rr_xchan() above
 // Loads up all the routing resource nodes in the y-directed channel   
 // segments starting at (i,j,k).                                         

 int itrack=0,num_edges=0,inode=0,jstart=0,jend=0,length=0,seg_index=0;
 t_linked_edge *edge_list_head;

 for (itrack=0;itrack<nodes_per_chan;itrack++) {
    jstart = get_closest_seg_start (seg_details_y, itrack, j, i);
    if (jstart != j)
       continue;        /* Not the start of this segment. */

    jend = get_seg_end (seg_details_y, itrack, jstart, i, ny);
    edge_list_head = NULL;

/* First count number of edges and put the edges in a linked list. */

    if (i == 0) {                   /* Between leftmost pads and clbs.  */
       num_edges = get_ytrack_to_clb_ipin_edges (jstart, jend, i, itrack, 
                   RIGHT, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                   rr_node_indices[k], 
                   seg_details_y, wire_to_ipin_switch,  k);

       num_edges += get_ytrack_to_pad_edges (jstart, jend, i, i, itrack, 
                    &edge_list_head, tracks_to_pads, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_y, wire_to_ipin_switch,  k);

       /* Only channel to right exists. */

       num_edges += get_ytrack_to_xtracks (jstart, jend, i, itrack, i+1, 
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, seg_details_y, switch_block_type,  k);
    }

    else if (i == nx) {           /* Between rightmost clbs and pads. */
       num_edges = get_ytrack_to_clb_ipin_edges (jstart, jend, i, itrack, 
                   LEFT, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                   rr_node_indices[k], 
                   seg_details_y, wire_to_ipin_switch,  k);

       num_edges += get_ytrack_to_pad_edges (jstart, jend, i, i+1, itrack, 
                    &edge_list_head, tracks_to_pads, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_y, wire_to_ipin_switch,  k);

       /* Only channel to left exists. */

       num_edges += get_ytrack_to_xtracks (jstart, jend, i, itrack, i, 
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, seg_details_y, switch_block_type,  k);
    }

    else {                          /* Between two rows of clbs. */
       num_edges = get_ytrack_to_clb_ipin_edges (jstart, jend, i, itrack, 
                   LEFT, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                   rr_node_indices[k], 
                   seg_details_y, wire_to_ipin_switch,  k);

       num_edges += get_ytrack_to_clb_ipin_edges (jstart, jend, i, itrack, 
                    RIGHT, &edge_list_head, tracks_to_clb_ipin, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_y, wire_to_ipin_switch,  k);

       /* Channels on both sides. */

       num_edges += get_ytrack_to_xtracks (jstart, jend, i, itrack, i+1, 
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, seg_details_y, switch_block_type,  k);

       num_edges += get_ytrack_to_xtracks (jstart, jend, i, itrack, i,
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_x, seg_details_y, switch_block_type,  k);
       
       // cristinel.ababei
       // For now I do not support connections between vias and
       // tracks on channels y but only to channels x.  Keeping
       // things simple initially...
       // Next should be called only for:
       //  if ( (k >= 0) && (k < num_layers-1) && 
       //       (i > 0) && (i < nx) && 
       //	//(j > 0) && (j < ny) )
       //       //It will be tested inside function
       if (num_layers > 1)
	 num_edges += get_ytrack_to_ztracks (jstart, jend, i, itrack, i,
                    &edge_list_head, 
		    nodes_per_chan, vias_per_zchan, 
                    rr_node_indices[k], 
                    seg_details_y, 
		    seg_details_z, 
		    switch_block_type,  k);

    }

    if (jstart != 1) {   /* y-chan below exists. */
       num_edges += get_ytrack_to_ytrack (i, jstart, itrack, jstart - 1, 
                    &edge_list_head, nodes_per_chan, 
                    rr_node_indices[k], 
                    seg_details_y, switch_block_type,  k); 
    }

    if (jend != ny) {      /* y-chan above exists. */
       num_edges += get_ytrack_to_ytrack (i, jend, itrack, jend + 1, 
                    &edge_list_head, nodes_per_chan, rr_node_indices[k], 
                    seg_details_y, switch_block_type,  k); 
    }


    inode = get_rr_node_index (i,j,k, CHANY, itrack, nodes_per_chan, 
             rr_node_indices[k]);

    // EDGES ? and SWITCHES ?
    alloc_and_load_edges_and_switches (inode, num_edges, edge_list_head);

/* Edge arrays have now been built up.  Do everything else.  */

    seg_index = seg_details_y[itrack].index;
    rr_node[inode].cost_index = cost_index_offset + seg_index;
    rr_node[inode].occ = 0;

    if (route_type == DETAILED) 
       rr_node[inode].capacity = 1;
    else                                    /* GLOBAL routing */
       rr_node[inode].capacity = chan_width_y[i];
 
    rr_node[inode].xlow = i;
    rr_node[inode].xhigh = i;
    rr_node[inode].ylow = jstart;
    rr_node[inode].yhigh = jend;
    rr_node[inode].zlow = k;//cristia
    rr_node[inode].zhigh = k;

    length = jend - jstart + 1;
    rr_node[inode].R = length * seg_details_y[itrack].Rmetal;
    rr_node[inode].C = length * seg_details_y[itrack].Cmetal;
 
    rr_node[inode].ptc_num = itrack;
    rr_node[inode].type = CHANY;

 }// for (itrack=0;itrack<nodes_per_chan;itrack++)
 
 //printf("build_rr_ychan.\n");
}//eof
// ======================================================================= //


void s_fpga3d::alloc_and_load_edges_and_switches (
		 int inode, 
		 int num_edges, 
		 t_linked_edge *edge_list_head ) 
{
// Sets up all the edge related information for rr_node inode (num_edges,   
// the edges array and the switches array).  The edge_list_head points to  
// a list of the num_edges edges and switches to put in the arrays.  This  
// linked list is freed by this routine. This routine also resets the      
// rr_edge_done array for the next rr_node (i.e., set it so that no edges   
// are marked as having been seen before).                                 
 
 t_linked_edge *list_ptr, *next_ptr;
 int i=0, to_node=0;

 rr_node[inode].num_edges = num_edges; 
 
 rr_node[inode].edges = (int *) my_chunk_malloc (num_edges * sizeof(int), 
       &rr_mem_chunk_list_head, &chunk_bytes_avail, &chunk_next_avail_mem); 
 
 rr_node[inode].switches = (short *) my_chunk_malloc (num_edges * 
       sizeof(short), &rr_mem_chunk_list_head, &chunk_bytes_avail, 
       &chunk_next_avail_mem); 
     
 // Load the edge and switch arrays, deallocate the linked list and reset
 // the rr_edge_done flags.                                              
  
 // cristinel.ababei
 // edge_list_head is a 1D array with rr_node indices to which inode
 // fanouts.
 list_ptr = edge_list_head;
 i = 0;

 // This edge_list_head of switches created/allocated
 // and populated, a venit ca argument.  A fost populata in 
 // build_rr_xchan() etc.
 while (list_ptr != NULL) {
    to_node = list_ptr->edge;

    // Sanity check:
    if ( to_node == 0) printf(" [%d,%d]",inode, to_node);

    rr_node[inode].edges[i] = to_node;
    rr_edge_done[to_node] = false;
    rr_node[inode].switches[i] = list_ptr->iswitch;
    next_ptr = list_ptr->next;

    // Add to free list
    free_linked_edge_soft (list_ptr, &free_edge_list_head); 

    list_ptr = next_ptr;
    i++;
 }
 //printf("Done alloc_and_load_edges_and_switches.\n");
}//eof
// ======================================================================= //
 

int ***s_fpga3d::alloc_and_load_clb_pin_to_tracks (enum e_pin_type pin_type, 
          int nodes_per_chan, int Fc, bool perturb_switch_pattern) 
{
/* Allocates and loads an array that contains a list of which tracks each  *
 * output or input pin of a clb should connect to on each side of the clb. *
 * The pin_type flag is either DRIVER or RECEIVER, and determines whether  *
 * information for output C blocks or input C blocks is generated.         *
 * Set Fc and nodes_per_chan to 1 if you're doing a global routing graph.  */


 int *num_dir;   /* [0..3] Number of *physical* pins on each clb side.      */
 int **dir_list; /* [0..3][0..pins_per_clb-1] list of pins of correct type  *
                  * on each side. Max possible space alloced for simplicity */

 int i, j, iside, ipin, iclass, num_phys_pins, pindex;
 int *num_done_per_dir, *pin_num_ordering, *side_ordering;
 int ***tracks_connected_to_pin;  /* [0..pins_per_clb-1][0..3][0..Fc-1] */
 float step_size;

/* NB:  This wastes some space.  Could set tracks_..._pin[ipin][iside] =   *
 * NULL if there is no pin on that side, or that pin is of the wrong type. *
 * Probably not enough memory to worry about, esp. as it's temporary.      *
 * If pin ipin on side iside does not exist or is of the wrong type,       *
 * tracks_connected_to_pin[ipin][iside][0] = OPEN.                         */

 tracks_connected_to_pin = (int ***) alloc_matrix3 (0, pins_per_clb-1, 0, 3,
                                     0, Fc-1, sizeof(int));// To be returned.

 for (ipin=0;ipin<pins_per_clb;ipin++) 
    for (iside=0;iside<=3;iside++)
       tracks_connected_to_pin[ipin][iside][0] = OPEN;  /* Unconnected. */


 num_dir = (int *) my_calloc (4, sizeof(int));
 dir_list = (int **) alloc_matrix (0, 3, 0, pins_per_clb-1, sizeof(int));

/* Defensive coding.  Try to crash hard if I use an unset entry.  */
 
 for (i=0;i<4;i++) 
    for (j=0;j<pins_per_clb;j++) 
       dir_list[i][j] = -1;
 

 for (ipin=0;ipin<pins_per_clb;ipin++) {
    iclass = clb_pin_class[ipin];
    if (class_inf[iclass].type != pin_type) /* Doing either ipins OR opins */
       continue;
     
/* Pins connecting only to global resources get no switches -> keeps the    *
 * area model accurate.                                                     */

    if (is_global_clb_pin[ipin])   
       continue;
    
    for (iside=0;iside<=3;iside++) {
       if (pinloc[iside][ipin] == 1) {
          dir_list[iside][num_dir[iside]] = ipin;
          num_dir[iside]++;
       }
    }
 }

 num_phys_pins = 0;
 for (iside=0;iside<4;iside++) 
    num_phys_pins += num_dir[iside];  /* Num. physical output pins per clb */

 num_done_per_dir = (int *) my_calloc (4, sizeof(int));
 pin_num_ordering = (int *) my_malloc (num_phys_pins * sizeof(int));
 side_ordering = (int *) my_malloc (num_phys_pins * sizeof(int));

/* Connection block I use distributes pins evenly across the tracks      *
 * of ALL sides of the clb at once.  Ensures that each pin connects      *
 * to spaced out tracks in its connection block, and that the other      *
 * pins (potentially in other C blocks) connect to the remaining tracks  *
 * first.  Doesn't matter for large Fc, but should make a fairly         *
 * good low Fc block that leverages the fact that usually lots of pins   *
 * are logically equivalent.                                             */

 iside = -1;
 ipin = 0;
 pindex = 0;

 while (ipin < num_phys_pins) {
    iside = (iside + 1);
    if (iside > 3) {
       pindex++;
       iside = 0;
    }
    if (num_done_per_dir[iside] >= num_dir[iside])
       continue;
    pin_num_ordering[ipin] = dir_list[iside][pindex]; 
    side_ordering[ipin] = iside;
    num_done_per_dir[iside]++;
    ipin++;
 }


 step_size = (float) nodes_per_chan / (float) (Fc * num_phys_pins);
 if (step_size > 1.) {
    if (pin_type == DRIVER) 
       printf("Warning:  some tracks are never driven by clb outputs.\n");
    else 
       printf("Warning:  some tracks cannot reach any inputs.\n");
 }

 if (perturb_switch_pattern) {
    load_perturbed_switch_pattern (tracks_connected_to_pin, num_phys_pins, 
          pin_num_ordering, side_ordering, nodes_per_chan, Fc, step_size);

    check_all_tracks_reach_pins (tracks_connected_to_pin, nodes_per_chan, Fc, 
          RECEIVER);
 }
 else {
    load_uniform_switch_pattern (tracks_connected_to_pin, num_phys_pins, 
          pin_num_ordering, side_ordering, nodes_per_chan, Fc, step_size);
 }

/* Free all temporary storage. */

 free (num_dir);
 free_matrix (dir_list, 0, 3, 0, sizeof(int));
 free (num_done_per_dir);
 free (pin_num_ordering);
 free (side_ordering);

 //printf("Done alloc_and_load_clb_pin_to_tracks.\n");
 return (tracks_connected_to_pin);
}//eof
// ======================================================================= //


void s_fpga3d::load_uniform_switch_pattern (int ***tracks_connected_to_pin, 
       int num_phys_pins, int *pin_num_ordering, int *side_ordering, 
       int nodes_per_chan, int Fc, float step_size) 
{
/* Loads the tracks_connected_to_pin array with an even distribution of     *
 * switches across the tracks for each pin.  For example, each pin connects *
 * to every 4.3rd track in a channel, with exactly which tracks a pin       *
 * connects to staggered from pin to pin.                                   */

 int i, j, ipin, iside, itrack;
 float f_track;


 for (i=0;i<num_phys_pins;i++) {
    ipin = pin_num_ordering[i];
    iside = side_ordering[i];
    for (j=0;j<Fc;j++) {
       f_track = i*step_size + j * (float) nodes_per_chan / (float) Fc;
       itrack = (int) f_track;

  /* In case of weird roundoff effects */
       itrack = min(itrack, nodes_per_chan-1);
       tracks_connected_to_pin[ipin][iside][j] = itrack;
    }
 }
}
// ======================================================================= //


void s_fpga3d::load_perturbed_switch_pattern (int ***tracks_connected_to_pin, 
       int num_phys_pins, int *pin_num_ordering, int *side_ordering, 
       int nodes_per_chan, int Fc, float step_size) 
{
/* Loads the tracks_connected_to_pin array with an unevenly distributed     *
 * set of switches across the channel.  This is done for inputs when        *
 * Fc_input = Fc_output to avoid creating "pin domains" -- certain output   *
 * pins being able to talk only to certain input pins because their switch  *
 * patterns exactly line up.  Distribute Fc/2 + 1 switches over half the    *
 * channel and Fc/2 - 1 switches over the other half to make the switch     * 
 * pattern different from the uniform one of the outputs.  Also, have half  *
 * the pins put the "dense" part of their connections in the first half of  *
 * the channel and the other half put the "dense" part in the second half,  *
 * to make sure each track can connect to about the same number of ipins.   */

 int i, j, ipin, iside, itrack, ihalf, iconn;
 int Fc_dense, Fc_sparse, Fc_half[2];
 float f_track, spacing_dense, spacing_sparse, spacing[2];
 
 Fc_dense = Fc/2 + 1;
 Fc_sparse = Fc - Fc_dense;  /* Works for even or odd Fc */

 spacing_dense = (float) nodes_per_chan / (float) (2 * Fc_dense);
 spacing_sparse = (float) nodes_per_chan / (float) (2 * Fc_sparse);

 for (i=0;i<num_phys_pins;i++) {
    ipin = pin_num_ordering[i];
    iside = side_ordering[i];

    /* Flip every pin to balance switch density */

    spacing[i%2] = spacing_dense;
    Fc_half[i%2] = Fc_dense;
    spacing[(i+1)%2] = spacing_sparse;
    Fc_half[(i+1)%2] = Fc_sparse;

    f_track = i * step_size;    /* Start point.  Staggered from pin to pin */
    iconn = 0;

    for (ihalf=0;ihalf<2;ihalf++) {  /* For both dense and sparse halves. */

       for (j=0;j<Fc_half[ihalf];j++) {
          itrack = (int) f_track;

     /* Can occasionally get wraparound. */
          itrack = itrack%nodes_per_chan;
          tracks_connected_to_pin[ipin][iside][iconn] = itrack;

          f_track += spacing[ihalf];
          iconn++;
       }

    }
 }   /* End for all physical pins. */
}
// ======================================================================= //


void s_fpga3d::check_all_tracks_reach_pins (int ***tracks_connected_to_pin,
         int nodes_per_chan, int Fc, enum e_pin_type ipin_or_opin) {

/* Checks that all tracks can be reached by some pin.   */

 int iconn, iside, itrack, ipin;
 int *num_conns_to_track;  /* [0..nodes_per_chan-1] */

 num_conns_to_track = (int *) my_calloc (nodes_per_chan, sizeof(int));
 
 for (ipin=0;ipin<pins_per_clb;ipin++) {
    for (iside=0;iside<=3;iside++) {
       if (tracks_connected_to_pin[ipin][iside][0] != OPEN) { /* Pin exists */
          for (iconn=0;iconn<Fc;iconn++) {
             itrack = tracks_connected_to_pin[ipin][iside][iconn];
             num_conns_to_track[itrack]++;
          }
       }
    }
 }

 for (itrack=0;itrack<nodes_per_chan;itrack++) {
    if (num_conns_to_track[itrack] <= 0) {
       printf ("Warning (check_all_tracks_reach_pins):  track %d does not \n"
               "\tconnect to any CLB ", itrack);

       if (ipin_or_opin == DRIVER)
          printf ("OPINs.\n");
       else
          printf ("IPINs.\n");
    }
 }
 
 free (num_conns_to_track);
}
// ======================================================================= //
 

s_ivec **s_fpga3d::alloc_and_load_tracks_to_clb_ipin (int nodes_per_chan,
      int Fc, int ***clb_ipin_to_tracks) 
{
/* The routing graph will connect tracks to input pins on the clbs.   *
 * This routine converts the list of which tracks each ipin connects  *
 * to into a list of the ipins each track connects to.                */

 int ipin, iside, itrack, iconn, ioff, tr_side;

/* [0..nodes_per_chan-1][0..3].  For each track number it stores a vector  *
 * for each of the four sides.  x-directed channels will use the TOP and   *
 * BOTTOM vectors to figure out what clb input pins they connect to above  *
 * and below them, respectively, while y-directed channels use the LEFT    *
 * and RIGHT vectors.  Each vector contains an nelem field saying how many *
 * ipins it connects to.  The list[0..nelem-1] array then gives the pin    *
 * numbers.                                                                */

/* Note that a clb pin that connects to a channel on its RIGHT means that  *
 * that channel connects to a clb pin on its LEFT.  I have to convert the  *
 * sides so that the new structure lists the sides of pins from the        *
 * perspective of the track.                                               */

 struct s_ivec **tracks_to_clb_ipin;
 
 tracks_to_clb_ipin = (struct s_ivec **) alloc_matrix (0, nodes_per_chan-1, 0,
      3, sizeof(struct s_ivec));

 for (itrack=0;itrack<nodes_per_chan;itrack++)
    for (iside=0;iside<=3;iside++) 
       tracks_to_clb_ipin[itrack][iside].nelem = 0;

/* Counting pass.  */

 for (ipin=0;ipin<pins_per_clb;ipin++) {
    for (iside=0;iside<=3;iside++) {
       if (clb_ipin_to_tracks[ipin][iside][0] == OPEN) 
          continue;
       
       tr_side = track_side (iside);
       for (iconn=0;iconn<Fc;iconn++) {
          itrack = clb_ipin_to_tracks[ipin][iside][iconn];
          tracks_to_clb_ipin[itrack][tr_side].nelem++;
       }
    }
 }

/* Allocate space.  */

 for (itrack=0;itrack<nodes_per_chan;itrack++) {
    for (iside=0;iside<=3;iside++) {
       if (tracks_to_clb_ipin[itrack][iside].nelem != 0) {
          tracks_to_clb_ipin[itrack][iside].list = (int *) my_malloc (
                    tracks_to_clb_ipin[itrack][iside].nelem * sizeof (int));
          tracks_to_clb_ipin[itrack][iside].nelem = 0;
       }
       else {
          tracks_to_clb_ipin[itrack][iside].list = NULL;  /* Defensive code */
       }
    }
 } 

/* Loading pass. */

 for (ipin=0;ipin<pins_per_clb;ipin++) {
    for (iside=0;iside<=3;iside++) {
       if (clb_ipin_to_tracks[ipin][iside][0] == OPEN) 
          continue;
       
       tr_side = track_side (iside);
       for (iconn=0;iconn<Fc;iconn++) {
          itrack = clb_ipin_to_tracks[ipin][iside][iconn];
          ioff = tracks_to_clb_ipin[itrack][tr_side].nelem;
          tracks_to_clb_ipin[itrack][tr_side].list[ioff] = ipin;
          tracks_to_clb_ipin[itrack][tr_side].nelem++;
       }
    }
 }
 //printf("Done alloc_and_load_tracks_to_clb_ipin.\n");
 return (tracks_to_clb_ipin);
}//eof
// ======================================================================= //


int s_fpga3d::track_side (int clb_side) 
{
/* Converts a side from referring to the world from a clb's perspective *
 * to a channel's perspective.  That is, a connection from a clb to the *
 * track above (TOP) it, is to the clb below (BOTTOM) the track.        */
 
 switch (clb_side) {
 
 case TOP:
    return (BOTTOM);

 case BOTTOM:
    return (TOP);
 
 case LEFT:
    return (RIGHT);

 case RIGHT:
    return (LEFT);

 default:
    printf("Error:  unexpected clb_side (%d) in track_side.\n", clb_side);
    exit (1);
 }
}
// ======================================================================= //


int **s_fpga3d::alloc_and_load_pads_to_tracks (int nodes_per_chan, int Fc_pad) 
{

  // Allocates and loads up a 2D array ([0..io_rat-1][0..Fc_pad-1]) where 
  // each entry gives a track number to which that pad connects if it is  
  // an INPUT pad.  Code below should work for both GLOBAL and DETAILED.  

 int **pads_to_tracks;
 float step_size;
 int ipad, iconn, itrack;

 pads_to_tracks = (int **) alloc_matrix (0, io_rat-1, 0, Fc_pad-1, sizeof(int));
 step_size = (float) nodes_per_chan / (float) (Fc_pad * io_rat);
 
 for (ipad=0;ipad<io_rat;ipad++) {
    for (iconn=0;iconn<Fc_pad;iconn++) {

       itrack = (int) (ipad * step_size + iconn * (float) nodes_per_chan / 
                 (float) Fc_pad);

/* Watch for weird round off effects.  */
       itrack = min(itrack, nodes_per_chan-1);
       pads_to_tracks[ipad][iconn] = itrack;
    }
 }
 //printf("Done alloc_and_load_pads_to_tracks.\n");
 return (pads_to_tracks);
}//eof
// ======================================================================= //


s_ivec *s_fpga3d::alloc_and_load_tracks_to_pads (int **pads_to_tracks, 
          int nodes_per_chan, int Fc_pad) 
{
/* Converts the list of tracks each IO pad connects to into a list of  *
 * pads each track should connect to.  Allocates and fills in an array *
 * of vectors [0..nodes_per_chan-1].  Each vector specifies the number *
 * of pads to which that track connects and gives a list of the pads.  */

 int itrack, ipad, i, iconn, ioff;
 struct s_ivec *tracks_to_pads;

 tracks_to_pads = (struct s_ivec *) my_malloc (nodes_per_chan * sizeof (
           struct s_ivec));
 
/* Counting pass. */

 for (i=0;i<nodes_per_chan;i++) 
    tracks_to_pads[i].nelem = 0;

 for (ipad=0;ipad<io_rat;ipad++) {
    for (iconn=0;iconn<Fc_pad;iconn++) {
       itrack = pads_to_tracks[ipad][iconn];
       tracks_to_pads[itrack].nelem++;
    }
 }

 for (i=0;i<nodes_per_chan;i++) {
    if (tracks_to_pads[i].nelem != 0) {
       tracks_to_pads[i].list = (int *) my_malloc (tracks_to_pads[i].nelem *
                        sizeof(int));
       tracks_to_pads[i].nelem = 0;
    }
    else {
       tracks_to_pads[i].list = NULL;  /* For safety only. */
    }
 }

/* Load pass. */

 for (ipad=0;ipad<io_rat;ipad++) {
    for (iconn=0;iconn<Fc_pad;iconn++) {
       itrack = pads_to_tracks[ipad][iconn];
       ioff = tracks_to_pads[itrack].nelem;
       tracks_to_pads[itrack].list[ioff] = ipad;
       tracks_to_pads[itrack].nelem++;
    }
 }
 //printf("Done alloc_and_load_tracks_to_pads.\n");
 return (tracks_to_pads);
}//eof
// ======================================================================= //



void s_fpga3d::dump_rr_graph (char *file_name)
{
// A utility routine to dump the contents of the routing resource graph   
// (everything -- connectivity, occupancy, cost, etc.) into a file.  Used 
// only for debugging.                                                    

 int inode, index;
 FILE *fp;

 fp = my_fopen (file_name, "w", 0);

 for (inode=0;inode<num_rr_nodes;inode++) {
    print_rr_node (fp, inode);
    fprintf (fp, "\n");
 }

 fprintf (fp, "\n\n%d rr_indexed_data entries.\n\n", num_rr_indexed_data);
 
 for (index=0;index<num_rr_indexed_data;index++) {
    print_rr_indexed_data (fp, index);
    fprintf (fp, "\n");
 }
     
 fclose (fp);
}//eof
// ======================================================================= //


void s_fpga3d::print_rr_node (FILE *fp, int inode) 
{
// Prints all the data about node inode to file fp.

 static char *name_type[] = {"SOURCE", "SINK", "IPIN", "OPIN", "CHANX",
       "CHANY","CHANZ"};
 t_rr_type rr_type;
 int iconn;

 rr_type = rr_node[inode].type;
 if (rr_node[inode].xlow == rr_node[inode].xhigh && rr_node[inode].ylow ==
            rr_node[inode].yhigh) {
    fprintf (fp, "Node: %d.  %s (%d, %d, %d)  Ptc_num: %d\n", inode,
       name_type[rr_type], rr_node[inode].xlow, rr_node[inode].ylow, 
	     rr_node[inode].zlow, rr_node[inode].ptc_num);
 }
 else {
    fprintf (fp, "Node: %d.  %s (%d, %d, %d) to (%d, %d, %d)  Ptc_num: %d\n", inode,
	     name_type[rr_type], 
	     rr_node[inode].xlow, rr_node[inode].ylow, rr_node[inode].zlow,
	     rr_node[inode].xhigh, rr_node[inode].yhigh, rr_node[inode].zhigh,
	     rr_node[inode].ptc_num);
 }
 
 fprintf (fp, "%4d edge(s):", rr_node[inode].num_edges);
 
 for (iconn=0;iconn<rr_node[inode].num_edges;iconn++)
    fprintf (fp," %d", rr_node[inode].edges[iconn]);

 fprintf (fp,"\nSwitch types:");

 for (iconn=0;iconn<rr_node[inode].num_edges;iconn++)
    fprintf (fp, " %d", rr_node[inode].switches[iconn]);
     
 fprintf (fp, "\n");
 fprintf (fp, "Occ: %d  Capacity: %d.\n", rr_node[inode].occ,
     rr_node[inode].capacity);
 fprintf (fp,"R: %g  C: %g\n", rr_node[inode].R, rr_node[inode].C);
 fprintf (fp, "Cost_index: %d\n", rr_node[inode].cost_index);
}//eof
// ======================================================================= //


void s_fpga3d::print_rr_indexed_data (FILE *fp, int index) 
{
// Prints all the rr_indexed_data of index to file fp.

 fprintf (fp, "Index: %d\n", index);
 fprintf (fp, "ortho_cost_index: %d  base_cost: %g  saved_base_cost: %g\n",
               rr_indexed_data[index].ortho_cost_index, 
               rr_indexed_data[index].base_cost,
               rr_indexed_data[index].saved_base_cost);
 fprintf (fp, "Seg_index: %d  inv_length: %g\n", 
       rr_indexed_data[index].seg_index, rr_indexed_data[index].inv_length);
 fprintf (fp, "T_linear: %g  T_quadratic: %g  C_load: %g\n", 
       rr_indexed_data[index].T_linear, rr_indexed_data[index].T_quadratic,
       rr_indexed_data[index].C_load);
}//eof
// ======================================================================= //
