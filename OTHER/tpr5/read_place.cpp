
#include <string.h> 
#include "util.h" 
#include "hash.h"

#include "ipr_types.h"
#include "fpga3d.h"


using namespace std;

// ======================================================================= //
/*---
void s_fpga3d::read_user_pad_loc (char *pad_loc_file) 
{

  // Reads in the locations of the IO pads from a file. 

 struct s_hash **hash_table, *h_ptr;
 int iblk, i=0,j=0,k=0, xtmp,ytmp,ztmp, bnum,isubblk;
 FILE *fp;
 char buf[BUFSIZE], bname[BUFSIZE], *ptr;
 
 printf ("\nReading locations of IO pads from %s.\n", pad_loc_file);
 linenum = 0;
 fp = my_fopen (pad_loc_file, "r", 0);

 hash_table = alloc_hash_table ();
 for (iblk=0;iblk<num_blocks;iblk++) {
    if (block[iblk].type == INPAD || block[iblk].type == OUTPAD) {
       h_ptr = insert_in_hash_table (hash_table, block[iblk].name, iblk);
       block[iblk].x = OPEN;   // Mark as not seen yet. 
    }
 }

 for (k=0;k<num_layers;k++) {
  for (i=0;i<=nx+1;i++) {
    for (j=0;j<=ny+1;j++) {
       if (clb[i][j][k].type == IO) {
          for (isubblk=0;isubblk<io_rat;isubblk++) 
	    clb[i][j][k].u.io_blocks[isubblk] = OPEN;  // Flag for err. check
       }
     }
   }
 }// for k

 ptr = my_fgets (buf, BUFSIZE, fp);

 while (ptr != NULL) {
    ptr = my_strtok (buf, TOKENS, fp, buf);
    if (ptr == NULL) {
       ptr = my_fgets (buf, BUFSIZE, fp);
       continue;      // Skip blank or comment lines. 
    }
     
    strcpy (bname, ptr); 

    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr == NULL) {
       printf ("Error:  line %d is incomplete.\n", linenum);
       exit (1);
    }
    sscanf (ptr, "%d", &xtmp);
   
    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr == NULL) {
       printf ("Error:  line %d is incomplete.\n", linenum);
       exit (1);
    }
    sscanf (ptr, "%d", &ytmp);

    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr == NULL) {
       printf ("Error:  line %d is incomplete.\n", linenum);
       exit (1);
    }
    sscanf (ptr, "%d", &ztmp);
    
    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr == NULL) {
       printf ("Error:  line %d is incomplete.\n", linenum);
       exit (1);
    }
    sscanf (ptr, "%d", &isubblk);

    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr != NULL) {
       printf ("Error:  extra characters at end of line %d.\n", linenum);
       exit (1);
    }

    h_ptr = get_hash_entry (hash_table, bname);
    if (h_ptr == NULL) {
       printf ("Error:  block %s on line %d: no such IO pad.\n",
                bname, linenum);
       exit (1);
    }
    bnum = h_ptr->index;
    i = xtmp;
    j = ytmp;
    k = ztmp;

    if (block[bnum].x != OPEN) {
       printf ("Error:  line %d.  Block %s listed twice in pad file.\n",
                linenum, bname);
       exit (1);
    }

    if (i < 0 || i > nx+1 || j < 0 || j > ny + 1 || k < 0 || k >= num_layers ) {
       printf("Error:  block #%d (%s) location\n", bnum, bname);
       printf("(%d,%d) is out of range.\n", i, j);
       exit (1);
    }

    block[bnum].x = i;   // Will be reloaded by initial_placement anyway. 
    block[bnum].y = j;   // I need to set .x only as a done flag.
    block[bnum].z = k;          

    if (clb[i][j][k].type != IO) {
       printf("Error:  attempt to place IO block %s in \n", bname);
       printf("an illegal location (%d, %d).\n", i, j);
       exit (1);
    }
 
    if (isubblk >= io_rat || isubblk < 0) {
       printf ("Error:  Block %s subblock number (%d) on line %d is out of "
               "range.\n", bname, isubblk, linenum);
       exit (1);
    }
    clb[i][j][k].u.io_blocks[isubblk] = bnum;
    clb[i][j][k].occ++;
 
    ptr = my_fgets (buf, BUFSIZE, fp);
 }

 for (iblk=0;iblk<num_blocks;iblk++) {
    if ((block[iblk].type == INPAD || block[iblk].type == OUTPAD) &&
             block[iblk].x == OPEN) {
       printf ("Error:  IO block %s location was not specified in "
               "the pad file.\n", block[iblk].name);
       exit (1);
    }
 }

 for (k=0;k<num_layers;k++) { 
   for (i=0;i<=nx+1;i++) {
    for (j=0;j<=ny+1;j++) {
       if (clb[i][j][k].type == IO) {
          for (isubblk=0;isubblk<clb[i][j][k].occ;isubblk++) {
             if (clb[i][j][k].u.io_blocks[isubblk] == OPEN) {
                printf ("Error:  The IO blocks at (%d, %d) do not have \n"
                        "consecutive subblock numbers starting at 0.\n", i, j);
                exit (1);
             }
          }
       }
      }
    }
 }//for k
 
 fclose (fp);
 free_hash_table (hash_table);
 printf ("Successfully read %s.\n\n", pad_loc_file);
}//eof
---*/
// ======================================================================= //


// ======================================================================= //
/*---
void s_fpga3d::dump_clbs (void) 
{

  // Output routine for debugging.

 int i=0,j=0,k=0, index;

 for (k=0;k<num_layers;k++) {
   for (i=0;i<=nx+1;i++) {
    for (j=0;j<=ny+1;j++) {
       printf("clb (%d,%d):  type: %d  occ: %d\n",
        i,j,clb[i][j][k].type, clb[i][j][k].occ);
       if (clb[i][j][k].type == CLB) 
          printf("block: %d\n",clb[i][j][k].u.block);
       if (clb[i][j][k].type == IO) {
          printf("io_blocks: ");
          for (index=0;index<clb[i][j][k].occ;index++) 
              printf("%d  ", clb[i][j][k].u.io_blocks[index]);
          printf("\n");
       }
      }
    }
 }// for k

 for (i=0;i<num_blocks;i++) {
    printf("block: %d, (i,j,k): (%d,%d,%d)\n",
            i, block[i].x, block[i].y, block[i].z);
 }
}//eof
---*/
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::print_place(char *place_file,char *net_file,char *arch_file) 
{
// Prints out the placement of the circuit.  The architecture and    
// netlist files used to generate this placement are recorded in the 
// file to avoid loading a placement with the wrong support files   
// later.  
                                                          
  int i=0,j=0,k=0,subblock=0;

  // --- First, mirror/copy all blocks and their locations into clbs,
  // which is the architecture itself.
  for (k=0;k<num_layers;k++)
    for (i=0;i<=nx+1;i++)
      for (j=0;j<=ny+1;j++)
	clb[i][j][k].occ = 0;//reset
  
  for (i=0;i<num_blocks;i++) {
    if (block[i].type == CLB) {
      clb[block[i].x][block[i].y][block[i].z].occ = 1;
      clb[block[i].x][block[i].y][block[i].z].u.block = i;
    }
    if( (block[i].type == INPAD)||(block[i].type == OUTPAD) ) {
      // An IO clb may be occupied by more INPAD OUTPAD blocks
      // assigned during io pad assignment on margins/perifery.
      j = clb[block[i].x][block[i].y][block[i].z].occ;
      clb[block[i].x][block[i].y][block[i].z].u.io_blocks[j] = i;
      clb[block[i].x][block[i].y][block[i].z].occ++;
      //printf(" %d",j);
    }
  }//for

  // --- Second, dump the placement file in vpr format for 2D
  // or in similar to VPR format for 3D
  FILE *fp; 
  fp = my_fopen(place_file,"w",0);
  
  fprintf(fp,"Netlist file: %s   Architecture file: %s\n", 
	  net_file, arch_file);
  if (num_layers == 1) { // 2D
    fprintf (fp, "Array size: %d x %d logic blocks\n\n",nx,ny);
    fprintf (fp, "#block name\tx\ty\tsubblk\tblock number\n");
    fprintf (fp, "#----------\t--\t--\t------\t------------\n");
  }
  else {                 // 3D
    fprintf (fp, "Array size: %d x %d x %d logic blocks\n\n",nx,ny,num_layers);
    fprintf (fp, "#block name\tx\ty\tz\tsubblk\tblock number\n");
    fprintf (fp, "#----------\t--\t--\t--\t------\t------------\n");
  }


  for (i=0;i<num_blocks;i++) {
    fprintf (fp,"%s\t", block[i].name);
    if (strlen(block[i].name) < 8) 
      fprintf (fp, "\t");
    
    if (num_layers == 1) { // 2D
      // This is in order for the placement 2D to be routable
      // with the VPR routing tool.
      fprintf (fp,"%d\t%d", block[i].x, block[i].y);
    }
    else {                 // 3D
      fprintf (fp,"%d\t%d\t%d", block[i].x, block[i].y, block[i].z);
    }

    if (block[i].type == CLB) {
      fprintf(fp,"\t%d", 0);        // Sub block number not meaningful. 
    }
    else {                          // IO block.  Save sub block number. 
      subblock = get_subblock (block[i].x,block[i].y,block[i].z, i);
      fprintf(fp,"\t%d", subblock);
    }
    fprintf (fp, "\t#%d\n", i);
  }//for (i=0;...
  
  fclose(fp);
}//eof
// ======================================================================= //


// ======================================================================= //
int s_fpga3d::get_subblock (int i, int j, int k, int bnum) 
{
  // Use this routine only for IO blocks.  It passes back the index of the 
  // subblock containing block bnum at location (i,j).                     
  int u=0;

  for (u=0;u<io_rat;u++) {
    if (clb[i][j][k].u.io_blocks[u] == bnum) 
      return (u);
  }
  printf("Error in get_subblock.  Block %d is not at location (%d,%d,%d)\n",
	 bnum, i, j, k);
  exit(1);
}//eof
// ======================================================================= //


// ======================================================================= //

void s_fpga3d::parse_placement_file (char *place_file, 
				     char *net_file, 
				     char *arch_file) 
{
// Reads the blocks in from a previously placed circuit.

 FILE *fp;
 char bname[BUFSIZE];
 char buf[BUFSIZE], *ptr;
 struct s_hash **hash_table, *h_ptr;
 int i=0,j=0,k=0; 
 int bnum=0, isubblock=0, xtmp=0, ytmp=0, ztmp=0;

 printf("\nReading the placement from file %s.\n", place_file);
 fp = my_fopen (place_file, "r", 0);
 linenum = 0;
 
 read_place_header (fp, net_file, arch_file, buf);

 for (k=0;k<num_layers;k++) {
  for (i=0;i<=nx+1;i++) {
   for (j=0;j<=ny+1;j++) {
     clb[i][j][k].occ = 0;
     if (clb[i][j][k].type == IO) {
       for (isubblock=0;isubblock<io_rat;isubblock++) {
	 clb[i][j][k].u.io_blocks[isubblock] = OPEN;
       }
     }
   }
  }
 }//for k

 for (i=0;i<num_blocks;i++)
   block[i].x = OPEN;        // Flag to show not read yet. 

 hash_table = alloc_hash_table ();
 for (i=0;i<num_blocks;i++)
    h_ptr = insert_in_hash_table (hash_table, block[i].name, i);

 ptr = my_fgets (buf, BUFSIZE, fp);


 while (ptr != NULL) {
   ptr = my_strtok (buf, TOKENS, fp, buf);
   if (ptr == NULL) {
     ptr = my_fgets (buf, BUFSIZE, fp);
     continue;      // Skip blank or comment lines.
   }
     
   strcpy (bname, ptr); 

   ptr = my_strtok (NULL, TOKENS, fp, buf);
   if (ptr == NULL) {
     printf ("Error:  line %d is incomplete - xtmp.\n", linenum);
     exit (1);
   }
   sscanf (ptr, "%d", &xtmp);
   
   ptr = my_strtok (NULL, TOKENS, fp, buf);
   if (ptr == NULL) {
     printf ("Error:  line %d is incomplete - ytmp.\n", linenum);
     exit (1);
   }
   sscanf (ptr, "%d", &ytmp);
   
   // cristinel.ababei: next is coordinate "z"
   if ( num_layers > 1 ) {
     ptr = my_strtok (NULL, TOKENS, fp, buf);
     if (ptr == NULL) {
       printf ("Error:  line %d is incomplete.\n", linenum);
       exit (1);
     }
     sscanf (ptr, "%d", &ztmp);
   }//if
    
   ptr = my_strtok (NULL, TOKENS, fp, buf);
   if (ptr == NULL) {
     printf ("Error:  line %d is incomplete.\n", linenum);
     exit (1);
   }
   sscanf (ptr, "%d", &isubblock);
   
   ptr = my_strtok (NULL, TOKENS, fp, buf);
   if (ptr != NULL) {
     printf ("Error:  extra characters at end of line %d.\n", linenum);
     exit (1);
   }
   
   h_ptr = get_hash_entry (hash_table, bname);
   if (h_ptr == NULL) {
     printf ("Error:  block %s on line %d does not exist in the netlist.\n",
	     bname, linenum);
     exit (1);
   }
   bnum = h_ptr->index;
   i = xtmp;
   j = ytmp;
   k = ztmp;
   
   if (block[bnum].x != OPEN) {
     printf ("Error:  line %d.  Block %s listed twice in placement file.\n",
	     linenum, bname);
     exit (1);
   }
   
   if ( i < 0 || i > nx + 1 || 
	j < 0 || j > ny + 1 ||
	k < 0 || k >= num_layers) {
     printf("Error in read_place.  Block #%d (%s) location\n", bnum, bname);
     printf("(%d,%d,%d) is out of range.\n", i, j, k);
     exit (1);
   }
   
   block[bnum].x = i;
   block[bnum].y = j;
   block[bnum].z = k;
   
   if (clb[i][j][k].type == CLB) {
     if (block[bnum].type != CLB) {
       printf("Error in read_place.  Attempt to place block #%d (%s) in\n",
	      bnum, bname);
       printf("a logic block location (%d, %d).\n", i, j);
       exit (1);
     } 
     clb[i][j][k].u.block = bnum;
     clb[i][j][k].occ++;
   }
   
   else if (clb[i][j][k].type == IO) {
     if (block[bnum].type != INPAD && block[bnum].type != OUTPAD) {
       printf("Error in read_place.  Attempt to place block #%d (%s) in\n",
	      bnum, bname);
       printf("an IO block location (%d, %d, %d).\n", i, j, k);
       exit (1);
     }
     if (isubblock >= io_rat || isubblock < 0) {
       printf ("Error:  Block %s subblock number (%d) on line %d is out of "
	       "range.\n", bname, isubblock, linenum);
       exit (1);
     }
     clb[i][j][k].u.io_blocks[isubblock] = bnum;
     clb[i][j][k].occ++;
   }
 
   else {    // Block type was ILLEGAL or some unknown value 
     printf("Error in read_place.  Block #%d (%s) is in an illegal ",
	    bnum, bname);
     printf("location.\nLocation specified: (%d,%d,%d).\n", i, j, k);
     exit (1);
   }
   
   ptr = my_fgets (buf, BUFSIZE, fp);
 }
 
 free_hash_table (hash_table);
 fclose (fp);
 
 for (i=0;i<num_blocks;i++) {
   if (block[i].x == OPEN) {
     printf ("Error in read_place:  block %s location was not specified in "
	     "the placement file.\n", block[i].name);
     exit (1);
   }
 }
 
 for (k=0;k<num_layers;k++) { 
   for (i=0;i<=nx+1;i++) {
     for (j=0;j<=ny+1;j++) {
       if (clb[i][j][k].type == IO) {
	 for (isubblock=0;isubblock<clb[i][j][k].occ;isubblock++) {
	   if (clb[i][j][k].u.io_blocks[isubblock] == OPEN) {
	     printf ("Error:  The IO blocks at (%d,%d,%d) do not have \n"
		     "consecutive subblock numbers starting at 0.\n",i,j,k);
	     exit (1);
	   }
	 }
       }
     }
   }
 }//for k
 
 printf ("Successfully read %s.\n", place_file);
}//eof

// ======================================================================= //


// ======================================================================= //

void s_fpga3d::read_place_header (FILE *fp, char *net_file, 
				  char *arch_file, char *buf) 
{
// Reads the header from the placement file.  Used only to check that this 
// placement file matches the current architecture, netlist, etc.          

  char *line_one_names[] = {"Netlist", "file:", " ", "Architecture", "file:"};
  char *line_two_names[]  
    = {"Array", "size:", " ", "x", " ", "logic", "blocks"};
  char *line_two_names3[] 
    = {"Array", "size:", " ", "x", " ", "x", " ","logic", "blocks"};

  char net_check[BUFSIZE], arch_check[BUFSIZE], *ptr;
  int nx_check=0, ny_check=0, nz_check=0, i=0;
  

  ptr = my_fgets (buf, BUFSIZE, fp);
 
  if (ptr == NULL) {
    printf ("Error:  netlist file and architecture file used not listed.\n");
    exit (1);
  }
 
  ptr = my_strtok (buf, TOKENS, fp, buf);
  while (ptr == NULL) {   // Skip blank or comment lines.
    ptr = my_fgets (buf, BUFSIZE, fp);
    if (ptr == NULL) {
      printf ("Error:  netlist file and architecture file used not listed.\n");
      exit (1);
    }
    ptr = my_strtok (buf, TOKENS, fp, buf);
  }
 

  for (i=0;i<=5;i++) {
    if (i == 2) {
      strcpy (net_check, ptr);
    }
    else if (i == 5) {
      strcpy (arch_check, ptr);
    }
    else {
      if (strcmp (ptr, line_one_names[i]) != 0) {
	printf ("Error on line %d, word %d:  \n"
		"Expected keyword %s, got %s.\n", linenum, i+1, 
		line_one_names[i], ptr);
	exit (1);
      } 
    }
    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr == NULL && i != 5) {
      printf ("Error:  Unexpected end of line on line %d.\n", linenum);
      exit (1);
    }
  }
  

  if (strcmp(net_check, net_file) != 0) {
    printf("Warning:  Placement generated with netlist file %s:\n", 
	   net_check);
    printf("current net file is %s.\n", net_file);
  }
  
  if (strcmp(arch_check, arch_file) != 0) {
    printf("Warning:  Placement generated with architecture file %s:\n", 
           arch_check);
    printf("current architecture file is %s.\n", arch_file);
  }
  

  // Now check the second line (array size). 
  ptr = my_fgets (buf, BUFSIZE, fp);
  
  if (ptr == NULL) {
    printf ("Error:  Array size not listed.\n");
    exit (1);
  }
  
  ptr = my_strtok (buf, TOKENS, fp, buf);
  while (ptr == NULL) {   // Skip blank or comment lines.
    ptr = my_fgets (buf, BUFSIZE, fp);
    if (ptr == NULL) {
      printf ("Error:  array size not listed.\n");
      exit (1);
    }
    ptr = my_strtok (buf, TOKENS, fp, buf);
  }
  

  if(num_layers == 1) { // 2D
    //printf("\n ooops 2d\n");
    for (i=0;i<=6;i++) {
      if (i == 2) {
	sscanf (ptr, "%d", &nx_check);
      }
      else if (i == 4) {
	sscanf (ptr, "%d", &ny_check);
      }
      else {
	if (strcmp (ptr, line_two_names[i]) != 0) {
	  printf ("Error on line %d, word %d:  \n"
		  "Expected keyword %s, got %s.\n", linenum, i+1, 
		  line_two_names[i], ptr);
	  exit (1);
	} 
      }
      ptr = my_strtok (NULL, TOKENS, fp, buf);
      if (ptr == NULL && i != 6) {
	printf ("Error:  Unexpected end of line on line %d.\n", linenum);
	exit (1);
      }
    }//for
    if (nx_check != nx || ny_check != ny) {
      printf ("Error:  placement file assumes an array size of %d x %d.\n",
	      nx_check, ny_check);
      printf ("Current size is %d x %d.\n", nx, ny);
      exit (1);
    }
  }//if

  else {                // 3D
    //printf("\n ooops 3d\n");
    for (i=0;i<=8;i++) {
      if (i == 2) {
	sscanf (ptr, "%d", &nx_check);
      }
      else if (i == 4) {
	sscanf (ptr, "%d", &ny_check);
      }
      else if (i == 6) {
	sscanf (ptr, "%d", &nz_check);
      }
      else {
	if (strcmp (ptr, line_two_names3[i]) != 0) {
	  printf ("Error on line %d, word %d:  \n"
		  "Expected keyword %s, got %s.\n", linenum, i+1, 
		  line_two_names3[i], ptr);
	  exit (1);
	} 
      }
      ptr = my_strtok (NULL, TOKENS, fp, buf);
      if (ptr == NULL && i != 8) {
	printf ("Error:  Unexpected end of line on line %d.\n", linenum);
	exit (1);
      }
    }
    if (nx_check != nx || ny_check != ny || nz_check != num_layers) {
      printf ("Error:  placement file assumes an array size of %d x %d x %d.\n",
	      nx_check, ny_check, nz_check);
      printf ("Current size is %d x %d x %d.\n", nx, ny, num_layers);
      exit (1);
      // cristinel.ababei
      // Will never be the case because of 
      // read_and_set_nx_ny_from_place_file ()
      // which will correct nx and ny to be equal to
      // those in the place_file!
    }
  }//else

}//eof
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::read_place (
       char *place_file, char *net_file, char *arch_file,
       struct s_placer_opts placer_opts, 
       struct s_router_opts router_opts,
       t_chan_width_dist chan_width_dist, 
       struct s_det_routing_arch det_routing_arch, 
       t_segment_inf *segment_inf, 
       t_timing_inf timing_inf,
       t_subblock_data *subblock_data_ptr) 
{
// Reads in a previously computed placement of the circuit.  It      
// checks that the placement corresponds to the current architecture 
// and netlist file.                                                 

  char msg[BUFSIZE];
  int chan_width_factor=0, num_connections=0, inet, ipin=0;
  float bb_cost=0, delay_cost=0, timing_cost=0, est_crit=0;
  float **dummy_x, **dummy_y;
  float **net_slack, **net_delay;
  float **remember_net_delay_original_ptr; 
  // used to free net_delay if it is re-assigned


  /*---
  remember_net_delay_original_ptr = NULL; //prevents compiler warning

  if (placer_opts.place_algorithm == NET_TIMING_DRIVEN_PLACE ||
      placer_opts.place_algorithm == PATH_TIMING_DRIVEN_PLACE ||
      placer_opts.enable_timing_computations) {
    //this must be called before alloc_and_load_placement_structs
    //and parse_placement_file since it modifies the structures
    alloc_lookups_and_criticalities(chan_width_dist,
				    router_opts, 
				    det_routing_arch, 
				    segment_inf,
				    timing_inf, 
				    *subblock_data_ptr,
				    &net_delay, &net_slack);

    num_connections = count_connections();

    remember_net_delay_original_ptr = net_delay;
  }
  else {
  num_connections = 0;
  }
  ---*/

  // First read in the placement.
  parse_placement_file (place_file, net_file, arch_file);


  // Load the channel occupancies and cost factors so that:   
  // (1) the cost check will be OK, and                       
  // (2) the geometry will draw correctly.
  /*---                    
  chan_width_factor = placer_opts.place_chan_width;
  init_chan (chan_width_factor, chan_width_dist);
  ---*/

  // NB:  dummy_x and dummy_y used because I'll never use the old_place_occ 
  // arrays in this routine.  I need the placement structures loaded for    
  // comp_cost and check_place to work.                                    

  /*---
  alloc_and_load_placement_structs (placer_opts.place_cost_type, 
				    placer_opts.num_regions, 
				    placer_opts.place_cost_exp, 
				    &dummy_x,  &dummy_y, placer_opts); 


  // Need cost in order to call check_place.
  bb_cost = comp_bb_cost (NORMAL, placer_opts.place_cost_type, 
			  placer_opts.num_regions);

  if (placer_opts.place_algorithm == NET_TIMING_DRIVEN_PLACE ||
      placer_opts.place_algorithm == PATH_TIMING_DRIVEN_PLACE ||
      placer_opts.enable_timing_computations) {

    for (inet = 0; inet<num_nets; inet++)  
      for (ipin=1; ipin<net[inet].num_pins; ipin++)
    	timing_place_crit[inet][ipin] = 0;    //dummy crit values
    
    comp_td_costs(&timing_cost, &delay_cost); //set up point_to_point_delay_cost
    
    net_delay = point_to_point_delay_cost; 
    //this keeps net_delay up to date with the
    //same values that the placer is using    
    
    load_timing_graph_net_delays(net_delay);
    est_crit = load_net_slack(net_slack, 0);
    
    printf("Placement. bb_cost: %g  delay_cost: %g.\n\n", 
	   bb_cost, delay_cost);
#ifdef PRINT_SINK_DELAYS
    print_sink_delays("Placement_Sink_Delays.echo");
#endif
#ifdef PRINT_NET_SLACKS
    print_net_slack("Placement_Net_Slacks.echo", net_slack);
#endif
#ifdef PRINT_PLACE_CRIT_PATH
    print_critical_path("Placement_Crit_Path.echo");
#endif
    printf("Placement Estimated Crit Path Delay: %g\n\n", est_crit);
  }
  else {
    timing_cost = 0;
    delay_cost = 0;
    printf("Placement bb_cost is %g.\n", bb_cost);
  }// if else
  ---*/
 

  check_place ();
    //      bb_cost, 
    //      timing_cost, 
    //      placer_opts.place_cost_type, 
    //      placer_opts.num_regions, 
    //      placer_opts.place_algorithm, 
    //      delay_cost
    //      );
  // cristinel.ababei
  // See how many io and clb are on each layer.
  get_sanity_check_of_placement();


 /*---
 free_placement_structs (placer_opts.place_cost_type, 
			 placer_opts.num_regions,
			 dummy_x, dummy_y, 
			 placer_opts);

 if (placer_opts.place_algorithm == NET_TIMING_DRIVEN_PLACE ||
     placer_opts.place_algorithm == PATH_TIMING_DRIVEN_PLACE ||
     placer_opts.enable_timing_computations) {
   net_delay = remember_net_delay_original_ptr;
   free_lookups_and_criticalities( &net_delay, &net_slack);
 }
 
 //init_draw_coords ((float) chan_width_factor);
 
 sprintf (msg, "Placement from file %s.  bb_cost %g.", place_file,
	  bb_cost);
 //update_screen (MAJOR, msg, PLACEMENT, FALSE);
 ---*/

}//eof
// ======================================================================= //


// ======================================================================= //
void s_fpga3d::check_place ()
			    //float bb_cost, 
			    //float timing_cost, 
			    //int place_cost_type, 
			    //int num_regions, 
			    //enum e_place_algorithm place_algorithm,
			    //float delay_cost) 
{
// Checks that the placement has not confused our data structures. 
// i.e. the clb and block structures agree about the locations of  
// every block, blocks are in legal spots, etc.  Also recomputes   
// the final placement cost from scratch and makes sure it is      
// within roundoff of what we think the cost is.                   

 static int *bdone; 
 int i=0, j=0, k=0, kool=0, error=0, bnum=0;
 float bb_cost_check=0;
 float timing_cost_check=0, delay_cost_check=0;

 /*---
 bb_cost_check = comp_bb_cost(CHECK, place_cost_type, num_regions);
 printf("bb_cost recomputed from scratch is %g.\n", bb_cost_check);
 if (fabs(bb_cost_check - bb_cost) > bb_cost * ERROR_TOL) {
    printf("Error:  bb_cost_check: %g and bb_cost: %g differ in check_place.\n",
      bb_cost_check, bb_cost);
    error++;
 }

 if (place_algorithm == NET_TIMING_DRIVEN_PLACE ||
     place_algorithm == PATH_TIMING_DRIVEN_PLACE) {
   comp_td_costs(&timing_cost_check, &delay_cost_check);
   printf("timing_cost recomputed from scratch is %g. \n", timing_cost_check);
   if (fabs(timing_cost_check - timing_cost) > timing_cost * ERROR_TOL) {
     printf("Error:  timing_cost_check: %g and timing_cost: "
	    "%g differ in check_place.\n",
	    timing_cost_check,timing_cost);
     error++;
   }
   printf("delay_cost recomputed from scratch is %g. \n", delay_cost_check);
   if (fabs(delay_cost_check - delay_cost) > delay_cost * ERROR_TOL) {
     printf("Error:  delay_cost_check: %g and delay_cost: "
	    "%g differ in check_place.\n",
	    delay_cost_check,delay_cost);
     error++;
   }
 }
 ---*/


 bdone = (int *) my_malloc (num_blocks*sizeof(int));
 for (i=0;i<num_blocks;i++) bdone[i] = 0;// Reset.


 //Step through clb array. Check it against block array.
 for (k=0;k<num_layers;k++) {
   for (i=0;i<=nx+1;i++) 
     for (j=0;j<=ny+1;j++) {

       if (clb[i][j][k].occ == 0) continue;
       if (clb[i][j][k].type == CLB) {
	 bnum = clb[i][j][k].u.block;
	 if (block[bnum].type != CLB) {
	   printf("Error:  block %d type does not match clb(%d,%d) type.\n",
		  bnum,i,j);
	   error++;
	 }
	 if ((block[bnum].x != i) || 
	     (block[bnum].y != j) ||
	     (block[bnum].z != k) ) {
	   printf("Error:  block %d location conflicts with clb(%d,%d,%d)"
		  "data.\n", bnum, i, j, k);
	   error++;
	 }
	 if (clb[i][j][k].occ > 1) {
	   printf("Error: clb(%d,%d,%d) has occupancy of %d\n",
		  i,j,k, clb[i][j][k].occ);
	   error++;
	 }
	 bdone[bnum]++;
       }//if
       else {  // IO block 
	 if (clb[i][j][k].occ > io_rat) {
	   printf("Error:  clb(%d,%d,%d) has occupancy of %d\n",i,j,k,
		  clb[i][j][k].occ);
	   error++;
	 }
	 for (kool=0;kool<clb[i][j][k].occ;kool++) {
	   bnum = clb[i][j][k].u.io_blocks[kool];
	   if ((block[bnum].type != INPAD) && block[bnum].type != OUTPAD) {
	     printf("Error:  block %d type does not match clb(%d,%d,%d) type.\n",
		    bnum,i,j,k);
	     error++;
	   }
	   if ((block[bnum].x != i) || 
	       (block[bnum].y != j) || 
	       (block[bnum].z != k) ) {
	     printf("Error:  block %d location conflicts with clb(%d,%d,%d)"
		    "data.\n", bnum, i, j, k);
	     error++; 
	   }
	   bdone[bnum]++;
	 }//for 
       }//if else 

     }//for (j=0...
 }//for (k=0...


 // Check that every block exists in the clb and block arrays somewhere.
 for (i=0;i<num_blocks;i++) 
   if (bdone[i] != 1) {
     printf("Error:  block %d listed %d times in data structures.\n",
	    i,bdone[i]);
     error++;
   }
 free (bdone);
 
 if (error == 0) {
   printf("\nCompleted placement consistency check successfully.\n\n");
 }
 else {
   printf("\nCompleted placement consistency check, %d Errors found.\n\n",
	  error);
   printf("Aborting program.\n");
   exit(1);
 }
}//eof
// ======================================================================= //


// ======================================================================= //

void s_fpga3d::read_and_set_nx_ny_from_place_file (
		 char *place_file ) 
{
// Developed from read_place_header().
// Reads the header from the placement file.  
// Sets nx and ny to the values from that file.

  char *line_one_names[] = {"Netlist", "file:", " ", "Architecture", "file:"};
  char *line_two_names[]  
    = {"Array", "size:", " ", "x", " ", "logic", "blocks"};
  char *line_two_names3[] 
    = {"Array", "size:", " ", "x", " ", "x", " ","logic", "blocks"};

  char net_check[BUFSIZE], arch_check[BUFSIZE], *ptr;
  int nx_check=0, ny_check=0, nz_check=0, i=0;
 
  FILE *fp;
  char buf[BUFSIZE];
  fp = my_fopen (place_file, "r", 0);
  linenum = 0;

  printf("\nChecking if nx and ny from the placement "
	 "file are different from the minimum ones, "
	 "and setting them accordingly.\n");

  ptr = my_fgets (buf, BUFSIZE, fp);
 
  if (ptr == NULL) {
    printf ("Error:  netlist file and architecture file used not listed.\n");
    exit (1);
  }
 
  ptr = my_strtok (buf, TOKENS, fp, buf);
  while (ptr == NULL) {   // Skip blank or comment lines.
    ptr = my_fgets (buf, BUFSIZE, fp);
    if (ptr == NULL) {
      printf ("Error:  netlist file and architecture file used not listed.\n");
      exit (1);
    }
    ptr = my_strtok (buf, TOKENS, fp, buf);
  }
 

  for (i=0;i<=5;i++) {
    if (i == 2) {
      strcpy (net_check, ptr);
    }
    else if (i == 5) {
      strcpy (arch_check, ptr);
    }
    else {
      if (strcmp (ptr, line_one_names[i]) != 0) {
	printf ("Error on line %d, word %d:  \n"
		"Expected keyword %s, got %s.\n", linenum, i+1, 
		line_one_names[i], ptr);
	exit (1);
      } 
    }
    ptr = my_strtok (NULL, TOKENS, fp, buf);
    if (ptr == NULL && i != 5) {
      printf ("Error:  Unexpected end of line on line %d.\n", linenum);
      exit (1);
    }
  }
  

  // Now check the second line (array size). 
  ptr = my_fgets (buf, BUFSIZE, fp);
  
  if (ptr == NULL) {
    printf ("Error:  Array size not listed.\n");
    exit (1);
  }
  
  ptr = my_strtok (buf, TOKENS, fp, buf);
  while (ptr == NULL) {   // Skip blank or comment lines.
    ptr = my_fgets (buf, BUFSIZE, fp);
    if (ptr == NULL) {
      printf ("Error:  array size not listed.\n");
      exit (1);
    }
    ptr = my_strtok (buf, TOKENS, fp, buf);
  }
  

  if(num_layers == 1) { // 2D
    //printf("\n ooops 2d\n");
    for (i=0;i<=6;i++) {
      if (i == 2) {
	sscanf (ptr, "%d", &nx_check);
      }
      else if (i == 4) {
	sscanf (ptr, "%d", &ny_check);
      }
      else {
	if (strcmp (ptr, line_two_names[i]) != 0) {
	  printf ("Error on line %d, word %d:  \n"
		  "Expected keyword %s, got %s.\n", linenum, i+1, 
		  line_two_names[i], ptr);
	  exit (1);
	} 
      }
      ptr = my_strtok (NULL, TOKENS, fp, buf);
      if (ptr == NULL && i != 6) {
	printf ("Error:  Unexpected end of line on line %d.\n", linenum);
	exit (1);
      }
    }//for
    if (nx_check != nx || ny_check != ny) {
      printf ("Error:  placement file assumes an array size of %d x %d.\n",
	      nx_check, ny_check);
      printf ("Current size is %d x %d.\n", nx, ny);
      exit (1);
    }
  }//if

  else {                // 3D
    //printf("\n ooops 3d\n");
    for (i=0;i<=8;i++) {
      if (i == 2) {
	sscanf (ptr, "%d", &nx_check);
      }
      else if (i == 4) {
	sscanf (ptr, "%d", &ny_check);
      }
      else if (i == 6) {
	sscanf (ptr, "%d", &nz_check);
      }
      else {
	if (strcmp (ptr, line_two_names3[i]) != 0) {
	  printf ("Error on line %d, word %d:  \n"
		  "Expected keyword %s, got %s.\n", linenum, i+1, 
		  line_two_names3[i], ptr);
	  exit (1);
	} 
      }
      ptr = my_strtok (NULL, TOKENS, fp, buf);
      if (ptr == NULL && i != 8) {
	printf ("Error:  Unexpected end of line on line %d.\n", linenum);
	exit (1);
      }
    }
    if (nx_check != nx || ny_check != ny || nz_check != num_layers) {
      printf ("Error:  placement file assumes an array size of %d x %d x %d.\n",
	      nx_check, ny_check, nz_check);
      printf ("Current size is %d x %d x %d.\n", nx, ny, num_layers);
      //exit (1);
      // cristinel.ababei 
      printf ("If placement you are reading now was obtained "
	      "with the partitioning-based TPR; then, that "
	      "is possible due to the imbalance due to initial "
	      "partitioning to layers.\n");
      printf ("So, I will just resize the architecture "
	      "to follow what the placement file says.\n");
      //exit (1);
      // Do corrections to account for the new dimensions
      // as dictated by the placement algorithm.
      nx = nx_check;
      ny = ny_check;
    }
  }//else

  fclose (fp);
}//eof
// ======================================================================= //



// ======================================================================= //


