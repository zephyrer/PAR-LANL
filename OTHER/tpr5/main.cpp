// ======================================================================= //
// Cristinel Ababei 10/3/2003
// ======================================================================= //
#include <stdio.h>
#include <string.h>
#include <iostream>

#include "util.h"
#include "ipr_types.h"
#include "fpga3d.h"
#include "tt_hgraph.h"
#include "tt_kmost.h"
//#include "place.h"
//#include "route.h"


using namespace std;

// ======================================================================= //

// Top-level fpga.  Global.  Contains vectors of nets, blocks, clbs one 
// which the place and route algos work.  (In some sense fpga3d class plays
// the role of global space in vpr of v. betz). 
s_fpga3d * fg3 = new s_fpga3d();

// Top-level timing graph.  Used for timing analysis.
tt_hgraph * tt_hg;
// Top-level k-most critical paths data-structure.  Stores k paths of 
// the top level hgraph.
KmostPaths * kmp = new KmostPaths(); 


// ======================================================================= //
int main (int argc, char *argv[])
{

  char title[] = 
    "\n\nTPR -- Three-D Place and Route for FPGAs -- Version 1.0 alpha\n"
    "Author: Cristinel Ababei, ababei@ece.umn.edu\n"
    "Source code under development; compiled " __DATE__ ".\n"
    "This code is licensed only for non-commercial use.\n\n";

  char net_file[BUFSIZE], place_file[BUFSIZE], arch_file[BUFSIZE];
  char route_file[BUFSIZE];
  float aspect_ratio;
  bool full_stats, user_sized; 
  char pad_loc_file[BUFSIZE];
  enum e_operation operation;
  bool verify_binary_search;
  bool show_graphics;
  int gr_automode;
  s_annealing_sched annealing_sched; 
  s_placer_opts placer_opts;
  s_router_opts router_opts;
  s_det_routing_arch det_routing_arch;
  t_segment_inf *segment_inf;
  t_segment_inf *via_inf;
  t_timing_inf timing_inf;
  t_subblock_data subblock_data;
  t_chan_width_dist chan_width_dist;
  float constant_net_delay;


  printf("%s",title);

  placer_opts.pad_loc_file = pad_loc_file;




  // -----------------------------------------------------------------------
  // -----------------------------------------------------------------------
  // Parse the command line

  fg3->parse_command (argc, argv, net_file, arch_file, place_file, route_file,
     &operation, &aspect_ratio,  &full_stats, &user_sized, &verify_binary_search,
     &gr_automode, &show_graphics, &annealing_sched, &placer_opts, &router_opts,
     &timing_inf.timing_analysis_enabled, &constant_net_delay);

  // Parse input circuit and architecture.  
  // Obs:  Inside this routine call
  // we also do partitioning into layers, using hmetis.  

  fg3->get_input (net_file, arch_file, 
		  //placer_opts.place_cost_type, 
		  placer_opts.place_freq, 
		  placer_opts.num_regions, 
		  aspect_ratio, user_sized, 
		  router_opts.route_type, 
		  &det_routing_arch, 
		  &segment_inf,
		  &via_inf,
		  &timing_inf, 
		  &subblock_data, 
		  &chan_width_dist,
		  // New arguments.
		  // In case we have to read a placement and do only
		  // routing, "init_arch" will also contain the 
		  // read_and_set_nx_ny_from_place_file ()
		  // in order to account for possible different nx,ny
		  // as dictated by the placement file.
		  place_file );

  // -----------------------------------------------------------------------
  // -----------------------------------------------------------------------

  /*---
  if (full_stats == TRUE) 
    print_lambda ();
    ---*/

#ifdef DEBUG 
  fg3->print_netlist ("dbg_net.echo", net_file, subblock_data);
  fg3->print_arch (arch_file, router_opts.route_type, det_routing_arch,
		   segment_inf, via_inf, 
		   timing_inf, subblock_data, chan_width_dist);
#endif


  // -----------------------------------------------------------------------
  // -----------------------------------------------------------------------
  // Read delay lut using fg3->read_LUT_delay_file().  This is done once 
  // for all.  I am not doing in this version because I generate
  // all delay tables and more than that as in VPR, locally for every 
  // circuit... 
  // Build the timing graph - type _02
  tt_hg = fg3->create_timing_tt_hgraph_02( subblock_data, timing_inf );
  //tt_hg->Print_tt_hgraph( "dbg_tt_graph1.echo" );
  tt_hg->Compute_Static_DelayForPOs();
  tt_hg->ComputeRequiredTimesFromPOsToPIs();
  //tt_hg->SortSlacksAndPrintThemInFileInNonIncreasingOrder("dbg_slacks.echo");
  cout<<"\n Zero Delay = "<<tt_hg->Get_Static_Delay();

  // -----------------------------------------------------------------------
  // -----------------------------------------------------------------------
  // k-most critical paths stuff
  //---------------------------------------------------
  // Some pre-stuff for working with k most critical paths
  //---------------------------------------------------
  int K = fg3->K; // Read from among command-line.
  tt_hg->K = fg3->K; // Set it also in timing graph.
  ll * temp_paths=(ll *)new ll[K];
  kmp->setPaths(temp_paths);
  double * temp_delays=(double*) new double[K];
  kmp->setDelays(temp_delays);
  int * temp_k_j=(int*) new int[K];
  kmp->setK_j(temp_k_j);
  int * temp_k_j_edge=(int*) new int[K];
  kmp->setK_j_edge(temp_k_j_edge); // reflects only edges cut on path
  //int * temp_k_j_hm=(int*) new int[K];
  //kmp->setK_j_hm(temp_k_j_hm);
  ll * temp_no_of_verts_in_frag_i_in_path_j=(ll *)new ll[K];
  ll * temp_verts_in_frag_i_in_path_j=(ll *)new ll[K];
  ll_double * temp_edgeweights_in_frag_i_in_path_j = 
    (ll_double *)new ll_double[K];
  kmp->setNo_of_verts_in_frag_i_in_path_j(temp_no_of_verts_in_frag_i_in_path_j);
  kmp->setVerts_in_frag_i_in_path_j(temp_verts_in_frag_i_in_path_j);
  kmp->setEdgeweights_in_frag_i_in_path_j(temp_edgeweights_in_frag_i_in_path_j);
  //---------------------------------------------------	
  // Pre-requisites which are run only once for all.
  tt_hg->Add_Source_S_Sink_T();	
  tt_hg->Compute_max_delay_to_sink_FromPOsToPIs();
  tt_hg->Build_ordFin_ordFout();
  tt_hg->Sort_ordFin_ordFout();
  // 1st path enumeration.
  tt_hg->PathEnumeration( "dbg_kpaths.echo" );

  // -----------------------------------------------------------------------
  // -----------------------------------------------------------------------

  /*---
  if (operation == TIMING_ANALYSIS_ONLY) 
    {  // Just run the timing analyzer
      do_constant_net_delay_timing_analysis (timing_inf, subblock_data,
					     constant_net_delay);
      free_subblock_data (&subblock_data);
      exit (0);
    }

  set_graphics_state (show_graphics, gr_automode, router_opts.route_type); 
  if (show_graphics) 
    {
      // Get graphics going
      init_graphics("IPR:  trI d Place & Route for 3D FPGAs");  
      alloc_draw_structs ();
    }
    ---*/
  
  fflush (stdout);

  
  // -----------------------------------------------------------------------
  // -----------------------------------------------------------------------
  // Place  &  Route

  fg3->place_and_route (
	 operation, placer_opts, 
	 place_file, net_file, arch_file,
	 route_file, full_stats, verify_binary_search, 
	 annealing_sched, router_opts,
	 det_routing_arch, segment_inf, via_inf, 
	 timing_inf, &subblock_data, 
	 chan_width_dist);
  fg3->dump_out_final_data(net_file);
  // -----------------------------------------------------------------------
  /*---
  if (show_graphics) 
    close_graphics(); // Close down X Display
    ---*/

  // Prind place file in vpr format 
  //fg3->print_place(place_file, net_file, arch_file);
  // -----------------------------------------------------------------------



  printf("\nDone TPR! \n");
  exit(0);
}//eom
// ======================================================================= //
