PRGUI is a graphical user interface for TPR. It was developed in Java by 
James Foltz under the NSF REU summer research program at the Univ of 
Minnesota in the summer of 2004.

The project report is in file PRGUI-projectReport and the user's manual is 
in the Help under the Help menu in the program.

