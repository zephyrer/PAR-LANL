#include <stdio.h>
main(int argc,char **argv)
{
  int i,j,h,w;

  if (argc == 1){
    h=w=10;
  } else if (argc==2){
    sscanf(argv[1],"%d",&w);h=w;
  } else if (argc==3){
    sscanf(argv[1],"%d",&h);
    sscanf(argv[2],"%d",&w);
  } else {
    fprintf(stderr,"Usage: %s [dim1 [dim2]]",argv[0]);
    return(1);
  }

  printf("M 1 40\n");
  for (i=0;i<h;i++){
    for (j=0;j<w;j++){
      printf("l %d %d gas.bin\n",i*31,j*24);
    }
  }
  printf("M 1 7\n");
  printf("vd c 12 0 dw 1 1\n");
  printf("vd p 6 0 dw 1 1\n");
  printf("vd n 0 18 dn 1 1\n");
}
