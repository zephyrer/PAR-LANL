#include "par.h"
// Check for malloc() failure
static int memuse=0;
mcheck(char * a)
{
  int i;

  if (a==NULL){
    fprintf(stderr,"Out of memory :(\n");
    exit(1);
  }
/***
  i=malloc_usable_size(a);
  memuse+=i;
  printf("Allocated space: %d  total=%d\n",i,memuse);
  if (memuse==244356) {
    i=i+2;
  }
***/
  return;
}

/*
 * match() accepts a base string, a pattern, and a location, and checks
 * to see if the patternm appears in the base string beginning at the
 * given location. It returns 1 if the pattern matches, otherwise 0
 */

int match(base,pattern,startloc)
char *base,*pattern;
int startloc;
{
  int i;
  for (i=0;i<strlen(pattern);i++){
    if (pattern[i]!=base[startloc]) return(0);
    if (base[startloc++]=='\0') return(0);
  }
  return(1); /* Matches! */
}

/*
 * find() searches for a pattern in a base string, beginning
 * at a given location. It returns the location, or (-1)
 * if the pattern is not found.
 */
int find(base,pattern,startloc)
char *base,*pattern;
int startloc;
{
  int i;
  for (i=startloc;i<strlen(base);i++)
    if (1==match(base,pattern,i)) return(i);
  return(-1);
}

char substr_return[2048]; /* but play nice... */
/* substr() - returns a substring */
/* DO NOT NEST! */

char *substr(string,start,len,end) /* Inclusive! */
/* If LEN=0, then END gives ending position */
char *string;
int start,len,end;
{
  int i,j;
  if (len != 0) end=start+len-1; /* We'll use END regardless */
  j=0; /* output location */
  for (i=start;i<=end;i++){
    substr_return[j++]=string[i];
  }
  substr_return[j]='\0';
  return(substr_return);
}

/* Circuit initialization */
init_circuit(struct circuit *circuit)
{
  circuit->components=malloc(sizeof(struct instance));
  circuit->components->next=(struct instance*) NULL;
  circuit->nets=malloc(sizeof(struct netlist));
  circuit->nets->next=(struct netlist*) NULL;
  circuit->library=malloc(sizeof(struct libentry));
  circuit->library->next=(struct libentry*)NULL;
  strcpy(circuit->libout,"");
}

static int raninit=0;
/* Return an integer between 0 and max-1 */
rando(int max)
{
  if (raninit==0) srand(827771);
  raninit=1;

  if (max <= 1) return(0);
  return(rand() % max);
}

/* Here we have a set of routines for traversing the various data structures */

/* get_net() initializes things for a subsequent get_node().
 * get_net(1) resets to the first net; get_net(0) does next net
 * return value of 0=OK, 1=no more nets
 */
static struct netlist *nl;
static struct netnode *nn;
get_net(struct circuit *circuit,int init)
{
  if (init==1){ /* re-init */
    nl=circuit->nets;
    return(0);
  }
  if (nl->next==(struct netlist*) NULL) return(1); /* No more networks */
  nl=nl->next; /* Next net in linked list */
  nn=nl->net; /* Start of this net */
  return(0);
}

int count_nodes()   /* counts nodes in current net. Call at beginning of net */
{
  int i;
  i=0;
  while (nn->next != NULL){
    ++i;
    nn=nn->next;
  }

/* Now reset nn */
  nn=nl->net;
  return(i);
}

struct netnode *get_node()
{
  if (nn->next == (struct netnode*) NULL) return(nn->next);
  nn=nn->next;
  return(nn);
}

insert_at_head_of_current_net(struct netnode *node)
{
  node->next=(nl->net)->next;
  (nl->net)->next=node;
}

/* We may want to return to the start of this current net... */
get_node_reset()
{
  nn=nl->net; /* Start of this (same) net */
}

/* Routines to retrieve instances. First, a specific instance */
struct instance *get_names_instance(struct circuit *circuit,char *name)
{
  struct instance *temp;
  temp=circuit->components;
  while (temp->next != NULL){
    temp=temp->next;
    if (0==strcmp(temp->name,name)) return(temp);
  }
  return(NULL);
}

/* Count instances */
count_instances(struct circuit *circuit)
{
  int i;
  struct instance *instance;

  i=0;
  instance=next_instance(circuit,1);
  while (NULL != (instance=next_instance(circuit,0))) ++i;
  return(i);
}

/* Find UL [r,c] of an instance */
find_instance_rc(struct circuit *circuit, struct instance *instance,
                  int *rout, int *cout,int *rotation)
{
  int r,c;
  struct instance *in;
  for (r=0;r<circuit->height;r++){
    for (c=0;c<circuit->width;c++){
      if (getcell(circuit,r,c)->instance == instance){
        *rout=r;*cout=c;*rotation=instance->rotation;
        return(0);
      }
    }
  }
  return(1);
}

/* Retrieve nth instance */
struct instance *get_indexed_instance(struct circuit *circuit,int ind)
{
  int i;
  struct instance *instance;

  i=0;
  instance=next_instance(circuit,1);
  while (NULL != (instance=next_instance(circuit,0))){
    if (i==ind) return(instance);
    ++i;
  }
  return(NULL);
}

/* Successively return instances */
static struct instance *nextinst;
struct instance *next_instance(struct circuit *circuit,int init)
{
  if (init==1){
    nextinst=circuit->components;
    return(NULL); /* Unused */
  }
  if (nextinst->next == NULL) return(NULL);
  nextinst=nextinst->next;
  return(nextinst);
}

sort_instances(struct circuit *circuit)
{
// want most-constrained instances to appear first

  struct instance *inst,*previnst,*tmpinst;
  int mincon,numinst,rem_inst,i,j,c1,c2;

  numinst=count_instances(circuit); // total # to do
  for (i=0;i<numinst;i++){  // bubble-sort numinst times
    inst=circuit->components;
    while (inst->next->next != NULL){ // move up and swap
      previnst=inst;  // points to node we're about to examine
      inst=inst->next;

// find tightest constraint on this instance
      c1=inst->lrrowc-inst->ulrowc;
      if ((inst->lrcolc-inst->ulcolc) < c1) c1=inst->lrcolc-inst->ulcolc;

// and on next instance
      c2=inst->next->lrrowc-inst->next->ulrowc;
      if ((inst->next->lrcolc-inst->next->ulcolc) < c2)
        c2=inst->next->lrcolc-inst->next->ulcolc;

// now see if this instance is less constrained than next one
      if (c1 > c2){ // need to swap these
        tmpinst=inst->next;
        inst->next=inst->next->next;
        tmpinst->next=inst;
        previnst->next=tmpinst;  // swapped!
        inst=tmpinst; // since that's now in the spot "inst" was in
      } // end of swap
    } // end of this pass
  } //end of all passes
}

struct cell *getcell(struct circuit *circuit,int row,int col)
{
  return(&circuit->matrix[row*circuit->width + col]);
}

static FILE *gridfp;
grid_output_init(char *fname)
{
  char buffer[1024];
  sprintf(buffer,"%s.grd",fname);
  gridfp=fopen(buffer,"w");
}

grid_output(char *s)
{
  fprintf(gridfp,"%s",s);
  fflush(gridfp);
}

/* Cheap ASCII dump of matrix */
display_placement(struct circuit *circuit,int key)
/* key=0 means just print placement
 * key=1 means show the legend
 * key=2 means create .GRD file
 */
{
  char m[76][76]; // Should be Dynamic!!! but it's just a cheesy ascii anyway
  int h,w,r,c,i,j,k,l,ulcol,ulrow,rotation,realwidth,realheight,r2,c2,
      r1,c1;
  char ch;
  struct instance *instance;
  struct cell *cell;
  char buffer[1024],entry[1024];
  struct libentry *le;
int count;

  h=circuit->height;w=circuit->width;
  h=(h>76)?76:h;
  w=(w>76)?76:w; // Only display a reasonably-displayable region

count=0;
// DON'T USE KEY=2 ANYMORE...USE write_grd()
  if (key==2){ /* Create .GRD file */
    grid_output_init(circuit->name);
    sprintf(buffer,"GRID_FILE\n%d\n%d\n",h,w);
    grid_output(buffer); /* Output header */
    for (r=0;r<h;r++){
      for (c=0;c<w;c++){
        cell=getcell(circuit,r,c);
        if ((cell->instance == NULL) ||
            (cell->instance == EDGEINST)) grid_output("\n");
        else {
          le=cell->instance->libdef;
          ulrow=cell->instance->ulrow;
          ulcol=cell->instance->ulcol;
          rotation=(cell->instance->rotation)%4;
/* When we're indexing cell[r,c], we need to know the width of this
 * circuit. Normally this is le->w, but if the circuit is rotated
 * 90 or 270 degrees, it will be le->h
 */
          realwidth=((rotation==1)||(rotation==3))?le->h:le->w;
          realheight=((rotation==1)||(rotation==3))?le->w:le->h;

/* Find loc within component */
          r1=r-ulrow;c1=c-ulcol;
/* also need to adjust row and col based on rotation */
          switch (rotation){
            case 0:r2=r1;c2=c1;break;
            case 3:r2=c1;c2=(le->w-1)-r1;break;
            case 2:r2=(le->h-1)-r1;c2=(le->w-1)-c1;break;
            case 1:r2=(le->h-1)-c1;c2=r1;break;
          }
/*printf("%s rot=%d:[r,c]=[%d,%d]  [r1,c1]=[%d,%d]  [r2,c2]=[%d,%d]\n",
cell->instance->basename,rotation,r,c,r1,c1,r2,c2);*/
          strcpy(entry,le->cells[(r2*realwidth)+c2]);
          rotate_entry(entry,rotation);

// See if this is a route or a component
          grid_output(entry);grid_output("\n");
/*printf("%d,%d:%s rot=%d\n",r,c,entry,rotation);*/
        }
      }
    }
    return;
  }

/* Initialize output array */
  for (r=0;r<h;r++){for(c=0;c<w;c++){m[r%76][c%76]='.';}}

/* Scan matrix[] and save info */
  ch='a';
  instance=next_instance(circuit,1);

  while (NULL != (instance=next_instance(circuit,0))){
/*printf("%d [%d,%d]\n",count,instance->ulrow,instance->ulcol);*/
    for (r=0;r<h;r++){
      for (c=0;c<w;c++){
        cell=getcell(circuit,r,c);
        if (cell->instance == instance) m[r%76][c%76]=ch;
        if (cell->instance == instance) ++count;
      }
    }
    if (++ch > 'z') ch='0'; // next character
  }

/* Now assign edge ports (use numbers) */
   for (i=0;i<circuit->numio;i++){
     r=circuit->placedrow[i];
     c=circuit->placedcol[i];
     r=(r>75)?75:r;c=(c>75)?75:c; // Place edge ports on sub-edge
     if ((r >= 0) && (c >= 0)) m[r%76][c%76]='0'+i;
   }

  if (key==1){
/* Output the key */
    ch='a';
    instance=next_instance(circuit,1);
    while (NULL != (instance=next_instance(circuit,0))){
      printf("%c:%s(%s)\n",ch,instance->name,instance->basename);
      if (++ch > 'z') ch='0';
    }

// Show edgeports
    for (i=0;i<circuit->numio;i++){
      printf("%c:%s[%s]\n",'0'+i,circuit->io[i],
                           (circuit->iodir[i]==0)?"input":"output");
    }
    
    printf("\n");
    return;
  }

/* Now dump the output */
  reset_page();
  for (r=0;r<h;r++){
    for (c=0;c<w;c++){
      printf("%c",m[r%76][c%76]);
    }
    printf("\n");
  }
  printf("\n");
}

clear_page()
{
  if (fancy==1) printf("%c[2J",27);
}

reset_page()
{
  if (fancy==1) printf("%c[1;1H",27);
}

iabs(int x)
{
  return((x<0)?(-x):x);
}

rotate_entry(char *entry,int rotation)
{
/* Parse standard loader .GRD file entry, and bump the rotations */
  char output[1024],start[1024],end[32],temp[1024];
  int more,look,i,r;


  if (entry[0]=='\0') return; // no op!

  strcpy(output,""); /* Build this output string */
  more=1;
  look=0;
  while (more==1){
    i=find(entry,"",look); /* Find libname terminator */
    i=find(entry,"",i+1); /* Find cellname terminator */
    strcat(output,substr(entry,look,0,i)); /* lib^Bcell^B */
    look=find(entry,"",i+1); /* Is there a ^B after rot? */
    if (look==-1){ /* No */
      strcpy(temp,substr(entry,i+1,0,strlen(entry)-1)); /* Rotation */
      more=0; /* End of parse */
    } else { /* ^B marks start of next member of composite cell */
      strcpy(temp,substr(entry,i+1,0,look-1)); /* Rotation */
      ++look; /* Look after ^B */
    }
/* Now bump up temp */
    sscanf(temp,"%d",&r);
    r=(r+rotation)%4;
    sprintf(temp,"%d",r);

    strcat(output,temp);
    if (more==1) strcat(output,"");
  }
  strcpy(entry,output); /* Save the output */
  return;
}

// final .GRD file creator
// Use computed statistics to write .LIB statistics
// Also, update statistics block with cell utilization numbers

write_grd(struct circuit *circuit, struct matrix *matrix,
          struct stat_block *stats)
{
// We'll also create the output library here if applicable
  int h,w,r,c,i,j,k,l,ulcol,ulrow,rotation,realwidth,realheight,r2,c2,
      r1,c1,ind,libout;
  char ch;
  struct instance *instance;
  struct cell *cell;
  char buffer[1024],entry[1024];
  struct libentry *le;
  struct finalcells fc;
  FILE *libfp,*CUSEFP; // CUSEFP is used to record cell utilization data

  h=circuit->height;w=circuit->width;
  stats->cells_total=h*w; // that was easy...
  stats->cells_route=stats->cells_comp=0;

  sprintf(buffer,"%s.cuse",circuit->name);
  CUSEFP=fopen(buffer,"w");
  if (CUSEFP < (FILE *) NULL)
    fprintf(stderr,"ERROR: Cannot create CUSE output file <%s>\n",buffer);

  if (circuit->libout[0] != '\0'){ // Output placed circuit to a library too
    libout=1;
    flush_lib(circuit->libout,circuit->name); // remove this component
    libfp=fopen(circuit->libout,"a");
    fprintf(libfp,"------------\n");
    fprintf(libfp,"%s\n",circuit->name);
    fprintf(libfp,"Autogen by PAR\n");
    fprintf(libfp,"%d\n",circuit->height);
    fprintf(libfp,"%d\n",circuit->width);
  } else libout=0;

  sprintf(buffer,"GRID_FILE\n%d\n%d\n",h,w);
  grid_output_init(circuit->name);
  grid_output(buffer); /* Output header */
  for (r=0;r<h;r++){
    for (c=0;c<w;c++){
// check for cell instance. Also check matrix->finalcells
      fc=matrix->finalcells[r*w+c];
      cell=getcell(circuit,r,c);
      if ((cell->instance != NULL) &&
          (cell->instance != EDGEINST)){ // output instance
        le=cell->instance->libdef;
        ulrow=cell->instance->ulrow;
        ulcol=cell->instance->ulcol;
        rotation=(cell->instance->rotation)%4;
/* When we're indexing cell[r,c], we need to know the width of this
 * circuit. Normally this is le->w, but if the circuit is rotated
 * 90 or 270 degrees, it will be le->h
 */
        realwidth=((rotation==1)||(rotation==3))?le->h:le->w;
        realheight=((rotation==1)||(rotation==3))?le->w:le->h;
        realwidth=le->w;
        realheight=le->h;

/* Find loc within component */
        r1=r-ulrow;c1=c-ulcol;
/* also need to adjust row and col based on rotation */
        switch (rotation){
/***
          case 0:r2=r1;c2=c1;break;
          case 1:r2=c1;c2=(le->w-1)-r1;break;
          case 2:r2=(le->h-1)-r1;c2=(le->w-1)-c1;break;
          case 3:r2=(le->h-1)-c1;c2=r1;break;
***/
          case 0:r2=r1;c2=c1;break;
          case 1:c2=r1;r2=(le->h-1)-c1;break;
          case 2:r2=(le->h-1)-r1;c2=(le->w-1)-c1;break;
          case 3:c2=(le->w-1)-r1;r2=c1;break;
/* Remember, le->h,w are from *un-rotated* component */
        }
#ifdef DEBUG
        printf("%s rot=%d:[r,c]=[%d,%d]  [r1,c1]=[%d,%d]  [r2,c2]=[%d,%d] realwidth=%d\n",
               cell->instance->basename,rotation,r,c,r1,c1,r2,c2,realwidth);
#endif
        strcpy(entry,le->cells[(r2*realwidth)+c2]);
        rotate_entry(entry,rotation);
        grid_output(entry);
        if (libout==1) fprintf(libfp,"%s",entry);
/*printf("%d,%d:%s rot=%d\n",r,c,entry,rotation);*/

// Now see if this is a route, component, or blank
        if (match(entry,"ROUTER.LIB",0)==1){
          ++stats->cells_route;
          fprintf(CUSEFP,"route:[%d,%d]\n",r,c);
        } else if (entry[0]!='\0'){
          ++stats->cells_comp;
          fprintf(CUSEFP,"comp:[%d,%d]\n",r,c);
        }

      } else { // output cell routings
        output_cell_route(r,c,&fc,libout,libfp,stats,CUSEFP);
      }
     grid_output("\n");
     if (libout==1) fprintf(libfp,"\n");
    }
  } // matrix has been swept
  fclose(CUSEFP);

  if (libout==1){
// Output I/O information
    fprintf(libfp,"%d\n",circuit->numio); // # of I/O
    for (i=0;i<circuit->numio;i++){
      fprintf(libfp,"%s %d %d d%c%c\n",
              circuit->io[i],
              circuit->placedrow[i],
              circuit->placedcol[i],
              circuit->placedside[i],
              (0==circuit->iodir[i])?'i':'o');
    }

// and output routing statistics
      fprintf(libfp,"%d %d %d %d %d %d %d\n",
              stats->Mlen_str, stats->Mlen_turn, stats->Mlen_sum,
              stats->Glen_str, stats->Glen_turn, stats->Glen_sum,
              stats->Glen_str_n, stats->Glen_turn_n, stats->Glen_sum_n);
    fclose(libfp);
  }
  return;
}

flush_lib(char *lib, char *comp)
{
  char temp[1280],buffer[4096],buf2[4096];
  FILE *fpin,*fpout;
  int skip; // set when we're skipping a component
  int h,w,numio,i;

// Copy current library to .bck file
  fpin=fopen(lib,"r");
  if (fpin <= (FILE *)NULL) return; // no such input file

  sprintf(temp,"%s.bck",lib);
  fpout=fopen(temp,"w");
  while (NULL != fgets(buffer,4096,fpin)){
    fprintf(fpout,"%s",buffer);
  }
  fclose(fpin);
  fclose(fpout);

// Now copy from .bck to current library, but exclude named component
  fpin=fopen(temp,"r");
  fpout=fopen(lib,"w");
  while (NULL != fgets(buffer,4096,fpin)){
    fgets(buf2,4096,fpin); // read name
    buf2[-1+strlen(buf2)]='\0'; // remove \n
    skip=(strcmp(buf2,comp)==0)?1:0; // skip=1 means don't output
    if (skip==0){
      fprintf(fpout,"%s",buffer); // Comment
      fprintf(fpout,"%s\n",buf2); // name  -  restore \n too!
    }
    fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // desc
    fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // rows
    sscanf(buffer,"%d",&h);
    fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // cols
    sscanf(buffer,"%d",&w);
    for (i=0;i<h*w;i++){
      fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // cells
    }
    fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // cols
    sscanf(buffer,"%d",&numio);
    for (i=0;i<numio;i++){
      fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // io
    }
    fgets(buffer,4096,fpin);if (skip==0) fprintf(fpout,"%s",buffer); // Stats
  } // end of this component's deinition in library
  fclose(fpin);fclose(fpout);
}

// grid_output a cell's routes
output_cell_route(int r,int c,struct finalcells *fc,int libout,FILE *fp,
                  struct stat_block *stats,FILE *CUSEFP)
{
//  if (fc->nn == '1') printf("[%d,%d] n->n\n",r,c);
//  if (fc->ns == '1') printf("[%d,%d] n->s\n",r,c);
//  if (fc->nw == '1') printf("[%d,%d] n->w\n",r,c);
//  if (fc->ne == '1') printf("[%d,%d] n->e\n",r,c);
//  if (fc->sn == '1') printf("[%d,%d] s->n\n",r,c);
//  if (fc->ss == '1') printf("[%d,%d] s->s\n",r,c);
//  if (fc->sw == '1') printf("[%d,%d] s->w\n",r,c);
//  if (fc->se == '1') printf("[%d,%d] s->e\n",r,c);
//  if (fc->wn == '1') printf("[%d,%d] w->n\n",r,c);
//  if (fc->ws == '1') printf("[%d,%d] w->s\n",r,c);
//  if (fc->ww == '1') printf("[%d,%d] w->w\n",r,c);
//  if (fc->we == '1') printf("[%d,%d] w->e\n",r,c);
//  if (fc->en == '1') printf("[%d,%d] e->n\n",r,c);
//  if (fc->es == '1') printf("[%d,%d] e->s\n",r,c);
//  if (fc->ew == '1') printf("[%d,%d] e->w\n",r,c);
//  if (fc->ee == '1') printf("[%d,%d] e->e\n",r,c);

  start_cell_output();
  if (fc->nn == '1') cell_output("N->N","0",libout,fp);
  if (fc->ns == '1') cell_output("N->S","0",libout,fp);
  if (fc->nw == '1') cell_output("N->W","0",libout,fp);
  if (fc->ne == '1') cell_output("N->E","0",libout,fp);
  if (fc->sn == '1') cell_output("N->S","2",libout,fp);
  if (fc->ss == '1') cell_output("N->N","2",libout,fp);
  if (fc->sw == '1') cell_output("N->E","2",libout,fp);
  if (fc->se == '1') cell_output("N->W","2",libout,fp);
  if (fc->wn == '1') cell_output("N->E","3",libout,fp);
  if (fc->ws == '1') cell_output("N->W","3",libout,fp);
  if (fc->ww == '1') cell_output("N->N","3",libout,fp);
  if (fc->we == '1') cell_output("N->S","3",libout,fp);
  if (fc->en == '1') cell_output("N->W","1",libout,fp);
  if (fc->es == '1') cell_output("N->E","1",libout,fp);
  if (fc->ew == '1') cell_output("N->S","1",libout,fp);
  if (fc->ee == '1') cell_output("N->N","1",libout,fp);

  if ((fc->nn=='1') || (fc->nn == '1') || (fc->ns == '1') || 
      (fc->nw == '1') || (fc->ne == '1') || (fc->sn == '1') || 
      (fc->ss == '1') || (fc->sw == '1') || (fc->se == '1') || 
      (fc->wn == '1') || (fc->ws == '1') || (fc->ww == '1') || 
      (fc->we == '1') || (fc->en == '1') || (fc->es == '1') || 
      (fc->ew == '1') || (fc->ee == '1')){
    ++stats->cells_route;
    fprintf(CUSEFP,"route:[%d,%d]\n",r,c);
  }
}

// cell output routines...
static int first_co;
start_cell_output()
{
  first_co=1;
}

cell_output(char *s1,char *s2,int libout,FILE *fp)
{
  char buffer[120];

  if (first_co==0){
    sprintf(buffer,"%c",2);grid_output(buffer);
    if (libout==1) fprintf(fp,"%s",buffer);
  }
  first_co=0;
  sprintf(buffer,"ROUTER.LIB%c%s%c%s",2,s1,2,s2);
  grid_output(buffer);
  if (libout==1) fprintf(fp,"%s",buffer);
}
