import visitor.*;
import syntaxtree.*;
import java.io.*;

/** This class provides the basic mechanism to parse an EDIF file.
 */
public class EdifParser {


  public static void main(String[] args) {
    if (args.length == 1) {
      new EdifParser(args[0], args[0]+".out",args[0]+".txt");
    } else {
      System.err.println("Usage: EdifParser basename");
      System.exit(1);
    }
  }

  /** The default target.  Initial value of "Virtex" */
  public EdifParser(String filename, String outFilename, String txtname) {
    EdifVisitor myVisitor = new EdifVisitor(outFilename,txtname);
    EdifParserCore parser;
    try {
      parser = new EdifParserCore(new FileInputStream(filename));
      Node root = parser.edif();
      root.accept(myVisitor);
    } catch (ParseException e) {
      System.out.println("Encountered errors during parse.");
      System.out.println(e);
    } catch (FileNotFoundException e) {
      System.out.println("File " + filename + " not found.");
    }
  }

}
