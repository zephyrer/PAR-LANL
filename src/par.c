#include "par.h"

main(argc,argv)
int argc;
char **argv;
/* Usage: main [input] */
{
  struct circuit *circuit; /* Main circuit! */
  struct matrix *matrix; // used for routing
  struct stat_block *stats;
  FILE *fp;
  char buffer[30000];
  int errcnt=0; /* # of parse etc. errors */
  int i,j;

  if (argc != 2){
    fprintf(stderr,"Usage: %s inputfile\n",argv[0]);
    exit(1);
  }

  circuit=malloc(sizeof(struct circuit));
  init_circuit(circuit);

  matrix=malloc(sizeof(struct matrix));
  mcheck((char *)matrix);

/*
 * Read the netlist description
 */
  fp=fopen(argv[1],"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Cannot open input file %s\n",argv[1]);
    exit(1);
  }

  while (0 == get_line(fp,buffer)){
    /*printf("<%s>\n",buffer);*/
    if (0 != parse_line(circuit,buffer)){ /* Load structures accordingly */
      fprintf(stderr,"ERROR: Cannot parse <%s>\n",buffer);
      ++errcnt;
    }
/* Structures have been loaded - continue! */
  }
  fclose(fp);
  if (errcnt > 0){
    fprintf(stderr,"Total of %d error%s\n",errcnt,(errcnt==1)?"":"s");
    exit(1);
  } else fprintf(stderr,"\nNo errors detected!\n\n");

  dump_circuit(circuit); /* Display results so far */

/* Run the placer */
  place(circuit);

/* Write final circuit placement into a LOADER .grd file */
  //display_placement(circuit,2);

/* Route the circuit */
  stats=route(circuit,matrix);

  write_grd(circuit,matrix,stats);

// Show usage stats

  printf("# of paths: %d\n",stats->Glen_sum_n);
  if (stats->Glen_sum_n > 0)
    printf("Average lengths: Straight=%d  Turn=%d  Sum=%d\n",
            stats->Glen_str/stats->Glen_str_n,
            stats->Glen_turn/stats->Glen_turn_n,
            stats->Glen_sum/stats->Glen_sum_n);
  printf("Max lengths: Straight=%d  Turn=%d  Sum=%d\n",
    stats->Mlen_str,stats->Mlen_turn,stats->Mlen_sum);

// output final .GRD file
  printf("Cell utilization: total=%d routing=%d components=%d\n",
          stats->cells_total,stats->cells_route,stats->cells_comp);

}
