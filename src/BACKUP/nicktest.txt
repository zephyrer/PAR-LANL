.define testcircuit(16,8,in:in)
.library "testlib.lib"
.instance ff1=dflipflop()
.instance ff2=random()
.instance inst72=and2()
.instance and1=and3()
.instance and2=and3()
.instance and3=and3()
.instance and4=and3()
.instance and5=and3()
.net (inst72.b,ff1.q)
.net (and1.a,and2.a,and3.a)
.net (and1.a,and4.a)
.net (and1.a,and5.a)
.net (and1.x,and2.b,and3.b)
.net (and2.b,and4.b)
.net (and3.b,and5.b)
