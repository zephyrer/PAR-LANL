#include "par.h"
/* Read a single "line" from the input file */
int get_line(FILE *fp, char *buf)
{
  int i,j,comment,quote;
  char buffer[1024];

  strcpy(buffer,"");
  quote=0; /* Set inside " " */
  while (strlen(buffer) == 0){
    if (NULL == fgets(buf,1024,fp)) return(1);
    comment=0; /* Set once we see ";" */
    j=0; /* Output location */
    for (i=0;i<strlen(buf);i++){
      if (buf[i]==';') comment=1;
      if (buf[i]=='"') quote=1-quote;
      if ((comment==0) && ((quote==1)||(!isspace(buf[i])))) buffer[j++]=buf[i];
    }
    buffer[j]='\0';
    strcpy(buf,buffer);
  } /* Loop until we actually read something */
  return(0);
}
