#include "par.h"
#include <math.h>


route(struct circuit *circuit)
{
  int s;

printf("Beginning routing\n");

  if (0 != (s=init_route_matrix(circuit))) return(s);

  return(0);
}

/*
 * Initialize the routing-related aspects of the cell[][] array.
 * This is only done once, since it loads static information into the cells
 */
init_route_matrix(struct circuit *circuit)
{
  int ind,i,j,k,row,col,done;
  struct cell *cell;
  struct netnode *netnode,*sourcenode,*prevnode;
  struct iodef *iodef;

  for (row=0;row<circuit->height;row++){
    for (col=0;col<circuit->width;col++){
      cell=getcell(circuit,row,col);
      if (cell->instance==NULL) i=0; /* Available, initial cost=0 */
      else i=(-1); /* Special flag - input is never available */
      cell->hcost_N=cell->hcost_S=cell->hcost_W=cell->hcost_E=
      cell->pcost_N=cell->pcost_S=cell->pcost_W=cell->pcost_E=i;

      cell->netN=cell->netS=cell->netW=cell->netE=NULL; /* Set below... */
    }
  }

/* Next we set the net-related elements */
/* First, scan each net, and move the source node to the head of the net list.
 * Then, sweep each net again, and for each sink node, find the cell and side
 * of the input, and store the SOURCE netnode in the apropriate netX
 */

  get_net(circuit,1); /* Initialize the net retriever */

  while (0==get_net(circuit,0)){
    sourcenode=prevnode=NULL;
/* Identify the source of this net */
    done=0; /* Set once we find a source node */
    while ((done==0) && (NULL != (netnode=get_node()))){
      i=netnode->ioindex; /* Connection point to net */
      iodef=&netnode->c->libdef->io[i]; /* IO definition for connection pt */
      /*printf("%s:%s:%s ",netnode->c->name,iodef->iobasename,iodef->side);*/
      if (iodef->side[2]=='o'){ /* Source! */
        sourcenode=netnode;
/* Is this the first node? */
        if (prevnode!=NULL){ /* move this node to front of net */
          prevnode->next=sourcenode->next;
          insert_at_head_of_current_net(sourcenode);
        }
        done=1; /* Exit this loop */
      } /* End of source node processing */
      prevnode=netnode; /* Remember next node's predecessor */
    } /* End of net */

/* Now re-display the net */
    get_node_reset();
    while (NULL != (netnode=get_node())){
      i=netnode->ioindex; /* Connection point to net */
      iodef=&netnode->c->libdef->io[i]; /* IO definition for connection pt */
      printf("%s:%s:%s ",netnode->c->name,iodef->iobasename,iodef->side);
    }
    printf("\n");

  } /* End of all nets */
}
