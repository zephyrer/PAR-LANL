.define testckt(20,12,in:in,d:in,ld:in,q:out)
.library "testlib.lib"
.locate in,w
.locate d,w,12,15
.locate ld,w,2
.locate q,n,2,6

.instance seter=DFF1()
.instance clearer=DFF1()

.net(clearer.clr,seter.set)
.net(q,seter.set)
.net(seter.clr,ld)
.net (clearer.clr,ld)
