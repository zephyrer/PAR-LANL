#include "par.h"
/*
 * Individual line parser. Legal statements are:
 *
 * .define newname(height,width,v1:in,v2:in,v3:out,...)
 * which begins the definition of a new circuit, and specifies its inputs
 * and output. height and width are the dims of the encompasing matrix
 *
 * .locate ioname[,n|s|w|e[,cellloc[,cellloc]]]
 * Indicates that an I/O port from the .define statement should be
 * located along the edge of the circuit. Without any arguments, this
 * locates the port along any edge. Specifying a side forces the
 * location to be on that side. A single cellloc also forces the
 * specific cell location on that edge. Two cellloc arguments specify
 * a permitted range of cell locations on that edge.
 *
 * .library "libname" specifies the name of a component library to load
 * note that libname is case- and space-sensitive!
 *
 * .libout "libname" names a library to be created or modified to
 *   include the netlist that is next defined
 *
 * .instance name=basename(ioname=baseioname,ioname=baseionane,...)
 *    plusoptional placement constraints:
 *    @[r,c]
 *    @[r,c]rot
 *    @[r,c]-[r,c]
 *    @[r,c]-[r,c]rot
 * which defines an instantiation of some base cell or subcircuit. This
 * syntax allows the optional renaming of inputs and outputs.
 *
 * NOTE: Ridgedly-constrained components are placed WITHOUT ANY
 * INTEGRITY CHECKING! so make sure they don't overlap with each other,
 * exceed the matrix dimensions, and so on!
 *
 * .net name([instance1.]io1,[instance2.]io2,[instance3.]io3,...)
 * which defines an (optionally-named) net. A net connects two or more nodes,
 * with each node specified by an instance/io pair (where the io is either:
 * the baseioname name in the instance's base; or, if the ioname=baseioname
 * structure was used to rename an io, the ioname assigned to a baseioname).
 * If instance is missing, the io is assumed to be a top-level circuit
 * I/O, defined in the original .define statement
 *
 */
int parse_line(circuit,buffer)
struct circuit *circuit;
char *buffer;
{
  char buf[1024],temp[1024],temp1[1024],temp2[1024],portname[128],side[8];
  char in[64][256],out[64][256]; /* Temp storage! */
  int i,j,k,l,more,more2,eov,numin,numout,err,foundit,cell1,cell2,ok;
  char *ioname;
  struct instance *inst;
  struct netnode *net,*netnode,*tempn; /* Temporary nets */
  struct netlist *templist;
    struct libentry *le;

/* Check the type of the statement */

/*** .LIBRARY STATEMENT ***/
  if (match(buffer,".library\"",0)){ /* Library load */
    i=find(buffer,"\"",9);
    if (i < 0){fprintf(stderr,"Expecting \"libname\"\n");return(1);}
    strcpy(temp,substr(buffer,9,0,i-1)); /* library name */
    return(load_library(circuit,temp));
  }

/*** .LIBOUT STATEMENT ***/
  if (match(buffer,".libout\"",0)){ /* Library creation */
    i=find(buffer,"\"",8);
    if (i < 0){fprintf(stderr,"Expecting \"libout\"\n");return(1);}
    strcpy(circuit->libout,substr(buffer,8,0,i-1)); /* library output name */
    return(0);
  }


/*** .DEFINE STATEMENT ***/

  if (match(buffer,".define",0)){ /* A circuit definition statement */
    i=find(buffer,"(",0);
    if (i < 0){
      fprintf(stderr,"Excpeting \"(\" in .define statement\n");
      return(1); /* Error */
    }

/* Now we can pick off the name */
    strcpy(temp,substr(buffer,7,0,i-1));
    circuit->name=malloc((1+strlen(temp)) * sizeof(char));
    strcpy(circuit->name,temp);

    ++i; /* Start of height */
    j=find(buffer,",",i);
    if (j < 0){fprintf(stderr,"Expecting height,width\n");return(1);}
    sscanf(substr(buffer,i,0,j-1),"%d",&circuit->height);
    i=j+1;

    more=1; // to start loop below...

    j=find(buffer,",",i);
    if (j < 0){j=find(buffer,")",i);if (j>=0) more=0;} // Found trailing ")"
    if (j < 0){fprintf(stderr,"Expecting height,width\n");return(1);}
    sscanf(substr(buffer,i,0,j-1),"%d",&circuit->width);
    i=j+1; /* Start of I/O defs */

    numin=numout=0;
    while (more==1){
      eov=find(buffer,",",i);
      if (eov < 0) {more=0;eov=find(buffer,")",i);}
      if (eov < 0){fprintf(stderr,"EOL not found\n");return(1);}
      strcpy(temp,substr(buffer,i,0,eov-1)); /* var:direc */
      j=find(temp,":",0);
      if (j < 1){fprintf(stderr,"Expecting \":\"\n");return(1);}
      strcpy(temp2,substr(temp,0,0,j-1)); /* VAR */
      if (0==strcmp("in",substr(temp,j+1,2,0))){ /* Input var */
        strcpy(in[numin++],temp2);
      } else if (0==strcmp("out",substr(temp,j+1,3,0))){ /* Output var */
        strcpy(out[numout++],temp2);
      } else {fprintf(stderr,"Expecting \"in\" or \"out\"\n");return(1);}

      i=eov+1;
    }
/* Copy the in[] and out[] arrays to the CIRCUIT structure */
    circuit->io=malloc((numin+numout+1)*sizeof(char *)); /* Make room for arrays */
    circuit->iodir=malloc((numin+numout+1)*sizeof(int)); /* Store direc */
    circuit->ioside=malloc((numin+numout+1)*sizeof(char)); /* Store edge spec. */
    circuit->iocell1=malloc((numin+numout+1)*sizeof(int)); /* Starting loc */
    circuit->iocell2=malloc((numin+numout+1)*sizeof(int)); /* Ending loc */
    circuit->placedside=malloc((numin+numout+1)*sizeof(char)); /* Store edge spec. */
    circuit->placedrow=malloc((numin+numout+1)*sizeof(int)); /* Ending loc */
    circuit->placedcol=malloc((numin+numout+1)*sizeof(int)); /* Ending loc */
/* +1 since we need extra space for the NULL array terminator */
/* (really only need this on the io[] array) */

    for (i=0;i<numin;i++){
      circuit->io[i]=malloc((1+strlen(in[i]))*sizeof(char));
      strcpy(circuit->io[i],in[i]);
      circuit->iodir[i]=0; /* INPUT */
      circuit->ioside[i]=' '; /* n, s, w or e once specified */
      circuit->iocell1[i]=circuit->iocell2[i]=
      circuit->placedrow[i]=circuit->placedcol[i]=(-1);
      circuit->placedside[i]=' ';
    }
    for (i=0;i<numout;i++){
      circuit->io[i+numin]=malloc((1+strlen(out[i]))*sizeof(char));
      strcpy(circuit->io[i+numin],out[i]);
      circuit->iodir[i+numin]=1; /* OUTPUT */
      circuit->ioside[i+numin]=' '; /* n, s, w or e once specified */
      circuit->iocell1[i+numin]=circuit->iocell2[i+numin]=
      circuit->placedrow[i+numin]=circuit->placedcol[i+numin]=(-1);
      circuit->placedside[i+numin]=' ';
    }
    circuit->io[numin+numout]=(char *)NULL;
    circuit->numio=numin+numout;

    if ((circuit->height==0)||(circuit->width==0)){
      fprintf(stderr,"Expecting height,width in start of .define statement\n");
      return(1);
    }

/* Initialize the matrix */
    circuit->matrix=malloc(circuit->height*circuit->width*sizeof(struct cell));
    for (i=0;i<circuit->height;i++){
      for (j=0;j<circuit->width;j++){
/***
        circuit->matrix[circuit->width*i+j].inputs=0;
        circuit->matrix[circuit->width*i+j].outputs=0;
***/
        circuit->matrix[circuit->width*i+j].instance=NULL;
      }
    }

    return(0);
  } /*** END OF .DEFINE STATEMENT ***/

/*** .LOCATE STATEMENT ***/

  if (match(buffer,".locate",0)){ /* A Force a port location */
    strcpy(portname,"");strcpy(side," ");cell1=cell2=(-1);

    i=find(buffer,",",0); /* Optional second argument */
/* Pick off required port name */
    if (i>=0) strcpy(portname,substr(buffer,7,0,i-1));
    else strcpy(portname,substr(buffer,7,0,strlen(buffer)-1));
    if (0==strlen(portname)){
      fprintf(stderr,"Expecting portname in <%s>\n",buffer);
      return(1);
    }
    if (i>=0){ /* Get side */
      ++i; /* Start of side */
      j=find(buffer,",",i); /* Optional third argument */
      if (j>=0) strcpy(side,substr(buffer,i,0,j-1));
      else strcpy(side,substr(buffer,i,0,strlen(buffer)-1));
      i=j; /* start of third arg, or -1 */
    }
    if (i>=0){ /* Get cell */
      ++i; /* Start of cell */
      j=find(buffer,",",i); /* Optional fourth argument */
      if (j>=0) strcpy(temp,substr(buffer,i,0,j-1));
      else strcpy(temp,substr(buffer,i,0,strlen(buffer)-1));
      if (1 != sscanf(temp,"%d",&cell1)){
        fprintf(stderr,"Expecting cell number but got <%s>\n",temp);
        return(1);
      }
      i=j; /* start of fourth arg, or -1 */
    }
    if (i>=0){ /* Get cell */
      ++i; /* Start of cell */
      strcpy(temp,substr(buffer,i,0,strlen(buffer)-1));
      if (1 != sscanf(temp,"%d",&cell2)){
        fprintf(stderr,"Expecting cell number but got <%s>\n",temp);
        return(1);
      }
    }

    if (cell2==(-1)) cell2=cell1; /* make things easy */

/* Now we have portname, side, cell1 and cell2 loaded. Add to structs */
    i=0;
    if (side[0]=='\0') strcpy(side," "); /* makes parse easier - handles stupid case */
    while (circuit->io[i] != NULL){
      if (0==strcmp(portname,circuit->io[i])){ /* Found the port */
        circuit->ioside[i]=side[0];
        circuit->iocell1[i]=cell1;
        circuit->iocell2[i]=cell2;
        return(0);
      }
      ++i;
    }
/* not found */
    fprintf(stderr,"ERROR: Cannot find top-level port matching <%s>\n",
            buffer);
    return(1);
  } /*** END OF .LOCATE STATEMENT ***/


/*** .INSTANCE STATEMENT ***/
  if (match(buffer,".instance",0)){ /* component instantiation */
    i=find(buffer,"=",0);
    if (i < 0){fprintf(stderr,"Expecting \"=\"\n");return(1);}
    strcpy(temp,substr(buffer,9,0,i-1)); /* Name of this instance */

/* Make an instance node for this instance */
    inst=malloc(sizeof(struct instance)); /* New memory please! */
    inst->name=malloc((1+strlen(temp))*sizeof(char));
    strcpy(inst->name,temp);

/* Insert this instance into circuit's linked list */
    inst->next=circuit->components->next;
    circuit->components->next=inst;

/* Get instance basename */
    j=find(buffer,"(",i+1);
    if (j < 0){fprintf(stderr,"Excepting \"(\"\n");return(1);}
    strcpy(temp,substr(buffer,i+1,0,j-1));
    inst->basename=malloc((1+strlen(temp))*sizeof(char));
    strcpy(inst->basename,temp);

/* Store a pointer to the library entry of this instance's basename */
    le=circuit->library;
    foundit=0;
    while ((foundit==0)&&(le->next != NULL)){
      le=le->next;
      if (0==strcmp(le->basename,inst->basename)){
        inst->libdef=le; /* Found the libray definition */
        foundit=1;
      }
    } /* Finished with library */
    if (foundit==0){
      fprintf(stderr,"ERROR: Cannot find \"%s\" in library\n",inst->basename);
      return(1);
    }

/*
 * Now parse the basename(io=baseio,io=baseio,...)
 * and load into arrays
 */

/* Now pull off the IO assignments */
    i=0;j++; /* j is loc of first io=io */
    //if (buffer[j]==')') ++j; /* () allowed */

    while (buffer[j] != ')'){ /* More to parse */
      k=find(buffer,"=",j);
      if (k < 0){fprintf(stderr,"Expecting \"=\"\n");return(1);}
      strcpy(in[i],substr(buffer,j,0,k-1));
/* Find RHS */
      j=k+1;
      k=find(buffer,",",j); // next IO rename, or inside @constraint
      if (k > find(buffer,")",j)) k=(-1); // "," comes after ")"
      more2=(k>=0)?1:0;
      if (k < 0) k=find(buffer,")",j);
      if (k < 0){fprintf(stderr,"Expecting \",\" or \")\"\n");return(1);}
      strcpy(out[i++],substr(buffer,j,0,k-1));
      j=k+more2;
    }

// buffer[j+1] is EOL or start of constraint
   ok=0;
   if (buffer[++j] == '\0'){ // unconstrained
     inst->ulrowc=inst->ulcolc=SPACING;
     inst->lrrowc=circuit->height-1-SPACING;
     inst->lrcolc=circuit->width-1-SPACING;
     inst->rotc=(-1);
     ok=1;
   } else {
     if (5==sscanf(&buffer[j],"@[%d,%d]-[%d,%d]%d",
                   &inst->ulrowc,&inst->ulcolc,
                   &inst->lrrowc,&inst->lrcolc,
                   &inst->rotc)) ok=1;
     else if (4==sscanf(&buffer[j],"@[%d,%d]-[%d,%d]",
                        &inst->ulrowc,&inst->ulcolc,
                        &inst->lrrowc,&inst->lrcolc)){
       inst->rotc=(-1);
       ok=1;
     }
     else if (3==sscanf(&buffer[j],"@[%d,%d]%d",
                        &inst->ulrowc,&inst->ulcolc,
                        &inst->rotc)){
       inst->lrcolc=inst->ulcolc;
       inst->lrrowc=inst->ulrowc;
       ok=1;
     } else if (2==sscanf(&buffer[j],"@[%d,%d]",
                        &inst->ulrowc,&inst->ulcolc)){
       inst->lrcolc=inst->ulcolc;
       inst->lrrowc=inst->ulrowc;
       inst->rotc=(-1);
       ok=1;
     } else if (1==sscanf(&buffer[j],"@%d",&inst->rotc)){
       inst->ulrowc=inst->ulcolc=SPACING;
       inst->lrrowc=circuit->height-1-SPACING;
       inst->lrcolc=circuit->width-1-SPACING;
       ok=1;
     }
   }
   if (ok==0){fprintf(stderr,"Error parsing @constraint\n");return(1);}
printf("%s [%d,%d]-[%d,%d] rot=%d\n",inst->name,
inst->ulrowc,inst->ulcolc,
inst->lrrowc,inst->lrcolc,
inst->rotc);

/* Now store in[] and out[] into the inst structure */
    inst->ioname=malloc((1+i+inst->libdef->numio)*sizeof(char *));
    inst->ioindex=malloc((1+i+inst->libdef->numio)*sizeof(int));
    err=0; /* Set if we miss an I/O port */

    for (j=0;j<i;j++){
      inst->ioname[j]=malloc((1+strlen(in[j]))*sizeof(char));
      strcpy(inst->ioname[j],in[j]);
      inst->ioindex[j]=find_io(circuit->library,inst->basename,out[j]);
      if (inst->ioindex[j] < 0){ /* Can't find I/O port */
        err=1; /* Will fail at end - but finish loop first! */
      }
    }

/***
 %%% Skip (a=b) re-assignments inside .instance statement
 %%% and reproduce the steps below whenever we switch to a new group member
 ***/
/* store the default assignments, in case there are no overrides */
    for (j=i;j<i+inst->libdef->numio;j++){
      ioname=(inst->libdef->io[j-i]).iobasename;
      inst->ioname[j]=malloc(1+strlen(ioname));
      strcpy(inst->ioname[j],ioname);
      inst->ioindex[j]=j-i;
    }

/* and terminate the arrays! */
    inst->ioname[i+inst->libdef->numio]=(char *)NULL;
    return(err);
  } /*** END OF .INSTANCE ***/

/*** .NET statement ***/
/* .net statement is ".net(inst.io,inst.io,...)" */
/* where "inst." is optional */

  if (match(buffer,".net(",0)){ /* Network membership */
    i=5; /* starting location of next inst.io, or EOL */
/* Start a new net */
    net=malloc(sizeof (struct netnode)); /* Make a new net */
    net->next=(struct netnode*) NULL; /* Empty list */

    while (buffer[i]!='\0'){
      j=find(buffer,",",i);
      if (j < 0) j=find(buffer,")",i);
      if (j<0){fprintf(stderr,"Expecting \",\" or \")\"\n");return(1);}
/* inst.io is sandwiched between i and j-1 */
      k=find(buffer,".",i);
      if ((k<0) || (k > j-1)){
        strcpy(temp1,"");
        strcpy(temp2,substr(buffer,i,0,j-1));
      } else {
        strcpy(temp1,substr(buffer,i,0,k-1));
        strcpy(temp2,substr(buffer,k+1,0,j-1));
      }

/* Find instance "temp1" and I/O "temp2," make node, and add to this net */
      netnode=make_node(circuit,temp1,temp2); /* new node or existing net */
      if (netnode == NULL) return(1); /* ERROR! */
/*
 * Need to add netnode to end of our net. Note that if the temp1/temp2 node
 * already existed, then netnode will be the net containing it
 * (so we can't just insert netnode into the beginning of our net...)
 */
      tempn=net;
      while (tempn->next != (struct netnode*) NULL) tempn=tempn->next;
      tempn->next=netnode; /* Add to end */

      i=j+1;
    }

/* Add this network to circuit structure */
    templist=circuit->nets;
    while (templist->next != (struct netlist*)NULL) templist=templist->next;
    templist->next=malloc(sizeof (struct netlist)); /* Point to new netlist node */
    templist=templist->next; /* This is the new netlist node */
    templist->next=(struct netlist*)NULL;
    templist->net=net;
    return(0);
  }

  return(1); /* Unknown statement */
}

/* find_io(library,basename,ioname) looks for an IO port named "ioname"
 * in the library entry for "basename"
 * Returns either the index, or -1 if the IO port is not found
 */
int find_io(struct libentry *library,char *basename, char *ioname)
{
  int i;
  struct libentry *le;
  struct iodef *iodef; /* Temp vars */

/* Scan the linked list beginning with library */
  le=library->next;
  while (le != (struct libentry*)NULL){
    if (0==strcmp(basename,le->basename)){ /* Found component */
      for (i=0;i<le->numio;i++){
        if (0==strcmp(ioname,(le->io[i]).iobasename)) return(i);
      }
      fprintf(stderr,
              "ERROR: No I/O port named \"%s\" found in component \"%s\"\n",
              ioname,basename);
      return(-1);
    }
    le=le->next;
  }
  fprintf(stderr,"ERROR: Cannot find component named \"%s\"\n",
         basename);
  return(-1);
}

/*
 * make_mode() reads the names of an instance and an I/O port,
 * and returns a netnode containing the corresponding instance and
 * I/O index. If this is a new node, the next member will be NULL.
 * If however this node already belongs to another network N, then:
 *  - the return node will be the nextwork N; and
 *  - N will be removed from circuit->nets (in anticipation of the
 *    currently-being assembled net being eventually added to circuit->nets)
 *
 */
struct netnode *make_node(struct circuit *circuit,char *temp1,char *temp2)
{
  struct netnode *new;
  struct instance *ins;
  struct netlist *nlist;
  struct netnode *nnode;
  struct netlist *prevnet;
        struct netnode *tempreturn;
  int i,found;

  new=malloc(sizeof (struct netnode));

  if (0 != strcmp(temp1,"")){ /* Port IO vs. edge IO */
/* Find the instance named "temp1" */
    ins=circuit->components->next; 
    while (ins != (struct instance*) NULL){
      if (0==strcmp(ins->name,temp1)) break;
      ins=ins->next;
    }
    if (ins==(struct instance*)NULL){
      fprintf(stderr,"Cannot find instance \"%s\"\n",temp1);
      return((struct netnode*) NULL);
    }

/* Now find the I/O port temp2 */
    i=0;
    while (ins->ioname[i] != (char *)NULL){
      if (0==strcmp(ins->ioname[i],temp2)) break;
      ++i;
    }
    if (ins->ioname[i] == (char *)NULL){
      fprintf(stderr,"Cannot find I/O port named \"%s\"\n",temp2);
      return((struct netnode*) NULL);
    }
  } /* Otherwise this is an edge IO: no real instance applies */

/* Now check all nets to see if this node belongs to any existing nets */
  prevnet=circuit->nets; /* Node prior to this one */
  nlist=prevnet->next; /* Linked list of networks */
  while (nlist != (struct netlist*)NULL){ /* nlist is a net */
    nnode=nlist->net; /* Starting node of this new net */
    while (nnode->next != NULL){ /* Check node n2 */
      nnode=nnode->next; /* Move to next node */

/* Now see if current port matches nnode */
      found=0;
      if (0 != strcmp(temp1,"")){ /* Check regular instance/port */
        if ((nnode->edge==0) && (nnode->c==ins) &&
            (nnode->ioindex==ins->ioindex[i])) found=1;
      } else { /* Edge IO */
        if ((nnode->edge==1) && (0==strcmp(temp2,circuit->io[nnode->ioindex])))
          found=1;
      }
      if (found==1){
/*
 * Node already belongs to a net. Remove the net from circuit->netlist
 * and return that net.
 */
        printf("FOUND pre-existing net containing %s.%s\n",temp1,temp2);
        tempreturn=nlist->net->next;
/* Remove from circuit->nets */
        prevnet->next=nlist->next;
        return(tempreturn); /* Network containing node! */
      }
    } /* End of this net */
    prevnet=nlist;
    nlist=nlist->next; /* Move to next net */
  }

/* Node is new! */
  new->c=ins;
  if (0!=strcmp(temp1,"")){ /* Normal instance port io */
    new->edge=0; /* 0=port io, 1=edge io */
    new->ioindex=ins->ioindex[i];
  } else { /* edge io */
    new->edge=1;
    new->c=EDGEINST; /* another flag for edge IO */
/* Need to find index into circuit->io[] */
    i=0;
    new->ioindex=(-1); /* Not found yet */
    while (circuit->io[i] != NULL){
      if (0==strcmp(temp2,circuit->io[i])){ /* Found the port */
        new->ioindex=i;break;
      }
      ++i;
    }
    if (new->ioindex == -1){
      fprintf(stderr,"ERROR: Cannot find edge IO <%s>\n",temp2);
      return(new);
    }
  }
  new->next=(struct netnode*) NULL;
  return(new);
}

/* Load a library of components from disc */
/* Input must be formatted precisely - no mercy! */
load_library(struct circuit *circuit,char *name)
{
  FILE *fp;
  char buffer[1024],temp1[1024],temp2[1024];
  struct libentry *le;
  int i,j,r,c,version,num_versions;
  char side[32];

  fp=fopen(name,"r");
  if (fp <= (FILE *) NULL){
    fprintf(stderr,"Cannot open library <%s>\n ",name);
    return(1);
  }

  while (NULL != fgets(buffer,1023,fp)){
    fgets(buffer,1023,fp); /* one blank line before each entry */
    buffer[-1+strlen(buffer)]='\0';
/* Build libentry node */
    le=malloc(sizeof (struct libentry)); /* New library entry! */
/* Parse basename */
    if (3 != sscanf(buffer,"%s {%d/%d}",temp1,&version,&num_versions)){
      version=0;num_versions=1;
    }
    le->basename=malloc((1+strlen(temp1))*sizeof(char));
    strcpy(le->basename,temp1); /* Store the component's name */
    le->version=version;le->num_versions=num_versions;

    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    le->desc=malloc((1+strlen(buffer))*sizeof(char));
    strcpy(le->desc,buffer); /* Store the component's description */

    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d",&(le->h));
    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d",&(le->w)); /* Dims */

/* Now load the cell definitions - these are needed only for final output */
    le->cells=malloc(((le->h)*(le->w))*(sizeof (char*)));
    for (i=0;i<(le->h)*(le->w);i++){
      fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
      le->cells[i]=malloc((1+strlen(buffer))*sizeof(char));
      strcpy(le->cells[i],buffer); /* Store the cell spec */
    }

/* How many IO? */
    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d",&(le->numio));

/* Store the I/O definitions */
    le->io=malloc((le->numio)*sizeof(struct iodef)); /* Array of iodefs */

    for (i=0;i<le->numio;i++){
      fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
/* Store the I/O def */
      sscanf(buffer,"%s %d %d %s",temp1,&r,&c,temp2);
      (le->io[i]).iobasename=malloc(1+strlen(buffer));
      strcpy((le->io[i]).iobasename,temp1);
      le->io[i].row=r;
      le->io[i].col=c;
      if (strlen(temp2) != 3){
        fprintf(stderr,"component %s: expecting def of side, but got \"%s\"\n",
                le->basename,temp2);
        return(1);
      }
      strcpy((le->io[i]).side,temp2);
    }

// Read component's routing statistics
    fgets(buffer,1023,fp);buffer[-1+strlen(buffer)]='\0';
    sscanf(buffer,"%d %d %d %d %d %d %d %d %d",
           &le->stats.Mlen_str,&le->stats.Mlen_turn,&le->stats.Mlen_sum,
           &le->stats.Glen_str,&le->stats.Glen_turn,&le->stats.Glen_sum,
           &le->stats.Glen_str_n,&le->stats.Glen_turn_n,&le->stats.Glen_sum_n);

/* Insert new node (le) into start of list (so new entries are used first) */
    le->next=circuit->library->next;
    circuit->library->next=le;
  }
  fclose(fp);
  return(0);
}
