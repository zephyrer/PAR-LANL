;
; support cells for PAR
;
; Copyright(C) 2006 Cell Matrix Corporation
; All Rights Reserved.
;

.start main2
.size 20,16

; README!
.cell 0,0
.bg 7
.text 1,"READ_ME"
;
; This library contains blocks and decals for
; supporting the PAR place and route code
;

;************************
; 1->2 decoder
.cell 1,0
.bg 7
.text 4,"1-2"
de=sn~&
ds=n
.io dni,"SEL"
.io deo,"Q0"

.cell 2,0
.text 4,"1-2"
de=n~s&
dn=s
.io deo,"Q1"
.io dsi,"D"


;************************
; 1->4 decoder
.cell 1,2
.text 4,"1-4"
ds=n
de=n~s&
.io dni,"S1"
.cell 1,3
.text 4,"1-4"
ds=n
de=n~w&
.io deo,"Q0"
.io dni,"S0"

.cell 2,2
.text 4,"1-4"
ds=n
dn=s
de=sn~&
.cell 2,3
.text 4,"1-4"
ds=n
de=wn&
.io deo,"Q1"

.cell 3,2
.text 4,"1-4"
ds=n
dn=s
de=sn&
.cell 3,3
.text 4,"1-4"
ds=n
de=wn~&
.io deo,"Q2"

.cell 4,2
.text 4,"1-4"
de=sn&
dn=s
.io dsi,"D"
.cell 4,3
.text 4,"1-4"
de=wn&
.io deo,"Q3"

;************************
; 1->8 decoder
.cell 1,5
.text 4,"1-8"
ds=n
de=n~s&
.io dni,"S2"
.cell 1,6
.text 4,"1-8"
ds=n
de=n~w&
.io dni,"S1"
.cell 1,7
.text 4,"1-8"
ds=n
de=n~w&
.io deo,"Q0"
.io dni,"S0"

.cell 2,5
.text 4,"1-8"
ds=n
dn=s
de=n~s&
.cell 2,6
.text 4,"1-8"
ds=n
dn=s
de=n~w&
.cell 2,7
.text 4,"1-8"
ds=n
de=nw&
.io deo,"Q1"

.cell 3,5
.text 4,"1-8"
ds=n
dn=s
de=n~s&
.cell 3,6
.text 4,"1-8"
ds=n
dn=s
de=nw&
.cell 3,7
.text 4,"1-8"
ds=n
de=n~w&
.io deo,"Q2"

.cell 4,5
.text 4,"1-8"
de=n~s&
dn=s
ds=n
.cell 4,6
.text 4,"1-8"
ds=n
dn=s
de=nw&
.cell 4,7
.text 4,"1-8"
de=nw&
ds=n
.io deo,"Q3"


.cell 5,5
.text 4,"1-8"
ds=n
dn=s
de=ns&
.cell 5,6
.text 4,"1-8"
ds=n
dn=s
de=n~w&
.cell 5,7
.text 4,"1-8"
ds=n
dn=s
de=n~w&
.io deo,"Q4"

.cell 6,5
.text 4,"1-8"
ds=n
dn=s
de=ns&
.cell 6,6
.text 4,"1-8"
ds=n
dn=s
de=n~w&
.cell 6,7
.text 4,"1-8"
ds=n
de=nw&
.io deo,"Q5"

.cell 7,5
.text 4,"1-8"
ds=n
dn=s
de=ns&
.cell 7,6
.text 4,"1-8"
ds=n
dn=s
de=nw&
.cell 7,7
.text 4,"1-8"
ds=n
de=n~w&
.io deo,"Q6"

.cell 8,5
.text 4,"1-8"
de=ns&
dn=s
ds=n
.io dsi,"D"
.cell 8,6
.text 4,"1-8"
ds=n
dn=s
de=nw&
.cell 8,7
.text 4,"1-8"
de=nw&
ds=n
.io deo,"Q7"





;-------------------------------------------------------
; Define integer add and subtract blocks

; Full single-bit adder
.cell 7,0
.bg 7
.text 4,"adders=a+b"
; Input bits from North and South
; Input incoming carry from East
; Output sum to South
; and output Carry to West

ds=se~n~&& s~en~&& + s~e~n&& + sne&& +
dw=se& en& sn&++
.io dei,"cin"
.io dwo,"cout"
.io dni,"A"
.io dsi,"B"
.io dso,"S"

.cell 8,0
; Input bits from North and South
; Input incoming carry from East
; Output sum to South
; and output Carry to West
ds=se~n~&& s~en~&& + s~e~n&& + sne&& +
dw=se& en& sn&++

; DECALS
.bg 7
.text 0
.cell 0,5
.io dwo,"cout"
.io dni,"A"
.io dsi,"B"
.io dso,"S"

.cell 9,3
.io dei,"cin"
.io dni,"A"
.io dsi,"B"
.io dso,"S"

.cell 9,4
.io dni,"A"
.io dsi,"B"
.io dso,"S"

.cell 10,2
.text "ADDERS=A+B"
.io dni,"A"
.io dsi,"B"
.io dso,"S"

.cell 10,3
.io dei,"cin"
.io dwo,"cout"
.io dni,"A"
.io dsi,"B"
.io dso,"S"

;-------------------------------------------------------------------

; Single-bit subtractor
.cell 11,0
.text 4,"sub"
; input A from North, B from South, and incoming borrow from East
; subtract, send difference to South and outgoing borrow to West
;
ds=se~n~&& s~en~&& + s~e~n&& + sne&& +
dw=n~se~&& n~s~e&& + n~se&& + nse&& +
.io dei,"bin"
.io dwo,"bout"
.io dni,"A"
.io dsi,"B"
.io dso,"D"

.cell 12,0
; input A from North, B from South, and incoming borrow from East
; subtract, send difference to South and outgoing borrow to West
;
ds=se~n~&& s~en~&& + s~e~n&& + sne&& +
dw=n~se~&& n~s~e&& + n~se&& + nse&& +


; DECALS

.cell 13,2
.io dwo,"bout"
.io dni,"A"
.io dsi,"B"
.io dso,"D"

.cell 13,3
.io dei,"bin"
.io dni,"A"
.io dsi,"B"
.io dso,"D"

.cell 13,4
.io dni,"A"
.io dsi,"B"
.io dso,"D"

.cell 14,2
.text "_SUB_D=A-B"
.io dni,"A"
.io dsi,"B"
.io dso,"D"

;
;--------------------------------------------------------
; Integer Multiply Block

.bg 2
.cell 15,0
.text 4
; A comes in from the North
ds=n
dw=e
de=n
.io dni,"A"

.cell ; Real workhorse here
; Add North (incoming partial sum), East (Cin) and West (Ai)
; but only if South (Bi)=1
; Otherwise, just pass N->S

; No direct user I/O on this cell
ds=we~n~&& w~en~&& + w~e~n&& + wne&& +     s& ns~& +
dw=we& en& wn&++

.cell 16,0
; B comes in from the West. Ignore southern output!
dw=n
ds=e
de=w
.io dwi,"B"

.cell
.text "P=AxB"
; partial product sent out South
dw=e
de=w
dn=w
ds=n
.io dso,"P"


;---------------------------------------------------
; Integer DIVIDE circuit...

.cell 17,0 ; Routing
dw=e	; Shifted version of B
ds=w	; Route trigger to indicate not-at-end-of-row
.text "A=X/B"

.cell 17,1
dn=e	; further shifting of B
ds=n	; X (incoming partial quotient)
dw=s	; further shifting of B
de=1	; This is the not-end-of-row trigger
.io dni,"X"

.cell 18,0
; Receives borrow from subtraction. If final, invert and return
; DNin is 0 at end-of-row
DW=NE& N~E~& + ; If N=1, just pass borrow; else invert (=bit of A)
DE=NW& N~E& + ; If N=1, pass sign bit; else recirculate borrow (=sign bit)
.io dwo,"A"

.cell 18,1; The Workhorse...
; Compute North-South, and either pass that, or North, depending on sign
; Sign bit enters on West
; N is partial quotient input
; S is (shifted) version of B
; Also pass sign bit from W->E, and B from S->N

; Remainder sent to South
; If W=0, send difference; else send North
ds=se~n~&& s~en~&& + s~e~n&& + sne&& +    w~& nw& +

dw=n~se~&& n~s~e&& + n~se&& + nse&& + ; From SUB circuit above

de=w
dn=s

.io dsi,"B"
.io dso,"REM"

; DECALS

.bg 7
.cell 18,4
.text 0,"LSB"
.cell
.text "MSB"

.finish
