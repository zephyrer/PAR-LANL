#include "par.h"
/* Read a single "line" from the input file */
int get_line(FILE *fp, char *buf)
{
  int i,j,comment,quote,more;
  char buffer[1024];
  char big[30000];

  strcpy(buffer,"");
  strcpy(big,"");
  quote=0; /* Set inside " " */
  more=0;  // set if trailing character='\'
  while ((more==1)||((strlen(buffer) == 0))){
    more=0;
    if (NULL == fgets(buf,1024,fp)) return(1);
    printf("%s",buf);
    comment=0; /* Set once we see ";" */
    j=0; /* Output location */
    for (i=0;i<strlen(buf);i++){
      if (buf[i]==';') comment=1;
      if (buf[i]=='"') quote=1-quote;
      if ((comment==0) && ((quote==1)||(!isspace(buf[i])))) buffer[j++]=buf[i];
    }
    buffer[j]='\0';
    if (j>0){if(buffer[j-1]=='\\'){buffer[j-1]='\0';more=1;}}
    strcpy(buf,buffer);
    strcat(big,buffer);
  } /* Loop until we actually read something */
  strcpy(buf,big);
  return(0);
}
