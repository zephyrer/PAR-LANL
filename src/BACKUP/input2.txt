;
; Here's the header
;

.define testckt(20,20,x17:in,yout:out,in:in) ; Start definition

.library "testlib.lib" ; Contains some components!

.instance ff4=DFF1(d=d,ld=ld,q=q);
.instance ff3=DFF1(d=d,ld=ld,q=q);
.instance ff2=DFF1(d=d,ld=ld,q=q);
.instance ff1=DFF1(d=d,ld=ld,q=q);

.net(ff1.q,ff2.d)
.net(ff2.q,ff3.d)
.net(ff3.q,ff4.ld)
